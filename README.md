
# TACHYON #

### What is this repository for? ###

* A GPU-CPU hybrid parallel algorithm for computing extremum graphs.
* Version: 1.0

### How do I get set up? ###

* Requires C++14 compliant compiler. Tested with `g++` and `clang`
* Install NVidia CUDA toolkit for your platform. With apt package manager, run `apt install nvidia-cuda-toolkit`
* Install `cmake` with a minimum version of `3.16` , as the project uses a cmake build system

## Building repository ##

* Create `build` directory under repository root and `chdir` into it
* Configure cmake by running `cmake .. -DCUDA_SUPPORT=[0|1]`
* For the flag `CUDA_SUPPORT`, pass `1` if your machine has an NVidia GPU installed, and `0` otherwise.
* Run `make -jN`, where `N` is the number of parallel jobs
* A binary `tachyon` will be generated on a successful build

## Computing extremum graphs ##
* The `tachyon` binary can be run to compute extremum graphs with various graph simplification and cancellation operations performed, all within a single command invocation
* Example: Input data: Hydrogen ([Open Scientific Visualization Datasets](https://klacansky.com/open-scivis-datasets/ "Open Scivis home page"))
* The following example computes the extremum graph and perform the following graph mutations in order
    * edge bundling
    * persistence driven cancellation with `threshold=0.075`
    * saturated-persistence driven simplification with `p_lo=0.05` and `p_hi=0.95`


```
tachyon
  -in hydrogen_atom_128x128x128_uint8.raw
  -type u8
  -dim 128 128 128
  -bundle
  -cancel 0.075
  -simplify 0.05 0.95
  -out hydrogen_atom.vtp
```

## Reference ##

For more information about the method, refer to the following paper. Please cite these publications if you use this method or the library in your work.

Abhijath Ande, Varshini Subhash, and Vijay Natarajan.  
[TACHYON: Efficient shared memory parallel computation of extremum graphs](http://arxiv.org/abs/2303.02724).  
Computer Graphics Forum, 2023, In Press.  
[[doi: 10.1111/cgf.14784]](https://doi.org/10.1111/cgf.14784)


## Replicability Stamp ##
Please refer to this readme for details [link](https://bitbucket.org/vgl_iisc/tachyon/src/master/replicability-stamp-instructions.md)


## Who do I talk to? ##

|Name| Email Address                     |
|----|-----------------------------------|
| Abhijath Ande | abhijathande [at] alum.iisc.ac.in |
| Vijay Natarajan | vijayn [at] iisc.ac.in            |

## Copyright

Program:   tachyon

Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
