/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#include <grid.hpp>
#include <grid_dataset.hpp>
#include <CUDA/cuda_defs.h>
#include <extremum_graph.hpp>
#include <plan.hpp>

#include <atomic>
#include <cmath>
#include <unistd.h>
#include <fcntl.h>

#define CPU_VERIFY_CYCLE_ABSENCE                    0
#define COLLECT_EXTREMUM_GRAPH_CRITICAL_POINTS_ONLY 0

namespace extgraph {

  __CUDA_HOST_ONLY__ int dataset_t :: perform_point_collection (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph)
   {
    #if ENABLE_INDEX_PLACEHOLDER
      return 0;
    #endif

    uint64_t tmp_start, tmp_end;

    puts("[2/3] Gathering Critical Points");
    host_clock(tmp_start); // Point Collection time

    std::vector <uint8_t> thread_map_vector;

    thread_map_vector.push_back(data.dataset_chunk_rect->num_dimensions());
    thread_map_vector.push_back(data.dataset_chunk_rect->num_dimensions() - 1);
    thread_map_vector.push_back(MULTISADDLE_LABEL);

    #if ENABLE_DEBUGGING
    for (uint32_t i=0; i<data.dataset_chunk_rect->num_dimensions()-1; ++i)
      thread_map_vector.push_back(i);
    #endif

    #if CUDA_CHECK_UNVISITED_VERTICES
    thread_map_vector.push_back(UNVISITED_VERTEX_LABEL);
    #endif

    data.chunk_maxima_count       = 0;
    data.chunk_saddles_count      = 0;
    data.chunk_1saddles_count     = 0;
    data.chunk_minima_count       = 0;
    data.chunk_multisaddles_count = 0;
    data.regular_pt_count         = 0;
    data.unknown_crit_pt_count    = 0;

    #if CHECK_REGULAR_POINT_CLASSIFICATION
    thread_map_vector.push_back(UNKNOWN_CRTI_PT_LABEL);
    thread_map_vector.push_back(REGULAR_PT_LABEL);
    #endif

    const uint64_t num_elements   = data.dataset_chunk_rect->num_elements();
    const uint32_t num_dimensions = data.dataset_chunk_rect->num_dimensions();

    auto point_collection_lambda =
    [=] (std::vector<point_index_t> * const vec, const uint8_t work_type, auto insert_if_predidcate, uint64_t& count_inserts, bool sort_vec, auto additional_work) -> void {

      #if !COLLECT_EXTREMUM_GRAPH_CRITICAL_POINTS_ONLY
      (void)work_type;
      #endif

      count_inserts = 0;
      for (point_index_t pt_index = 0; pt_index<num_elements; ++pt_index)
        if (insert_if_predidcate(pt_index)) {
          ++count_inserts;

          #if COLLECT_EXTREMUM_GRAPH_CRITICAL_POINTS_ONLY
          if (work_type == num_dimensions || work_type == (num_dimensions - 1) || work_type == UNVISITED_VERTEX_LABEL)
          #endif
            vec->push_back(data.compute_start_index + pt_index);

          additional_work();
        }

      vec->shrink_to_fit();

      if (sort_vec)
        std::sort(vec->begin(), vec->end());
    };

    auto worker_thread_main  = 
    [&]
    (int thread_idx) -> void {

      const uint8_t work_type = thread_map_vector[thread_idx];
      uint64_t dummy_u64;

      if (work_type == num_dimensions) // Maximum
        point_collection_lambda(&ext_graph->m_chunk_maxima, work_type,
            [data, num_dimensions] (point_index_t pt_index) -> bool
              { return data.point_index_array[pt_index] == num_dimensions; },
            data.chunk_maxima_count, true, [ext_graph]() { ext_graph->m_maxima_saddle_mapping.push_back(std::vector<uint64_t>()); });

      else if (work_type == (num_dimensions - 1))// (n-1) Saddles
        point_collection_lambda(&ext_graph->m_chunk_saddles, work_type,
            [data, num_dimensions] (point_index_t pt_index) -> bool
              { return (data.point_index_array[pt_index] == (num_dimensions - 1)) || (COLLECT_MULTISAD_INTO_SAD && (data.point_index_array[pt_index] == MULTISADDLE_LABEL)); },
            data.chunk_saddles_count, true, [ext_graph]() { ext_graph->m_saddle_maxima_mapping.push_back(critical_point_map_element_t()); });

      else if (work_type == 1)// 1-Saddles
        point_collection_lambda(&ext_graph->m_chunk_1saddles, work_type,
            [data] (point_index_t pt_index) -> bool
              { return (data.point_index_array[pt_index] == 1) || (data.point_index_array[pt_index] == MULTISADDLE_LABEL); },
            data.chunk_1saddles_count, true, [](){});

      else if (work_type == 0)// Minimum
        point_collection_lambda(&ext_graph->m_chunk_minima, work_type,
            [data] (point_index_t pt_index) -> bool
              { return data.point_index_array[pt_index] == 0; },
            data.chunk_minima_count, false, [](){});

      else if (work_type == MULTISADDLE_LABEL)// Multi Saddles
        point_collection_lambda(&ext_graph->m_chunk_multi_saddles, work_type,
            [data] (point_index_t pt_index) -> bool
              { return data.point_index_array[pt_index] == (MULTISADDLE_LABEL); },
            data.chunk_multisaddles_count, true, [](){});

      else if (work_type == UNVISITED_VERTEX_LABEL)// Unvisited points
        point_collection_lambda(&data.unvisited_vertices, work_type,
            [data] (point_index_t pt_index) -> bool
              { return data.point_index_array[pt_index] == UNVISITED_VERTEX_LABEL; },
            dummy_u64, false, [](){});

      else if (work_type == REGULAR_PT_LABEL) {
        for (point_index_t pt_index = 0; pt_index<num_elements; ++pt_index)
          if (data.point_index_array[pt_index] == REGULAR_PT_LABEL)
            ++data.regular_pt_count;
      }

      else if (work_type == UNKNOWN_CRTI_PT_LABEL) {
        for (point_index_t pt_index = 0; pt_index<num_elements; ++pt_index)
          if (data.point_index_array[pt_index] == UNKNOWN_CRTI_PT_LABEL)
            ++data.unknown_crit_pt_count;
      }

    };

    std::vector <std::thread> worker_thread_list;

    for (uint32_t i=0; i<thread_map_vector.size(); ++i)
      worker_thread_list.push_back(std::thread(worker_thread_main, i));

    for (uint32_t i=0; i<thread_map_vector.size(); ++i)
      worker_thread_list[i].join();

//    printf("|m_saddle_maxima_mapping|: %lu\n", ext_graph->m_saddle_maxima_mapping.size());

    host_clock(tmp_end);
    data.point_collection_time += tmp_end - tmp_start;

    #if !ENABLE_DEBUGGING
    delete [] data.point_index_array;
    #endif

    return 0;
   }

  /*
   * WARNING!!!! DO NOT REMOVE THIS FUNCTION!!!
   *
   * This is dead code but not completely useless.
   * Calling single_path_tracing for both true and false template arguments ensures the
   * templatized function is instantiated for all possible template argument combinations.
   */
  void __instantiate_single_path_tracing_template_functions (const extremum_graph_t * const ext_graph, const point_index_t start_pt_index, uint64_t& path_len, std::vector <point_index_t> * const path_vector)
   {
    bool aaa;
    //volatile ensures unused function call is not optimized away
    volatile point_index_t a = single_path_tracing <false> (ext_graph, 0, 0, start_pt_index, aaa, path_len, path_vector);
    volatile point_index_t b = single_path_tracing <true>  (ext_graph, 0, 0, start_pt_index, aaa, path_len, path_vector);

    //Ensures both variables a and b dont throw unused variable warnings.
    (void)a;
    (void)b;
    b = a;
   }

   /*
    *
    * INFO:
    *   path_vector is not cleared. We essentially apppend any gradient path points found onto the vector.
   */
  template <bool collect_path_points>
  point_index_t single_path_tracing (const extremum_graph_t * const ext_graph, const uint64_t exclude_region_start, const uint64_t exclude_region_end, const point_index_t start_pt_index, bool& partial_termination, uint64_t& path_len, std::vector <point_index_t> * const path_vector)
   {
    point_index_t curr_pt_index = start_pt_index, next_pt_index = start_pt_index;

    path_len = 0;
    partial_termination = false;

    #if CPU_VERIFY_CYCLE_ABSENCE
    std::set <point_index_t> path_verifier_set;
    #endif

    if (
        (exclude_region_start < exclude_region_end) &&
        (exclude_region_start <= start_pt_index && start_pt_index < exclude_region_end)
       ) {
      partial_termination = true;
      return start_pt_index;
    }

    const chunk_rect_t * const m_ref_rect = &ext_graph->m_ref_dataset_instance->m_dataset_rect;

    while (true)
     {
      #if CPU_VERIFY_CYCLE_ABSENCE
      bool cond = path_verifier_set.find(curr_pt_index) == path_verifier_set.end();
      if (!cond) {
        printf("Cycle detected after %lu points, with path_len: %lu, path_vector.size: %lu\n", path_verifier_set.size(), path_len, path_vector->size());
        printf("start_pt_index: %lu, curr_pt_index: %lu, next_pt_index: %lu\n", start_pt_index, curr_pt_index, next_pt_index);
        printf("exclude_region_start: %lu, exclude_region_end: %lu\n", exclude_region_start, exclude_region_end);
      }

      assert(cond);
      path_verifier_set.insert(curr_pt_index);
      #endif

      next_pt_index = m_ref_rect->neighbour_bitmap_to_index(curr_pt_index, ext_graph->m_max_neighbour_array[curr_pt_index]);

      if (next_pt_index == curr_pt_index)
       break;

      if (
          (exclude_region_start < exclude_region_end) &&
          (exclude_region_start <= next_pt_index && next_pt_index < exclude_region_end)
         ) {
        partial_termination = true;
        break;
      }

      if (collect_path_points)
        path_vector->push_back(curr_pt_index);

      ++path_len;

      curr_pt_index = next_pt_index;
     }

    assert(
               partial_termination |
            ( !partial_termination && (0 == ext_graph->m_max_neighbour_array[curr_pt_index]) )
          );

    path_len += (!partial_termination);

    if (collect_path_points && !partial_termination)
      path_vector->push_back(curr_pt_index);

    return curr_pt_index;
   }

  __CUDA_HOST_ONLY__ void print_pending_pairs (const std::vector<std::tuple<point_index_t, point_index_t, uint64_t>> * const list, const char* s)
   {
    printf("Pending list[%lu] [%s]:\n", list->size(), s);
    for (uint64_t i=0; i<list->size(); ++i) {
      printf("(%lu, %lu, %lu), ", std::get<0>(list->at(i)), std::get<1>(list->at(i)), std::get<2>(list->at(i)) );
    }

    printf("\n");
   }

  __CUDA_HOST_ONLY__ int dataset_t :: create_saddle_maxima_mappings (extremum_graph_computation_data_t& data, dataset_t * const dataset_instance, extremum_graph_t * const ext_graph, const int thread_idx, const int num_cpus)
   {
    chunk_rect_t dataset_chunk_rect = dataset_instance->get_dataset_rect();

    uint32_t true_upper_link_count = 0, upper_link_count = 0, lower_link_count = 0;
    point_index_t *component_highest = new point_index_t[data.p_neighbours->max_neighbours()];
    uf_bfs_data_t *uf_data_arr       = new uf_bfs_data_t[data.p_neighbours->max_neighbours()];
    uint32_t      * tmp_arr          = new uint32_t     [data.p_neighbours->max_neighbours()];

    assert(nullptr != component_highest);
    assert(nullptr != uf_data_arr);
    assert(nullptr != tmp_arr);

    const uint64_t num_saddles_to_skip    = (ext_graph->m_chunk_saddles.size() == data.chunk_saddles_count) ? 0 : (ext_graph->m_chunk_saddles.size() - data.chunk_saddles_count);
    const uint64_t num_saddles_to_process =  ext_graph->m_chunk_saddles.size() - num_saddles_to_skip;

    const uint64_t start_saddle = segment_range(num_saddles_to_process, num_cpus, thread_idx);
    const uint64_t end_saddle   = segment_range(num_saddles_to_process, num_cpus, thread_idx + 1);
    const uint64_t end_saddle_pending = end_saddle + data.pending_saddle_maximum_path_tracing[thread_idx].size();

    std::vector <uint64_t> pending_satisfied_indices;

    uint64_t path_len             = 0;

    grid_point_t pt(data.dataset_chunk_rect->num_dimensions()), pt_i(data.dataset_chunk_rect->num_dimensions()), pt_j(data.dataset_chunk_rect->num_dimensions());
    std::vector <point_index_t> dummy_vector;

    uint64_t maximum_ordinal = 0;
    bool partial_termination = false;

    point_neighbourhood p_neighbours_bounding_cumulative_rect(data.bounding_cumulative_rect);

    for (uint64_t ii=start_saddle; ii<end_saddle_pending; ++ii)
     {
      const bool tracing_pending_paths = (ii >= end_saddle);
      const uint64_t pending_work_index = tracing_pending_paths ? (ii - end_saddle) : ii;

      const point_index_t saddle_index =
        tracing_pending_paths
            ? std::get<0>(data.pending_saddle_maximum_path_tracing[thread_idx][pending_work_index])
            : ext_graph->m_chunk_saddles[num_saddles_to_skip + ii];

      assert(saddle_index < dataset_chunk_rect.num_elements());

      uint64_t saddle_ordinal = num_saddles_to_skip + ii;

      if (tracing_pending_paths)
        assert(binary_search_find_index<point_index_t>(&ext_graph->m_chunk_saddles, saddle_index, saddle_ordinal));

      if (saddle_index >= dataset_chunk_rect.num_elements()) {
        printf("\n");
        printf("thread_idx: %d\n", thread_idx);
        printf("tracing_pending_paths: %d\n", (int)tracing_pending_paths);
        printf("start, end, end_pending: %lu, %lu, %lu\n", start_saddle, end_saddle, end_saddle_pending);
        printf("pending_work_index: %lu\n", pending_work_index);
        printf("tuple [%lu, %lu, %lu]\n", std::get<0>(data.pending_saddle_maximum_path_tracing[thread_idx][pending_work_index]), std::get<1>(data.pending_saddle_maximum_path_tracing[thread_idx][pending_work_index]), std::get<2>(data.pending_saddle_maximum_path_tracing[thread_idx][pending_work_index]));
      }

      if (!tracing_pending_paths) {

        neighbour_bfs_union_find_non_template
          (
            saddle_index, data.bounding_cumulative_rect, &p_neighbours_bounding_cumulative_rect,
            data.func_vals, data.bound_start_index, data.invert_fn_val,
            true_upper_link_count, upper_link_count, lower_link_count, component_highest
            ,&pt, &pt_i, &pt_j, uf_data_arr, tmp_arr
          );

        data.aggregate_upper_link_counts[thread_idx] += upper_link_count;

         ext_graph->m_saddle_maxima_mapping[saddle_ordinal].reserve(upper_link_count);
      }
      else {
        upper_link_count = 1;
        component_highest[0] = std::get<1>(data.pending_saddle_maximum_path_tracing[thread_idx][pending_work_index]);
      }

       for (uint64_t i=0; i<upper_link_count; ++i)
        {
         assert(component_highest[i] < dataset_chunk_rect.num_elements());

         const point_index_t path_terminal = single_path_tracing <false> (ext_graph, data.compute_end_index, data.bound_end_index, component_highest[i], partial_termination, path_len, &dummy_vector);
         assert(path_terminal < dataset_chunk_rect.num_elements());

         if (partial_termination) { // Path crosses chunk boundary. Save for further traversal

           if (tracing_pending_paths) { // Previously pending path still pending. Poor guy
            std::get<1>(data.pending_saddle_maximum_path_tracing[thread_idx][pending_work_index])  = path_terminal;
            std::get<2>(data.pending_saddle_maximum_path_tracing[thread_idx][pending_work_index]) += path_len;
           }

           else { // New partially traversed path. Add to future work
             std::tuple <point_index_t, point_index_t, uint64_t> new_tuple = std::make_tuple(saddle_index, path_terminal, path_len);
             data.pending_saddle_maximum_path_tracing[thread_idx].push_back(new_tuple);

             assert(std::get<0>(new_tuple) < dataset_chunk_rect.num_elements());
             assert(std::get<1>(new_tuple) < dataset_chunk_rect.num_elements());
           }

           continue;
         }

         else if (!partial_termination && tracing_pending_paths) { // Previously pending path completes traversal. Yay!
          path_len += std::get<2>(data.pending_saddle_maximum_path_tracing[thread_idx][pending_work_index]);
          pending_satisfied_indices.push_back(pending_work_index);
         }

         const point_index_t maxima_index = path_terminal;

         data.path_len_totals[thread_idx] += path_len;
         data.min_path_len[thread_idx] = min(data.min_path_len[thread_idx], path_len);
         data.max_path_len[thread_idx] = max(data.max_path_len[thread_idx], path_len);

         if (!binary_search_find_index<point_index_t>(&ext_graph->m_chunk_maxima, maxima_index, maximum_ordinal)) {
            printf("Start saddle: %lu; Offending point: %lu; path_len: %lu\n", (unsigned long)saddle_index, (unsigned long)maxima_index, path_len);
            printf("Is Maximum: %d\n", (int)( ext_graph->m_chunk_maxima.end()  != std::find(ext_graph->m_chunk_maxima .begin(), ext_graph->m_chunk_maxima .end(), maxima_index) ));
            printf("Is Saddle : %d\n", (int)( ext_graph->m_chunk_saddles.end() != std::find(ext_graph->m_chunk_saddles.begin(), ext_graph->m_chunk_saddles.end(), saddle_index) ));

            assert(binary_search_find_index<point_index_t>(&ext_graph->m_chunk_maxima, maxima_index, maximum_ordinal));
         }

         ext_graph->m_saddle_maxima_mapping[saddle_ordinal].push_back(maximum_ordinal);


        }

       ext_graph->m_saddle_maxima_mapping[saddle_ordinal].shrink_to_fit();

      }

    while (!pending_satisfied_indices.empty()) {
      uint64_t remove_idx = pending_satisfied_indices.back();
      data.pending_saddle_maximum_path_tracing[thread_idx].erase(data.pending_saddle_maximum_path_tracing[thread_idx].begin() + remove_idx);
      pending_satisfied_indices.pop_back();
    }

    delete [] uf_data_arr;
    delete [] tmp_arr;
    delete [] component_highest;

    return 0;
   }

  __CUDA_HOST_ONLY__ int dataset_t :: create_maxima_saddle_mappings (extremum_graph_computation_data_t& data,  dataset_t * const dataset_instance, extremum_graph_t * const ext_graph, const int thread_idx, const int num_cpus)
   {
    (void)data;
    (void)dataset_instance;

    const point_index_t start_index = segment_range(ext_graph->m_chunk_maxima.size(), num_cpus, thread_idx);
    const point_index_t end_index   = segment_range(ext_graph->m_chunk_maxima.size(), num_cpus, thread_idx + 1);
    uint64_t maximum_ordinal;

    for (uint64_t saddle_ordinal=0; saddle_ordinal<ext_graph->m_saddle_maxima_mapping.size(); ++saddle_ordinal) {

      for (uint64_t k=0; k<ext_graph->m_saddle_maxima_mapping[saddle_ordinal].size(); ++k) {
        maximum_ordinal = ext_graph->m_saddle_maxima_mapping[saddle_ordinal][k];

        if ( start_index <= maximum_ordinal && maximum_ordinal < end_index )
          ext_graph->m_maxima_saddle_mapping[maximum_ordinal].push_back(saddle_ordinal);
      }

    }

    for (maximum_ordinal=start_index; maximum_ordinal<end_index; ++maximum_ordinal)
      ext_graph->m_maxima_saddle_mapping[maximum_ordinal].shrink_to_fit();

    return 0;
   }

  __CUDA_HOST_ONLY__ int dataset_t :: perform_path_tracing (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph)
   {
    uint64_t tmp_start, tmp_end;

    puts("[3/3] Path Traversal");

    host_clock(tmp_start); // Path Traversal time
    const int num_cpus = data.num_cpus;

    std::vector <std::thread> worker_thread_list;

    for (int i=0; i<num_cpus; ++i) {
      worker_thread_list.push_back(std::thread(dataset_t :: create_saddle_maxima_mappings, std::ref(data), this, ext_graph, i, num_cpus));
    }

    for (int i=0; i<num_cpus; ++i)
      worker_thread_list[i].join();

    worker_thread_list.clear();

    ext_graph->min_path_len = data.min_path_len[0];
    ext_graph->max_path_len = data.max_path_len[0];

    #if PERFORM_PATH_TRACING_RESTRUCTURE_PENDING_PATHS
    #endif

    for (int i=1; i<num_cpus; ++i) {
      ext_graph->min_path_len = min(ext_graph->min_path_len, data.min_path_len[i]);
      ext_graph->max_path_len = max(ext_graph->max_path_len, data.max_path_len[i]);
    }

    #define PERFORM_PATH_TRACING_RESTRUCTURE_PENDING_PATHS 1

    #if PERFORM_PATH_TRACING_RESTRUCTURE_PENDING_PATHS

    std::vector<std::tuple<point_index_t, point_index_t, uint64_t>> pending_saddle_maximum_path_tracing_list;
 
    for (int i=0; i<num_cpus; ++i)
      pending_saddle_maximum_path_tracing_list.insert(pending_saddle_maximum_path_tracing_list.end(), data.pending_saddle_maximum_path_tracing[i].begin(), data.pending_saddle_maximum_path_tracing[i].end());

    data.pending_saddle_maximum_path_tracing.clear();

    for (int i=0; i<data.num_cpus; ++i) {
      std::vector<std::tuple<point_index_t, point_index_t, uint64_t>> tmp_pending_saddle_maximum_path_tracing_sublist;

      /*
      const uint64_t start_idx = i * num_pending_paths;
      const uint64_t end_idx   = min(start_idx + num_pending_paths, pending_saddle_maximum_path_tracing_list.size());
      */
      const uint64_t start_idx = segment_range(pending_saddle_maximum_path_tracing_list.size(), data.num_cpus, i);
      const uint64_t end_idx   = segment_range(pending_saddle_maximum_path_tracing_list.size(), data.num_cpus, i + 1);

      tmp_pending_saddle_maximum_path_tracing_sublist.insert(tmp_pending_saddle_maximum_path_tracing_sublist.end(),
          pending_saddle_maximum_path_tracing_list.begin() + start_idx,
          pending_saddle_maximum_path_tracing_list.begin() + end_idx );

      data.pending_saddle_maximum_path_tracing.push_back(tmp_pending_saddle_maximum_path_tracing_sublist);
    }

    #endif

    uint64_t tmp_start2;

    host_clock(tmp_start2);

    for (int i=0; i<num_cpus; ++i)
      worker_thread_list.push_back(std::thread(dataset_t :: create_maxima_saddle_mappings, std::ref(data), this, ext_graph, i, num_cpus));

    for (int i=0; i<num_cpus; ++i)
      worker_thread_list[i].join();

    host_clock(tmp_end); // Path Traversal time
    data.path_collection_time       += tmp_end - tmp_start;
    data.maxima_saddle_mapping_time += tmp_end - tmp_start2;

    return 0;
   }

  __CUDA_HOST_ONLY__ int dataset_t :: perform_point_classification_cpu (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph)
   {
    uint64_t tmp_start, tmp_start2, tmp_end;

    host_clock(tmp_start2);
    data.use_gpu = 0;

    const int num_cpus = data.num_cpus;

    const uint64_t num_elements = data.dataset_chunk_rect->num_elements();
    chunk_rect_t chunk_rect = *data.dataset_chunk_rect;

    data.func_vals = data.device_func_vals = m_chunk_fn_vals;
    data.dataset_chunk_rect_device_copy = &chunk_rect;

    #if CUDA_CHECK_ALTERNATING_SUM
      data.point_contribution = new int [num_elements];
    #endif

    #if CUDA_SUBKERNEL_PROFILING
      data.profiling_arr = new cuda_profiling_unit_t [num_elements];
    #endif

    data.p_neighbours = new point_neighbourhood(data.dataset_chunk_rect_device_copy);
    assert(nullptr != data.p_neighbours);

    data.point_index_array = data.device_point_index_array = new uint8_t[num_elements];
    data.device_edge_connectivity_array = ext_graph->m_max_neighbour_array;

    #if CUDA_CHECK_UNVISITED_VERTICES
      memset(data.device_point_index_array, UNVISITED_VERTEX_LABEL, sizeof(uint8_t) * num_elements);
    #endif

    const uint64_t elements_per_cpu = ceilf( ((long double)num_elements) / num_cpus );

    printf("Num Vertices        : %lu\n", num_elements);
    printf("Num CPU Processors  : %d\n" , num_cpus);
    printf("Vertices / processor: %lu\n", elements_per_cpu);

    printf("\n");
    printf("[1/3] Point Classification\n");

    #if ENABLE_DEBUGGING
    for (int i=0; i<num_cpus; ++i) {
      point_index_t start_index = segment_range(num_elements, num_cpus, i);
      point_index_t end_index   = segment_range(num_elements, num_cpus, i + 1);
      printf("thread[%d]: [%lu, %lu)\n", i, (unsigned long)start_index, (unsigned long)end_index);
    }
    #endif

    auto point_classification_lambda = 
    [=] (const int thread_idx) -> void {

      const point_index_t start_index = segment_range(num_elements, num_cpus, thread_idx);
      const point_index_t end_index   = segment_range(num_elements, num_cpus, thread_idx + 1);
      const uint32_t num_dimensions = chunk_rect.num_dimensions();

        if (2 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {                    
                extremum_graph_point_single_point <2> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }

        else if (3 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <3> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }

        else if (4 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <4> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }

        else if (4 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <5> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }

        else if (5 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <5> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }

        else if (6 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <6> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (7 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <7> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        #if MAX_DIMENSIONS > 7
        else if (8 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <8> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (9 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <9> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (10 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <10> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (11 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <11> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (12 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <12> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (13 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <13> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (14 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <14> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (15 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <15> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        #if MAX_DIMENSIONS > 15
        else if (16 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <16> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (17 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <17> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (18 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <18> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (19 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <19> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (20 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <20> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (21 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <21> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (22 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <22> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (23 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <23> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (24 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <24> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (25 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <25> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (26 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <26> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (27 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <27> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (28 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <28> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (29 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <29> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (30 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <30> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }


        else if (31 == num_dimensions) {
            for (point_index_t pt_index=start_index; pt_index<end_index; ++pt_index) {
                extremum_graph_point_single_point <31> (
                    pt_index, &m_dataset_rect, &chunk_rect, data.p_neighbours,
                    data.func_vals, data.func_val_neg_offset, data.bound_start_index, data.invert_fn_val,
                    data.point_index_array, data.edge_connectivity_array

                    #if CUDA_CHECK_ALTERNATING_SUM
                    , data.point_contribution
                    #endif
                );
            }
        }
        #endif
        #endif

        else {
            fprintf(stderr, "Extremum Graph call for data dimensions=%u not configured\n", num_dimensions);
            exit(-1);
        }
    };

    host_clock(tmp_start);

    std::vector <std::thread> worker_thread_list;

    for (int i=0; i<num_cpus; ++i)
      worker_thread_list.push_back(std::thread(point_classification_lambda, i));

    for (int i=0; i<num_cpus; ++i)
      worker_thread_list[i].join();

    host_clock(tmp_end);

    data.kernel_time += tmp_end - tmp_start;

    data.point_classification_time += tmp_end - tmp_start2;

    return 0;
   }

  __CUDA_HOST_ONLY__ int dataset_t :: perform_point_classification_gpu (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph)
   {
    (void)ext_graph; // Used for CPU only. Hides unused variable error.

    int retval = -1;
    uint64_t tmp_start, tmp_end;
    uint64_t tmp_start2, tmp_end2;

    host_clock(tmp_start2);
    data.use_gpu = 1;

    #if !CUDA_SUPPORT
      (void)tmp_start;
      (void)tmp_end;
      fprintf(stderr, "Not compiled against CUDA for GPU compute.\n");
      retval = -1;
    #else
      int deviceID = -1, deviceCount = -1;

      #if ENABLE_NVPROF_PROFILING
      cudaProfilerStart();
      #endif

      CUDA_SAFE_CALL(cudaGetDeviceCount(&deviceCount));

      if (deviceCount < 1) {
        fprintf(stderr, "No Nvidia GPU online for compute. Possible driver error. System restart usually fixes this error.\n");
        retval = -1;
      }

      cudaGetDevice(&deviceID);

      const uint64_t num_elements = data.dataset_chunk_rect->num_elements();

      #if CUDA_CHECK_ALTERNATING_SUM
        data.point_contribution = umalloc <int> (num_elements, deviceID);
      #endif

      #if CUDA_SUBKERNEL_PROFILING
        data.profiling_arr = umalloc <cuda_profiling_unit_t> (num_elements, deviceID);
      #endif

      int threads_per_warp;
      int multi_processor_count = 0, cores_per_multi_processor = 0;

      printf("CUDA Device ID: %d\n", deviceID);
  
      CUDA_core_stats (&multi_processor_count, &cores_per_multi_processor);
      cudaDeviceGetAttribute(&threads_per_warp, cudaDevAttrWarpSize, deviceID);

      printf("data.bounding_rect: %s\n", data.bounding_rect->to_string().c_str());

      CUDA_SAFE_CALL(cudaMalloc(&data.device_func_vals, sizeof(func_val_t) * data.bounding_rect->num_elements()));
      assert(nullptr != data.device_func_vals);

      host_clock(tmp_start);
      CUDA_SAFE_CALL(cudaMemcpy(data.device_func_vals, &m_chunk_fn_vals[data.bound_start_index], sizeof(func_val_t) * data.bounding_rect->num_elements(), cudaMemcpyHostToDevice ));
      host_clock(tmp_end);
      data.cuda_mem_transfer_time += (tmp_end - tmp_start);

      data.func_vals = m_chunk_fn_vals;

      data.dataset_chunk_rect_device_copy = data.bounding_rect->create_device_copy(deviceID);
      data.dataset_chunk_rect_device_copy->move_to_device(deviceID);
      chunk_rect_t * const m_dataset_rect_device_copy = m_dataset_rect.create_device_copy(deviceID);
      m_dataset_rect_device_copy->move_to_device(deviceID);

      CUDA_SAFE_CALL(cudaMallocManaged(((void**)&data.p_neighbours), sizeof(point_neighbourhood)));
      new (data.p_neighbours) point_neighbourhood(data.dataset_chunk_rect_device_copy); // Construct on allocated memory
      data.p_neighbours->move_to_device(deviceID);

      CUDA_SAFE_CALL(cudaMalloc(&data.device_point_index_array      , sizeof(uint8_t) * data.dataset_chunk_rect->num_elements() ));
      assert(data.device_point_index_array != nullptr);

      CUDA_SAFE_CALL(cudaMalloc(&data.device_edge_connectivity_array, sizeof(neighbour_bitmap_t) * data.dataset_chunk_rect->num_elements() ));
      assert(data.device_edge_connectivity_array != nullptr);

      #if CUDA_CHECK_UNVISITED_VERTICES
        CUDA_SAFE_CALL(cudaMemset(data.device_point_index_array, UNVISITED_VERTEX_LABEL, sizeof(uint8_t) * data.dataset_chunk_rect->num_elements() ));
      #endif

      int num_blocks_x, num_blocks_y = 1, num_blocks_z = 1;
      uint64_t num_blocks;

//      dim3 block_layout(1, threads_per_warp,  cores_per_multi_processor/threads_per_warp * 2);
//      dim3 block_layout(1, threads_per_warp, 128/threads_per_warp);
      dim3 block_layout(1, threads_per_warp, 128/threads_per_warp);
      const uint64_t threads_per_block = (block_layout.x * block_layout.y * block_layout.z);

      const uint64_t num_threads_requred = data.bounding_rect->num_elements();

      num_blocks = (num_threads_requred + threads_per_block - 1) / threads_per_block;
//      num_blocks = (num_elements + threads_per_block - 1) / threads_per_block;
      num_blocks_z = num_blocks_y = num_blocks_x = ceill(cbrtl( ((long double)num_blocks) ));
      num_blocks = num_blocks_z * num_blocks_y * ((uint64_t)num_blocks_x);

      dim3 grid_layout(num_blocks_x, num_blocks_y, num_blocks_z);
      uint64_t num_threads_spawned = grid_layout.x * grid_layout.y * grid_layout.z * block_layout.x * block_layout.y * ((uint64_t)block_layout.z);

      printf("\n");
      printf("CUDA Cores       : %d\n", CUDA_core_count());
      printf("Cores/MP         : %d\n", cores_per_multi_processor);
      printf("Num threads/warp : %d\n", threads_per_warp);

      printf("\n");
      printf("Grid  Layout     : (%u, %u, %u)\n", grid_layout.x, grid_layout.y, grid_layout.z);
      printf("Block Layout     : (%u, %u, %u)\n", block_layout.x, block_layout.y, block_layout.z);
      printf("Num threads/block: %lu\n", threads_per_block);
      printf("Num blocks       : %lu\n", num_blocks);
      printf("num_threads      : %lu\n", num_threads_spawned);
      printf("num_vertices     : %lu\n", num_elements);

      printf("\n");
      printf("[1/3] Point Classification\n");

//      assert(num_threads_spawned >= (num_elements + (data.compute_start_index-data.bound_start_index) + (data.bound_end_index - data.compute_end_index)));
      assert(num_threads_spawned >= num_threads_requred);

      uint64_t compute_available_memory, compute_total_memory, compute_used_memory;

      cudaMemGetInfo(&compute_available_memory, &compute_total_memory);
      compute_used_memory = compute_total_memory - compute_available_memory;
      compute_used_memory -= (130 * 1024 * 1024); // Change by looking at `nvidia-smi` before running process

      printf("GPU Mem Usage: %lu B | [%s] | %lu MB / %lu MB\n", compute_used_memory, prettify_memory_value(compute_used_memory).c_str(), compute_used_memory/(1024 * 1024), compute_total_memory/(1024 * 1024));

//      printf("data.func_val_neg_offset: %lu\n", data.func_val_neg_offset);

      host_clock(tmp_start);

      cuda_extremum_graph_point_classification_non_template
        (
          grid_layout, block_layout,
          data.func_val_neg_offset, (data.compute_end_index - data.compute_start_index + data.func_val_neg_offset),
          m_dataset_rect_device_copy, data.dataset_chunk_rect_device_copy, data.p_neighbours,
          data.device_func_vals, data.func_val_neg_offset, data.compute_start_index, data.invert_fn_val,
          data.device_point_index_array, data.device_edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          ,data.point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING
          , (cuda_profiling_unit_t*)data.profiling_arr
          #endif
        );

      data.point_index_array = new uint8_t [num_elements];
      assert(nullptr != data.point_index_array);

      cudaError_t kernel_error = cudaDeviceSynchronize();
      host_clock(tmp_end);
      data.kernel_time += tmp_end - tmp_start;

      if (cudaSuccess != kernel_error)
        printf("Kernel finished with [%d: %s]\n", kernel_error, cudaGetErrorString(kernel_error));

      host_clock(tmp_start);
      data.dataset_chunk_rect_device_copy->move_to_device(-1);
      m_dataset_rect_device_copy->move_to_device(-1);
      CUDA_SAFE_CALL(cudaMemcpy(data.point_index_array, data.device_point_index_array, sizeof(uint8_t) * data.dataset_chunk_rect->num_elements(), cudaMemcpyDeviceToHost));
      CUDA_SAFE_CALL(cudaMemcpy(&ext_graph->m_max_neighbour_array[data.compute_start_index], data.device_edge_connectivity_array, sizeof(neighbour_bitmap_t) * data.dataset_chunk_rect->num_elements(), cudaMemcpyDeviceToHost));
      host_clock(tmp_end);
      data.cuda_mem_transfer_time += (tmp_end - tmp_start);

      data.dataset_chunk_rect_device_copy->~chunk_rect_t();
      m_dataset_rect_device_copy->~chunk_rect_t();
      CUDA_SAFE_CALL(cudaFree(data.device_edge_connectivity_array));
      CUDA_SAFE_CALL(cudaFree(data.device_point_index_array));
      CUDA_SAFE_CALL(cudaFree(data.device_func_vals));

      retval = 0;
    #endif

    host_clock(tmp_end2);

    data.point_classification_time = tmp_end2 - tmp_start2;
    return retval;
   }

  __CUDA_HOST_ONLY__ void dataset_t :: print_compute_debug (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph)
   {
    (void)data;
    (void)ext_graph;
    #if CUDA_CHECK_UNVISITED_VERTICES
    if (data.unvisited_vertices.size()>0) {
      printf("\x1b[38;5;1m");
      printf("\nUnvisited Vertex Count: %lu\n[ ", data.unvisited_vertices.size());
      int max_count = 30;
      for (auto i=data.unvisited_vertices.begin(); i!=data.unvisited_vertices.end() && max_count>0; ++i,--max_count)
        printf("%lu, ", (unsigned long)*i);
      printf("\b\b ]\nAll vertices have not been visited!\n\x1b(B\x1b[m");
    }

    data.unvisited_vertices.clear();
    #endif

    #if CHECK_REGULAR_POINT_CLASSIFICATION
      #if ENABLE_INDEX_PLACEHOLDER
       uint64_t total_pt_count = data.dataset_chunk_rect->num_elements();
      #else
//        uint64_t total_pt_count = data.regular_pt_count + ext_graph->m_chunk_maxima.size() + ext_graph->m_chunk_saddles.size() + ext_graph->m_chunk_1saddles.size() + ext_graph->m_chunk_minima.size() + (COLLECT_MULTISAD_INTO_SAD ? -1 : 0) * ((data.dataset_chunk_rect->num_dimensions() & 0x1) ? 1 : 0) * ext_graph->m_chunk_multi_saddles.size();
        uint64_t total_pt_count =   data.regular_pt_count + data.chunk_maxima_count
                                  + data.chunk_saddles_count + data.chunk_1saddles_count +
                                  + data.chunk_minima_count + (COLLECT_MULTISAD_INTO_SAD ? -1 : 0) * ((data.dataset_chunk_rect->num_dimensions() & 0x1) ? 1 : 0) * data.chunk_multisaddles_count;
      #endif

      printf("\nRegular point count: %lu\nTotal point count  : %lu\nNum elements       : %lu\n", data.regular_pt_count, total_pt_count, data.dataset_chunk_rect->num_elements());
      printf("Uncertain crit pts : %lu\n", data.unknown_crit_pt_count);
      assert(total_pt_count == data.dataset_chunk_rect->num_elements());
      assert(0 == data.unknown_crit_pt_count);
    #endif

    #if CUDA_CHECK_ALTERNATING_SUM

      if (data.use_gpu)
        cudaMemPrefetchAsync(data.point_contribution, data.dataset_chunk_rect->num_elements() * sizeof(int), -1, nullptr);

      int64_t min_contribution = 0x7fffffffffffffff, max_contribution = 0xffffffffffffffff;

      point_index_t min_contrib_pt = 0, max_contrib_pt = 0;

      for (uint64_t i=0; i<data.dataset_chunk_rect->num_elements(); ++i) {
        data.alternating_sum_computed += data.point_contribution[i];

        if (data.point_contribution[i] < min_contribution) {
          min_contribution = data.point_contribution[i];
          min_contrib_pt = i;
        }
        else if (data.point_contribution[i] > max_contribution) {
          max_contribution = data.point_contribution[i];
          max_contrib_pt = i;
        }

          min_contribution = min(min_contribution, data.point_contribution[i]);
          max_contribution = max(max_contribution, data.point_contribution[i]);
      }

      printf("\nContribution: [min: %ld, pt=%lu, cp_index=%d]", min_contribution, (unsigned long)min_contrib_pt, (int)data.point_index_array[min_contrib_pt]);
      printf(" [max: %ld, pt=%lu, cp_index=%d]\n", max_contribution, (unsigned long)max_contrib_pt, (int)data.point_index_array[max_contrib_pt]);
      ufree(data.point_contribution);
    #endif

    #if CUDA_SUBKERNEL_PROFILING
      puts("");
      double totals[PROFILING_SAMPLES];
      cudaMemPrefetchAsync(data.profiling_arr, data.dataset_chunk_rect->num_elements()*sizeof(cuda_profiling_unit_t), -1, nullptr);
      cuda_sample_aggregate(data.profiling_arr, data.dataset_chunk_rect->num_elements(), totals);
      printf("Profiling aggregate: [Total]: %lf [%lf]\n",  (totals[1]+totals[4]+totals[5]), ((totals[1]+totals[4])/((double)data.dataset_chunk_rect->num_elements())) );
      printf("                        |\n");
      printf("BFS Traversal          [0]:           %lf [%lf]\n",  (totals[0]), ((totals[0])/data.dataset_chunk_rect->num_elements()) );
      printf("                        |\n");
      printf("Component First         +---[1]:      %lf [%lf]\n",  (totals[1]), ((totals[1])/data.dataset_chunk_rect->num_elements()) );
      printf("                        |\n");
      printf("Component Traversal     +---[2]:      %lf [%lf]\n",  (totals[2]), ((totals[2])/data.dataset_chunk_rect->num_elements()) );
      printf("                        |    |\n");
      printf("Neighbour finding       |    +---[3]: %lf [%lf]\n",  (totals[3]), ((totals[3])/data.dataset_chunk_rect->num_elements()) );
      printf("                        |\n");
      printf("Point Classification   [4]:           %lf [%lf]\n",  totals[4], (totals[4]/data.dataset_chunk_rect->num_elements()) );
      printf("                        |\n");
      printf("Point Highest Neighbour[5]:           %lf [%lf]\n",  totals[5], (totals[5]/data.dataset_chunk_rect->num_elements()) );
      printf("\n");
      printf("Unified total          [6]:           %lf [%lf]\n",  totals[6], (totals[6]/data.dataset_chunk_rect->num_elements()) );
      ufree(data.profiling_arr);
      puts("");
    #endif
   }

  __CUDA_HOST_ONLY__ void dataset_t :: perform_cleanup (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph, bool on_success)
   {
    if (!on_success) {
      delete [] ext_graph->m_max_neighbour_array;
      ext_graph->m_max_neighbour_array = nullptr;
      ext_graph->clear();
    }

    #if ENABLE_DEBUGGING
      delete [] data.point_index_array; // if !ENABLE_DEBUGGING, deleted in perform_point_collection()
    #endif

    if (data.use_gpu) {

      data.p_neighbours->~point_neighbourhood();
      cudaFree(data.p_neighbours);

      #if ENABLE_NVPROF_PROFILING
      cudaProfilerStop();
      #endif
    }

    else {
      delete data.p_neighbours;
    }

   }

  __CUDA_HOST_ONLY__ void dataset_t :: post_compute_info (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph, bool post_full_compute)
   {
    printf("\n");
    print_time_diff(data.kernel_time          , "Point Classification  ");
    print_time_diff(data.point_collection_time, "Point Collection      ");
    print_time_diff(data.path_collection_time , "Gradient Path Tracing ");
//    print_time_diff(data.maxima_saddle_mapping_time, "Maxima-Saddle Mapping ");

    if (data.use_gpu)
      print_time_diff(data.cuda_mem_transfer_time, "CUDA Mem Transfer Time");

    printf("\x1b[38;5;3m");
    print_time_diff(data.ext_graph_time      , "Ext Graph Computation ");
    printf("\x1b(B\x1b[m");

    #if CUDA_SUPPORT
    if (data.use_gpu) {
      print_time_diff(data.kernel_time * CUDA_core_count()         , "Time per CUDA thread  ", data.compute_end_index);
      print_time_diff(data.kernel_time                             , "Time per vertex       ", data.compute_end_index);
    }
    else
    #endif
      print_time_diff(data.kernel_time * data.num_cpus         , "Time per vertex       ", data.compute_end_index);

    printf("Num blocks            : %3lu\n", data.chunk_count);
    printf("\n");

//    const uint64_t num_elements = m_dataset_rect.num_elements();
//    const uint64_t num_crit_pts = ext_graph->m_chunk_maxima.size() + ext_graph->m_chunk_saddles.size() + ext_graph->m_chunk_1saddles.size() - ext_graph->m_chunk_multi_saddles.size() + ext_graph->m_chunk_minima.size();

    printf("Num maxima   : %lu\n", ext_graph->m_chunk_maxima.size());
    printf("Num saddles  : %lu\n", ext_graph->m_chunk_saddles.size());

//    printf("Num 1-saddles: %10lu / %10lu [ %7.3f%% ] %10lu\n", ext_graph->m_chunk_1saddles.size(), num_elements, (100 * ((double)ext_graph->m_chunk_1saddles.size())/((double)num_elements)), (unsigned long)((ext_graph->m_chunk_1saddles.empty() ? ((point_index_t)-1) : *ext_graph->m_chunk_1saddles.begin())) );

//    printf("Num minima   : %10lu / %10lu [ %7.3f%% ] %10lu\n", ext_graph->m_chunk_minima.size(), num_elements, (100 * ((double)ext_graph->m_chunk_minima.size())/((double)num_elements)), (unsigned long)((ext_graph->m_chunk_minima.empty() ? ((point_index_t)-1) : *ext_graph->m_chunk_minima.begin())) );

//    printf("Num multi-sad: %10lu / %10lu [ %7.3f%% ] %10lu\n", ext_graph->m_chunk_multi_saddles.size(), num_elements, (100 * ((double)ext_graph->m_chunk_multi_saddles.size())/((double)num_elements)), (unsigned long) ((ext_graph->m_chunk_multi_saddles.empty() ? ((point_index_t)-1) : *ext_graph->m_chunk_multi_saddles.begin())) );

//    printf("Num crit pts: %10lu / %10lu [ %7.3f%% ]\n", num_crit_pts, num_elements, (100 * ((double)num_crit_pts)/((double)num_elements)) );

    uint64_t path_count = 0, total_upper_link_count = 0, path_len_totals = 0;

    for (uint64_t i=0; i<ext_graph->m_saddle_maxima_mapping.size(); ++i)
      path_count += ext_graph->m_saddle_maxima_mapping[i].size();

    for (int i=0; i<data.num_cpus; ++i) {
      total_upper_link_count += data.aggregate_upper_link_counts[i];
      path_len_totals += data.path_len_totals[i];
    }

    uint64_t pending_path_tracings = 0;
    for (int i=0; i<data.num_cpus; ++i)
      pending_path_tracings += data.pending_saddle_maximum_path_tracing[i].size();

//    printf("Num paths    : %lu [ min: %lu, max: %lu , true: %lu, edge_count: %lu]\n", path_count, ext_graph->min_path_len, ext_graph->max_path_len, total_upper_link_count, path_len_totals);
    printf("Num paths    : %lu\n", path_count);
    printf("Pending paths: %lu\n", pending_path_tracings);

    assert(total_upper_link_count == (path_count + pending_path_tracings));

    assert((data.bound_end_index < m_dataset_rect.num_elements()) | ((data.bound_end_index == m_dataset_rect.num_elements()) && path_count == total_upper_link_count));

    assert(!post_full_compute | (post_full_compute && 0 == pending_path_tracings));

    #if CUDA_CHECK_ALTERNATING_SUM
    printf("\x1b[38;5;%dm\nAlternate Sum: %ld\n\x1b(B\x1b[m", ( (data.alternating_sum_computed==1) ? 2 : 1), data.alternating_sum_computed);
    #endif

    #if ENABLE_DEBUGGING || (DATASET_CHOICE>50) 
      chunk_rect_t *rect = &m_dataset_rect;
      auto index_to_point_string_lambda =
        [=] (const point_index_t pt_index) -> std::string {
              grid_point_t pt(rect->num_dimensions());
              rect->index_to_point(pt_index, &pt);
              return pt.to_string();
            };

      puts("\n================================\n");
      if (!ext_graph->m_chunk_maxima.empty() && ext_graph->m_chunk_maxima.size() < 16)
      print_container_extended <point_index_t, std::string> (ext_graph->m_chunk_maxima , index_to_point_string_lambda, "Maxima   : ", "\n\n");

      if (!ext_graph->m_chunk_saddles.empty() && ext_graph->m_chunk_saddles.size() < 18)
      print_container_extended <point_index_t, std::string> (ext_graph->m_chunk_saddles , index_to_point_string_lambda, "2-Saddles: ", "\n\n");

      if (!ext_graph->m_chunk_1saddles.empty() && ext_graph->m_chunk_1saddles.size() < 16)
      print_container_extended <point_index_t, std::string> (ext_graph->m_chunk_1saddles, index_to_point_string_lambda, "1-Saddles: ", "\n\n");

      if (!ext_graph->m_chunk_minima.empty() && ext_graph->m_chunk_minima.size() < 16)
      print_container_extended <point_index_t, std::string> (ext_graph->m_chunk_minima  , index_to_point_string_lambda, "Minima   : ", "\n\n");

      if (!ext_graph->m_chunk_multi_saddles.empty() && ext_graph->m_chunk_multi_saddles.size() < 16)
      print_container_extended <point_index_t, std::string> (ext_graph->m_chunk_multi_saddles  , index_to_point_string_lambda, "Multi-Sad: ", "\n\n");
      puts("\n================================\n");
    #endif
   }

  __CUDA_HOST_ONLY__ void perform_init (dataset_t * const dataset_instance, dataset_t :: extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph, neighbour_bitmap_t * const neighbour_bitmap_ptr)
   {
    if (dataset_instance->is_chunk_loaded() && (dataset_instance->m_chunk_rect.num_elements() < dataset_instance->m_dataset_rect.num_elements())) {
      dataset_instance->unload_chunk();
      dataset_instance->load_chunk(dataset_instance->m_dataset_rect);
    }

    if ((dataset_instance->m_chunk_rect.num_elements() < dataset_instance->m_dataset_rect.num_elements()))
      assert(!dataset_instance->load_chunk(dataset_instance->m_dataset_rect));

    assert(dataset_instance->get_dataset_func_range(ext_graph->m_dataset_func_range));

    data.num_cpus = properties::num_threads();

    for (int i=0; i<data.num_cpus; ++i) {
      data.pending_saddle_maximum_path_tracing.push_back(std::vector<std::tuple<point_index_t, point_index_t, uint64_t>>());
      data.min_path_len.push_back(0xffffffffffffffff);
      data.max_path_len.push_back(0);

      data.aggregate_upper_link_counts.push_back(0);
      data.path_len_totals.push_back(0);
    }

    data.kernel_time                = 0;
    data.path_collection_time       = 0;
    data.maxima_saddle_mapping_time = 0;
    data.point_collection_time      = 0;
    data.cuda_mem_transfer_time     = 0;
    data.ext_graph_time             = 0;
    data.chunk_count                = 0;

    data.invert_fn_val = dataset_instance->m_dataset_negate_fn_val;
    data.alternating_sum_computed = 0;
    data.edge_connectivity_array = ext_graph->m_max_neighbour_array = neighbour_bitmap_ptr;
    assert(nullptr != data.edge_connectivity_array);
   }

  void save_edge_conn_arr (const neighbour_bitmap_t * const edge_connectivity_array, const uint64_t len, const char* filename)
   {
    FILE * f = fopen(filename ,"wb+");
    fwrite(edge_connectivity_array, sizeof(neighbour_bitmap_t) ,len ,f);
    fclose(f);
   }

  void update_block_params (dataset_t :: extremum_graph_computation_data_t * const data, plan_t :: plan_item_t * const item)
   {
    data->compute_start_index      =  item->compute_start_index;
    data->compute_end_index        =  item->compute_end_index;
    data->dataset_chunk_rect       = &item->compute_rect;

    data->bound_start_index        =  item->bound_start_index;
    data->bound_end_index          =  item->bound_end_index;
    data->bounding_rect            = &item->bounding_rect;
    data->bounding_cumulative_rect = &item->bounding_cumulative_rect;

    data->func_val_neg_offset      = data->compute_start_index - data->bound_start_index;
   }

  void update_ext_data (dataset_t :: extremum_graph_computation_data_t * const dest_data, dataset_t :: extremum_graph_computation_data_t * const src_data)
   {

    for (int i=0; i<dest_data->num_cpus; ++i) {

      dest_data->pending_saddle_maximum_path_tracing[i]
        .insert(
                dest_data->pending_saddle_maximum_path_tracing[i].end(),
                src_data->pending_saddle_maximum_path_tracing[i].begin(),
                src_data->pending_saddle_maximum_path_tracing[i].end()
               );

      dest_data->min_path_len[i] = min(dest_data->min_path_len[i], src_data->min_path_len[i]);
      dest_data->max_path_len[i] = max(dest_data->max_path_len[i], src_data->max_path_len[i]);

      dest_data->aggregate_upper_link_counts[i] += src_data->aggregate_upper_link_counts[i];

      dest_data->path_len_totals[i] += src_data->path_len_totals[i];
    }

    dest_data->ext_graph_time             += src_data->ext_graph_time;
    dest_data->point_classification_time  += src_data->point_classification_time;
    dest_data->kernel_time                += src_data->kernel_time;
    dest_data->path_collection_time       += src_data->path_collection_time;
    dest_data->maxima_saddle_mapping_time += src_data->maxima_saddle_mapping_time;
    dest_data->cuda_mem_transfer_time     += src_data->cuda_mem_transfer_time;
    dest_data->point_collection_time      += src_data->point_collection_time;

    dest_data->regular_pt_count           += src_data->regular_pt_count;
    dest_data->unknown_crit_pt_count      += src_data->unknown_crit_pt_count;

    dest_data->chunk_maxima_count         += src_data->chunk_maxima_count;
    dest_data->chunk_saddles_count        += src_data->chunk_saddles_count;
    dest_data->chunk_1saddles_count       += src_data->chunk_1saddles_count;
    dest_data->chunk_minima_count         += src_data->chunk_minima_count;
    dest_data->chunk_multisaddles_count   += src_data->chunk_multisaddles_count;
    dest_data->chunk_count                += src_data->chunk_count;

    dest_data->alternating_sum_computed   += src_data->alternating_sum_computed;
   }

  bool dataset_t :: compute_ext_graph (extremum_graph_t * const ext_graph, bool use_gpu)
   {
    std::vector <plan_t :: plan_item_t> hybrid_item_vec;
    std::vector <extremum_graph_computation_data_t> hybrid_exec_data_vec;
    std::thread gpu_compute_thread;

    // Atomically set block index for which step 1 on GPU is running
    std::atomic_size_t step1_current_block(0);
    std::atomic_int8_t step1_error_val(0), step2_error_val(0);
    size_t step2_curr_blockid = 0;

    uint64_t compute_ext_graph_start, compute_ext_graph_end;
    int deviceID = -1;
    plan_t :: plan_item_t item;

    host_clock(compute_ext_graph_start);

    neighbour_bitmap_t * const neighbour_bitmap_ptr = new neighbour_bitmap_t[m_dataset_rect.num_elements()];

    #if CUDA_SUPPORT
    int deviceCount;
    cudaGetDeviceCount(&deviceCount);
    #endif

    plan_t dataset_chunk_planner(&m_dataset_rect);

    if (use_gpu) {

      #if CUDA_SUPPORT

        if (deviceCount == 0) {
          fprintf(stderr, "Requested GPU compute but no Nvidia GPU found online, most probably a driver error. A System restart usually fixes this error.\n");
          delete [] neighbour_bitmap_ptr;
          return false;
        }

      #else

        fprintf(stderr, "Requested GPU computation but not built with CUDA libraries. Please rebuild the project with CUDA_SUPPORT=1.\n");
        use_gpu = 0;
        exit(-1);

      #endif
    }

    deviceID = use_gpu ? properties::gpu_id() : -1;

    if (use_gpu && deviceID >= 0) {
      cudaSetDevice(deviceID); // TODO: Use best GPU in the presense of multiple CUDA enabled GPUs with Metric := CUDA_core_count() * peak_clock_freq
                               // Note: No multi-gpu implementation for now. Should be easy to implement (Famous last words)
    }

    while (dataset_chunk_planner.next_item(deviceID, item)) { // Collect blocking plans for each dataset block
      hybrid_item_vec.push_back(item);
    }

    for (size_t blockid=0; blockid<hybrid_item_vec.size(); ++blockid) { // Collect and initialize compute data for each dataset block
      extremum_graph_computation_data_t data_element;
      perform_init(this, data_element, ext_graph, neighbour_bitmap_ptr);
      update_block_params(&data_element, &hybrid_item_vec[blockid]);
      hybrid_exec_data_vec.push_back(data_element);
    }

    if (use_gpu) { // Spawn separate thread to perform GPU compuation for CPU-GPU hybrid execution

      auto gpu_compute_thread_func = [&] () {

        for (size_t blockid=0; blockid<hybrid_exec_data_vec.size(); ++blockid) {

          if (step2_error_val.load() != 0)
            return;

	        printf("GPU thread: blockID %lu\n", blockid);
          extremum_graph_computation_data_t& data = hybrid_exec_data_vec[blockid];

          if (0 != perform_point_classification_gpu(data, ext_graph)) {
            step1_error_val.store(-1);
            return;
          }

          step1_current_block.store(1 + blockid);
          printf("GPU thread: block %lu/%lu done\n", blockid + 1, hybrid_exec_data_vec.size());
        }

      };

      gpu_compute_thread = std::thread(gpu_compute_thread_func);
    }

    uint64_t waiting_time = 0, tmp_time1, tmp_time2;

    for (size_t blockid=0; blockid<hybrid_exec_data_vec.size(); ++blockid) {

      step2_curr_blockid = blockid;

      bool first_time = 1;

      if (step1_error_val.load() != 0) {
        break;
      }

      host_clock(tmp_time1);
      while (use_gpu && (step1_current_block.load() <= blockid)) {
        usleep(5); // Step 1 on GPU is lagging behind, wait for it!
        if (first_time && (hybrid_exec_data_vec.size() > 1)) printf("CPU thread: Waiting for block %ld!\n", blockid);
        first_time = 0;
      }

      if (use_gpu && (hybrid_exec_data_vec.size() > 1))
        printf("CPU thread: Waiting done for block %lu!\n", blockid);

      extremum_graph_computation_data_t& data = hybrid_exec_data_vec[blockid];
      host_clock(tmp_time2);
      waiting_time += (tmp_time2 - tmp_time1);

      int step_retval;

      if (!use_gpu)
        step_retval = perform_point_classification_cpu(data, ext_graph);


      if (step_retval != 0) {
        perform_cleanup(data, ext_graph, false);
        step2_error_val.store(-1);
        break;
      }

      step_retval = perform_point_collection(data, ext_graph);
      if (step_retval != 0) {
        perform_cleanup(data, ext_graph, false);
        step2_error_val.store(-1);
        break;
      }

      step_retval = perform_path_tracing(data, ext_graph);
      if (step_retval != 0) {
        perform_cleanup(data, ext_graph, false);
        step2_error_val.store(-1);
        break;
      }

      print_compute_debug(data, ext_graph);
      perform_cleanup(data, ext_graph, true);

      if (data.compute_end_index < m_dataset_rect.num_elements())
        post_compute_info(data, ext_graph);

      data.chunk_count += 1;

      // Move statistical and computation information into next block, if current block is not the last
      if ((blockid + 1) < hybrid_exec_data_vec.size())
        update_ext_data(&hybrid_exec_data_vec[blockid+1], &data);

    }

    if (use_gpu) gpu_compute_thread.join();

    const bool final_retval = (step1_error_val.load() == 0) && (step2_error_val.load() == 0);

    if (!final_retval) {
      perform_cleanup(hybrid_exec_data_vec[step2_curr_blockid], ext_graph, false); // Ensure ext_graph.m_max_neighbour_array is freed

      for (size_t blockid=1 + step2_curr_blockid; blockid<hybrid_exec_data_vec.size(); ++blockid)   
          perform_cleanup(hybrid_exec_data_vec[step2_curr_blockid], ext_graph, true);
    }

    ext_graph->init_after_setup();
    host_clock(compute_ext_graph_end);

    extremum_graph_computation_data_t& last_block_data = hybrid_exec_data_vec[hybrid_exec_data_vec.size()-1];

    last_block_data.ext_graph_time = compute_ext_graph_end - compute_ext_graph_start;

    post_compute_info(last_block_data, ext_graph, true);
    printf("Waiting time : %lu.%06lus\n", waiting_time/1000000, waiting_time%1000000);

    uint64_t max_maxima_adjacency = 0;
    for (uint64_t i=0; i<ext_graph->m_maxima_saddle_mapping.size(); ++i)
      max_maxima_adjacency = max(max_maxima_adjacency, ext_graph->m_maxima_saddle_mapping[i].size());

    uint64_t max_saddle_adjacency = 0;
    for (uint64_t i=0; i<ext_graph->m_saddle_maxima_mapping.size(); ++i)
      max_saddle_adjacency = max(max_saddle_adjacency, ext_graph->m_saddle_maxima_mapping[i].size());

//    printf("max(maximum adjacency) = %lu\n", max_maxima_adjacency);
//    printf("max(saddle  adjacency) = %lu\n", max_saddle_adjacency);

    puts("");
    return final_retval;
   }

  __CUDA_HOST_ONLY__ void dataset_t :: saddle_ascending_manifold (const extremum_graph_t * const ext_graph, const uint64_t saddle_ordinal, std::vector<std::pair<uint64_t, uint64_t>> * const vector_of_edges)
   {
    (void)ext_graph;
    (void)saddle_ordinal;
    (void)vector_of_edges;
    // TODO
   }

  __CUDA_HOST_ONLY__ void dataset_t :: maximum_decending_manifold (const extremum_graph_t * const ext_graph, const uint64_t maximum_ordinal, std::vector<std::pair<uint64_t, uint64_t>> * const vector_of_edges)
   {
    (void)ext_graph;
    (void)maximum_ordinal;
    (void)vector_of_edges;
    // TODO
   }

  __CUDA_HOST_ONLY__ point_index_t interpolate_point (const dataset_t * const dataset_instance, const point_index_t start_pt_index, const point_diff_t * const pt_diff, const func_val_t step)
   {
    const uint32_t num_dimensions = dataset_instance->m_dataset_rect.num_dimensions();

    point_index_t interpolated_pt_index = start_pt_index;

    for (uint32_t k=0; k<num_dimensions; ++k) {
      const dimension_diff_t span_step = round(pt_diff->get(k) * step);
      interpolated_pt_index += span_step * dataset_instance->m_dataset_rect.m_prefix_products[k];
    }

    return interpolated_pt_index;
   }

  __CUDA_HOST_ONLY__ void interpolate_path (const dataset_t * const dataset_instance, const point_index_t start_pt_index, const point_index_t end_pt_index, std::vector <point_index_t> * const path_vector)
   {
    const uint32_t num_dimensions = dataset_instance->m_dataset_rect.num_dimensions();

    grid_point_t start_pt(num_dimensions), end_pt(num_dimensions);
    point_diff_t pt_diff(num_dimensions);

    dataset_instance->m_dataset_rect.index_to_point(start_pt_index, &start_pt);
    dataset_instance->m_dataset_rect.index_to_point(end_pt_index, &end_pt);

    end_pt.minus(&start_pt, &pt_diff);
    dimension_span_t max_diff = 0;
    for (uint32_t k=0; k<num_dimensions; ++k)
      max_diff = max(max_diff, (dimension_span_t)abs(pt_diff[k]));

    const uint64_t num_samples = max_diff;
    const func_val_t increment = 1.0f / num_samples;
    func_val_t curr_step = 0;

    static uint64_t count = 0;

    if (count>0) ++count;

    if (count == 0 && max_diff > 0) {
      ++count;
      printf("num_samples: %lu, increment: %lf\n", num_samples, increment);
    }

    path_vector->clear();

    for (uint64_t ii=0; ii<=num_samples; ++ii, curr_step += increment) {
      const point_index_t next_pt_index = interpolate_point(dataset_instance, start_pt_index, &pt_diff, curr_step);

      if (path_vector->empty() || path_vector->at(path_vector->size()-1) != next_pt_index)
        path_vector->push_back(next_pt_index);
    }

    assert(!path_vector->empty() && path_vector->at(path_vector->size()-1) == end_pt_index);

    if (count==1) {
      printf("path_vector: (%lu -> %lu) [ ", start_pt_index, end_pt_index);
      for (uint64_t i=0; i<path_vector->size(); ++i)
        printf("%lu, ", path_vector->at(i));
      printf("]\n");
    }
   }

  __CUDA_HOST_ONLY__ void dataset_t :: saddle_immediate_ascending_manifold (const extremum_graph_t * const ext_graph, const uint64_t saddle_ordinal,  std::vector<std::vector<point_index_t>> * const vector_of_paths) const
   {
    point_neighbourhood p_neighbours(&m_chunk_rect);

    // TODO: Make this dimension generic
    point_index_t component_highest[14];
    uf_bfs_data_t uf_data_arr[14];
    uint64_t path_len = 0, maximum_ordinal = 0;
    uint32_t tmp_arr[14], true_upper_link_count = 0, upper_link_count = 0, lower_link_count = 0;
    bool partial_termination = false;

    grid_point_t pt(m_dataset_rect.num_dimensions()), pt_i(m_dataset_rect.num_dimensions()), pt_j(m_dataset_rect.num_dimensions());
    std::vector <point_index_t> path_vector;

    const point_index_t saddle_index = ext_graph->m_chunk_saddles[saddle_ordinal];
    const bool enable_interpolation = 0;

    neighbour_bfs_union_find_non_template
      (
        saddle_index, &m_chunk_rect, &p_neighbours,
        m_chunk_fn_vals, 0, m_dataset_negate_fn_val, 
        true_upper_link_count, upper_link_count, lower_link_count, component_highest
        ,&pt, &pt_i, &pt_j, uf_data_arr, tmp_arr
      );

    vector_of_paths->clear();
    vector_of_paths->reserve(upper_link_count);

    for (uint64_t i=0; i<upper_link_count; ++i) {

      path_vector.clear();

      point_index_t maxima_index;

      if (enable_interpolation && ext_graph->m_saddle_zero_persistence[saddle_ordinal])
        maxima_index = single_path_tracing <false> (ext_graph, 0, 0, component_highest[i], partial_termination, path_len, &path_vector);
      else
        maxima_index = single_path_tracing <true> (ext_graph, 0, 0, component_highest[i], partial_termination, path_len, &path_vector);        

      assert(!partial_termination);

      if (!binary_search_find_index<point_index_t>(&ext_graph->m_chunk_maxima, maxima_index, maximum_ordinal)) {
         printf("Offending point: %lu; path_len: %lu\n", (unsigned long)maxima_index, path_len);
         printf("Is Maximum: %d\n", (int)( ext_graph->m_chunk_maxima.end()  != std::find(ext_graph->m_chunk_maxima .begin(), ext_graph->m_chunk_maxima .end(), maxima_index) ));
         printf("Is Saddle : %d\n", (int)( ext_graph->m_chunk_saddles.end() != std::find(ext_graph->m_chunk_saddles.begin(), ext_graph->m_chunk_saddles.end(), saddle_index) ));
         assert(binary_search_find_index<point_index_t>(&ext_graph->m_chunk_maxima, maxima_index, maximum_ordinal));
      }

      if (enable_interpolation && !ext_graph->m_saddle_surviving[saddle_ordinal]) {
        path_vector.push_back(maxima_index); // Let Paraview perform a much better visual interpolation
      }

      vector_of_paths->push_back(path_vector);
    }

   }


  int dataset_t :: dump_field_as_csv (const extremum_graph_t * const ext_graph, const std::string& filename) const
   {
    unlink(filename.c_str());
    printf("Writing file \"%s\"\n", filename.c_str());
    int fd = open(filename.c_str(), O_CREAT|O_RDWR, S_IRWXU | S_IRGRP|S_IXGRP | S_IXOTH|S_IXOTH );

    if (-1 == fd)
      return -1;

    grid_point_t p(m_num_dimensions);
    int point_type;
    ssize_t written_bytes;

    for (uint32_t i=0; i<m_dataset_rect.num_dimensions(); ++i) {
        written_bytes = dprintf(fd, "x%u, ", i+1);
        assert(written_bytes>0);      
    }

    written_bytes = dprintf(fd, "val, type\n");
    assert(written_bytes>0);

    for (point_index_t i=0; i<m_dataset_rect.num_elements(); ++i) {

      if ( std::find(ext_graph->m_chunk_maxima.begin(), ext_graph->m_chunk_maxima.end(), i) != ext_graph->m_chunk_maxima.end() )
        point_type = m_dataset_rect.num_dimensions();
      else if ( std::find(ext_graph->m_chunk_saddles.begin(), ext_graph->m_chunk_saddles.end(), i) != ext_graph->m_chunk_saddles.end() )
        point_type = m_dataset_rect.num_dimensions() - 1;
      else
        point_type = -1;

      m_dataset_rect.index_to_point(i,p);

      for (uint32_t k=0; k<p.size(); ++k)
        assert(dprintf(fd, "%u, ", p[k]) > 0);

      assert(dprintf(fd, "%f, %d\n", m_chunk_fn_vals[i], point_type) > 0);
    }

    close(fd);
    printf("Done writing \"%s\" \n", filename.c_str());

    return 0;
   }

};
