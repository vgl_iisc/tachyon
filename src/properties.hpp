/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#ifndef PROPERTIES_H
#define PROPERTIES_H

#include <cstdio>
#include <string>
#include <cstring>
#include <map>
#include <cctype>

#include <sys/sysinfo.h>
#include <unistd.h>

namespace extgraph {

    class properties {

        public:

            // Property values
            static int m_num_threads;
            static int m_gpu_id;


            static int num_threads() noexcept;
            static int gpu_id() noexcept;

            static void __attribute__((constructor)) init_properties ();

    };

};

#endif // PROPERTIES_H
