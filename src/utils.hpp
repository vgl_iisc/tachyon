/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#ifndef UTILS_HPP
#define UTILS_HPP

#include <algorithm>
#include <thread>
#include <cmath>
#include <cinttypes>
#include <thread>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cctype>

#include <sys/sysinfo.h>
#include <sys/time.h>
#include <unistd.h>
#include <poll.h>

#define ENABLE_INPUT_POLLING 1

namespace extgraph {

  extern struct pollfd has_input_pfd[1];// = {0, 0, 0};

  void __attribute__((constructor)) init_has_input () noexcept;

  #if ENABLE_INPUT_POLLING

    inline __attribute__((hot)) bool has_input (const char trigger='s') noexcept
     {
      return (poll(has_input_pfd, 1, 0) == 1) && (tolower(getchar()) == trigger);
     }

  #else

    inline __attribute__((hot)) constexpr bool has_input (const char trigger='s') noexcept
     {
      return false;
     }

  #endif

  template <typename T, typename U> __CUDA_PREFIX__ __CUDA_FORCEINLINE__ T min (const T& a, const U& b) noexcept
   {
    return (a < b) ? a : b;
   }

  template <typename T, typename U> __CUDA_PREFIX__ __CUDA_FORCEINLINE__ T max (const T& a, const U& b) noexcept
   {
    return (a > b) ? a : b;
   }

  template <typename T>
  __CUDA_PREFIX__ __CUDA_FORCEINLINE__ void swap (T& a, T& b) noexcept
   {
    T c;
    c = a;
    a = b;
    b = c;
   }

  __CUDA_PREFIX__ __CUDA_FORCEINLINE__ uint64_t segment_range (const uint64_t range, const int num_segments, const int segment_index) noexcept
   {
    assert(num_segments > 0);
    assert(0 <= segment_index && segment_index <= num_segments);
    const imaxdiv_t d = imaxdiv(range, num_segments);
    return (segment_index * d.quot) + min(segment_index, d.rem);
   }

  __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ void host_clock (uint64_t& t) noexcept
   {
    struct timeval tv;
    gettimeofday(&tv, nullptr);
    t = 1000000 * tv.tv_sec + tv.tv_usec;
   }

  #define PRINT_TIME_SIMPLE 1

  __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ void print_time_diff (const uint64_t t, const char* prefix, const char* suffix, const uint64_t divisor=1) noexcept
   {
    const uint64_t  s =  t / 1000000UL;

    #if PRINT_TIME_SIMPLE
      if (divisor==1)
        printf("%s: %3lu.%06lus%s", prefix, s, (t - 1000000UL * s), suffix);
      else if (divisor > 0) {
        long double tt = t;
        tt /= divisor;
        printf("%s: %10Lfus%s", prefix, tt, suffix);
      }
      else
        printf("%s: Divive by 0 error [t=%lu, divisor=0]%s\n", prefix, t, suffix);
    #else
      const uint64_t ms = (t - s) / 1000UL;
      const uint64_t us = (t - s - 1000 * ms);
      printf("%s: %lus %lums %luus%s", prefix, s, ms, us , suffix);
    #endif
   }

  __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ void print_time_diff (const uint64_t t, const char* prefix="", const uint64_t divisor=1) noexcept
   {
    print_time_diff(t, prefix, "\n", divisor);
   }

  /**
   * @param v - sorted vector instance
   * @param data - value to search
   * @return 0-based index if data found, -1 otherwise
  */
  template <typename T>
  __CUDA_FORCEINLINE__ bool binary_search_find_index (const std::vector<T> * const v, const T& data, uint64_t& index) noexcept {
    auto it = std::lower_bound(v->begin(), v->end(), data);

    if (it == v->end() || *it != data) {
        return false;
    }
    else {
        index = std::distance(v->begin(), it);
        return true;
    }   

  }

  /**
   * @param v - sorted vector instance
   * @param data - value to search
   * @return 0-based index if data found, -1 otherwise
  */
  template <typename T>
  inline uint64_t vector_get_index (const std::vector<T> * const v, const T& data) {

    for (uint64_t i=0; i<v->size(); ++i)
      if (v->operator[](i) == data)
        return i;

    return 0xffffffffffffffff;
  }

  template <typename T>
  __CUDA_HOST_ONLY__ inline bool sorted_vector_contains (const std::vector <T> * const vec, const T& val) noexcept {
    return std::binary_search(vec->begin(), vec->end(), val);
  }

  template <typename T>
  __CUDA_HOST_ONLY__ inline bool vector_contains (const std::vector <T> * const vec, const T& val) noexcept {
    return std::find(vec->begin(), vec->end(), val) != vec->end();
  }

  template <typename T>
  __CUDA_HOST_ONLY__ inline bool vector_equals (const std::vector <T> * const vec1, const std::vector <T> * const vec2) noexcept {
    if (vec1->size() != vec2->size())
      return false;
    for (uint64_t i=0; i<vec1->size(); ++i)
      if (vec1->operator[](i) != vec2->operator[](i))
        return false;
    return true;
  }

  template <typename T>
  __CUDA_HOST_ONLY__ inline bool vector_contains_val_ptr (const std::vector <std::vector<T>> * const vec, const std::vector <T> * const val) noexcept {
    for (uint64_t i=0; i<vec->size(); ++i) {
      if (vector_equals(&vec->operator[](i), val))
        return true;
    }
    return false;
  }

  template <typename T>
  __CUDA_HOST_ONLY__ inline void vector_remove_item (std::vector <T> * const vec, const T& val) {
    vec->erase(std::find(vec->begin(), vec->end(), val));
  }

  template <typename T>
  __CUDA_HOST_ONLY__ inline void sort_vector (std::vector <T> * const vec) {
    std::sort(vec->begin(), vec->end());
  }

  template <typename Function, class ...FunctionArgs>
  void begin_thread_pool (const int num_threads, Function func, FunctionArgs... args)
   {
    std::vector <std::thread> thread_vector;

    for (int i=0; i<num_threads; ++i)
      thread_vector.push_back(std::thread(func, i, num_threads, args...));

    for (int i=0; i<num_threads; ++i)
      thread_vector[i].join();
   }

  template <typename Function, class ...FunctionArgs>
  void begin_thread_pool (Function func, FunctionArgs... args)
   {
    begin_thread_pool<Function, FunctionArgs...>(properties::num_threads(), func, args...);
   }

  template <typename R, typename T, typename LambdaEval, typename OperatorEval, class ...StaticArgs>
  __CUDA_FORCEINLINE__ const T __attribute__((hot)) arg_eval (const LambdaEval eval_function, const OperatorEval op_func, const R init_val, const std::vector<T> * const arg_list, StaticArgs... static_args)
   {
    R curr_val = init_val;
    T arg     = 0;

    for (auto i=arg_list->begin(); i!=arg_list->end(); ++i) {

      const R tmp_val = eval_function(*i, static_args...);

      if (op_func(tmp_val, curr_val)) {
        curr_val = tmp_val;
        arg = *i;
      }

    }

    return arg;
   }

  template <typename R, typename T, typename LambdaEval, class ...StaticArgs>
  __CUDA_FORCEINLINE__ const T __attribute__((hot)) arg_max (const LambdaEval eval_function, const R init_val, const std::vector<T> * const arg_list, StaticArgs... static_args)
   {
    #if 1

      static auto arg_eval_as_max_lambda = [](const R op1, const R op2) { return op1 > op2; };
      return arg_eval(eval_function, arg_eval_as_max_lambda, init_val, arg_list, static_args...);

    #else
    R max_val = init_val;
    T arg     = 0;

    for (uint64_t i=0; i<arg_list->size(); ++i) {

      const R tmp_val = eval_function(arg_list->at(i), static_args...);

      if (tmp_val > max_val) {
        max_val = tmp_val;
        arg = arg_list->at(i);
      }

    }

    return arg;
    #endif
   }

  template <typename R, typename T, typename LambdaEval, class ...StaticArgs>
  __CUDA_FORCEINLINE__ const T __attribute__((hot)) arg_min (const LambdaEval eval_function, const R init_val, const std::vector<T> * const arg_list, StaticArgs... static_args)
   {
    #if 1

      static auto arg_eval_as_min_lambda = [](const R op1, const R op2) { return op1 < op2; };
      return arg_eval(eval_function, arg_eval_as_min_lambda, init_val, arg_list, static_args...);

    #else
    R max_val = init_val;
    T arg     = 0;

    for (uint64_t i=0; i<arg_list->size(); ++i) {

      const R tmp_val = eval_function(arg_list->at(i), static_args...);

      if (tmp_val < max_val) {
        max_val = tmp_val;
        arg = arg_list->at(i);
      }

    }

    return arg;
    #endif
   }

  template <typename T, class Container>
  __CUDA_PREFIX__ __CUDA_FORCEINLINE__ void print_container (const Container& c, const char prefix[], const char suffix[])
   {
    printf("%s",prefix);

    for (uint64_t i=0; i<c.size(); ++i)
      printf("%lu ,", ((unsigned long int)((T)c[i])));

    printf("%s", suffix);
   }

  template <typename T, class Container>
  __CUDA_PREFIX__ __CUDA_FORCEINLINE__ void print_container (const Container& c, const char prefix[])
   {
    print_container<T, Container>(c, prefix, "\n");
   }

  template <typename T, typename V, class Container, typename LambdaTD>
  __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ void print_container_extended (const Container& c, LambdaTD lambda, const char prefix[], const char suffix[])
   {
    std::cout<<prefix;

    for (auto it=c.begin(); it!=c.end(); ++it)
      std::cout << *it << " " << ((V)lambda(*it)) << ", ";

    std::cout<<suffix;
   }

  template <typename T, typename V, class Container, typename LambdaTD>
  __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ void print_container_extended (const Container& c, LambdaTD lambda, const char prefix[])
   {
    print_container_extended<T,V,Container,LambdaTD>(c, lambda, prefix, "\n");
   }

    #if CUDA_SUPPORT

        template <typename T>
        __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ T* umalloc (const uint64_t num_elements, const int device = -1)
         {
          T *ptr = nullptr;
          cudaError_t cudaerr = cudaMallocManaged(((void**)&ptr), num_elements * sizeof(T));
          assert(cudaerr == cudaSuccess);

          if (nullptr == ptr)
            printf("umalloc::cudaMallocManaged() failed for (sizeof(T)=%lu, num_elements=%lu, device=%d). Error: [%d, %s]\n", sizeof(T), num_elements, device, cudaerr, cudaGetErrorString(cudaerr));

          assert(ptr != nullptr);

          if (device >= 0)
              cudaMemPrefetchAsync(ptr, num_elements * sizeof(T), device, nullptr);

          return ptr;
         }

        template <typename T>
        __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ void ufree (T*& ptr)
         {
          if (nullptr != ptr)
            cudaFree((void*)ptr);

          ptr = nullptr;
         }

    #else

        template <typename T>
        __CUDA_FORCEINLINE__ T* umalloc (const uint64_t num_elements, int device=-1)
         {
          (void)device; // Removes unused variable warning. Kept to match function signature when CUDA_SUPPORT is enabled.

           T * ptr = (T*)malloc(num_elements * sizeof(T));

           if (nullptr == ptr)
             printf("umalloc::malloc() failed for (sizeof(T)=%lu, num_elements=%lu)\n", sizeof(T), num_elements);

           assert(ptr != nullptr);
           return ptr;
         }
    
        template <typename T>
        __CUDA_FORCEINLINE__ void ufree (T*& ptr)
         {
          if (nullptr != ptr)
            free((void*)ptr);

          ptr = nullptr;
         }

    #endif

  template <typename T>
  class arc_redirection_info_template_t : public std::vector <T>
   {

     public:

      arc_redirection_info_template_t () {}

      arc_redirection_info_template_t (const uint64_t maxima_count, const uint64_t saddle_count)
       {
        init(maxima_count, saddle_count);
       }

      void init (const uint64_t maxima_count, const uint64_t saddle_count)
       {
        const uint64_t total_count = maxima_count + saddle_count;
        this->clear();

        for (uint64_t i=0; i<total_count; ++i)
          this->push_back(no_redirection_val());
       }

      inline T no_redirection_val () const noexcept {
        return std :: numeric_limits <T> :: max();
      }

      T get_redirection (const T index) const noexcept {
        return this->operator[](index);
      }

      void set_redirection (const T index, const T dest) noexcept {
        this->at(index) = dest;
      }

      void remove_redirection (const T index) noexcept {
        this->at(index) = no_redirection_val();
      }

      bool has_redirection (const T index) const noexcept {
        return no_redirection_val() != this->at(index);
      }

   };

  template <typename T>
  class index_mapper
   {

    public:

      virtual uint64_t to_index (const T val) const noexcept = 0;

   };

  template <>
  class index_mapper <uint64_t>
   {

    public:

      __CUDA_FORCEINLINE__ uint64_t to_index (const uint64_t val) const noexcept { return val; }

   };

  template <typename T, typename size_type_t, size_type_t max_size>
  class hybrid_vector
   {

     protected:

      void move_to_array ();
      void move_to_vector ();

     public:

//        using reference       = typename std::vector <T>::reference;
//        using const_reference = typename std::vector <T>::const_reference;

        uint8_t access_array  : 1;
        size_type_t curr_size : 8*sizeof(size_type_t)-1;

        T arr[max_size];
        std::vector <T> * vec;

        hybrid_vector ();
        hybrid_vector (const hybrid_vector& other);
        hybrid_vector (hybrid_vector&& other);
        ~hybrid_vector ();

        hybrid_vector& operator = (const hybrid_vector& other);

        uint64_t size () const noexcept;
        bool empty () const noexcept;

        T&       operator [] (uint64_t index) noexcept;
        const T& operator [] (uint64_t index) const noexcept;

        T&       at (uint64_t index);
        const T& at (uint64_t index) const;

        void push_back (const T& v);
//        void push_back (T&& v);

        void erase (uint64_t index);
        void erase_item (const T val);

        bool contains (const T val)                  const noexcept;
        bool find     (const T val, uint64_t& index) const noexcept;

        void clear() noexcept;

        T&       back () noexcept;
        const T& back () const noexcept;

        const T *       data () const noexcept;

        void reserve (uint64_t count);
        void shrink_to_fit ();

   };

  template <typename T, typename size_type_t, size_type_t max_size>
  hybrid_vector <T, size_type_t, max_size> :: hybrid_vector ()
   {
    access_array = 1;
    curr_size    = 0;
    vec          = nullptr;
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  hybrid_vector <T, size_type_t, max_size> :: hybrid_vector (const hybrid_vector& other)
   {
    access_array = other.access_array;
    curr_size    = other.curr_size;

    if (access_array) {
      memcpy(arr, other.arr, curr_size * sizeof(T));
      vec = nullptr;
    }
    else {
      vec = new std::vector<T> ();
      vec->insert(vec->end(), other.vec->begin(), other.vec->end());
    }
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  hybrid_vector <T, size_type_t, max_size> :: hybrid_vector (hybrid_vector&& other)
   {
    access_array = other.access_array;
    curr_size    = other.curr_size;
    vec          = other.vec;

    other.access_array = 1;
    other.curr_size    = 0;
    other.vec          = nullptr;
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  hybrid_vector <T, size_type_t, max_size> :: ~hybrid_vector ()
   {
    if (nullptr != vec)
      delete vec;
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  hybrid_vector <T, size_type_t, max_size>& hybrid_vector <T, size_type_t, max_size> :: operator = (const hybrid_vector& other)
   {
    if (!access_array) {
      if (other.access_array) {
        delete vec;
        vec = nullptr;
      }
      else
        vec->clear();
    }
    else if (!other.access_array)
      vec = new std::vector <T>();

    if (other.access_array)
      memcpy(arr, other.arr, curr_size * sizeof(T));
    else
      vec->insert(vec->end(), other.vec->begin(), other.vec->end());

    access_array = other.access_array;
    curr_size    = other.curr_size;

    return *this;
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  __CUDA_FORCEINLINE__ uint64_t hybrid_vector <T, size_type_t, max_size> :: size () const noexcept
   {
    return access_array ? curr_size : vec->size();
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  __CUDA_FORCEINLINE__ bool hybrid_vector <T, size_type_t, max_size> :: empty () const noexcept
   {
    return access_array ? (0 == curr_size) : vec->empty();
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  void hybrid_vector <T, size_type_t, max_size> :: push_back (const T& v)
   {
    if (access_array) {
      if (curr_size == max_size) {
        move_to_vector();
        vec->push_back(v);
      }
      else
        arr[curr_size++] = v;
    }
    else
      vec->push_back(v);
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  void hybrid_vector <T, size_type_t, max_size> :: erase (uint64_t index)
   {
    if (access_array) {

      if (index >= curr_size)
        throw std::out_of_range("hybrid_vector::erase");

      for (uint8_t i=index; i+1<curr_size; ++i)
        arr[i] = arr[i+1];

      curr_size -= 1;
    }
    else {
      vec->erase(vec->begin() + index);

      if (vec->size() <= curr_size)
        move_to_array();
    }
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  void hybrid_vector <T, size_type_t, max_size> :: erase_item (const T val)
   {
    if (access_array) {

      uint64_t index = curr_size;

      for (uint64_t i=0; i<curr_size; ++i)
        if (val == arr[i]) {
          index = i;
          break;
        }

      if (index >= curr_size)
        throw std::out_of_range("hybrid_vector::erase_item");

      for (uint8_t i=index; i+1<curr_size; ++i)
        arr[i] = arr[i+1];

      curr_size -= 1;
    }
    else {
      vec->erase(std::find(vec->begin(), vec->end(), val));

      if (vec->size() <= curr_size)
        move_to_array();
    }
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  bool hybrid_vector <T, size_type_t, max_size> :: find (const T val, uint64_t& index) const noexcept
   {
    if (access_array) {

      for (uint8_t i=0; i<curr_size; ++i)
        if (val == arr[i]) {
          index = i;
          return true;
        }

      index = curr_size;
      return false;
    }

    else {
      const auto iter = std::find(vec->begin(), vec->end(), val);

      if (iter != vec->end())
        index = std::distance(vec->begin(), iter);
      else
        index = vec->size();

      return (index < vec->size());
    }

   }

  template <typename T, typename size_type_t, size_type_t max_size>
  __CUDA_FORCEINLINE__ bool hybrid_vector <T, size_type_t, max_size> :: contains (const T val) const noexcept
   {
    uint64_t tmp;
    return find(val, tmp);
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  void hybrid_vector <T, size_type_t, max_size> :: clear () noexcept
   {
    if (access_array)
      curr_size = 0;
    else
      vec->clear();

    move_to_array();
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  __CUDA_FORCEINLINE__ T& hybrid_vector <T, size_type_t, max_size> :: operator [] (uint64_t index) noexcept
   {
    return access_array ? arr[index] : vec->operator[](index);
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  __CUDA_FORCEINLINE__ const T& hybrid_vector <T, size_type_t, max_size> :: operator [] (uint64_t index) const noexcept
   {
    return access_array ? arr[index] : vec->operator[](index);
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  __CUDA_FORCEINLINE__ T& hybrid_vector <T, size_type_t, max_size> :: at (uint64_t index)
   {
    if (access_array) {
      if (index >= curr_size)
        throw std::out_of_range("hybrid_vector::at");
      return arr[index];
    }
    else
      return vec->at(index);
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  __CUDA_FORCEINLINE__ const T& hybrid_vector <T, size_type_t, max_size> :: at (uint64_t index) const
   {
    if (access_array) {
      if (index >= curr_size)
        throw std::out_of_range("hybrid_vector::at");
      return arr[index];
    }
    else
      return vec->at(index);
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  void hybrid_vector <T, size_type_t, max_size> :: reserve (uint64_t count)
   {
    if (access_array) {
      if (count > max_size)
        move_to_vector();
    }
    else
      vec->reserve(count);
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  __CUDA_FORCEINLINE__ void hybrid_vector <T, size_type_t, max_size> :: shrink_to_fit ()
   {
    if (!access_array)
      vec->shrink_to_fit();
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  void hybrid_vector <T, size_type_t, max_size> :: move_to_array ()
   {
    if (access_array) return;

    assert(vec->size() <= max_size);
    curr_size = vec->size();

    for (uint64_t i=0; i<vec->size(); ++i)
      arr[i] = vec->operator[](i); // vector::at() performs bounds check, which are not required here

    delete vec;
    vec = nullptr;

    access_array = 1;
   }

  template <typename T, typename size_type_t, size_type_t max_size>
  void hybrid_vector <T, size_type_t, max_size> :: move_to_vector ()
   {
    if (!access_array) return;

    vec = new std::vector <T>();
    vec->reserve(curr_size);

    for (uint64_t i=0; i<curr_size; ++i)
      vec->push_back(arr[i]);

    curr_size = 0;
    access_array = 0;
   }

  template <typename K, typename V, typename size_type_t, size_type_t multi_map_size, class key_index_mapper = index_mapper<uint64_t>, class val_index_mapper = index_mapper<uint64_t>>
  class multi_bimap
   {

      protected:

//        std::vector <std::vector<V>> key_val_map;
//        std::vector <std::vector<K>> val_key_map;

        using value_collection = hybrid_vector <V, size_type_t, multi_map_size>;
        using key_collection   = hybrid_vector <K, size_type_t, multi_map_size>;

        std::vector <value_collection> key_val_map;
        std::vector <key_collection>   val_key_map;

        key_index_mapper key_mapper;
        val_index_mapper val_mapper;

      public:

        multi_bimap () {}
        multi_bimap (const uint64_t init_num_keys, const uint64_t init_num_vals);

        void add_key ();
        void add_val ();

        void add_key_val_map (const K& key, const V& val);
        void del_key_val_map (const K& key, const V& val);

        std::vector <V> key_mappings (const K& key);
        std::vector <K> val_mappings (const V& val);

        uint64_t key_count () const noexcept { return key_val_map.size(); }
        uint64_t val_count () const noexcept { return val_key_map.size(); }

        void shrink_to_fit();
   };

  template <typename K, typename V, typename size_type_t, size_type_t multi_map_size, class key_index_mapper, class val_index_mapper>
  multi_bimap <K, V, size_type_t, multi_map_size, key_index_mapper, val_index_mapper> :: multi_bimap (const uint64_t init_num_keys, const uint64_t init_num_vals)
   {
    key_val_map.reserve(init_num_keys);
    val_key_map.reserve(init_num_vals);
   }

  template <typename K, typename V, typename size_type_t, size_type_t multi_map_size, class key_index_mapper, class val_index_mapper>
  void multi_bimap <K, V, size_type_t, multi_map_size, key_index_mapper, val_index_mapper> :: add_key ()
   {
    key_val_map.push_back(value_collection());
   }

  template <typename K, typename V, typename size_type_t, size_type_t multi_map_size, class key_index_mapper, class val_index_mapper>
  void multi_bimap <K, V, size_type_t, multi_map_size, key_index_mapper, val_index_mapper> :: add_val ()
   {
    val_key_map.push_back(key_collection());
   }

  template <typename K, typename V, typename size_type_t, size_type_t multi_map_size, class key_index_mapper, class val_index_mapper>
  void multi_bimap <K, V, size_type_t, multi_map_size, key_index_mapper, val_index_mapper> :: add_key_val_map (const K& key, const V& val)
   {
    const uint64_t key_index = key_mapper.to_index(key);
    const uint64_t val_index = val_mapper.to_index(val);

    key_val_map[key_index].push_back(val);
    val_key_map[val_index].push_back(key);
   }

  template <typename K, typename V, typename size_type_t, size_type_t multi_map_size, class key_index_mapper, class val_index_mapper>
  void multi_bimap <K, V, size_type_t, multi_map_size, key_index_mapper, val_index_mapper> :: del_key_val_map (const K& key, const V& val)
   {
    key_val_map[key_mapper.to_index(key)].erase_item(val);
    val_key_map[val_mapper.to_index(val)].erase_item(key);

//    key_val_map[key_index].erase(std::find(key_val_map[key_index].begin(), key_val_map[key_index].end(), val));
//    val_key_map[val_index].erase(std::find(val_key_map[val_index].begin(), val_key_map[val_index].end(), key));
   }

  template <typename K, typename V, typename size_type_t, size_type_t multi_map_size, class key_index_mapper, class val_index_mapper>
  std::vector <V> multi_bimap <K, V, size_type_t, multi_map_size, key_index_mapper, val_index_mapper> :: key_mappings (const K& key)
   {
    return key_val_map[key_mapper.to_index(key)];
   }

  template <typename K, typename V, typename size_type_t, size_type_t multi_map_size, class key_index_mapper, class val_index_mapper>
  std::vector <K> multi_bimap <K, V, size_type_t, multi_map_size, key_index_mapper, val_index_mapper> :: val_mappings (const V& val)
   {
    return val_key_map[val_mapper.to_index(val)];
   }

  template <typename K, typename V, typename size_type_t, size_type_t multi_map_size, class key_index_mapper, class val_index_mapper>
  void multi_bimap <K, V, size_type_t, multi_map_size, key_index_mapper, val_index_mapper> :: shrink_to_fit ()
   {
    std::vector<std::vector<V>> * const key_val_map_ptr = &key_val_map;
    std::vector<std::vector<K>> * const val_key_map_ptr = &val_key_map;

    auto mapping_shrink_lambda =
      []
      (auto vector_of_vectors_ptr, const int thread_idx, const int num_cpus, const bool shrink_vector) -> void
        {
          const uint64_t elements_per_thread = (vector_of_vectors_ptr->size() + num_cpus - 1) / num_cpus;
          const uint64_t element_start_index = thread_idx * elements_per_thread;
          const uint64_t element_end_index   = min(element_start_index + elements_per_thread, vector_of_vectors_ptr->size());

          for (uint64_t element_index=element_start_index; element_index<element_end_index; ++element_index)
            vector_of_vectors_ptr->at(element_index).shrink_to_fit();

          if (shrink_vector)
            vector_of_vectors_ptr->shrink_to_fit();
        };

    auto key_val_mapping_shrink_lambda =
      [key_val_map_ptr, val_key_map_ptr, mapping_shrink_lambda]
      (const int thread_idx, const int num_cpus) -> void
        {
          mapping_shrink_generic_lambda(key_val_map_ptr, thread_idx, num_cpus, thread_idx==0);
          mapping_shrink_generic_lambda(val_key_map_ptr, thread_idx, num_cpus, thread_idx==1);
        };

    const int num_cpus = properties::num_threads();
    std::vector <std::thread> worker_threads;

    for (int i=0; i<num_cpus; ++i)
      worker_threads.push_back(std::thread(key_val_mapping_shrink_lambda, i, num_cpus));

    for (int i=0; i<num_cpus; ++i)
      worker_threads[i].join();

   }

};

#endif
