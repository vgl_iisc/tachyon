/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#ifndef EXT_GRAPH_ALGS
#define EXT_GRAPH_ALGS

#include <grid.hpp>
#include <extremum_graph.hpp>
#include <grid_dataset.hpp>

#include <queue>
#include <utility>
#include <stack>

#define PERSISTENCE_DEBUG 0

namespace extgraph {

  typedef std::pair<point_index_t, func_val_t> saddle_persistence_pair_t;

  class saddle_persistence_pair_t_comparator
   {
    public:

      bool operator() (const std::pair<point_index_t, func_val_t>& left, const std::pair<point_index_t, func_val_t>& right) const noexcept
       {
        #if 1
        if (right.second == left.second)
            return right.first < left.first;
        else
        #endif
            return right.second < left.second;
       }

   };

  typedef std::priority_queue <saddle_persistence_pair_t, std::vector<saddle_persistence_pair_t>, saddle_persistence_pair_t_comparator> saddle_persistence_queue_t;

  void create_adjacency_graph (critical_point_map_t * const adjacency_map, const extremum_graph_t * const ext_graph)
   {
    adjacency_map->clear();
    adjacency_map->insert(adjacency_map->end(), ext_graph->m_maxima_saddle_mapping.begin(), ext_graph->m_maxima_saddle_mapping.end());
    adjacency_map->insert(adjacency_map->end(), ext_graph->m_saddle_maxima_mapping.begin(), ext_graph->m_saddle_maxima_mapping.end());
   }

  void print_adjacency_graph (const critical_point_map_t * const adjacency_map, const std::vector <point_index_t> * const m_chunk_maxima, const std::vector <point_index_t> * const m_chunk_saddles)
   {
    const uint64_t num_maxima  = m_chunk_maxima->size();

    printf("Maxima-Saddle Mapping\n");

    for (uint64_t i=0; i<num_maxima; ++i) {
      printf("Maxima [%6lu, %6lu]: [ ", i, (uint64_t)m_chunk_maxima->at(i));
      for (uint64_t j=0; j<adjacency_map->at(i).size(); ++j)
        printf("%lu, ", (uint64_t)adjacency_map->at(i).at(j));
      printf("]\n");
    }

    printf("\nSaddle-Maxima Mapping\n");

    for (uint64_t i=num_maxima; i<adjacency_map->size(); ++i) {
      printf("Saddle [%6lu, %6lu]: [ ", i - num_maxima, (uint64_t)m_chunk_saddles->at(i - num_maxima));
      for (uint64_t j=0; j<adjacency_map->at(i).size(); ++j)
        printf("%lu, ", (uint64_t)adjacency_map->at(i).at(j));
      printf("]\n");
    }

   }

  inline bool range_intersect (const func_val_t start1, const func_val_t end1, const func_val_t start2, const func_val_t end2)
   {
    return (start1 <= end2) && (start2 <= end1);
   }

  uint64_t count_surviving_arcs (const critical_point_map_t * const adjacency_map, const uint64_t num_maxima, const std::vector<bool> * const m_saddle_removed)
   {
    uint64_t surviving_arc_count = 0;
    uint64_t max_extrema_saddle_arcs = 0, max_saddle_extrema_arcs = 0;
    uint64_t extrema_saddle_arcs = 0, saddle_extrema_arcs = 0;

    for (uint64_t ordinal=0; ordinal<adjacency_map->size(); ++ordinal) {
      if (ordinal >= num_maxima && m_saddle_removed->at(ordinal - num_maxima))
        continue;

      if (ordinal < num_maxima)
        max_extrema_saddle_arcs = max(max_extrema_saddle_arcs, adjacency_map->at(ordinal).size());
      else
        max_saddle_extrema_arcs = max(max_saddle_extrema_arcs, adjacency_map->at(ordinal).size());

      if (ordinal  < num_maxima) extrema_saddle_arcs += adjacency_map->at(ordinal).size();
      if (ordinal >= num_maxima) saddle_extrema_arcs += adjacency_map->at(ordinal).size();
      surviving_arc_count += adjacency_map->at(ordinal).size();
    }

    printf("mtos-arcs: %lu, stom-arcs: %lu\n", extrema_saddle_arcs, saddle_extrema_arcs);
    printf("max_extrema_saddle_arcs: %lu\n", max_extrema_saddle_arcs);
    printf("max_saddle_extrema_arcs: %lu\n", max_saddle_extrema_arcs);
    return surviving_arc_count;
   }

  uint64_t count_surviving_cp (const std::vector <bool> * const bool_vector)
   {
    uint64_t surviving_count = 0;

    for (uint64_t i=0; i<bool_vector->size(); ++i)
        surviving_count += bool_vector->at(i);

    return surviving_count;
   }

  void init_persistence_structures
     (
             saddle_persistence_queue_t * const saddle_queue,
             persistence_map_t          * const persistence_vals,
             persistence_map_t          * const saturated_persistence_vals,
             critical_point_map_t       * const adjacency_map,
       const critical_point_map_t       * const m_saddle_maxima_mapping,
       const critical_point_map_t       * const m_maxima_saddle_mapping,
       const std::vector <bool>         * const m_saddle_surviving,
       const bool                               m_invert_fn_val,
       const bool                               m_std_persistence_simplification_performed,

       const uint64_t                           num_maxima,
       const uint64_t                           num_saddles,
       const bool                               skip_queue_filling=false

     )
   {
       (void)m_invert_fn_val;

      auto structures_init_lambda =
      [=]
//        [ &saddle_queue, &persistence_vals, &saturated_persistence_vals, &adjacency_map, m_saddle_maxima_mapping, m_saddle_surviving, num_maxima, num_saddles]
        (const int choice, const int num_threads) -> void {

          (void)num_threads;

          const uint64_t num_cps = num_maxima + num_saddles;
  
          if (choice==0) {
            for (uint64_t i=0; i<num_cps; ++i)
//                persistence_vals->push_back(func_val_max_toggled(m_invert_fn_val));
              persistence_vals->push_back(FUNC_VAL_MAX);

            assert(persistence_vals->size() == num_cps);
          }
  
          else if (choice==1) {
            for (uint64_t i=0; i<num_saddles; ++i)
//                saturated_persistence_vals->push_back(func_val_max_toggled(m_invert_fn_val));
              saturated_persistence_vals->push_back(FUNC_VAL_MAX);

            assert(saturated_persistence_vals->size() == num_saddles);
          }
  
          else if (choice==2) {
            if (skip_queue_filling)
              return;

            for (point_index_t i=0; i<num_saddles; ++i)
              if (m_saddle_surviving->at(i) && !m_saddle_maxima_mapping->at(i).empty())
                saddle_queue->push(std::make_pair(i, 0));

//            assert(saddle_queue->size() == num_saddles);
          }

          else if (choice==3) {

            if (m_std_persistence_simplification_performed) {
              adjacency_map->insert(adjacency_map->end(), m_maxima_saddle_mapping->begin(), m_maxima_saddle_mapping->end());
            }

            else {
              for (uint64_t i=0; i<num_maxima; ++i)
                  adjacency_map->push_back(critical_point_map_element_t());
            }

            adjacency_map->insert(adjacency_map->end(), m_saddle_maxima_mapping->begin(), m_saddle_maxima_mapping->end());

            assert(adjacency_map->size() == num_cps);
          }

      };

      begin_thread_pool(4, structures_init_lambda);
   }

  void verify_mapping_validity
   (
    const std::vector <std::vector<uint64_t>> * const mapping,
    const uint64_t                                    num_indices
   )
   {
    for (uint64_t from_ordinal=0; from_ordinal<mapping->size(); ++from_ordinal)
      for (uint64_t to_ordinal: mapping->at(from_ordinal))
        assert(to_ordinal < num_indices);
   }

  inline void flip_saddle_maxima_path_status
   (
    const uint64_t                                          saddle_ordinal,
    const uint64_t                                          maximum_ordinal,
    const std::vector <std::vector<uint64_t>>       * const m_saddle_maxima_mapping,
          std::vector <saddle_maxima_path_status_t> * const m_saddle_maxima_mapping_status
   )
   {
    m_saddle_maxima_mapping_status->at(saddle_ordinal)
      .flip(vector_get_index(&m_saddle_maxima_mapping->at(saddle_ordinal), maximum_ordinal));
   }

  inline bool is_saddle_maxima_arc_reversed
   (
    const uint64_t                                          saddle_ordinal,
    const uint64_t                                          maximum_ordinal,
    const std::vector <std::vector<uint64_t>>       * const m_saddle_maxima_mapping,
    const std::vector <saddle_maxima_path_status_t> * const m_saddle_maxima_mapping_status
   )
   {
    return m_saddle_maxima_mapping_status->at(saddle_ordinal)[vector_get_index(&m_saddle_maxima_mapping->at(saddle_ordinal), maximum_ordinal)];
   }

  inline bool check_maxima_outgoing_arc_exists
   (
    const uint64_t                                          maximum_ordinal,
    const std::vector <std::vector<uint64_t>>       * const m_maxima_saddle_mapping,
    const std::vector <std::vector<uint64_t>>       * const m_saddle_maxima_mapping,
    const std::vector <saddle_maxima_path_status_t> * const m_saddle_maxima_mapping_status,
          uint64_t&                                         reversed_arc_saddle_ordinal
   )
   {
    uint64_t num_outgoing_arcs = 0;

    for (uint64_t adjacent_saddle_ordinal: m_maxima_saddle_mapping->at(maximum_ordinal)) {
      if (is_saddle_maxima_arc_reversed(adjacent_saddle_ordinal, maximum_ordinal, m_saddle_maxima_mapping, m_saddle_maxima_mapping_status)) {
        ++num_outgoing_arcs;
        reversed_arc_saddle_ordinal = adjacent_saddle_ordinal;
      }

      if (num_outgoing_arcs > 1)
        return false;
    }

    return true;
   }

  void find_non_strangulation_adjacent_maxima (const std::vector <uint64_t> * const tail_maxima, std::vector <uint64_t> * const valid_adjacent_maxima)
   {
    valid_adjacent_maxima->clear();

    for (uint64_t i=0; i<tail_maxima->size(); ++i) {
      if (std::count(tail_maxima->begin(), tail_maxima->end(), tail_maxima->at(i)) == 1)
        valid_adjacent_maxima->push_back(i);
    }

    /* Below is a fix for multi-saddle multi-strangulation scenario

      Here ==>>

                Saddle Queue size: 7135
                Removed saddle=[2168, 1305719] with p=0.000000
                Adjacent Maxima [3]: [ 756 ,758 ,837 , ]
                ==Adjacent_maxima for saddle-1788: [ 547, 616, 619, ]
                ==Tail maxima for saddle-1788: [ 835, 835, 835, ]
                ==Path Vector[3]: [ 2168, 758, 1788, ]

    */

//    if (valid_adjacent_maxima->empty() && (std::count(tail_maxima->begin(), tail_maxima->end(), tail_maxima->at(0)) == tail_maxima->size()))
//      valid_adjacent_maxima->push_back(0);
   }

  void extremum_graph_t :: fetch_transitive_paths
    (
      const uint64_t                                               saddle_ordinal,
      const arc_redirection_info_t                         * const arc_redirection_list,
      const critical_point_map_t                           * const adjacency_map,
            std::vector <std::vector<uint64_t>>            * const adjacent_maxima_transitive_paths,
            std::vector <uint64_t>                         * const tail_maxima
    ) const
   {
    std::vector <uint64_t> tmp_path_vector;

    adjacent_maxima_transitive_paths->clear();
    tail_maxima->clear();

    #define FETCH_TRANSITIVE_PATHS_SEPARATE_LOOP 0

    // Fetch adjacent Maxima paths
    for (uint64_t i=0; i<adjacency_map->at(saddle_ordinal + m_chunk_maxima.size()).size(); ++i) {

        const uint64_t adjacent_maxima_ordinal = adjacency_map->at(saddle_ordinal + m_chunk_maxima.size())[i];

        saddle_arc_tracing(arc_redirection_list , saddle_ordinal, adjacent_maxima_ordinal, &tmp_path_vector);
        assert(!tmp_path_vector.empty());

        adjacent_maxima_transitive_paths->push_back(tmp_path_vector);

        #if !FETCH_TRANSITIVE_PATHS_SEPARATE_LOOP
        const uint64_t tail_maximum = adjacent_maxima_transitive_paths->at(i)[adjacent_maxima_transitive_paths->at(i).size()-1];
        tail_maxima->push_back(tail_maximum);
        #endif
    }

    #if FETCH_TRANSITIVE_PATHS_SEPARATE_LOOP
    // Fetch transitively connected Maxima
    for (uint64_t i=0; i<adjacent_maxima_transitive_paths->size(); ++i) {
        const uint64_t tail_maximum = adjacent_maxima_transitive_paths->at(i)[adjacent_maxima_transitive_paths->at(i).size()-1];
        tail_maxima->push_back(tail_maximum);
    }
    #endif

   }

  /*
   * Path vector will consist of points as (start_saddle_ordinal, adjacent_maximum_ordinal, adjacent_maximum_adjacent_saddle_ordinal, ...)
   * i.e. Alternating between saddles and maximum as (saddle, maximum, saddle, maximum, ...)
   */
  void extremum_graph_t :: saddle_arc_tracing
    (
      const arc_redirection_info_t * const arc_redirection_list,
      const uint64_t                       start_saddle_ordinal,
      const point_index_t                  via_maximum_ordinal,
            std::vector <uint64_t> * const path_vector
    ) const
   {
    path_vector->clear();
    path_vector->push_back(start_saddle_ordinal);

    constexpr bool saddle_arc_tracing_debug = PERSISTENCE_DEBUG;

    if (saddle_arc_tracing_debug) printf("saddle_arc_tracing(saddle=%lu, via_max=%lu): [ %lu, ", (uint64_t)m_chunk_saddles[start_saddle_ordinal], (uint64_t)m_chunk_maxima[via_maximum_ordinal], (uint64_t)start_saddle_ordinal);

    uint64_t curr_pt = via_maximum_ordinal;
    uint64_t curr_pt_real;

    do {

        #if 1
        if (path_vector->size() & 0x1) // Curr pt has maximum ordinal
            curr_pt_real = curr_pt;
        else                           // Curr pt has saddle  ordinal
            curr_pt_real = curr_pt - m_chunk_maxima.size();
        #endif

        path_vector->push_back(curr_pt_real);

        if (saddle_arc_tracing_debug) printf("%lu, ", curr_pt);

        if (!arc_redirection_list->has_redirection(curr_pt))
          break;

        curr_pt = arc_redirection_list->at(curr_pt);
    }
    while (true);

    if (saddle_arc_tracing_debug) printf(" ]\n");
   }

  void fetch_path_saddles (const std::vector<uint64_t> * const path_vector, std::vector<uint64_t> * const path_saddles)
   {
    for (uint64_t i=0; i<path_vector->size(); ++i)
        if (!(i & 0x1)) // Even i
            path_saddles->push_back(path_vector->at(i));
   }

  void extremum_graph_t :: reverse_path
    (
      arc_redirection_info_t                         * const arc_redirection_list,
      std::vector <uint64_t>                         * const path_vector
    )
   {
    uint64_t src_pt, dest_pt;
    uint64_t src_real_pt, dest_real_pt;

    #if PERSISTENCE_DEBUG
    printf("Reversing path %lu -> %lu\n", path_vector->at(0), path_vector->at(path_vector->size()-1));
    #endif

    for (uint64_t i=0; i<path_vector->size()-1; ++i) {
        src_pt  = path_vector->at(path_vector->size() - 2 - i);
        dest_pt = path_vector->at(path_vector->size() - 1 - i);

        if ((path_vector->size() - 2 - i) & 0x1) { // Odd i  => src_pt=Maximum dest_pt=Saddle
            src_real_pt  = src_pt;
            dest_real_pt = dest_pt + m_chunk_maxima.size();
        }
        else { // Even i => src_pt=Saddle dest_pt=Maximum
            src_real_pt  = src_pt + m_chunk_maxima.size();
            dest_real_pt = dest_pt;
        }

      #if PERSISTENCE_DEBUG
      printf("Reversing arc: %lu(%c) -> %lu(%c)\n", src_pt, (src_pt == src_real_pt ? 'm' : 's'), dest_pt, (dest_pt == dest_real_pt ? 'm' : 's') );
      #endif

      if (src_real_pt == src_pt)
        arc_redirection_list->set_redirection(dest_real_pt, src_real_pt);
      else
        arc_redirection_list->remove_redirection(src_real_pt);
    }

    arc_redirection_list->remove_redirection(m_chunk_maxima.size() + path_vector->at(0));

    #if PERSISTENCE_DEBUG
    printf("\n");
    #endif
   } 

  inline bool find_representative_saddle
    (
      const dataset_t * const m_ref_dataset_instance,
      const std::vector<point_index_t> * const m_chunk_saddles,
//      const critical_point_map_element_t * const vec1,
//      const critical_point_map_element_t * const vec2,
      const std::vector<uint64_t> * const vec1,
      const std::vector<uint64_t> * const vec2,
      point_index_t& representative_saddle,
      std::vector<point_index_t> * const tmp_vector
    )
   {
    tmp_vector->clear();

    for (uint64_t i=0; i<vec1->size(); ++i) {
//      if (vec2->contains(vec1->at(i)))
      if (std::find(vec2->begin(), vec2->end(), vec1->at(i)) != vec2->end())
        tmp_vector->push_back(vec1->at(i));
    }

    if (tmp_vector->size() == 0)
      return false;

    representative_saddle = tmp_vector->at(0);
    func_val_t tmp_fn_val = m_ref_dataset_instance->get_fn_val(m_chunk_saddles->at(representative_saddle));

    for (uint64_t i=1; i<tmp_vector->size(); ++i)
      if (m_ref_dataset_instance->get_fn_val(m_chunk_saddles->at(tmp_vector->at(i))) > tmp_fn_val) {
        representative_saddle = tmp_vector->at(i);
        tmp_fn_val = m_ref_dataset_instance->get_fn_val(m_chunk_saddles->at(tmp_vector->at(i)));
      }

    return true;
   }

  #define SIMPLIFICATION_POLL_STATUS 1

  void extremum_graph_t :: persistence_simplification_compute
    (
      persistence_map_t                              * const persistence_vals,
      persistence_map_t                              * const saturated_persistence_vals,
      arc_redirection_info_t                         * const arc_redirection_list,
      critical_point_map_t                           * const adjacency_map
    )
   {
    #if PERSISTENCE_DEBUG
    printf("\n");
    #endif

    uint64_t tmp_start, tmp_end;
    host_clock(tmp_start);

    saddle_persistence_queue_t saddle_queue;

    std::vector <std::vector<uint64_t>> adjacent_maxima_transitive_paths;
    std::vector <uint64_t> tail_maxima;
    std::vector <uint64_t> valid_adjacent_maxima;

    std::vector <uint64_t> tmp_saddle_vector;
    std::vector <uint64_t> tmp_maxima_collection;
    arc_redirection_list->init(m_chunk_maxima.size(), m_chunk_saddles.size());

    const uint64_t num_maxima  = m_chunk_maxima.size();
    const uint64_t num_saddles = m_chunk_saddles.size();

    init_persistence_structures(&saddle_queue, persistence_vals, saturated_persistence_vals, adjacency_map, &m_saddle_maxima_mapping, &m_maxima_saddle_mapping, &m_saddle_surviving, m_invert_fn_val, m_std_persistence_simplification_performed, num_maxima, num_saddles);
    assert(persistence_vals->size() == (m_chunk_maxima.size() + m_chunk_saddles.size()));
    assert(adjacency_map->size() == (m_chunk_maxima.size() + m_chunk_saddles.size()));
    assert(saturated_persistence_vals->size() == m_chunk_saddles.size());

//    printf("Saddle Queue size: %lu\n", saddle_queue.size());

    auto arg_max_highest_val_tail_maximum = [this]
        (uint64_t arg_i, const func_val_t saddle_func_val, const std::vector<uint64_t> * const tail_maxima) -> func_val_t {
          const uint64_t tmp_maximum_ordinal = tail_maxima->at(arg_i);
          return fabs(m_ref_dataset_instance->get_fn_val(get_maximum_index(tmp_maximum_ordinal)) - saddle_func_val);
        };

    uint64_t start, curr;
    uint64_t num_saddles_processed = 0;
    host_clock(start);

    while (!saddle_queue.empty()) {

        #if SIMPLIFICATION_POLL_STATUS
        if (has_input()) {
          host_clock(curr);
          long double num_saddles_processed_per_second = 1e6 * ((long double)num_saddles_processed) / (curr - start);

          printf("Queue_size: %lu. [%lu processed, %lu sec] %lu saddles/sec\n", saddle_queue.size(), num_saddles_processed, (curr-start)/1000000, ((unsigned long)num_saddles_processed_per_second));
        }
        #endif

        saddle_persistence_pair_t curr_queue_pair = saddle_queue.top();
        saddle_queue.pop();

        ++num_saddles_processed;

        #if PERSISTENCE_DEBUG
        printf("Saddle Queue size: %lu\n", saddle_queue.size());
        printf("Removed saddle=[%lu, %lu] with p=%f\n", curr_queue_pair.first, m_chunk_saddles[curr_queue_pair.first], curr_queue_pair.second);
        #endif

        const uint64_t saddle_ordinal = curr_queue_pair.first; // get_saddle_ordinal(curr_queue_pair.first);
        const func_val_t saddle_func_val = m_ref_dataset_instance->get_fn_val(m_chunk_saddles[saddle_ordinal]);

        // Fetch adjacent Maxima
        critical_point_map_element_t adjacent_maxima = adjacency_map->at(saddle_ordinal + m_chunk_maxima.size()); // m_saddle_maxima_mapping[saddle_ordinal];

        #if PERSISTENCE_DEBUG
        printf("Adjacent Maxima [%lu]: [ ", adjacent_maxima.size());
        for (uint64_t i=0; i<adjacent_maxima.size(); ++i)
            printf("%lu ,", adjacent_maxima[i]);
        printf(" ]\n");
        #endif

        fetch_transitive_paths(saddle_ordinal, arc_redirection_list, adjacency_map, &adjacent_maxima_transitive_paths, &tail_maxima);

        // Compute New Persistence for popped saddle

        #if 1
        func_val_t new_persistence = FUNC_VAL_MAX;
        for (uint64_t i=0; i<tail_maxima.size(); ++i)
            new_persistence = min(new_persistence, fabs(m_ref_dataset_instance->get_fn_val(get_maximum_index(tail_maxima[i])) - saddle_func_val) );

        #else
        const func_val_t new_persistence = arg_min <func_val_t> (arg_max_saddle_persistence, func_val_max_toggled(m_invert_fn_val), &tail_maxima, this, saddle_func_val);
        #endif

        saddle_persistence_pair_t top_pair = saddle_queue.top();

        #if PERSISTENCE_DEBUG
        printf("Persistence for saddle=%lu is %f\n", curr_queue_pair.first, new_persistence);
        printf("Tail Maxima [%lu] [ ", tail_maxima.size());
        for (uint64_t k=0; k<tail_maxima.size(); ++k)
          printf("%lu, ", tail_maxima[k]);
        printf("]\nPriority Queue top: (%lu, %f)\n", m_chunk_saddles[top_pair.first], top_pair.second);
        #endif

        assert(new_persistence != FUNC_VAL_MAX);


        // Update persistence for popped saddle if not minimum in priority queue
        if (!saddle_queue.empty() && new_persistence > top_pair.second) {
            curr_queue_pair.second = new_persistence;
            saddle_queue.push(curr_queue_pair);
        }

        else {

            // Update final persistence for saddle
            persistence_vals->at(m_chunk_maxima.size() + saddle_ordinal) = new_persistence;

            tmp_saddle_vector.clear();

            // Update saturated persistence for adjacent-maxima-path saddles
            for (uint64_t i=0; i<adjacent_maxima_transitive_paths.size(); ++i) {
                fetch_path_saddles(&adjacent_maxima_transitive_paths[i], &tmp_saddle_vector);

                #if PERSISTENCE_DEBUG
                printf("saddle_vector[%lu]: [ ", i);
                for (uint64_t j=0; j<tmp_saddle_vector.size(); ++j)
                  printf("%lu, ", tmp_saddle_vector[j]);
                printf("]\n");
                #endif
            }

            for (uint64_t j=0; j<tmp_saddle_vector.size(); ++j)
                saturated_persistence_vals->at(tmp_saddle_vector[j]) = new_persistence;

            // Check which non-strangulation maximum will survive
            uint64_t surviving_maximum_ordinal = 0;

            find_non_strangulation_adjacent_maxima(&tail_maxima, &valid_adjacent_maxima);

//              surviving_maximum_ordinal = arg_max (arg_max_highest_val_tail_maximum, func_val_min_toggled(m_invert_fn_val), &valid_adjacent_maxima, saddle_func_val, &tail_maxima);
              surviving_maximum_ordinal = arg_max (arg_max_highest_val_tail_maximum, FUNC_VAL_MIN, &valid_adjacent_maxima, saddle_func_val, &tail_maxima);

            if (valid_adjacent_maxima.empty()) {
              #if PERSISTENCE_DEBUG
                  printf("Cancellation for saddle=[%lu, %lu] not possible. Strangulation detected\n", saddle_ordinal, m_chunk_saddles[saddle_ordinal]);
              #endif
              continue;
            }

            #if PERSISTENCE_DEBUG
              printf("Cancellation for saddle=[%lu, %lu]\n", saddle_ordinal, m_chunk_saddles[saddle_ordinal]);
              printf("Surviving maximum=[%lu, %lu]\n", tail_maxima[surviving_maximum_ordinal], m_chunk_maxima[surviving_maximum_ordinal]);
            #endif

            // Cancel all adjacent maximum except surviving maximum
            for (uint64_t i=0; i<valid_adjacent_maxima.size(); ++i) {

              if (valid_adjacent_maxima[i] == surviving_maximum_ordinal) // && (valid_adjacent_maxima.size() != 1))
                continue;

              const uint64_t curr_maximum_ordinal = tail_maxima[valid_adjacent_maxima[i]];
              persistence_vals->at(curr_maximum_ordinal) = new_persistence;
              reverse_path(arc_redirection_list, &adjacent_maxima_transitive_paths[valid_adjacent_maxima[i]]);
            }

            arc_redirection_list->set_redirection(m_chunk_maxima.size() + saddle_ordinal, tail_maxima[surviving_maximum_ordinal]);
//            arc_redirection_list->set_redirection(tail_maxima[surviving_maximum_ordinal], m_chunk_maxima.size() + saddle_ordinal);
        }

        #if PERSISTENCE_DEBUG
        printf("\n");
        #endif

    }

    host_clock(tmp_end);

    print_time_diff(tmp_end  - tmp_start , "Persistence compute");
    //printf("Removed   Saddles  :   %lu\n", count_surviving_cp(&m_saddle_removed));
//    printf("\n");

   }

  void extremum_graph_t :: persistence_simplification_cancellation
    (
      const persistence_map_t                              * const persistence_vals,
      const persistence_map_t                              * const saturated_persistence_vals,
      const arc_redirection_info_t                         * const arc_redirection_list,
            critical_point_map_t                           * const adjacency_map,
      const critical_point_map_t                           * const original_adjacency_map,

      const func_val_t pers_low, const func_val_t pers_high, const bool normalized_thresholds
    )
   {
    uint64_t tmp_start, tmp_end;

    host_clock(tmp_start);

    adjacency_map->clear();
    adjacency_map->insert(adjacency_map->end(), original_adjacency_map->begin(), original_adjacency_map->end());

    if (normalized_thresholds) {
        assert(0 <= pers_low  && pers_low  <= 1);
        assert(0 <= pers_high && pers_high <= 1);
    }

    const func_val_t final_pers_low  = pers_low  * (normalized_thresholds ? m_dataset_func_range : 1.0f);
    const func_val_t final_pers_high = pers_high * (normalized_thresholds ? m_dataset_func_range : 1.0f);

    #if 1
    if (debug_simplification) {
      printf("p_lo      =%f, p_hi      =%f\n", pers_low, pers_high);
      printf("final_p_lo=%f, final_p_hi=%f\n", final_pers_low, final_pers_high);
    }
    #endif

//    // We want a directed arc graph
//    for (uint64_t i=0; i<m_chunk_maxima.size(); ++i)
//      m_maxima_saddle_mapping[i].clear();

//    m_maxima_saddle_mapping.clear(); // Drop Maxima-Saddle mapping temporarily to save memory. Can be created again from saddle_maxima_mapping  

    const uint64_t num_maxima  = m_chunk_maxima.size();
    const uint64_t num_saddles = m_chunk_saddles.size();

    auto persistence_range_check =
    [=] (const func_val_t start, const func_val_t end) -> bool { return range_intersect(final_pers_low, final_pers_high, start, end); };

    auto point_cancellation_lambda =
    [&] (const int work, const int num_threads) -> void {

        (void)num_threads;

        if (work == 0) {
            func_val_t min_maxima_persistence = FUNC_VAL_MAX; // func_val_max_toggled(m_invert_fn_val);
            func_val_t max_maxima_persistence = FUNC_VAL_MIN; // func_val_min_toggled(m_invert_fn_val);

            for (uint64_t maximum_ordinal=0; maximum_ordinal<num_maxima; ++maximum_ordinal) {
                min_maxima_persistence = min(min_maxima_persistence, persistence_vals->at(maximum_ordinal));
                max_maxima_persistence = max(max_maxima_persistence, persistence_vals->at(maximum_ordinal));

                m_maxima_surviving.at(maximum_ordinal) = 
                  m_maxima_saddle_mapping[maximum_ordinal].empty()
                    ? false
                    : persistence_range_check(0, persistence_vals->at(maximum_ordinal));
            }

            #if 0
            printf("Maxima Persistence: min=%f, max=%f\n", min_maxima_persistence, max_maxima_persistence);
            #endif
        }

        else if (work == 1) {
            for (uint64_t saddle_ordinal=0; saddle_ordinal<num_saddles; ++saddle_ordinal) {
              m_saddle_surviving.at(saddle_ordinal) =  persistence_range_check(persistence_vals->at(num_maxima + saddle_ordinal), persistence_vals->at(num_maxima + saddle_ordinal));
              m_saddle_zero_persistence.at(saddle_ordinal) = (m_saddle_surviving.at(saddle_ordinal) && (!persistence_vals->at(num_maxima + saddle_ordinal)));
            }
        }

        else if (work == 2) {

          std::set <std::pair<uint64_t, uint64_t>> saddle_maxima_pairs;

          for (uint64_t ordinal=0; ordinal<adjacency_map->size(); ++ordinal) {

            saddle_maxima_pairs.clear();

            if (ordinal < m_chunk_maxima.size()) {

              const uint64_t maximum_ordinal = ordinal;

              for (uint64_t k=0; k<adjacency_map->at(ordinal).size(); ++k) {
                const uint64_t saddle_ordinal = adjacency_map->at(ordinal).at(k);

                if (!saddle_exists(saddle_ordinal))
                  continue;

                saddle_maxima_pairs.insert(std::make_pair(saddle_ordinal, maximum_ordinal));
              }

            }
            else {

              const uint64_t saddle_ordinal = ordinal - m_chunk_maxima.size();

                if (!saddle_exists(saddle_ordinal))
                 continue;

              for (uint64_t k=0; k<adjacency_map->at(ordinal).size(); ++k)
                saddle_maxima_pairs.insert(std::make_pair(saddle_ordinal, adjacency_map->at(ordinal).at(k)));

            }

            for (auto p=saddle_maxima_pairs.begin(); p!=saddle_maxima_pairs.end(); ++p) {

              const uint64_t from_ordinal = (ordinal < num_maxima) ? p->second : (num_maxima + p->first);
              const uint64_t to_ordinal   = (ordinal < num_maxima) ? p->first  : p->second;

              if (!persistence_range_check(persistence_vals->at(num_maxima + p->first), saturated_persistence_vals->at(p->first))) {

		#if SADDLE_MAXIMA_MAPPING_OPTIMIZE
		adjacency_map->at(from_ordinal).erase_item(to_ordinal);
		#else
                    adjacency_map->at(from_ordinal).erase(std::find(adjacency_map->at(from_ordinal).begin(), adjacency_map->at(from_ordinal).end(), to_ordinal));
		#endif
              }

            }

          }

        }

    };

    begin_thread_pool(3, point_cancellation_lambda);

    // TODO: Repopulate m_maxima_saddle_mapping
    host_clock(tmp_end);

    #if 1
    uint64_t surviving_max_count = 0, surviving_sad_count = 0;

    for (uint64_t i=0; i<m_maxima_surviving.size(); ++i) {
      surviving_max_count += m_maxima_surviving[i];

    }

    std::map <uint64_t, std::set<uint64_t>>    tail_maxima_saddle_map;
    std::map <uint64_t, std::set<func_val_t>>  tail_maxima_saddle_persistence_map;
    std::map <uint64_t, std::set<func_val_t>>  tail_maxima_saddle_sat_persistence_map;

    std::vector <std::vector<uint64_t>> adjacent_maxima_transitive_paths;
    std::vector <uint64_t> tail_maxima;

    for (uint64_t i=0; i<m_saddle_surviving.size(); ++i) {
      surviving_sad_count += m_saddle_surviving[i];

      fetch_transitive_paths(i, arc_redirection_list, adjacency_map, &adjacent_maxima_transitive_paths, &tail_maxima);

      for (uint64_t k=0; k<tail_maxima.size(); ++k) {
        tail_maxima_saddle_map[tail_maxima[k]].insert(i);
        tail_maxima_saddle_persistence_map[tail_maxima[i]].insert(persistence_vals->at(m_chunk_maxima.size() + i));
        tail_maxima_saddle_sat_persistence_map[tail_maxima[i]].insert(saturated_persistence_vals->at(i));
      }
    }

    /*
    printf("Surviving Maxima : %lu\n", count_surviving_cp(&m_maxima_surviving));
    printf("Surviving Saddles: %lu\n", count_surviving_cp(&m_saddle_surviving));
    printf("Surviving Arcs   : %lu\n", count_surviving_arcs(adjacency_map, m_chunk_maxima.size(), &m_saddle_removed));
    */
    print_time_diff(tmp_end - tmp_start , "Simplification     ");
    #endif

   }

  void extremum_graph_t :: persistence_simplification (const func_val_t saturation_threshold, const func_val_t threshold, const bool normalized_thresholds)
   {
    persistence_map_t persistence_vals;                // Size == num_maxima + num_saddles
    persistence_map_t saturated_persistence_vals;      // Size == num_saddles
    critical_point_map_t adjacency_map, tmp_adjacency_map; // Size == num_maxima + num_saddles
    arc_redirection_info_t arc_redirection_list;

    persistence_simplification_compute(&persistence_vals, &saturated_persistence_vals, &arc_redirection_list, &adjacency_map);
    persistence_simplification_cancellation(&persistence_vals, &saturated_persistence_vals, &arc_redirection_list, &tmp_adjacency_map, &adjacency_map, saturation_threshold, threshold, normalized_thresholds);
//    tmp_adjacency_map = adjacency_map;
   }

  uint64_t get_persistence_calc_maximum_index (const uint64_t adjacent_maxima_count)
   {
    if (adjacent_maxima_count < 2)
      return 0;
    else
      return adjacent_maxima_count - 2;
   }

  template <typename Comparator>
  func_val_t get_saddle_persistence
   (
    const uint64_t                       saddle_ordinal,
          critical_point_map_t   * const m_saddle_maxima_mapping,
    const std::vector <uint64_t> * const m_chunk_maxima,
    const std::vector <uint64_t> * const m_chunk_saddles,
    const dataset_t              * const m_ref_dataset_instance,
    const Comparator                     comparator_func
   )
   {
    // Pair saddle with second highest function valued adjacent maximum (if |adjacent maxima| > 1)
    // or pair with the only adjacent maxima (|adjacent maxima| == 1)
    const uint64_t selected_maximum_for_persistence_calc =
        get_persistence_calc_maximum_index(m_saddle_maxima_mapping->at(saddle_ordinal).size());

    std::sort(m_saddle_maxima_mapping->at(saddle_ordinal).begin(),
              m_saddle_maxima_mapping->at(saddle_ordinal).end(),
              comparator_func);

    return
      m_ref_dataset_instance->get_fn_val(
        m_chunk_maxima->at(
          m_saddle_maxima_mapping->at(saddle_ordinal)[selected_maximum_for_persistence_calc]
        )
      )

      - m_ref_dataset_instance->get_fn_val(m_chunk_saddles->at(saddle_ordinal));
   }

  template <typename T>
  uint64_t vector_intersection_size
   (
    const std::vector <T> * const vec1,
    const std::vector <T> * const vec2      
   )
   {
    uint64_t intersection_size = 0;

    for (uint64_t i=0; i<vec1->size(); ++i)
      if (std::find(vec2->begin(), vec2->end(), vec1->at(i)) != vec2->end())
        ++intersection_size;

    return intersection_size;
   }

  template <typename T>
  uint64_t vector_intersection_size_vec2sorted
   (
    const std::vector <T> * const vec1,
    const std::vector <T> * const vec2 // Sorted vector `vec2`
   )
   {
    uint64_t intersection_size = 0;

    for (uint64_t i=0; i<vec1->size(); ++i)
      if (std::binary_search(vec2->begin(), vec2->end(), vec1->at(i)))
        ++intersection_size;

    return intersection_size;
   }

 void update_saddle_adjancent_extrema_cancelled_path
  (
           extremum_graph_t * const ext_graph,
     const uint64_t                 saddle_ordinal,
     const uint64_t                 old_extremum,
     const uint64_t                 new_extremum
  )
  {
    /**
     * update_saddle_adjancent_extrema_cancelled_path
     *      (ext_graph, iter_adjacent_saddle_ordinal, selected_maximum_ordinal, surviving_maximum_ordinal)
     */

    uint64_t idx = 0xffffffffffffffff;

    for (uint64_t i=0; i<ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal].size(); ++i)
      if (ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][i].surviving_connected_extremum == old_extremum) {
        idx = i;
        break;
      }

    if (idx != 0xffffffffffffffff) {
      ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][idx].surviving_connected_extremum = new_extremum;

      bool flag = true;
      int count = 0;

      for (uint64_t i=0; i<ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal].size(); ++i)
        if (i != idx && ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][i].surviving_connected_extremum == new_extremum) {
          ++count;

          if (ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][i].surviving_connected_extremum ==
              ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][i].adjacent_extremum) {

            flag = false; // This is a cancellation result, not a strangulation
            break;
          }

        }

      if (flag && count > 0) {

        for (uint64_t i=0; i<ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal].size(); ++i)
          if (ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][i].surviving_connected_extremum == new_extremum)
            ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][i].is_strangulation = true;
      }

    }

    if (saddle_ordinal==5) {
      printf("Update: saddle_ordinal=%lu, old_extremum=%lu, new_extremum=%lu\n", saddle_ordinal, old_extremum, new_extremum);

      printf("Pairs: < ");
      for (auto pair: ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal]) printf("(%lu <- %lu), ", pair.adjacent_extremum, pair.surviving_connected_extremum);
      printf("\b\b>\n");
    }

    assert(idx != 0xffffffffffffffff);
  }

 void erase_saddle_adjancent_extrema_with
  (
           extremum_graph_t * const ext_graph,
     const uint64_t                 saddle_ordinal,
     const uint64_t                 surviving_extremum
  )
  {
    for (uint32_t i=0; i < ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal].size(); ++i) {
      ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][i].surviving_connected_extremum = surviving_extremum;
    }
  }

 uint64_t get_saddle_adjacent_extremum_for_path_v1
  (
     const extremum_graph_t * const ext_graph,
     const uint64_t                 saddle_ordinal,
     const uint64_t                 target_extremum
  )
  {
    //TODO: Accept vector <uint64_t> to add intermediate saddles and extrema
    if (ext_graph->saddle_exists(saddle_ordinal)) {

      for (auto new_old_pair : ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal]) {
        if (new_old_pair.adjacent_extremum == target_extremum)
          return new_old_pair.surviving_connected_extremum;
      }

      assert(false);
      return 0xffffffffffffffff;
    }
    else {
      // If saddle is cancelled, recursively move to the extremum it was cancelled with and follow the next saddle this newly found extremum was cancelled with

//      const uint64_t surviving_extremum = ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][0].first;
      uint64_t surviving_extremum = 0xffffffffffffffff;

      for (auto new_old_pair : ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal]) {
        if (new_old_pair.adjacent_extremum == new_old_pair.surviving_connected_extremum) {
          surviving_extremum = new_old_pair.surviving_connected_extremum;
          break;
        }
      }

      if (surviving_extremum == 0xffffffffffffffff)
        surviving_extremum = ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][0].adjacent_extremum;

      return (surviving_extremum == target_extremum)
        ? target_extremum
        : get_saddle_adjacent_extremum_for_path_v1  (ext_graph, ext_graph->m_cancelled_cp_path_cp[surviving_extremum], target_extremum);

      }

  }

 void create_multiplicity_map
  (
     const extremum_graph_t              * const ext_graph,
     const uint64_t                              saddle_ordinal,
           std::map <uint64_t, uint16_t> * const multiplicity_map
  )
  {
    multiplicity_map->clear();

    for (auto conn : ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal]) {
      const uint16_t count = (multiplicity_map->find(conn.surviving_connected_extremum) == multiplicity_map->end())
          ? 0
          : multiplicity_map->at(conn.surviving_connected_extremum);

      multiplicity_map->operator[](conn.surviving_connected_extremum) =  (1 + count);
    }
  }

 uint16_t strangulation_count
  (
     const extremum_graph_t              * const ext_graph,
     const uint64_t                              saddle_ordinal,
           std::map <uint64_t, uint16_t> * const multiplicity_map,
           uint64_t&                             surviving_extremum
  )
  {
    if (multiplicity_map->size() == ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal].size())
      return 0;

    else {
      uint16_t count = 0;

      for (auto pair=multiplicity_map->begin(); pair!=multiplicity_map->end(); ++pair)
        if (pair->second > 1) {

          bool flag = true;

          for (auto conn : ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal])
            if (pair->first == conn.surviving_connected_extremum) {
              flag = false;
              surviving_extremum = pair->first;
              break;
            }

          count += flag;
        }

      return count;
    }

  }

 template <bool collect>
 uint64_t get_saddle_adjacent_extremum_path_trace
  (
     const extremum_graph_t              * const ext_graph,
           uint64_t                              saddle_ordinal,
           uint64_t                              target_extremum,
           std::vector <uint64_t>        * const collect_vec,
           std::map <uint64_t, uint16_t> * const multiplicity_map
  )
  {
    uint64_t surviving_extremum = 0xffffffffffffffff;
    const uint64_t start_saddle_ordinal = saddle_ordinal;
    uint64_t iter_count = 0;
    std::vector <int> path_taken;
 
    while (true) {

      ++ iter_count;

      if (collect)
        collect_vec->push_back(saddle_ordinal);

      if (ext_graph->saddle_exists(saddle_ordinal)) {

        for (auto conn : ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal]) {
          if (conn.adjacent_extremum == target_extremum) {

            if (collect)
              collect_vec->push_back(target_extremum);

            return target_extremum;
          }
          else if (conn.surviving_connected_extremum == target_extremum) {

            if (collect)
              collect_vec->push_back(conn.adjacent_extremum);

            saddle_ordinal = ext_graph->m_cancelled_cp_path_cp[conn.adjacent_extremum];
            path_taken.push_back(0);
          }

        }

      }

      else {
        // If saddle is cancelled, recursively move to the extremum it was cancelled with and follow the next saddle this newly found extremum was cancelled with

        surviving_extremum = 0xffffffffffffffff;
        create_multiplicity_map(ext_graph, saddle_ordinal, multiplicity_map);
        const uint16_t scount = 1 + strangulation_count(ext_graph, saddle_ordinal, multiplicity_map, surviving_extremum);

        if (surviving_extremum != 0xffffffffffffffff) {
          /**
           *  In this case, we have found a cancelled path from (saddle_ordinal) to (target_extremum) via (surviving_extremum) 
           *  Rough sketch of path:  (saddle_ordinal) -> (surviving_extremum) -> ... -> (target_extremum)
           */

          assert(multiplicity_map->size() == scount);

          if (collect)
            collect_vec->push_back(surviving_extremum);

          if (surviving_extremum == target_extremum)
            return surviving_extremum;

          else {
            saddle_ordinal = ext_graph->m_cancelled_cp_path_cp[surviving_extremum];
            path_taken.push_back(1);
          }
        }

        else {

          bool flag = false;

          for (auto conn : ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal])
            if (
                // Follow the first path available
                ((conn.adjacent_extremum == conn.surviving_connected_extremum) && ext_graph->m_cancelled_cp_path_cp[conn.adjacent_extremum] != 0xffffffffffffffff) ||

                // If strangulation of (saddle_ordinal) with (target_extremum), pick first path which leads us to the (target_extremum)
                (conn.is_strangulation && conn.surviving_connected_extremum == target_extremum)) {

              if (collect)
                collect_vec->push_back(conn.adjacent_extremum);

              saddle_ordinal = ext_graph->m_cancelled_cp_path_cp[conn.adjacent_extremum];
              path_taken.push_back((conn.adjacent_extremum == conn.surviving_connected_extremum) ? 2 : 3);
              flag = true;
              break;
            }

          assert(flag);
        }

      }

    }

  }

 template <bool collect>
 uint64_t get_saddle_adjacent_extremum_path_trace_v2
  (
     const extremum_graph_t              * const ext_graph,
           uint64_t                              saddle_ordinal,
     const uint64_t                              target_extremum,
           std::vector <uint64_t>        * const collect_vec,
           std::map <uint64_t, uint16_t> * const multiplicity_map
  )
  {
    uint64_t surviving_extremum = 0xffffffffffffffff;
    uint64_t iter_count = 0;
    uint64_t last_extremum = 0xffffffffffffffff;
    std::vector <int> path_taken;

    while (true) {

      ++ iter_count;

      if (collect)
        collect_vec->push_back(saddle_ordinal);

      bool last_extremum_present = false;
      bool flag1 = false;
      std::vector <uint64_t> surviving_extremum_list;

      for (auto conn : ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal]) {

        if (last_extremum == conn.adjacent_extremum)
          last_extremum_present = true;

        if (conn.adjacent_extremum == conn.surviving_connected_extremum)
          surviving_extremum_list.push_back(conn.adjacent_extremum);

        if (conn.adjacent_extremum == target_extremum) {
          if (collect)
            collect_vec->push_back(target_extremum);
          return target_extremum;
        }

        else if ((conn.surviving_connected_extremum == target_extremum) && (saddle_ordinal != ext_graph->m_cancelled_cp_path_cp[conn.adjacent_extremum])) {

          path_taken.push_back(0);
          last_extremum = conn.adjacent_extremum;

          assert(conn.adjacent_extremum != conn.surviving_connected_extremum);
          flag1 = true;

          if (collect)
            collect_vec->push_back(conn.adjacent_extremum);

          // assuming this works regardless of it being a strangulation
          saddle_ordinal = ext_graph->m_cancelled_cp_path_cp[conn.adjacent_extremum];
          break;
        }

      }

      if (flag1) continue;

      if (last_extremum_present && ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal].size() == 2) {
        int idx = -1;
        if (ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][0].adjacent_extremum == last_extremum)
          idx = 1;
        else
          idx = 0;

        const uint64_t candidate = ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][idx].adjacent_extremum;

        if ((ext_graph->m_cancelled_cp_path_cp[candidate] != 0xffffffffffffffff) && (ext_graph->m_cancelled_cp_path_cp[candidate] != saddle_ordinal)) {
          last_extremum = candidate;
          saddle_ordinal = ext_graph->m_cancelled_cp_path_cp[candidate];
          flag1 = true;
        }

      }

      if (flag1) continue;
      flag1 = false;

      if (surviving_extremum_list.size() == 1) {
        const uint64_t surviving_extremum_singleton = surviving_extremum_list[0];

        if (surviving_extremum_singleton == target_extremum) {
          if (collect)
            collect_vec->push_back(surviving_extremum_singleton);
          return target_extremum;
        }

        else if ((0xffffffffffffffff != ext_graph->m_cancelled_cp_path_cp[surviving_extremum_singleton]) && (saddle_ordinal != ext_graph->m_cancelled_cp_path_cp[surviving_extremum_singleton])) {
          path_taken.push_back(1);
          if (collect)
            collect_vec->push_back(surviving_extremum_singleton);
          last_extremum = surviving_extremum_singleton;
          saddle_ordinal = ext_graph->m_cancelled_cp_path_cp[surviving_extremum_singleton];
          flag1 = true;
        }

      }

      if (flag1) continue;
      flag1 = false;

      for (uint32_t i=0; i<ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal].size(); ++i) {

        for (uint32_t j=0; j<ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal].size(); ++j) {
          if (i==j)
            continue;
          else if (
              (ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][i].surviving_connected_extremum ==
               ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][j].adjacent_extremum
              ) &&
              (0xffffffffffffffff != ext_graph->m_cancelled_cp_path_cp[ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][j].adjacent_extremum])
              &&
              (saddle_ordinal != ext_graph->m_cancelled_cp_path_cp[ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][j].adjacent_extremum])
             ) {
            path_taken.push_back(2);
            saddle_ordinal = ext_graph->m_cancelled_cp_path_cp[ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal][j].adjacent_extremum];
            flag1 = true;
            break;
          }
        }

        if (flag1) break;
      }

      if (flag1) continue;
      flag1 = false;
      assert(false);

      if (!flag1) {
        // If saddle is cancelled, recursively move to the extremum it was cancelled with and follow the next saddle this newly found extremum was cancelled with

        surviving_extremum = 0xffffffffffffffff;
        create_multiplicity_map(ext_graph, saddle_ordinal, multiplicity_map);
        bool flag2 = false;

        if (!flag2) {

          const uint16_t scount = 1 + strangulation_count(ext_graph, saddle_ordinal, multiplicity_map, surviving_extremum);

          if (surviving_extremum != 0xffffffffffffffff) {
          /**
           *  In this case, we have found a cancelled path from (saddle_ordinal) to (target_extremum) via (surviving_extremum) 
           *  Rough sketch of path:  (saddle_ordinal) -> (surviving_extremum) -> ... -> (target_extremum)
           */

            assert(multiplicity_map->size() == scount);

            if (collect)
              collect_vec->push_back(surviving_extremum);

            if (surviving_extremum == target_extremum)
              return surviving_extremum;

            else {
              saddle_ordinal = ext_graph->m_cancelled_cp_path_cp[surviving_extremum];
              path_taken.push_back(3);
            }
          }

          else {

            bool flag = false;

            for (auto conn : ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal])
              if (
                  // Follow the first path available
                  ((conn.adjacent_extremum == conn.surviving_connected_extremum) && ext_graph->m_cancelled_cp_path_cp[conn.adjacent_extremum] != 0xffffffffffffffff) ||

                  // If strangulation of (saddle_ordinal) with (target_extremum), pick first path which leads us to the (target_extremum)
                  (conn.is_strangulation && conn.surviving_connected_extremum == target_extremum)) {

                if (collect)
                  collect_vec->push_back(conn.adjacent_extremum);

                path_taken.push_back(4);
                saddle_ordinal = ext_graph->m_cancelled_cp_path_cp[conn.adjacent_extremum];
                path_taken.push_back((conn.adjacent_extremum == conn.surviving_connected_extremum) ? 2 : 3);
                flag = true;
                break;
              }

            assert(flag);
          }

        }

      }

    }

  }

 uint64_t get_saddle_adjacent_extremum_for_path
  (
     const extremum_graph_t       * const ext_graph,
     const uint64_t                       saddle_ordinal,
     const uint64_t                       target_extremum,
           std::vector <uint64_t> * const collect_vec = nullptr
  )
  {
   std::map <uint64_t, uint16_t> multiplicity_map;

   if (collect_vec == nullptr)
     return get_saddle_adjacent_extremum_path_trace_v2 <false> (ext_graph, saddle_ordinal, target_extremum, nullptr, &multiplicity_map);
   else
     return get_saddle_adjacent_extremum_path_trace_v2 <true> (ext_graph, saddle_ordinal, target_extremum, collect_vec, &multiplicity_map);
  }

 void perform_std_maximum_removal
   (
           extremum_graph_t                   * const ext_graph,
     const uint64_t                                   selected_maximum_ordinal,
     const uint64_t                                   adjacent_saddle_ordinal,
     const uint64_t                                   surviving_maximum_ordinal,
           critical_point_map_t               * const m_saddle_maxima_mapping,
           std::vector<std::vector<uint64_t>> * const m_maxima_saddle_mapping,
           std::vector <point_index_t>        * const m_cancelled_cp_path_cp
    )
   {
    (void)m_cancelled_cp_path_cp;

    assert(selected_maximum_ordinal < m_maxima_saddle_mapping->size());
    assert(adjacent_saddle_ordinal < m_saddle_maxima_mapping->size());
    assert(surviving_maximum_ordinal < m_maxima_saddle_mapping->size());

    critical_point_map_element_t adjacent_maxima_list = m_saddle_maxima_mapping->at(adjacent_saddle_ordinal);
    const std::vector <uint64_t> maximum_adjacent_saddles_list = m_maxima_saddle_mapping->at(selected_maximum_ordinal);

    #define MAXIMUM_REMOVAL_DEBUG 0

    #if MAXIMUM_REMOVAL_DEBUG
    static const std::set <uint64_t> debug_maxima = { 2903 };

    if (debug_maxima.find(selected_maximum_ordinal) != debug_maxima.end()) {
      printf("BEFORE[%lu]: [ ", selected_maximum_ordinal);
      for (uint64_t s: m_maxima_saddle_mapping->at(selected_maximum_ordinal))
        printf("%lu, ", s);
      printf("]\n");
    }
    #endif

    for (uint64_t i=0; i<maximum_adjacent_saddles_list.size(); ++i)
     {

      const uint64_t iter_adjacent_saddle_ordinal = maximum_adjacent_saddles_list[i];

      const int int_size = vector_intersection_size(
            &m_saddle_maxima_mapping->at(iter_adjacent_saddle_ordinal),
            &adjacent_maxima_list);

      // Remove after all siblings
      if (iter_adjacent_saddle_ordinal == adjacent_saddle_ordinal)
        continue;

      // Must be removed for another iter saddle in the below else if conditions or by edge bundling
      else if (!ext_graph->saddle_exists(iter_adjacent_saddle_ordinal)) {
        ext_graph->erase_saddle(iter_adjacent_saddle_ordinal);
//        erase_saddle_adjancent_extrema_with(ext_graph, iter_adjacent_saddle_ordinal, surviving_maximum_ordinal);
        continue;
      }

      // Intersection cannot be empty. iter_adjacent_saddle_ordinal is always adjacent to selected_maximum_ordinal
      if (int_size == 1)
      {
        // `iter_adjacent_saddle_ordinal` is not adjacent to any other maxima of interest (maxima adjacent to `adjacent_saddle_ordinal`) except `selected_maximum_ordinal`
        // Remove (iter_adjacent_saddle_ordinal, selected_maximum_ordinal) connection and
        // Insert (iter_adjacent_saddle_ordinal, surviving_maximum_ordinal) connection
        vector_remove_item(&m_saddle_maxima_mapping->at(iter_adjacent_saddle_ordinal), selected_maximum_ordinal);
        vector_remove_item(&m_maxima_saddle_mapping->at(selected_maximum_ordinal), iter_adjacent_saddle_ordinal);

        m_saddle_maxima_mapping->at(iter_adjacent_saddle_ordinal).push_back(surviving_maximum_ordinal);
        m_maxima_saddle_mapping->at(surviving_maximum_ordinal).push_back(iter_adjacent_saddle_ordinal);

        update_saddle_adjancent_extrema_cancelled_path(ext_graph, iter_adjacent_saddle_ordinal, selected_maximum_ordinal, surviving_maximum_ordinal);

//        sort_vector(&m_saddle_maxima_mapping->at(iter_adjacent_saddle_ordinal));
      }

      else {

        // `iter_adjacent_saddle_ordinal` is adjacent to both `selected_maximum_ordinal` and another maximum adjacent to `adjacent_saddle_ordinal`
        // Can happen only when `adjacent_saddle_ordinal` is a multi-saddle
        /*
          m1 -- s2 -- m3
          |   / 
          s1 /      Here, s2 = adjacent_saddle_ordinal, m2 = selected_maximum_ordinal, m3 = surviving_maximum_ordinal, s1 = iter_adjacent_saddle_ordinal
          | /
          m2
        */
        ext_graph->erase_saddle(iter_adjacent_saddle_ordinal);
      }

    }

    vector_remove_item(&m_saddle_maxima_mapping->at(adjacent_saddle_ordinal), selected_maximum_ordinal);
    vector_remove_item(&m_maxima_saddle_mapping->at(selected_maximum_ordinal), adjacent_saddle_ordinal);
    bool flag1 = false;

    std::vector <uint64_t> cancelled_path_vector;
    std::vector <uint64_t> path_vec2;
    get_saddle_adjacent_extremum_for_path(ext_graph, adjacent_saddle_ordinal, selected_maximum_ordinal, &cancelled_path_vector);

    for (uint64_t i=0; i<cancelled_path_vector.size(); ++i) {
      if (i%2) {
        // Extremum
        ext_graph->m_cancelled_cp_path_cp[cancelled_path_vector[i]] = cancelled_path_vector[i-1];
      }
      else {
        // Saddle
        if (i > 0) {
//          erase_saddle_adjancent_extrema_with(ext_graph, adjacent_saddle_ordinal, cancelled_path_vector[i-1]);
          erase_saddle_adjancent_extrema_with(ext_graph, cancelled_path_vector[i], cancelled_path_vector[i-1]);
        }
        else {
          get_saddle_adjacent_extremum_for_path(ext_graph, adjacent_saddle_ordinal, surviving_maximum_ordinal, &path_vec2);
          const uint64_t first_extremum_on_path = path_vec2[1];
          update_saddle_adjancent_extrema_cancelled_path(ext_graph, adjacent_saddle_ordinal, selected_maximum_ordinal, first_extremum_on_path);        
        }
      }
    }

//    m_cancelled_cp_path_cp->at(selected_maximum_ordinal) = adjacent_saddle_ordinal;
    flag1 |= false;

    #if MAXIMUM_REMOVAL_DEBUG
    if (debug_maxima.find(selected_maximum_ordinal) != debug_maxima.end()) {
      printf("AFTER[%lu]: [ ", selected_maximum_ordinal);
      for (uint64_t s: m_maxima_saddle_mapping->at(selected_maximum_ordinal))
        printf("%lu, ", s);
      printf("]\n");
    }
    #endif
   }

  void perform_std_maxima_removal
   (
           extremum_graph_t                    * const ext_graph,
     const uint64_t                                    selected_saddle_ordinal,
     const uint64_t                                    num_maxima_to_remove,
           critical_point_map_t                * const m_saddle_maxima_mapping,
           std::vector <std::vector<uint64_t>> * const m_maxima_saddle_mapping,
           std::vector <point_index_t>         * const m_cancelled_cp_path_cp
   )
   {
    assert(selected_saddle_ordinal < m_saddle_maxima_mapping->size());
    assert(num_maxima_to_remove < m_saddle_maxima_mapping->at(selected_saddle_ordinal).size());

    const critical_point_map_element_t adjacent_maxima_list = m_saddle_maxima_mapping->at(selected_saddle_ordinal);
    const uint64_t surviving_maximum_ordinal = adjacent_maxima_list[adjacent_maxima_list.size()-1];

    for (uint64_t i=0; i<num_maxima_to_remove; ++i)
      if (adjacent_maxima_list[i] != surviving_maximum_ordinal)
        perform_std_maximum_removal(
          ext_graph,
          adjacent_maxima_list[i],
          selected_saddle_ordinal,
          surviving_maximum_ordinal,
          m_saddle_maxima_mapping,
          m_maxima_saddle_mapping,
          m_cancelled_cp_path_cp
          );

    ext_graph->erase_saddle(selected_saddle_ordinal);
   }

  static uint64_t check_valid_cancellation_inner_counter = 0;
  std::vector <std::vector<uint64_t>> check_valid_cancellation_vector_of_paths;

  bool check_valid_cancellation
   (
          extremum_graph_t * const ext_graph,
    const uint64_t                 saddle_ordinal,
    const uint64_t                 extrema_ordinal
   )
   {
    uint64_t curr_pt = get_saddle_adjacent_extremum_for_path(ext_graph, saddle_ordinal, extrema_ordinal);

    if (curr_pt == extrema_ordinal)
      return true;

    ++check_valid_cancellation_inner_counter;

    std::vector <uint64_t> path_vector;
    path_vector.push_back(curr_pt);
    static int yolo = 0;

    for (uint64_t i=0; true; ++i) {

      const bool is_curr_pt_maximum = ((i&0x1) == 0);

      const uint64_t next_pt_in_path =
        is_curr_pt_maximum
        ? ext_graph->m_cancelled_cp_path_cp[curr_pt]
        : get_saddle_adjacent_extremum_for_path(ext_graph, curr_pt, extrema_ordinal);

      if (next_pt_in_path == 0xffffffffffffffff) {
        if (curr_pt != extrema_ordinal) {
          ++yolo;
          if (yolo==1 || saddle_ordinal==24) {
            printf("\n");

            std::vector <uint64_t> saddle_connected_to_extrema_ordinal;
            for (uint64_t k=0; k<ext_graph->m_chunk_saddles.size(); ++k)
              if (ext_graph->m_cancelled_cp_path_cp[ext_graph->m_chunk_maxima.size() + k] == extrema_ordinal)
                saddle_connected_to_extrema_ordinal.push_back(k);

            printf("check_valid_cancellation[FAILED]: saddle=%lu, curr_pt=%lu[num_maxima=%lu], extrema_ordinal=%lu, i=%lu\n", saddle_ordinal, curr_pt, ext_graph->m_chunk_maxima.size(), extrema_ordinal, i);
            printf("num_adjacent_maxima: %lu: < ", check_valid_cancellation_vector_of_paths.size());
            for (auto path: check_valid_cancellation_vector_of_paths) printf("%lu, ", path[path.size()-1]);
            printf("\b\b >\n");

            printf("path_vector: < ");
            for (auto v: path_vector) printf("%lu, ", v);
            printf("\b\b >\n");

            printf("saddles cancelled with extrema_ordinal=%lu: < ", extrema_ordinal);
            for (auto v: saddle_connected_to_extrema_ordinal) printf("%lu, ", v);
            printf("\b\b >\n");

            printf("m_cancelled_cp_path_cp[sad:24  ] = %lu\n", ext_graph->m_cancelled_cp_path_cp[ ext_graph->m_chunk_maxima.size() + 24]);
            printf("m_cancelled_cp_path_cp[sad:2327] = %lu\n", ext_graph->m_cancelled_cp_path_cp[ ext_graph->m_chunk_maxima.size() + 2327]);

            printf("\n");
          }

        }

        if (saddle_ordinal==24) {
          printf("GGGGGGGGGGG!!!!: saddle_ordinal=%lu, check=%d\n", saddle_ordinal, (curr_pt == extrema_ordinal));
            printf("path_vector: < ");
            for (auto v: path_vector) printf("%lu, ", v);
            printf("\b\b >\n");
          printf("\n");
        }

        return curr_pt == extrema_ordinal;
      }
      else {
        curr_pt = next_pt_in_path;

//        curr_pt = next_pt_in_path + ((i&0x1) ? ext_graph->m_chunk_maxima.size() : 0);
      }

//      path_vector.push_back(curr_pt);
    }

    assert(false);
   }

  void extremum_graph_t :: std_persistence_based_cancellation_v1
   (
    const func_val_t  given_pers_low,
    const bool        normalized_threshold
   )
   {
    uint64_t tmp_start, tmp_end, tmp_end2;
    uint64_t start = 0, curr;
    uint64_t num_saddles_processed = 0;

    host_clock(tmp_start);
    const func_val_t pers_low  = given_pers_low  * (normalized_threshold ? m_dataset_func_range : 1.0f);

//    printf("pers_low: %f\n", pers_low);

    #if 1
    m_saddle_maxima_mapping_copy = m_saddle_maxima_mapping;
    m_maxima_saddle_mapping_copy = m_maxima_saddle_mapping;
    #endif

    saddle_persistence_queue_t saddle_queue;

    std::vector <uint64_t> tail_maxima;
    std::vector <uint64_t> tmp_saddle_vector;

    if (m_cancelled_cp_path_cp.empty()) {
      const uint64_t total_cp = m_chunk_maxima.size() + m_chunk_saddles.size();
      m_cancelled_cp_path_cp.reserve(total_cp);
      printf("Filling m_cancelled_cp_path_cp with %lu entries\n", total_cp);

      for (uint64_t i=0; i<total_cp; ++i)
        m_cancelled_cp_path_cp.push_back(0xffffffffffffffff);

      m_cancelled_cp_path_cp.shrink_to_fit();
    }

    if (m_saddle_adjacent_cancelled_extrema_paths.empty()) {
      m_saddle_adjacent_cancelled_extrema_paths.reserve(m_chunk_saddles.size());

      for (uint64_t i=0; i<m_chunk_saddles.size(); ++i) {

        std::vector <saddle_extremum_connection_info_t> v;

        v.reserve(m_saddle_maxima_mapping[i].size());

        for (uint64_t m: m_saddle_maxima_mapping[i])
          v.push_back(saddle_extremum_connection_info_t(m, m));

        m_saddle_adjacent_cancelled_extrema_paths.push_back(v);

      }

    }

    auto sort_maxima_by_func_val_comparator = [&] (const uint64_t maximum1_ordinal, const uint64_t maximum2_ordinal) -> bool
     {
      const func_val_t maximum1_func_val = m_ref_dataset_instance->get_fn_val(m_chunk_maxima[maximum1_ordinal]);
      const func_val_t maximum2_func_val = m_ref_dataset_instance->get_fn_val(m_chunk_maxima[maximum2_ordinal]);

      if (maximum1_func_val != maximum2_func_val)
        return maximum1_func_val < maximum2_func_val;
      else
        return maximum1_ordinal < maximum2_ordinal;
     };

    for (uint64_t saddle_ordinal=0; saddle_ordinal<m_chunk_saddles.size(); ++saddle_ordinal) {

      if (!saddle_exists(saddle_ordinal))
          continue;

      #if 0
      if (m_saddle_maxima_mapping[saddle_ordinal].size() == 1) {
        erase_saddle(saddle_ordinal);
        continue;
      }
      #endif

      const func_val_t saddle_persistence =
        get_saddle_persistence(saddle_ordinal, &m_saddle_maxima_mapping, &m_chunk_maxima, &m_chunk_saddles, m_ref_dataset_instance, sort_maxima_by_func_val_comparator);

      saddle_queue.push(std::make_pair(saddle_ordinal, saddle_persistence));
    }

    printf("std::persistence Saddle Queue size: %lu\n", saddle_queue.size());

    host_clock(tmp_end2);

    while (!saddle_queue.empty()) {

        #if SIMPLIFICATION_POLL_STATUS
        if (has_input()) {
          host_clock(curr);
          long double num_saddles_processed_per_second = 1e6 * ((long double)num_saddles_processed) / (curr - start);

          printf("std::persistence Queue_size: %lu. [%lu processed, %lu sec] %lu saddles/sec\n", saddle_queue.size(), num_saddles_processed, (curr-start)/1000000, ((unsigned long)num_saddles_processed_per_second));
        }
        #endif

        saddle_persistence_pair_t curr_queue_pair = saddle_queue.top();
        saddle_queue.pop();

        const uint64_t saddle_ordinal     = curr_queue_pair.first;
        debug_simplification = (saddle_ordinal == 386); // m_chunk_saddles[saddle_ordinal] == 1111489; // Maxima 26

        ++num_saddles_processed;

        if (!saddle_exists(saddle_ordinal))
          continue;

        else if (m_saddle_maxima_mapping[saddle_ordinal].empty()) {
          m_saddle_surviving[saddle_ordinal] = false;
          continue;
        }

        #if PERSISTENCE_DEBUG
        printf("Saddle Queue size: %lu\n", saddle_queue.size());
        printf("Removed saddle=[%lu, %lu] with p=%f\n", curr_queue_pair.first, m_chunk_saddles[curr_queue_pair.first], curr_queue_pair.second);
        #endif

        #if PERSISTENCE_DEBUG
        printf("Adjacent Maxima [%lu]: [ ", adjacent_maxima.size());
        for (uint64_t i=0; i<adjacent_maxima.size(); ++i)
            printf("%lu ,", adjacent_maxima[i]);
        printf(" ]\n");
        #endif

        // Compute persistence for popped saddle
        const func_val_t new_persistence =
          get_saddle_persistence(saddle_ordinal, &m_saddle_maxima_mapping, &m_chunk_maxima, &m_chunk_saddles, m_ref_dataset_instance, sort_maxima_by_func_val_comparator);

        const saddle_persistence_pair_t top_pair = saddle_queue.top();

        // Update persistence for popped saddle if not minimum in priority queue
        if (!saddle_queue.empty() && new_persistence > top_pair.second) {
            curr_queue_pair.second = new_persistence;
            saddle_queue.push(curr_queue_pair);
        }

        // Cancel saddle
        else {

            // Do not cancel saddle, continue
            if (
                   new_persistence >= pers_low
                || m_saddle_maxima_mapping[saddle_ordinal].size() == 1
               )
              continue;

            const critical_point_map_element_t adjacent_maxima = m_saddle_maxima_mapping[saddle_ordinal];

            const uint64_t persistence_calc_adjacent_maximum_idx = get_persistence_calc_maximum_index(adjacent_maxima.size());

            const func_val_t pers_calc_adj_maximum_fn_val = 
                m_ref_dataset_instance->get_fn_val(m_chunk_maxima[adjacent_maxima[persistence_calc_adjacent_maximum_idx]]);

            const func_val_t saddle_func_val = m_ref_dataset_instance->get_fn_val(m_chunk_saddles[saddle_ordinal]);

            const func_val_t pers_calc_third_largest_adj_maximum_fn_val = 
              (adjacent_maxima.size() > 2)
                ? m_ref_dataset_instance->get_fn_val(m_chunk_maxima[adjacent_maxima[adjacent_maxima.size()-3]])
                : saddle_func_val;

            uint64_t adjacent_maxima_to_remove;

            // f(2nd max) - f(sad) < pers_low
            // Cancel all except 1st max
            if ((pers_calc_adj_maximum_fn_val - saddle_func_val) < pers_low)
              adjacent_maxima_to_remove = adjacent_maxima.size() - 1;

            else if (adjacent_maxima.size() > 2) {

              // Conservative cancellation. In case of at least 2 adjacent maxima, do not remove 3rd highest maximum if 
              // f(3rd max) - f(sad) < pers_low <= f(2nd max) - f(sad)
              if ((pers_calc_third_largest_adj_maximum_fn_val - saddle_func_val) < pers_low)
                adjacent_maxima_to_remove = adjacent_maxima.size() - 3;

              // Cancel all maxima whose persistence with saddle_ordinal fall below pers_low
              else {
                adjacent_maxima_to_remove = adjacent_maxima.size() + 1;

                for (uint64_t ii = adjacent_maxima.size()-1; ii<adjacent_maxima.size(); --ii) { // Decrement will cause unsigned int overflow to 0xFFFFFFFFFFFFFFFF which is > adjacent_maxima.size()

                  const func_val_t ith_maximum_func_val = 
                    m_ref_dataset_instance->get_fn_val(m_chunk_maxima[adjacent_maxima[ii]]);

                  if ((ith_maximum_func_val - saddle_func_val) < pers_low) {
                    adjacent_maxima_to_remove = ii + 1;
                    break;
                  }

                 }

                // Loop exited without triggering break. All adjacent maxima survive.
                if (adjacent_maxima_to_remove > adjacent_maxima.size())
                  adjacent_maxima_to_remove = 0;

              }
            }

            else {

              // All adjacent maxima survive, no removal
              adjacent_maxima_to_remove = 0;
            }

            perform_std_maxima_removal(
              this,
              saddle_ordinal,
              adjacent_maxima_to_remove,
              &m_saddle_maxima_mapping,
              &m_maxima_saddle_mapping,
              &m_cancelled_cp_path_cp
              );
        }

        #if PERSISTENCE_DEBUG
        printf("\n");
        #endif

    }

    verify_mapping_validity(&m_saddle_maxima_mapping, m_chunk_maxima.size());
    verify_mapping_validity(&m_maxima_saddle_mapping, m_chunk_saddles.size());
    host_clock(tmp_end);

    uint64_t cancellation_check_fails = 0;
    std::vector <uint64_t> distinct_survivng_extrema_collection;

    for (uint64_t saddle_ordinal=0; saddle_ordinal<m_chunk_saddles.size(); ++saddle_ordinal) {

      if (!saddle_exists(saddle_ordinal)) {

        if (m_saddle_maxima_mapping[saddle_ordinal].empty())
          continue;

        distinct_survivng_extrema_collection.clear();

        for (auto pair: m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal]) {
          if (pair.adjacent_extremum == pair.surviving_connected_extremum)
            if (!vector_contains(&distinct_survivng_extrema_collection, pair.adjacent_extremum))
              distinct_survivng_extrema_collection.push_back(pair.adjacent_extremum);
        }

        if (distinct_survivng_extrema_collection.size() < m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal].size()) {
          assert(distinct_survivng_extrema_collection.size() == 1);
          /*
          for (auto pair: m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal]) {
            if (pair.adjacent_extremum != v)
              ++b_count;
          }
          */
        }

      }

      else {

        for (uint64_t m: m_saddle_maxima_mapping[saddle_ordinal])
          if (!check_valid_cancellation(this, saddle_ordinal, m))
            ++cancellation_check_fails;
      }

      check_valid_cancellation_vector_of_paths.clear();

    }

    print_time_diff(tmp_end2 - tmp_start, "Allocation     Time");
    print_time_diff(tmp_end  - tmp_start , "Simplification Time");
    printf("std::remove Removed   Saddles  :   %lu\n", count_surviving_cp(&m_saddle_removed));
    printf("num_saddles_processed: %lu\n", num_saddles_processed);
    printf("cancellation_check_fails: %lu, %lu\n", cancellation_check_fails, check_valid_cancellation_inner_counter);
    printf("\n");

    #if 0
    puts("Adjacency Map after Simplification");
    print_adjacency_graph(adjacency_map, &m_chunk_maxima, &m_chunk_saddles);
    #endif
   }

  #define INFER_PROGRESS 0

  template <typename Comparator>
  func_val_t get_saddle_persistence_v2
   (
    const uint64_t                         saddle_ordinal,
          reachable_extrema_list_t * const reachable_extrema,
    const std::vector <uint64_t>   * const m_chunk_maxima,
    const std::vector <uint64_t>   * const m_chunk_saddles,
    const dataset_t                * const m_ref_dataset_instance,
    const Comparator                       comparator_func
   )
   {
    // Pair saddle with second highest function valued adjacent maximum (if |adjacent maxima| > 1)
    // or pair with the only adjacent maxima (|adjacent maxima| == 1)
    const uint64_t selected_maximum_for_persistence_calc =
        get_persistence_calc_maximum_index(reachable_extrema->size());

    std::sort(reachable_extrema->begin(),
              reachable_extrema->end(),
              comparator_func);

    #if INFER_PROGRESS
    printf("get_saddle_persistence_v2[%lu]: %lu extrema\n", saddle_ordinal, reachable_extrema->size());
    #endif

    return  
      m_ref_dataset_instance->get_fn_val(
        m_chunk_maxima->at(
          reachable_extrema->at(selected_maximum_for_persistence_calc).second
        )
      )

      - m_ref_dataset_instance->get_fn_val(m_chunk_saddles->at(saddle_ordinal));
   }

  template <typename Comparator>
  func_val_t get_saddle_persistence_v3
   (
    const uint64_t                                    saddle_ordinal,
          std::vector <std::vector<uint64_t>> * const traversed_paths,
          reachable_extrema_list_t            * const reachable_extrema,
    const std::vector <uint64_t>              * const m_chunk_maxima,
    const std::vector <uint64_t>              * const m_chunk_saddles,
    const dataset_t                           * const m_ref_dataset_instance,
    const Comparator                                  comparator_func
   )
   {
    // Pair saddle with second highest function valued adjacent maximum (if |adjacent maxima| > 1)
    // or pair with the only adjacent maxima (|adjacent maxima| == 1)
    const uint64_t selected_maximum_for_persistence_calc =
        get_persistence_calc_maximum_index(reachable_extrema->size());

    std::sort(traversed_paths->begin(),
              traversed_paths->end(),
              comparator_func);

    reachable_extrema->clear();
    for (uint64_t i=0; i<traversed_paths->size(); ++i) {
      std::vector <uint64_t>& v = traversed_paths->operator[](i);
      reachable_extrema->push_back(std::make_pair(v[1], v[v.size()-1]));
    }

    #if INFER_PROGRESS
    printf("get_saddle_persistence_v3[%lu]: %lu extrema\n", saddle_ordinal, reachable_extrema->size());
    #endif

    return  
      m_ref_dataset_instance->get_fn_val(
        m_chunk_maxima->at(
          reachable_extrema->at(selected_maximum_for_persistence_calc).second
        )
      )

      - m_ref_dataset_instance->get_fn_val(m_chunk_saddles->at(saddle_ordinal));
   }

  template <typename T>
  bool vector_contains_at_even_odd_indices (const std::vector <T> * const vec, const T& val)
   {
    const uint8_t even_odd_selector = 1 - (vec->size() & 0x1);
    bool at_even = false, at_odd = false;

    for (uint64_t i=0; i<vec->size()-1; ++i)
      if (vec->operator[](i) == val) {

        at_even |= ((i & 0x1) == even_odd_selector);
        at_odd  |= ((i & 0x1) == even_odd_selector);

        if (at_odd || at_even)
          break;
      }

    return at_odd || at_even;
   }

  void path_traversal_v2
   (
    const uint64_t                                    saddle_ordinal,
          extremum_graph_t                    * const ext_graph,
          std::vector <std::vector<uint64_t>> * const traversed_paths,
          reachable_extrema_list_t            * const reachable_extrema
   )
   {
    const std::vector <std::vector<uint64_t>> * const saddle_extrema_arc_alternate[2] = { &ext_graph->m_maxima_saddle_mapping, &ext_graph->m_saddle_maxima_mapping };
    std::stack  <std::vector<uint64_t>> split_path_stack;
    std::set <uint64_t> split_points;

    traversed_paths->clear();
    reachable_extrema->clear();

    // Start with adjacent surviving extrema arcs
    for (auto m: ext_graph->m_saddle_maxima_mapping[saddle_ordinal]) {
      std::vector <uint64_t> path;
      path.push_back(saddle_ordinal);
      path.push_back(m);
      split_path_stack.push(path);
    }

    uint64_t path_counter = 0;

    while (!split_path_stack.empty()) {
      std::vector <uint64_t> curr_path = split_path_stack.top();
      split_path_stack.pop();
      ++path_counter;

      if (vector_contains_val_ptr(traversed_paths, &curr_path))
        continue;

      uint64_t curr_pt = curr_path[curr_path.size()-1];

      while (true) {
        const int8_t is_curr_pt_saddle = (curr_path.size() & 0x1);

        const std::vector <uint64_t> * const curr_adjacency_list = &saddle_extrema_arc_alternate[is_curr_pt_saddle]->at(curr_pt);

        if (curr_adjacency_list->size() == 1) {
          const uint64_t next_pt = curr_adjacency_list->operator[](0);

          curr_path.push_back(next_pt);
          curr_pt = next_pt;

          if (vector_contains_at_even_odd_indices(&curr_path, next_pt)) { // Encountered cycle
            if (is_curr_pt_saddle) {
            }
            else {
              curr_path.pop_back();
              curr_pt = curr_path[curr_path.size()-1];
            }

            assert((curr_path.size() % 2) == 0);
            if (!vector_contains_val_ptr(traversed_paths, &curr_path)) {
              traversed_paths->push_back(curr_path);
              reachable_extrema->push_back(std::make_pair(curr_path[1], is_curr_pt_saddle ? next_pt : curr_pt));
            }
            #if 0
            printf("path_traversal_v2[%lu]: single outward arc. <", saddle_ordinal);
            for (auto pp: curr_path)
              printf("%lu, ", pp);
            printf("\b\b>\n");
            #endif
            break; // Stops when curr_pt is already present in the path i.e. a cycle is found
          }
          else
            continue;
        }

        else if (curr_adjacency_list->empty()) {
          if (is_curr_pt_saddle) {
            curr_path.pop_back(); // Path terminating at saddle with no outgoing arcs. Current mitigation to erase this saddle.
            vector_remove_item<uint64_t>(&ext_graph->m_maxima_saddle_mapping[curr_path[curr_path.size()-1]], curr_pt);
            curr_pt = curr_path[curr_path.size()-1];
          }
          else {
            assert((is_curr_pt_saddle == 0) && (curr_path.size() & 0x1) == 0); // Path should always be terminated by an extremum            
          }

          assert((curr_path.size() % 2) == 0);
          if (!vector_contains_val_ptr(traversed_paths, &curr_path)) {
            traversed_paths->push_back(curr_path);
            reachable_extrema->push_back(std::make_pair(curr_path[1], curr_pt));
          }
          break;
        }

        else {
          // We have a path split. Traverse each branch later
          assert(curr_adjacency_list->size() > 1);

          for (uint32_t i=0; i<curr_adjacency_list->size(); ++i) {

            uint32_t count = 0;
            for (uint32_t k=0; k<curr_adjacency_list->size(); ++k)
              if (curr_adjacency_list->operator[](i) == curr_adjacency_list->operator[](k))
                ++count;

            if (count > 1) continue;

            std::vector <uint64_t> v = curr_path;
            const uint64_t next_pt = curr_adjacency_list->operator[](i);
            v.push_back(next_pt);

            if (vector_contains_at_even_odd_indices(&v, next_pt) || vector_contains_val_ptr(traversed_paths, &v))
              continue; // Encountered a cycle, do not further traverse this path

            split_path_stack.push(v);
          }

          #if 0
          const uint64_t curr_real_pt = curr_pt + ((is_curr_pt_saddle) ? ext_graph->m_chunk_saddles.size() : 0);

          if (split_points.find(curr_real_pt) == split_points.end()) {

            assert(curr_adjacency_list->size() > 1);

            for (uint32_t i=0; i<curr_adjacency_list->size(); ++i) {
              bool flag = false;

              for (uint32_t k=0; k<i; ++k)
                if (curr_adjacency_list->operator[](k)==curr_adjacency_list->operator[](i)) {
                  flag = true;
                  break;
                }

              if (flag) continue;

              std::vector <uint64_t> v = curr_path;
              v.push_back(curr_adjacency_list->operator[](i));
              split_path_stack.push(v);
              split_points.insert(curr_real_pt);
            }
          }

          else {
            traversed_paths->push_back(curr_path);
            reachable_extrema->push_back(std::make_pair(curr_path[1], curr_pt));
            break;
          }
          #endif

          break;
        }

      }

    }

    #if INFER_PROGRESS
    printf("path_traversal_v2[%lu]: %lu paths\n", saddle_ordinal, traversed_paths->size());
    #endif
    assert(traversed_paths->size() == reachable_extrema->size());
   }

  static uint64_t path_traversal_v3_max_length = 0;

  void path_traversal_v3
   (
    const uint64_t                                      saddle_ordinal,
    const extremum_graph_t                    * const   ext_graph,
          std::vector <std::vector<uint64_t>> * const   traversed_paths,
          reachable_extrema_list_t            * const   reachable_extrema,
          std::vector <uint64_t>              * const   path_prefix,
    const std::vector <std::vector<uint64_t>> * const * saddle_extrema_arc_alternate
   )
   {
    std::vector <uint64_t> path(path_prefix->begin(), path_prefix->end());

    while (true) {
      const uint64_t curr_pt = path[path.size() - 1];
      const uint8_t is_curr_pt_saddle = (path.size() & 0x1);
      const std::vector <uint64_t> * const curr_adjacency_list = &saddle_extrema_arc_alternate[is_curr_pt_saddle]->at(curr_pt);

      if (curr_adjacency_list->empty()) {
        assert((path.size() & 0x1) == 0);
        path_traversal_v3_max_length = max(path_traversal_v3_max_length, path.size());
        traversed_paths->push_back(path);
        reachable_extrema->push_back(std::make_pair(path[1], curr_pt));
        return;
      }

      else if (curr_adjacency_list->size() == 1) {
        const uint64_t next_pt = curr_adjacency_list->operator[](0);
        path.push_back(next_pt);

        if (vector_contains_at_even_odd_indices(&path, next_pt)) { // Cycle found. Terminate here

          if (!is_curr_pt_saddle)
            path.pop_back();

          assert((path.size() & 0x1) == 0);
          path_traversal_v3_max_length = max(path_traversal_v3_max_length, path.size());
          traversed_paths->push_back(path);
          reachable_extrema->push_back(std::make_pair(path[1], curr_pt));
          return;
        }

      }

      else // Split found
        break;
    }

    const uint64_t curr_pt = path[path.size() - 1];
    const uint8_t is_curr_pt_saddle = (path.size() & 0x1);
    const std::vector <uint64_t> * const curr_adjacency_list = &saddle_extrema_arc_alternate[is_curr_pt_saddle]->at(curr_pt);

    std::vector <uint64_t> queued_next_pts;

    for (uint32_t i=0; i<curr_adjacency_list->size(); ++i) {
      const uint64_t next_pt = curr_adjacency_list->operator[](i);

      if (vector_contains(&queued_next_pts, next_pt))
        continue;

      queued_next_pts.push_back(next_pt);
      
      path.push_back(next_pt);
      if (vector_contains_at_even_odd_indices(&path, next_pt)) {
        path.pop_back();
        continue;
      }

      path_traversal_v3(saddle_ordinal, ext_graph, traversed_paths, reachable_extrema, &path, saddle_extrema_arc_alternate);
      path.pop_back(); // Reuse path instead of copying and appending next_pt
    }

   }

  void path_traversal_v3
   (
    const uint64_t                                    saddle_ordinal,
    const extremum_graph_t                    * const ext_graph,
          std::vector <std::vector<uint64_t>> * const traversed_paths,
          reachable_extrema_list_t            * const reachable_extrema
   )
   {
    const std::vector <std::vector<uint64_t>> * const saddle_extrema_arc_alternate[2] = { &ext_graph->m_maxima_saddle_mapping, &ext_graph->m_saddle_maxima_mapping };

    traversed_paths->clear();
    reachable_extrema->clear();

    std::vector <uint64_t> path_prefix;
    path_prefix.push_back(saddle_ordinal);
    path_traversal_v3(saddle_ordinal, ext_graph, traversed_paths, reachable_extrema, &path_prefix, saddle_extrema_arc_alternate);
    #if INFER_PROGRESS
    printf("path_traversal_v3[%lu]: %lu paths\n", saddle_ordinal, traversed_paths->size());
    #endif
    assert(traversed_paths->size() == reachable_extrema->size());
   }

  void find_non_strangulation_extrema_v2
   (
    const reachable_extrema_list_t * const reachable_extrema,
          std::vector <uint32_t>   * const non_strangulation_extrema_indices
   )
   {
    non_strangulation_extrema_indices->clear();

    for (uint32_t i=0; i<reachable_extrema->size(); ++i) {
      uint32_t count = 0;

      for (uint32_t j=0; j<reachable_extrema->size(); ++j)
        if (reachable_extrema->operator[](i).second == reachable_extrema->operator[](j).second)
          ++count;

      if (count==1)
        non_strangulation_extrema_indices->push_back(i);
    }

    #if INFER_PROGRESS
    printf("find_non_strangulation_extrema_v2: good extrema=%lu\n", non_strangulation_extrema_indices->size());
    #endif
   }

  inline __attribute__((hot)) void flip_edge_v2
   (
    std::vector <std::vector<uint64_t>> * const * saddle_extrema_arc_alternate,

    const uint64_t first_pt , const int8_t is_curr_pt_maximum,
    const uint64_t second_pt, const int8_t is_next_pt_maximum
   )
   {
    if (vector_contains(&saddle_extrema_arc_alternate[is_curr_pt_maximum]->operator[](first_pt), second_pt))
      vector_remove_item(&saddle_extrema_arc_alternate[is_curr_pt_maximum]->operator[](first_pt), second_pt);
    saddle_extrema_arc_alternate[is_next_pt_maximum]->operator[](second_pt).push_back(first_pt);
   }

  void reverse_path_v2
   (
    const uint64_t                                    saddle_ordinal,
          extremum_graph_t                    * const ext_graph,
          std::vector <std::vector<uint64_t>> * const traversed_paths,
          reachable_extrema_list_t            * const reachable_extrema,
          std::vector <uint32_t>              * const selected_arc_indices
   )
   {
    (void)saddle_ordinal;
    (void)reachable_extrema;
    std::vector <std::vector<uint64_t>> * const saddle_extrema_arc_alternate[2] = { &ext_graph->m_saddle_maxima_mapping, &ext_graph->m_maxima_saddle_mapping };

    std::set <uint64_t> visited_critical_points;
    uint64_t k;

    for (uint32_t i=0; i<selected_arc_indices->size(); ++i) {
      std::vector <uint64_t>& v = traversed_paths->operator[](selected_arc_indices->operator[](i)); // Take reference, no copying. For brevity

      for (k=0; k<v.size(); ++k) // Skip arcs reversed by previously processed traversed_paths[_]
        if (visited_critical_points.find(v[k]) == visited_critical_points.end())
          break;

      k = (k==0) ? 0 : (k-1); // We encountered the fist unvisited arc on the current path. Move one step behind and start arc reversals

      for (; k<v.size()-1; ++k) {

        const int8_t is_curr_pt_maximum = (k & 0x1);
        const int8_t is_next_pt_maximum = 1 - is_curr_pt_maximum;

        flip_edge_v2(saddle_extrema_arc_alternate, v[k], is_curr_pt_maximum, v[k+1], is_next_pt_maximum);
        visited_critical_points.insert(v[k]);
      }

//      Should not be needed as this would be the path terminal
//      visited_critical_points.insert(v[v.size()-1]);
    }

    #if INFER_PROGRESS
    printf("reverse_path_v2[%lu]: %lu reversals\n", saddle_ordinal, selected_arc_indices->size());
    #endif
   }

  void reverse_path_v3
   (
    const uint64_t                                    saddle_ordinal,
          extremum_graph_t                    * const ext_graph,
          std::vector <std::vector<uint64_t>> * const traversed_paths,
          reachable_extrema_list_t            * const reachable_extrema,
          std::vector <uint32_t>              * const selected_arc_indices
   )
   {
    (void)saddle_ordinal;
    (void)reachable_extrema;
    std::vector <std::vector<uint64_t>> * const saddle_extrema_arc_alternate[2] = { &ext_graph->m_saddle_maxima_mapping, &ext_graph->m_maxima_saddle_mapping };

    std::set <std::pair<uint64_t, uint64_t>> reversed_arcs;

    for (uint32_t i=0; i<selected_arc_indices->size(); ++i) {
      std::vector <uint64_t>& v = traversed_paths->operator[](selected_arc_indices->operator[](i)); // Take reference, no copying. For brevity

      for (uint64_t k=0; k<v.size()-1; ++k) {

        const int8_t is_curr_pt_maximum = (k & 0x1);
        const int8_t is_next_pt_maximum = 1 - is_curr_pt_maximum;

        // is_next_pt_maximum implies current pt is saddle
        const uint64_t saddle   = (is_next_pt_maximum ? v[k] : v[k+1]) + ext_graph->m_chunk_maxima.size();
        const uint64_t extremum =  is_curr_pt_maximum ? v[k] : v[k+1];

        const std::pair <uint64_t, uint64_t> curr_arc = std::make_pair(saddle, extremum);
        if (reversed_arcs.find(curr_arc) != reversed_arcs.end())
          continue;

        reversed_arcs.insert(curr_arc);
        flip_edge_v2(saddle_extrema_arc_alternate, v[k], is_curr_pt_maximum, v[k+1], is_next_pt_maximum);
      }

//      Should not be needed as this would be the path terminal
//      visited_critical_points.insert(v[v.size()-1]);
    }

    #if INFER_PROGRESS
    printf("reverse_path_v3[%lu]: %lu reversals\n", saddle_ordinal, selected_arc_indices->size());
    #endif
   }

  #define GET_SADDLE_PERSISTENCE_VERSION 3

  void extremum_graph_t :: std_persistence_based_cancellation_v2
   (
    const func_val_t  given_pers_low,
    const bool        normalized_threshold
   )
   {
    uint64_t tmp_start, tmp_end, tmp_end2;
    uint64_t start = 0, curr;
    uint64_t num_saddles_processed = 0;

    host_clock(tmp_start);
    const func_val_t pers_low  = given_pers_low  * (normalized_threshold ? m_dataset_func_range : 1.0f);

//    printf("pers_low: %f\n", pers_low);

    #if 1
    m_saddle_maxima_mapping_copy = m_saddle_maxima_mapping;
    m_maxima_saddle_mapping_copy = m_maxima_saddle_mapping;
    #endif

    // We want a directed arc graph
    for (uint64_t i=0; i<m_chunk_maxima.size(); ++i)
      m_maxima_saddle_mapping[i].clear();

    maxima_saddle_mapping_cleared = true;

    saddle_persistence_queue_t saddle_queue;
    reachable_extrema_list_t reachable_extrema;
    std::vector <std::vector<uint64_t>> traversed_paths;
    std::vector <uint32_t> non_strangulation_extrema_indices;
    std::vector <uint32_t> selected_arc_indices;
    std::map <uint64_t, int> multi_saddle_persistence_calc_map;

    auto sort_maxima_by_func_val_comparator_basic = [&] (const uint64_t maximum1_ordinal, const uint64_t maximum2_ordinal) -> bool
     {
      const func_val_t maximum1_func_val = m_ref_dataset_instance->get_fn_val(m_chunk_maxima[maximum1_ordinal]);
      const func_val_t maximum2_func_val = m_ref_dataset_instance->get_fn_val(m_chunk_maxima[maximum2_ordinal]);

      if (maximum1_func_val != maximum2_func_val)
        return maximum1_func_val < maximum2_func_val;
      else
        return maximum1_ordinal < maximum2_ordinal;
     };

    auto sort_maxima_by_func_val_comparator_for_v3 = [&] (const std::vector <uint64_t>& path1, const std::vector <uint64_t>& path2) -> bool
     {
      const uint64_t maximum1_ordinal = path1[path1.size()-1];
      const uint64_t maximum2_ordinal = path2[path2.size()-1];
      return sort_maxima_by_func_val_comparator_basic(maximum1_ordinal, maximum2_ordinal);
     };

    for (uint64_t saddle_ordinal=0; saddle_ordinal<m_chunk_saddles.size(); ++saddle_ordinal) {

      #if SIMPLIFICATION_POLL_STATUS
      if (has_input()) {
        host_clock(curr);
        long double num_saddles_processed_per_second = 1e6 * ((long double)saddle_ordinal) / (curr - start);
         printf("std::persistence_v2 init_persistence: %lu. [%lu processed, %lu sec] %lu saddles/sec\n", saddle_ordinal, num_saddles_processed, (curr-start)/1000000, ((unsigned long)num_saddles_processed_per_second));
      }
      #endif

      if (!saddle_exists(saddle_ordinal) || m_saddle_persistence_removed[saddle_ordinal])
          continue;

      #if STD_PERSISTENCE_REMOVE_SINGLE_SADDLE_EXTREMA_ARCS
      if (m_saddle_maxima_mapping[saddle_ordinal].size() == 1) {
        m_saddle_persistence_removed[saddle_ordinal] = true;
        continue;
      }
      #endif

      #if GET_SADDLE_PERSISTENCE_VERSION == 3

//        if (m_saddle_maxima_mapping[saddle_ordinal].size() > 2) {
//          multi_saddle_persistence_calc_map[saddle_ordinal] = 0;
//        }

        std::vector <uint64_t> adjacency_map = m_saddle_maxima_mapping[saddle_ordinal];
        std::sort(
          m_saddle_maxima_mapping[saddle_ordinal].begin(),
          m_saddle_maxima_mapping[saddle_ordinal].end(),
          sort_maxima_by_func_val_comparator_basic);

        const uint64_t selected_maximum_ordinal = get_persistence_calc_maximum_index(m_saddle_maxima_mapping[saddle_ordinal].size());
        const func_val_t saddle_persistence =
          m_ref_dataset_instance->get_fn_val(m_chunk_maxima[m_saddle_maxima_mapping[saddle_ordinal][selected_maximum_ordinal]])
          -
          m_ref_dataset_instance->get_fn_val(m_chunk_saddles[saddle_ordinal]);

      #elif GET_SADDLE_PERSISTENCE_VERSION==2 // v2

        reachable_extrema_list_t init_list;
        for (auto m: m_saddle_maxima_mapping[saddle_ordinal])
          init_list.push_back(std::make_pair(m, m));

        const func_val_t saddle_persistence =
          get_saddle_persistence_v2(saddle_ordinal, &init_list, &m_chunk_maxima, &m_chunk_saddles, m_ref_dataset_instance, sort_maxima_by_func_val_comparator);

      #endif

      saddle_queue.push(std::make_pair(saddle_ordinal, saddle_persistence));
    }

//    printf("std::persistence_v2 Saddle Queue size: %lu\n", saddle_queue.size());

    host_clock(tmp_end2);

    while (!saddle_queue.empty()) {

        #if SIMPLIFICATION_POLL_STATUS
        if (has_input()) {
          host_clock(curr);
          long double num_saddles_processed_per_second = 1e6 * ((long double)num_saddles_processed) / (curr - start);

          printf("std::persistence_v2 Queue_size: %lu. [%lu processed, %lu sec] %lu saddles/sec\n", saddle_queue.size(), num_saddles_processed, (curr-start)/1000000, ((unsigned long)num_saddles_processed_per_second));
        }
        #endif

        saddle_persistence_pair_t curr_queue_pair = saddle_queue.top();
        saddle_queue.pop();

        const uint64_t saddle_ordinal     = curr_queue_pair.first;

        ++num_saddles_processed;

        if (m_saddle_persistence_removed[saddle_ordinal] || m_saddle_maxima_mapping[saddle_ordinal].empty())
          continue;

        path_traversal_v3(saddle_ordinal, this, &traversed_paths, &reachable_extrema);

        #if INFER_PROGRESS
        printf("saddle_queue_size=%lu\n", saddle_queue.size());
        #endif

        // Compute persistence for popped saddle
        const func_val_t new_persistence =

        #if GET_SADDLE_PERSISTENCE_VERSION==3
          get_saddle_persistence_v3(saddle_ordinal, &traversed_paths, &reachable_extrema, &m_chunk_maxima, &m_chunk_saddles, m_ref_dataset_instance, sort_maxima_by_func_val_comparator_for_v3);
        #elif GET_SADDLE_PERSISTENCE_VERSION==2
          get_saddle_persistence_v2(saddle_ordinal, &reachable_extrema, &m_chunk_maxima, &m_chunk_saddles, m_ref_dataset_instance, sort_maxima_by_func_val_comparator_for_v2);
        #endif

        const saddle_persistence_pair_t top_pair = saddle_queue.top();

        // Update persistence for popped saddle if not minimum in priority queue
        if (!saddle_queue.empty() && new_persistence > top_pair.second) {
            curr_queue_pair.second = new_persistence;
            saddle_queue.push(curr_queue_pair);
        }

        // Cancel saddle
        else {

          find_non_strangulation_extrema_v2(&reachable_extrema, &non_strangulation_extrema_indices);

          // Do not cancel saddle
          if (new_persistence >= pers_low && non_strangulation_extrema_indices.size() < 3)
            continue;

          selected_arc_indices.clear();

          if (non_strangulation_extrema_indices.size() < 2) {
            continue; // Nothing to cancel
          }

          else if (non_strangulation_extrema_indices.size() == 2) {
            if (new_persistence < pers_low)
              selected_arc_indices.push_back(non_strangulation_extrema_indices[0]); // exactly one arc to reverse
          }

          /*
          else if (non_strangulation_extrema_indices.size() == 3) {
            if (new_persistence < pers_low)
              selected_arc_indices.push_back(non_strangulation_extrema_indices[0]); // exactly one arc to reverse
          }
          */

          else {
            uint64_t limit = get_persistence_calc_maximum_index(non_strangulation_extrema_indices.size());
            const func_val_t saddle_func_val = m_ref_dataset_instance->get_fn_val(m_chunk_saddles[saddle_ordinal]);
            const func_val_t prev_limit_extremum_func_val =
            m_ref_dataset_instance->get_fn_val(
              m_chunk_maxima[
                reachable_extrema[
                  non_strangulation_extrema_indices[limit-1]
                  ].second
                ]
              );

            const func_val_t prev_limit_extremum_persistence = prev_limit_extremum_func_val - saddle_func_val;

            uint64_t final_limit;

            if (new_persistence < pers_low)
              final_limit = limit + 1;

            else if (prev_limit_extremum_persistence <= pers_low && pers_low < new_persistence)
              final_limit = limit;

            else {

              bool flag = true;

//              for (uint64_t ii=non_strangulation_extrema_indices.size()-1; ii<non_strangulation_extrema_indices.size(); --ii) {
              for (uint64_t ii=non_strangulation_extrema_indices.size()-1; ii>0; --ii) {
                const func_val_t ith_maximum_func_val = 
                  m_ref_dataset_instance->get_fn_val(m_chunk_maxima[reachable_extrema[non_strangulation_extrema_indices[ii]].second]);

                if ((ith_maximum_func_val - saddle_func_val) < pers_low) {
                  final_limit = ii + 1;
                  flag = false;
                  break;
                }

              }

              if (flag)
                final_limit = 0;
            }

            for (uint32_t i=0; i<final_limit; ++i)
              selected_arc_indices.push_back(non_strangulation_extrema_indices[i]);
          }

          reverse_path_v3(saddle_ordinal, this, &traversed_paths, &reachable_extrema, &selected_arc_indices);

          #if 1
          if (!selected_arc_indices.empty())
            m_saddle_persistence_removed[saddle_ordinal] = true;
          #else
          if (m_saddle_maxima_mapping[saddle_ordinal].empty())
            m_saddle_persistence_removed[saddle_ordinal] = true;
          #endif
        }

      }

//    fill_maxima_saddle_mapping();
    host_clock(tmp_end);

    m_std_persistence_simplification_performed = true;

//    print_time_diff(tmp_end2 - tmp_start, "Allocation     Time");
    print_time_diff(tmp_end  - tmp_start , "Cancellation       ");
//    printf("std::remove Removed   Saddles  :   %lu\n", count_surviving_cp(&m_saddle_persistence_removed));
//    printf("num_saddles_processed: %lu\n", num_saddles_processed);
//    printf("path_traversal_v3_max_length: %lu\n", path_traversal_v3_max_length);
//    printf("\n");

   }

  void extremum_graph_t :: std_persistence_based_cancellation
   (
    const func_val_t  given_pers_low,
    const bool        normalized_threshold
   )
   {
    return std_persistence_based_cancellation_v2(given_pers_low, normalized_threshold);
   }

  void extremum_graph_t :: persistence_plot_info_gather
     (
      const persistence_map_t                              * const persistence_vals,
      const persistence_map_t                              * const saturated_persistence_vals,
      const arc_redirection_info_t                         * const arc_redirection_list,
      const critical_point_map_t                           * const adjacency_map,
      func_val_t p_lo_start, func_val_t p_lo_end, func_val_t p_lo_increments,
      func_val_t p_hi_start, func_val_t p_hi_end, func_val_t p_hi_decrements,
      std::vector <uint64_t> * const arc_count,
      std::vector <uint64_t> * const extrema_count,
      std::vector <uint64_t> * const saddle_count,
      const bool normalized_thresholds
     )
   {
    uint64_t tmp_start, tmp_end;

    host_clock(tmp_start);

    critical_point_map_t tmp_adjacency_map;

    assert(p_lo_start <= p_lo_end  );
    assert(p_hi_end   <= p_hi_start);

    const func_val_t p_lo_min = 0;
    const func_val_t p_hi_max = normalized_thresholds ? 1 : m_dataset_func_range;

    assert(p_lo_start >= p_lo_min);
    assert(p_hi_end <= p_hi_max);

    for (func_val_t p_hi=p_hi_start; p_hi>p_hi_end; p_hi -= p_hi_decrements) {
        persistence_simplification_cancellation(persistence_vals, saturated_persistence_vals, arc_redirection_list, &tmp_adjacency_map, adjacency_map, p_lo_min, p_hi, normalized_thresholds);
        arc_count->push_back(count_surviving_arcs(&tmp_adjacency_map, m_chunk_maxima.size(), &m_saddle_removed));
        saddle_count->push_back(count_surviving_cp(&m_saddle_surviving));
    }

    for (func_val_t p_lo=p_lo_start; p_lo<p_lo_end; p_lo += p_lo_increments) {
        persistence_simplification_cancellation(persistence_vals, saturated_persistence_vals, arc_redirection_list, &tmp_adjacency_map, adjacency_map, p_lo, p_hi_max,  normalized_thresholds);
        extrema_count->push_back(count_surviving_cp(&m_maxima_surviving));
    }

    host_clock(tmp_end);
    print_time_diff(tmp_end - tmp_start, "Persistence Plot Info Gather");
   }

  void extremum_graph_t :: perform_edge_bundling ()
   {
    uint64_t t1, t2;
    std::map <uint64_t, std::vector<point_index_t>> m_maxima_saddle_mapping_over_arcs;
    std::map <uint64_t, std::vector<point_index_t>> m_saddle_maxima_mapping_over_arcs;

    host_clock(t1);
    point_index_t representative_saddle;
    std::vector<point_index_t> tmp_vector;

    const bool is_graph_simplified = m_saturated_persistence_simplification_performed || m_std_persistence_simplification_performed;

    static int edge_bundling_iter_count = 0;

    if (is_graph_simplified)
      ++edge_bundling_iter_count;

    if (is_graph_simplified) {

      reachable_extrema_list_t reachable_extrema;
      std::vector <std::vector<uint64_t>> traversed_paths;

      for (uint64_t s=0; s<m_chunk_saddles.size(); ++s) {

        if (!saddle_exists(s) || m_saddle_persistence_removed[s])
          continue;

        path_traversal_v3(s, this, &traversed_paths, &reachable_extrema);

        for (auto m_m: reachable_extrema) {
          m_maxima_saddle_mapping_over_arcs[m_m.second].push_back(s);
          m_saddle_maxima_mapping_over_arcs[s].push_back(m_m.second);
        }

      }

    }

    for (uint64_t i=0; i<m_chunk_saddles.size(); ++i) {

      if (!saddle_exists(i) || m_saddle_persistence_removed[i])
        continue;

      #if SIMPLIFICATION_POLL_STATUS
      if (has_input()) {
        printf("Flagging saddles: %lu/%lu\n", i, m_chunk_saddles.size());
      }
      #endif

      const uint64_t saddle_adjacency_count = (is_graph_simplified ? m_saddle_maxima_mapping_over_arcs[i].size() : m_saddle_maxima_mapping[i].size());

      if (saddle_adjacency_count == 1) {
          point_index_t curr_saddle, curr_maximum;
          uint64_t adjacency_count = 0;
          bool is_saddle, should_stop;

          curr_saddle = i;
          is_saddle = true;
          should_stop = false;

          while (!should_stop) {
              if (is_saddle) {
                  adjacency_count = (is_graph_simplified ? m_saddle_maxima_mapping_over_arcs[curr_saddle].size() : m_saddle_maxima_mapping[curr_saddle].size());
                  if (adjacency_count == 1) {
                      if (is_graph_simplified) {
                          curr_maximum = m_saddle_maxima_mapping_over_arcs[curr_saddle][0];
                          m_saddle_maxima_mapping_over_arcs[curr_saddle].clear();
                          vector_remove_item(&m_maxima_saddle_mapping_over_arcs[curr_maximum], curr_saddle);
                      }
                      else {
                          curr_maximum = m_saddle_maxima_mapping[curr_saddle][0];
                          m_saddle_maxima_mapping[curr_saddle].clear();
                          vector_remove_item(&m_maxima_saddle_mapping[curr_maximum], curr_saddle);
                      }
                      m_saddle_surviving[curr_saddle] = false;
                      m_saddle_removed[curr_saddle] = true;
                  }
                  else
                      should_stop = true;
              }

              // is_saddle = false
              else {
                  adjacency_count = (is_graph_simplified ? m_maxima_saddle_mapping_over_arcs[curr_maximum].size() : m_maxima_saddle_mapping[curr_maximum].size());
                  if (adjacency_count == 1) {
                      if (is_graph_simplified) {
                          curr_saddle = m_maxima_saddle_mapping_over_arcs[curr_maximum][0];
//                          m_maxima_saddle_mapping_over_arcs[curr_maximum].clear();
                          vector_remove_item(&m_saddle_maxima_mapping_over_arcs[curr_saddle], curr_maximum);
                      }
                      else {
                          curr_saddle = m_maxima_saddle_mapping[curr_maximum][0];
//                          m_maxima_saddle_mapping[curr_maximum].clear();
                          vector_remove_item(&m_saddle_maxima_mapping[curr_saddle], curr_maximum);

                      }
                  }
                  else
                      should_stop = true;
              }

              is_saddle = !is_saddle;
          }
      }

      else if (saddle_adjacency_count > 2) {
        m_saddle_surviving[i] = true;
        m_saddle_removed  [i] = false;
        continue;
      }

      /*
      if (is_graph_simplified) {
        if (edge_bundling_iter_count==2) {
          printf("surviving saddle=%lu adjacency[%lu]: { ", i, saddle_adjacency_count);
          for (uint64_t m: m_saddle_maxima_mapping_over_arcs[i])
            printf("%lu, ", m);
          printf("\b\b}\n");
        }
      }
       */

      bool should_compute_representative = true;

      if (is_graph_simplified) {
          if (m_saddle_maxima_mapping_over_arcs[i].size() < 2)
              should_compute_representative = false;
      }
      else {
          if (m_saddle_maxima_mapping[i].size() < 2)
              should_compute_representative = false;
      }

      if (should_compute_representative) {
          const uint64_t ma = is_graph_simplified ? m_saddle_maxima_mapping_over_arcs[i][0] : m_saddle_maxima_mapping[i][ 0 ];
          const uint64_t mb = is_graph_simplified ? m_saddle_maxima_mapping_over_arcs[i][1] : m_saddle_maxima_mapping[i][ 1 ];

          if (mb >= m_chunk_maxima.size()) {
              printf("mb=%lu, |max|=%lu\n", mb, m_chunk_maxima.size());
          }

          assert(ma < m_chunk_maxima.size());
          assert(mb < m_chunk_maxima.size());

          if (
                  (is_graph_simplified && find_representative_saddle(m_ref_dataset_instance, &m_chunk_saddles, &m_maxima_saddle_mapping_over_arcs[ma], &m_maxima_saddle_mapping_over_arcs[mb], representative_saddle, &tmp_vector))
                  ||
                  (!is_graph_simplified && find_representative_saddle(m_ref_dataset_instance, &m_chunk_saddles, &m_maxima_saddle_mapping[ma], &m_maxima_saddle_mapping[mb], representative_saddle, &tmp_vector))
                  ) {

              if (is_graph_simplified && edge_bundling_iter_count==2) {
//          if (representative_saddle == i)
                  printf("rep for [%lu, %lu]=%lu\n", ma, mb, representative_saddle);
              }

              m_saddle_surviving[i] = (representative_saddle == i);
              m_saddle_removed  [i] = (representative_saddle != i);
          }

      else {

//        Should not be removing
//        erase_saddle(i);

          }
      }

    }

    host_clock(t2);

    print_time_diff(t2 - t1, "Edge bundling      ");
    m_edge_bundling_performed = true;
   }

};

#endif // EXT_GRAPH_ALGS
