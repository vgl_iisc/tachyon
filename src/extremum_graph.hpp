/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#ifndef EXTREMUM_GRAPH_HPP
#define EXTREMUM_GRAPH_HPP

#include <grid_dataset.hpp>
#include <plan.hpp>

#define CHECK_SURVIVING_MAXIMUM 0
#define CHECK_SURVIVING_SADDLE  0
#define STD_PERSISTENCE_REMOVE_SINGLE_SADDLE_EXTREMA_ARCS 1

namespace extgraph
 {

   class extremum_graph_t
    {

      protected:

        void fetch_transitive_paths (const uint64_t saddle_ordinal, const arc_redirection_info_t * const arc_redirection_list, const critical_point_map_t * const adjacency_map, std::vector <std::vector<uint64_t>> * const adjacent_maxima_transitive_paths, std::vector <uint64_t> * const tail_maxima) const;
        void saddle_arc_tracing (const arc_redirection_info_t * const arc_redirection_list, const uint64_t start_saddle_ordinal, const point_index_t via_maximum, std::vector<uint64_t> * const path_vector) const;
        void reverse_path (arc_redirection_info_t * const arc_redirection_list, std::vector<uint64_t> * const path_vector);

        bool maxima_saddle_mapping_cleared = false;

      public:

        func_val_t                                           m_dataset_func_range;
        chunk_rect_t                                         m_dataset_rect;
        bool                                                 m_invert_fn_val;
        bool                                                 m_edge_bundling_performed;
        bool                                                 m_std_persistence_simplification_performed;
        bool                                                 m_saturated_persistence_simplification_performed;

        std::vector <point_index_t>                          m_chunk_maxima;
        std::vector <point_index_t>                          m_chunk_saddles;

        std::vector <bool>                                   m_maxima_surviving;
        std::vector <bool>                                   m_saddle_surviving;
        std::vector <bool>                                   m_saddle_removed;
        std::vector <bool>                                   m_saddle_zero_persistence;
        std::vector <bool>                                   m_saddle_persistence_removed;

        std::vector <point_index_t>                          m_chunk_1saddles;
        std::vector <point_index_t>                          m_chunk_minima;
        std::vector <point_index_t>                          m_chunk_multi_saddles;

        std::vector <std::vector<point_index_t>>             m_maxima_saddle_mapping;

        critical_point_map_t                                 m_saddle_maxima_mapping;
        std::vector <saddle_maxima_path_status_t>            m_saddle_maxima_mapping_status;

        neighbour_bitmap_t*                                  m_max_neighbour_array;

        critical_point_map_t                                 m_saddle_maxima_mapping_copy;
        std::vector <std::vector<point_index_t>>             m_maxima_saddle_mapping_copy;

        class saddle_extremum_connection_info_t {

          public:

            uint64_t adjacent_extremum;
            uint64_t surviving_connected_extremum;
            bool     is_strangulation;

            saddle_extremum_connection_info_t (uint64_t adj, uint64_t surviving) {
              adjacent_extremum = adj;
              surviving_connected_extremum = surviving;
              is_strangulation = false;
            }

        };

        std::vector <std::vector<saddle_extremum_connection_info_t>> m_saddle_adjacent_cancelled_extrema_paths;
        std::vector <point_index_t>                              m_cancelled_cp_path_cp;

        persistence_map_t persistence_vals;
        persistence_map_t saturated_persistence_vals;
        arc_redirection_info_t arc_redirection_list;

        uint64_t min_path_len, max_path_len, num_paths;
        bool debug_simplification;

        const dataset_t * const m_ref_dataset_instance;

      public:

        extremum_graph_t (dataset_t * const dataset_instance);
        ~extremum_graph_t ();

        chunk_rect_t  get_dataset_rect () const noexcept { return m_dataset_rect; }

        void          add_maximum  (const point_index_t maximum_index);
        void          add_saddle   (const point_index_t saddle_index);

        bool          add_maximum_saddle_map  (const point_index_t maximum_index, const point_index_t saddle_index );
        bool          add_saddle_maximum_map  (const point_index_t saddle_index , const point_index_t maximum_index);

        bool          add_maximum_ordinal_saddle_map  (const uint64_t maximum_ordinal, const point_index_t saddle_index );
        bool          add_saddle_ordinal_maximum_map  (const uint64_t saddle_ordinal , const point_index_t maximum_index);

        uint64_t      maxima_count () const noexcept { return m_chunk_maxima.size(); }
        uint64_t      saddle_count () const noexcept { return m_chunk_saddles.size(); }

        point_index_t       get_maximum_index (const uint64_t maximum_ordinal) const { return m_chunk_maxima[maximum_ordinal]; }
        point_index_t       get_saddle_index  (const uint64_t saddle_ordinal ) const { return m_chunk_saddles[saddle_ordinal]; }

        uint64_t get_maximum_ordinal (const point_index_t maximum_index) const;
        uint64_t get_saddle_ordinal (const point_index_t saddle_index) const;

        std::vector <point_index_t> get_maximas () const;
        std::vector <point_index_t> get_saddles () const;

        bool is_maximum (const point_index_t maximum_index) const { return std::binary_search(m_chunk_maxima.begin(), m_chunk_maxima.end(), maximum_index); }
        bool is_saddle  (const point_index_t saddle_index ) const { return std::binary_search(m_chunk_saddles.begin(), m_chunk_saddles.end(), saddle_index ); }

        bool erase_saddle (const uint64_t saddle_ordinal);
        bool saddle_exists (const uint64_t saddle_ordinal) const noexcept;
        bool remove_sadle_maximum_mapping (const uint64_t saddle_ordinal, const uint64_t maximum_ordinal);

        void init_after_setup ();
        void clear () noexcept;

        void perform_edge_bundling                   ();
        void persistence_simplification_compute      (persistence_map_t * const persistence_vals, persistence_map_t * const saturated_persistence_vals, arc_redirection_info_t * const arc_redirection_list, critical_point_map_t * const adjacency_map);
        void persistence_simplification_cancellation (const persistence_map_t * const persistence_vals, const persistence_map_t * const saturated_persistence_vals, const arc_redirection_info_t * const arc_redirection_list, critical_point_map_t * const adjacency_map, const critical_point_map_t * const original_adjacency_map, const func_val_t pers_low, const func_val_t pers_high, const bool normalized_thresholds = true);
        void persistence_simplification              (const func_val_t saturation_threshold = 0.0f, const func_val_t threshold = 1.0f, const bool normalized_thresholds = true);
        void persistence_plot_info_gather            (const persistence_map_t * const persistence_vals, const persistence_map_t * const saturated_persistence_vals, const arc_redirection_info_t * const arc_redirection_list, const critical_point_map_t * const adjacency_map, func_val_t p_lo_start, func_val_t p_lo_end, func_val_t p_lo_increments, func_val_t p_hi_start, func_val_t p_hi_end, func_val_t p_hi_decrements, std::vector <uint64_t> * const arc_count, std::vector <uint64_t>* const extrema_count, std::vector <uint64_t>* const saddle_count, const bool normalized_thresholds = true);

        void std_persistence_based_cancellation_v2 (
          const func_val_t  pers_low,
          const bool        normalized_thresholds = true
         );

        void std_persistence_based_cancellation_v1 (
          const func_val_t  pers_low,
          const bool        normalized_thresholds = true
         );

        void std_persistence_based_cancellation (
          const func_val_t  pers_low,
          const bool        normalized_thresholds = true
         );

        int dump_field_as_csv    (const char* filename) const;
        int dump_geometry_as_vtp (const arc_redirection_info_t * const arc_redirection_list, const std::string& filename, const uint8_t direction=0b11);
        int dump_geometry_as_vtp_v1 (const arc_redirection_info_t * const arc_redirection_list, const std::string& filename, const uint8_t direction=0b11) const;
        int dump_geometry_as_vtp_v2 (const arc_redirection_info_t * const arc_redirection_list, const std::string& filename, const uint8_t direction=0b11);

    };

  void print_adjacency_graph (const critical_point_map_t * const adjacency_map, const std::vector <point_index_t> * const m_chunk_maxima, const std::vector <point_index_t> * const m_chunk_saddles);
  void create_adjacency_graph (critical_point_map_t * const adjacency_map, const extremum_graph_t * const ext_graph);

 };

#endif // EXTREMUM_GRAPH_HPP
