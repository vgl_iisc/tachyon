/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#include <grid.hpp>
#include <extremum_graph.hpp>
#include <CUDA/cuda_defs.h>

#include <iostream>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

using namespace extgraph;

typedef enum {
    EDGE_BUNDLING = 0,
    PERSISTENCE_DIRECTED_CANCELLATION = 1,
    SATURATED_PERSISTENCE_DIRECTED_SIMPLIFICATION = 2
} simplification_task_type;

typedef struct {
    simplification_task_type task_type;

    // For cancellation task
    func_val_t threshold;

    // For simplification task
    func_val_t p_lo, p_hi;
    bool should_normalize;
} simplification_task_data_t;

std::string input_filename = "";
std::string input_element_type = "f32";
std::vector <dimension_span_t> input_dimensions;
std::string output_filename = "";
std::vector <simplification_task_data_t> simplification_tasks;
bool negate_func_val = false;

void parse_args (int argc, char** argv);

int main (int argc, char** argv)
 {
  parse_args(argc, argv);

  const dimensions_t dim(input_dimensions.data(), input_dimensions.size());

  dataset_t dataset_handle(input_filename, dim, input_element_type);
  dataset_handle.set_endianness(false);  // Set if dataset endianness is different from machine
  dataset_handle.negate_fn_value(negate_func_val); // Set true for minimal-extremum graph (Minima + 1-saddle extremum graph)

  // Create bounded box for dataset domain
  const grid_point_t start_pt = grid_point_t::zero(dim.size());
  const grid_point_t end_pt = dim;
  const chunk_rect_t dataset_rect = chunk_rect_t(start_pt, end_pt);

  if (dataset_handle.load_chunk(dataset_rect)) {
      fprintf(stderr, "error: Failed to load dataset: %s\n", strerror(errno));
      exit(-1);
  }

  // Initialize and compute extremum graph
  extremum_graph_t ext_graph(&dataset_handle);
  dataset_handle.compute_ext_graph(&ext_graph);

  persistence_map_t persistence_vals;                // Size == num_maxima + num_saddles
  persistence_map_t saturated_persistence_vals;      // Size == num_saddles
  critical_point_map_t adjacency_map, tmp_adjacency_map; // Size == num_maxima + num_saddles
  arc_redirection_info_t arc_redirection_list;
  bool should_perform_simplification_compute;

  printf("%zu graph mutation tasks specified\n\n", simplification_tasks.size());

  for (uint32_t taskIdx=0; taskIdx<simplification_tasks.size(); ++taskIdx) {
      simplification_task_data_t task = simplification_tasks[taskIdx];
      printf("Task [%u/%lu]\n", taskIdx+1, simplification_tasks.size());

      switch (task.task_type) {
          case EDGE_BUNDLING:
              ext_graph.perform_edge_bundling();
              break;

          case PERSISTENCE_DIRECTED_CANCELLATION:
              ext_graph.std_persistence_based_cancellation(task.threshold, task.should_normalize);
              break;

          case SATURATED_PERSISTENCE_DIRECTED_SIMPLIFICATION:
              should_perform_simplification_compute = false;

              if (
                  taskIdx == 0 || // First graph modification task
                  (simplification_tasks[taskIdx - 1].task_type != SATURATED_PERSISTENCE_DIRECTED_SIMPLIFICATION) // Previous task was not a "saturated persistence based simplification"
              )
                should_perform_simplification_compute = true;

              if (should_perform_simplification_compute) {
                  persistence_vals.clear();
                  saturated_persistence_vals.clear();
                  arc_redirection_list.clear();
                  adjacency_map.clear();
                  ext_graph.persistence_simplification_compute(&persistence_vals, &saturated_persistence_vals, &arc_redirection_list, &adjacency_map);
              }

              ext_graph.persistence_simplification_cancellation(&persistence_vals, &saturated_persistence_vals, &arc_redirection_list, &tmp_adjacency_map, &adjacency_map, task.p_lo, task.p_hi, task.should_normalize);
              break;

          default:
              fprintf(stderr, "error: Unknown graph mutation task %d\n", task.task_type);
              exit(-1);
      }

    puts("");
  }

  arc_redirection_list.init(ext_graph.m_chunk_maxima.size(), ext_graph.m_chunk_saddles.size());

  if (output_filename != "/dev/null") // Skip output file generation
    ext_graph.dump_geometry_as_vtp(&arc_redirection_list, output_filename);

  return 0;
}

int count_non_option_args (int argc, char** argv, int startIdx) {
    int count = 0;

    if (startIdx >= argc || argv[startIdx] == nullptr || argv[startIdx][0]!='-')
        return -1;

    startIdx += 1; // Ignore first arg, it should be an option

    while (startIdx <= argc) {
        const char* arg = argv[startIdx];
        if (arg == nullptr || arg[0]=='-')
            break;

        count += 1;
        startIdx += 1;
    }

    return count;
}

int ensure_args (int argc, char** argv, int startIdx, int count) {

    if (startIdx >= argc || argv[startIdx] == nullptr || argv[startIdx][0]!='-')
        return -1;

    startIdx += 1; // Ignore first arg, it should be an option

    while (startIdx <= argc && argv[startIdx] != nullptr && count > 0) {
        count -= 1;
        startIdx += 1;
    }

    if (count == 0)
        return (startIdx < argc && argv[startIdx][0] == '-') ? 1 : 0;
    else
        return -1;
}

bool validate_input_type (const char* dtype) {
    return
        !strncasecmp("u8" , dtype, 2) || !strncasecmp("i8" , dtype, 2) ||
        !strncasecmp("u16", dtype, 3) || !strncasecmp("i16", dtype, 3) ||
        !strncasecmp("u32", dtype, 3) || !strncasecmp("i32", dtype, 3) ||
        !strncasecmp("u64", dtype, 3) || !strncasecmp("i64", dtype, 3) ||
        !strncasecmp("f32", dtype, 3) || !strncasecmp("f64", dtype, 3);

}

void help() {
    printf(
            "tachyon\n" \
            "\t-i, -in  \tInput filename\n" \
            "\t-t, -type\tInput scalar element type: One of [u8, i8, u16, i16, u32, i32, u64, i64, f32, f64]\n" \
            "\t-d, -dim \tInput data dimensionality: Integer list e.g.: 64 64 64\n" \
            "\t-o, -out \tOutput filename\n" \
            "\t[-m, -minimal]\tCompute minimal extremum graphs\n" \
            "\t[-p, -cores]\tLimit utilized CPU cores to provided value" \
            "\t[-g, -gpu]\tProvide a 0-indexed value for which GPU to utilize (for multi-gpu machines)"
            "\n" \
            "Graph mutation algorithms, invoked in provided order\n" \
            "\t-b, -bundle\n" \
            "\t       Edge/Arc bundling\n" \
            "\t-c, -cancel <threshold> [normalize: (0|false)|(1|true)]\n" \
            "\t       Persistence driven cancellation\n" \
            "\t-s, -simplify <p_lo> <p_hi> [normalize: (0|false)|(1|true)]\n" \
            "\t       Saturated persistence driven simplification\n" \
            "\t-h  This help message\n" \
    );

    exit(0);
}

void parse_args (int argc, char** argv) {
    bool input_set = false;
    bool input_type_set = false;
    bool input_dim_set = false;
    bool output_set = false;

    char* tmp;
    int cancellation_task_count = 0;
    int simplification_task_count = 0;

    if (argc == 0)
        exit(-1);

    for (int argIdx=0; argIdx<=argc; ++argIdx) {
        const char* arg = argv[argIdx];

        if (arg == nullptr)
            break;

        else if (!strncmp("-h", arg, 1 + 1)) {
            help();
        }

        else if (!strncmp("-i", arg, 1 + 1) || !strncmp("-in", arg, 1 + 2)) {
            int count = count_non_option_args(argc, argv, argIdx);
            if ((argIdx+1) < argc && argv[argIdx+1] != nullptr && !strcmp("-", argv[argIdx+1]))
                count = 1;

            if (count != 1) {
                fprintf(stderr, "error: Input filename not specified\n");
                exit(-1);
            }
            input_filename = std::string(argv[argIdx+1]);
            if (input_filename == "-")
                input_filename = "/dev/stdin";
            argIdx += count;
            input_set = true;
        }

        else if (!strncmp("-t", arg, 1 + 1) || !strncmp("-type", arg, 1 + 4)) {
            int count = count_non_option_args(argc, argv, argIdx);
            if (count != 1) {
                fprintf(stderr, "error: Input data element type not specified\n");
                exit(-1);
            }

            input_element_type = std::string(argv[argIdx+1]);

            if (!validate_input_type(argv[argIdx+1])) {
                fprintf(stderr, "error: Invalid element type '%s'\n", argv[argIdx+1]);
                exit(-1);
            }

            #if !VAL_TYPE_DOUBLE
            if (!strncasecmp(argv[argIdx+1], "f64", 3)) {
                fprintf(stderr, "Support for double/f64 type (double-precision floating point value) requires additional configuration during compile time\n");
                fprintf(stderr, "Please recompile with \"cmake (...) -DVAL_TYPE_DOUBLE=1\"\n");
                exit(-1);
            }
            #endif

            argIdx += count;
            input_type_set = true;
        }

        else if (!strncmp("-d", arg, 1 + 1) || !strncmp("-dim", arg, 1 + 3)) {

            int count = 1;

            while (argIdx <= argc && argv[argIdx + count] != nullptr) {
                const char * dimArg = argv[argIdx + count];
                int64_t v = strtol(dimArg, &tmp, 10);
                const uint64_t parseLength = tmp - dimArg;

                if (dimArg[0] == '-') {
                    count -= 1;
                    break;
                }

                if (parseLength != strlen(dimArg) || v <= 0 || v >= UINT32_MAX) {
                    if (v <= 0 || v >= UINT32_MAX)
                        fprintf(stderr, "Condition 0 < span(%li) < UINT32_MAX(%u)", v, UINT32_MAX);
                    else
                        fprintf(stderr, "error: Invalid dimension span '%s' at position %d\n", dimArg, count);
                    exit(-2);
                }

                input_dimensions.push_back(v);
                count += 1;
            }

            if (input_dimensions.size() > MAX_DIMENSIONS) {
                fprintf(stderr, "Support for higher dimensions requires additional configuration during compile time\n");
                fprintf(stderr, "Please recompile with \"cmake (...) -DMAX_DIMENSIONS=%lu\"\n", input_dimensions.size());
                exit(-1);
            }

            argIdx += count;
            input_dim_set = true;
        }

        else if (!strncmp("-m", arg, 1 + 1) || !strncmp("-minimal", arg, 1 + 6)) {
            negate_func_val = true;
        }

        else if (!strncmp("-o", arg, 1 + 1) || !strncmp("-out", arg, 1 + 3)) {
            int count = count_non_option_args(argc, argv, argIdx);
            if (count != 1) {
                fprintf(stderr, "error: Output filename not specified\n");
                exit(-1);
            }
            output_filename = std::string(argv[argIdx+1]);
            argIdx += count;
            output_set = true;
        }

        else if (!strncmp("-b", arg, 2) || !strncmp("-bundle", arg, 1 + 6)) {
            simplification_task_data_t taskData;
            taskData.task_type = EDGE_BUNDLING;
            simplification_tasks.push_back(taskData);
        }

        else if (!strncmp("-c", arg, 2) || !strncmp("-cancel", arg, 1 + 6)) {
            cancellation_task_count += 1;
            std::string canc = "cancellation";
            if (cancellation_task_count > 1) {
                canc += "-";
                canc += std::to_string(cancellation_task_count);
            }

            int count1 = ensure_args(argc, argv, argIdx, 1);
            int count2 = ensure_args(argc, argv, argIdx, 2);

            if (count1 != 0 && count2 < 0) {
                if (count1 < 0)
                    fprintf(stderr, "error: Threshold value not provided for %s\n", canc.c_str());
                else
                    fprintf(stderr, "error: Extra arguments provided for %s\n", canc.c_str());
                exit(-1);
            }

            int count = 1;
            bool should_normalize = true;

            const char * thresholdArg = argv[argIdx+1];
            func_val_t threshold = strtof(thresholdArg, &tmp);
            const uint64_t parseLength = tmp - thresholdArg;

            if (thresholdArg == tmp || parseLength != strlen(thresholdArg)) {
                fprintf(stderr, "error: Invalid threshold value provided for %s\n", canc.c_str());
                exit(-4);
            }

            if (count2 >= 0) {
                const char * shouldNormalizeArg = argv[argIdx+2];
                if (!strncasecmp("true", shouldNormalizeArg, 4) || !strncasecmp("1", shouldNormalizeArg, 1)) {
                    should_normalize = true;
                    count += 1;
                }
                else if (!strncasecmp("false", shouldNormalizeArg, 5) || !strncasecmp("0", shouldNormalizeArg, 1)) {
                    should_normalize = false;
                    count += 1;
                }
            }

            if (should_normalize && (threshold < 0 || threshold > 1)) {
                fprintf(stderr, "error: Condition 0 <= threshold(%f) <= 1 not satisfied for %s\n", threshold, canc.c_str());
                exit(-1);
            }

            simplification_task_data_t taskData;
            taskData.task_type = PERSISTENCE_DIRECTED_CANCELLATION;
            taskData.threshold = threshold;
            taskData.should_normalize = should_normalize;
            simplification_tasks.push_back(taskData);
            argIdx += count;
        }

        else if (!strncmp("-s", arg, 2) || !strncmp("-simplify", arg, 1 + 8)) {
            simplification_task_count += 1;
            std::string simpl = "simplification";
            if (cancellation_task_count > 1) {
                simpl += "-";
                simpl += std::to_string(simplification_task_count);
            }

            int count2 = ensure_args(argc, argv, argIdx, 2);
            int count3 = ensure_args(argc, argv, argIdx, 3);

            if (count2 !=0 && count3 < 0) {
                if (count2 < 0)
                    fprintf(stderr, "error: Insufficient arguments provided for %s\n", simpl.c_str());
                else
                    fprintf(stderr, "error: Too many arguments provided for %s\n", simpl.c_str());
                exit(-5);
            }

            int count = 2;

            const char * pLoArg = argv[argIdx+1];
            const char * pHiArg = argv[argIdx+2];
            bool should_normalize = true;

            func_val_t p_lo = strtof(pLoArg, &tmp);
            uint64_t parseLength = tmp - pLoArg;
            if (parseLength != strlen(pLoArg)) {
                fprintf(stderr, "error: Invalid p_lo value provided for %s\n", simpl.c_str());
                exit(-6);
            }

            func_val_t p_hi = strtof(pHiArg, &tmp);
            parseLength = tmp - pHiArg;
            if (parseLength != strlen(pHiArg)) {
                fprintf(stderr, "error: Invalid p_hi value provided for %s\n", simpl.c_str());
                exit(-7);
            }

            if (count3 >= 0) {
                const char * shouldNormalizeArg = argv[argIdx+3];
                if (!strncasecmp("true", shouldNormalizeArg, 4) || !strncasecmp("1", shouldNormalizeArg, 1)) {
                    should_normalize = true;
                    count += 1;
                }
                else if (!strncasecmp("false", shouldNormalizeArg, 5) || !strncasecmp("0", shouldNormalizeArg, 1)) {
                    should_normalize = false;
                    count += 1;
                }
            }

            if (p_lo > p_hi) {
                fprintf(stderr, "error: Condition p_lo(%f) <= p_hi(%f) not satisfied for %s\n", p_lo, p_hi, simpl.c_str());
                exit(-1);
            }
            else if (should_normalize && (p_lo < 0 || p_hi > 1)) {
                fprintf(stderr, "error: Condition 0 <= p_lo(%f) <= p_hi(%f) <= 1 not satisfied for %s\n", p_lo, p_hi, simpl.c_str());
                exit(-1);
            }

            simplification_task_data_t taskData;
            taskData.task_type = SATURATED_PERSISTENCE_DIRECTED_SIMPLIFICATION;
            taskData.p_lo = p_lo;
            taskData.p_hi = p_hi;
            taskData.should_normalize = should_normalize;
            simplification_tasks.push_back(taskData);
            argIdx += count;
        }

        else if (!strncmp("-p", arg, 1 + 1) || !strncmp("-cores", arg, 1 + 5)) {
            int count = count_non_option_args(argc, argv, argIdx);
            if (count != 1) {
                fprintf(stderr, "error: CPU core limit not specified\n");
                exit(-1);
            }

            const char * limitArg = argv[argIdx+1];
            int limit = strtof(limitArg, &tmp);
            const uint64_t parseLength = tmp - limitArg;

            if (limitArg == tmp || parseLength != strlen(limitArg)) {
                fprintf(stderr, "error: Invalid CPU core limit value provided\n");
                exit(-4);
            }

            properties::m_num_threads = limit;
            argIdx += count;
        }

        else if (!strncmp("-g", arg, 1 + 1) || !strncmp("-gpu", arg, 1 + 3)) {
            int count = count_non_option_args(argc, argv, argIdx);
            if (count != 1) {
                fprintf(stderr, "error: GPU selection not specified\n");
                exit(-1);
            }

            #if CUDA_SUPPORT || 1
            const char * selectionArg = argv[argIdx+1];
            int selection = strtof(selectionArg, &tmp);
            const uint64_t parseLength = tmp - selectionArg;

            if (selectionArg == tmp || parseLength != strlen(selectionArg)) {
                fprintf(stderr, "error: Invalid GPU selection value provided\n");
                exit(-4);
            }

            int gpuCount = -1;
            CUDA_SAFE_CALL(cudaGetDeviceCount(&gpuCount));

            if (selection >= gpuCount) {
                fprintf(stderr, "error: %d GPUs available, but selection was %d\n", gpuCount, selection);
                exit(-5);
            }

            properties::m_gpu_id = selection;
            #endif

            argIdx += count;
        }

    }

    if (!input_set || !input_type_set || !input_dim_set || !output_set)
        help();

}
