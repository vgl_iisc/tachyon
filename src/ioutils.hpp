/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#ifndef IOUTILS_HPP
#define IOUTILS_HPP

#include <grid.hpp>

#include <iostream>
#include <sstream>
#include <cstring>

#include <unistd.h>
#include <fcntl.h>

namespace extgraph {

  inline bool write_fd (int fd, const void* buf, ssize_t count)
   {
    return write(fd,buf,count) == count;
   }

  inline bool read_fd (int fd, void* buf, ssize_t count)
   {
    return read(fd,buf,count) == count;
   }

  template <typename T, class Iterator>
  inline ssize_t write_container_to_stream (int fd, Iterator it, const uint64_t count)
   {
    T t;

    if (!write_fd(fd,(const void*)&count,sizeof(count)))
      return -1;

    for (uint64_t i=0; i<count; ++i,++it)
     {
      t = *it;
      if (!write_fd(fd,(const void*)&t,sizeof(T)))
        return i+1;
     }

    return 0;
   }

  template <typename T, class Container>
  inline ssize_t read_container_from_stream (int fd, std::insert_iterator<Container> ins_it)
   {
    T t;
    uint64_t count;

    if (!read_fd(fd,(void*)&count,sizeof(count)))
      return -1;

    for (uint64_t i=0; i<count; ++i,++ins_it)
     {
      if (!read_fd(fd,(void*)&t,sizeof(T)))
        return i+1;

      ins_it = t;
     }

    return 0;
   }

  template <typename T>
  inline func_val_t convert_to_fn_val (const char * const buf, const char dtype, const uint8_t dsize_bytes, bool swap_element_bytes)
   {
    T t = *((const T*)buf); // Works for big-endianness / i8 / u8

    if (!swap_element_bytes)
      return t;

    union { char buf[2]; uint16_t v; } u_16;
    union { char buf[4]; uint32_t v; } u_32;
    union { char buf[8]; uint64_t v; } u_64;

    union { char buf[4]; float    v; } f_32;
    union { char buf[8]; double   v; } f_64;

    switch (dsize_bytes)
     {
      case  2 :
                memcpy(u_16.buf,buf,dsize_bytes);
                t = u_16.v;
                break;

      case  4 :
                if (dtype=='f') {
                  memcpy(f_32.buf,buf,dsize_bytes);
                  t = f_32.v;
                }
                else {
                  memcpy(u_32.buf,buf,dsize_bytes);
                  t = u_32.v;
                }
                break;

      case  8 :
                if (dtype=='f') {
                  memcpy(f_64.buf,buf,dsize_bytes);
                  t = f_64.v;
                }
                else {
                  memcpy(u_64.buf,buf,dsize_bytes);
                  t = u_64.v;
                }
                break;

     }

    return (func_val_t)t;
   }

  template <typename UT, typename ST>
  inline func_val_t convert_to_fn_val_signed (const char * const buf, const char dtype, const uint8_t dsize_bytes, bool swap_element_bytes, const char unsigned_char, const char signed_char)
   {

    if (dtype==unsigned_char)
      return convert_to_fn_val<UT>(buf,dtype,dsize_bytes,swap_element_bytes);

    else if (dtype == signed_char)
      return convert_to_fn_val<ST>(buf,dtype,dsize_bytes,swap_element_bytes);

    else {
      std::stringstream ss;
      ss << "Invalid datatype '"<<dtype<<"'. Must be among \"iuf\".\n";
      const std::string error_string = ss.str();

      std::cerr<<error_string;
      throw std::invalid_argument(error_string);
    }
   }

  template <typename T>
  inline void swapp (T& a, T& b) {
      T t = a;
      a = b;
      b = t;
  }

  inline func_val_t data_conversion (const index_t i, const char * const buf, const char dtype, const uint8_t dsize_bytes, bool swap_element_bytes)
   {
    (void)i;
    func_val_t val = (func_val_t)-1;

    switch (dsize_bytes)
     {
       case  1 :
                  val = convert_to_fn_val_signed <uint8_t,    int8_t> (buf,dtype,dsize_bytes,swap_element_bytes,'u','i');
                  break;

       case  2 :
                  val = convert_to_fn_val_signed <uint16_t,  int16_t> (buf,dtype,dsize_bytes,swap_element_bytes,'u','i');
                  break;

       case  4 :
                  if (dtype=='f')
                      val = convert_to_fn_val_signed <float,      float> (buf,'f',dsize_bytes,swap_element_bytes,'_','f');
                  else
                      val = convert_to_fn_val_signed <uint32_t, int32_t> (buf,dtype,dsize_bytes,swap_element_bytes,'u','i');
                  break;

       case 8 :
                  if (dtype=='f')
                      val = convert_to_fn_val_signed <double,    double> (buf,'f',dsize_bytes,swap_element_bytes,'_','f');
                  else
                      val = convert_to_fn_val_signed <uint64_t, int64_t> (buf,dtype,dsize_bytes,swap_element_bytes,'u','i');
                  break;

       default :
                  std::stringstream ss;
                  ss << "Invalid datatype size [" << dsize_bytes << "]\n";

                  const std::string error_string = ss.str();

                  std::cerr<<error_string;
                  throw std::invalid_argument(error_string);
     }

    return val;
   }

  template <typename T>
  inline void swap_byte_endianness (T* val)
   {
    char * b = (char*) val;
    for (uint8_t i=0; i<sizeof(T)/2; ++i)
      swapp(b[i],b[sizeof(T)-1-i]);
   }

  #if USE_OLD_PARSER==0
  template <typename T>
  inline bool block_read_from_file (const int fd, func_val_t* dest, const uint64_t offset, const uint64_t num_bytes, const uint64_t num_elements, bool should_swap_byte_endianness, char* buffer)
   {
    if (read(fd, buffer, num_bytes) != num_bytes)
      return false;

    for (uint64_t i=0; i<num_elements; ++i)
      dest[offset+i] = ((T*)buffer)[i];

    if (should_swap_byte_endianness)
      for (uint64_t i=0; i<num_elements; ++i)
        swap_byte_endianness(&dest[offset+i]);

    return true;
   }

  inline int64_t read_from_file (func_val_t* dest, uint64_t num_elements, const std::string filename, off64_t seek_pos, const char dtype, uint8_t element_bytes, bool should_swap_byte_endianness)
   {
    int fd = open(filename.c_str(),O_RDONLY | O_LARGEFILE);
    assert(fd>=0);

    lseek(fd, SEEK_SET, seek_pos);

    const uint64_t block_size = 4096;
    const uint64_t num_total_bytes = element_bytes * num_elements;
    const uint64_t num_blocks = num_total_bytes / block_size;

    const uint64_t num_func_per_block  = block_size / sizeof(func_val_t);
    const uint64_t num_dtype_per_block = block_size / element_bytes;

    char* buffer = new char [block_size];
    bool flag;

    for (uint64_t block_index=0; block_index<num_blocks; ++block_index) {
        switch (dtype) {

          case 'u':

              switch (element_bytes) {

                case 1:
                  flag = block_read_from_file<uint8_t>(fd, dest, block_index*block_size, block_size, num_dtype_per_block, should_swap_byte_endianness, buffer);
                  break;

                case 2:
                  flag = block_read_from_file<uint16_t>(fd, dest, block_index*block_size, block_size, num_dtype_per_block, should_swap_byte_endianness, buffer);
                  break;

                case 4:
                  flag = block_read_from_file<uint32_t>(fd, dest, block_index*block_size, block_size, num_dtype_per_block, should_swap_byte_endianness, buffer);
                  break;

                case 8:
                  flag = block_read_from_file<uint64_t>(fd, dest, block_index*block_size, block_size, num_dtype_per_block, should_swap_byte_endianness, buffer);
                  break;
              }

              break;

          case 'i':

              switch (element_bytes) {

                case 1:
                  flag = block_read_from_file<int8_t>(fd, dest, block_index*block_size, block_size, num_dtype_per_block, should_swap_byte_endianness, buffer);
                  break;

                case 2:
                  flag = block_read_from_file<int16_t>(fd, dest, block_index*block_size, block_size, num_dtype_per_block, should_swap_byte_endianness, buffer);
                  break;

                case 4:
                  flag = block_read_from_file<int32_t>(fd, dest, block_index*block_size, block_size, num_dtype_per_block, should_swap_byte_endianness, buffer);
                  break;

                case 8:
                  flag = block_read_from_file<int64_t>(fd, dest, block_index*block_size, block_size, num_dtype_per_block, should_swap_byte_endianness, buffer);
                  break;
              }

              break;

          case 'f':
              flag = block_read_from_file<float>(fd, dest, block_index*block_size, block_size, num_dtype_per_block, should_swap_byte_endianness, buffer);
              break;

          case 'd':
              flag = block_read_from_file<double>(fd, dest, block_index*block_size, block_size, num_dtype_per_block, should_swap_byte_endianness, buffer);
              break;
        }

        if (flag)
          return -block_index;
    }

    close(fd);
    return 0;
   }

  #endif

};


#endif // IOUTILS_HPP
