/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#ifndef MEMUTILS_HPP
#define MEMUTILS_HPP

#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/sysinfo.h>

namespace extgraph
 {

  inline uint64_t get_max_mem_usage ()
   {
    struct rusage usage;
    getrusage(RUSAGE_SELF, &usage);
    return ((uint64_t)usage.ru_maxrss) * 1024;
   }

  inline std::string prettify_memory_value (uint64_t mem) noexcept
   {

    const uint64_t mem_bytes  =    mem      & 1023;
    const uint64_t mem_kbytes = ( (mem>>10) & 1023);
    const uint64_t mem_mbytes = ( (mem>>20) & 1023);
    const uint64_t mem_gbytes = mem >> 30; // ( (mem>>30) & 0x3ff);

    std::string mem_str = "";
    char buf[64];

    if (mem_gbytes>0) {
      snprintf(buf, 63, "%luGB ", mem_gbytes);
      mem_str += buf;
    }

    if (mem_mbytes>0) {
      snprintf(buf, 63, "%luMB ", mem_mbytes);
      mem_str += buf;
    }

    if (mem_kbytes>0) {
      snprintf(buf, 63, "%luKB ", mem_kbytes);
      mem_str += buf;
    }

    if (mem_bytes>0) {
      snprintf(buf, 63, "%luB ", mem_bytes);
      mem_str += buf;
    }

    if (mem_str[mem_str.size()-1] == ' ') {
      mem_str = mem_str.substr(0, mem_str.size()-1);
    }

    return mem_str;
   }

  inline void pretty_print_max_mem_usage (FILE* file, const char* prefix, const char* suffix) noexcept
   {
   	uint64_t mem = get_max_mem_usage();

   	uint64_t mem_bytes  =    mem      & 1023;
   	uint64_t mem_kbytes = ( (mem>>10) & 1023);
   	uint64_t mem_mbytes = ( (mem>>20) & 1023);
   	uint64_t mem_gbytes = mem >> 30; // ( (mem>>30) & 0x3ff);

   	fprintf(file, "%s", prefix);

   	if (mem_gbytes>0)
	   	fprintf(file, "%luGB ", mem_gbytes);

   	if (mem_mbytes>0)
	   	fprintf(file, "%luMB ", mem_mbytes);

   	if (mem_kbytes>0)
	   	fprintf(file, "%luKB ", mem_kbytes);

   	if (mem_bytes>0)
	   	fprintf(file, "%luB ", mem_bytes);

   	fprintf(file, "| %lu KB%s", mem/1024UL, suffix);
   }

  inline void pretty_print_max_mem_usage (const char* prefix="") noexcept
   {
   	pretty_print_max_mem_usage(stdout, prefix, "\n");
   }

  inline uint64_t get_page_size ()
   {
    return ((uint64_t)sysconf(_SC_PAGESIZE));
   }

  inline uint64_t get_available_pages ()
   {
    return ((uint64_t)sysconf(_SC_AVPHYS_PAGES));
   }

  inline uint64_t get_physical_pages ()
   {
    return ((uint64_t)sysconf(_SC_PHYS_PAGES));
   }

  inline uint64_t get_max_free_memory ()
   {
//    Includes available + buffer pages
    struct sysinfo info;
    sysinfo(&info);
    uint64_t freemem = info.freeram;
    freemem += info.bufferram;
    return freemem * info.mem_unit;
   }

  inline uint64_t get_free_memory ()
   {
    return get_page_size() * get_available_pages();
   }

  inline uint64_t get_total_memory ()
   {
    return get_page_size() * get_physical_pages();
   }

 };

#endif // MEMUTILS_HPP
