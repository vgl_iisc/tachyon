/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#ifndef GRID_DATASET_HPP
#define GRID_DATASET_HPP

#include <grid.hpp>
#include <tuple>

namespace extgraph
 {

   class extremum_graph_t;

   class dataset_t
    { 
       friend class extremum_graph_t;

       public:

         uint64_t          m_instance_id;

         std::string       m_dataset_filename;
         dimensions_t      m_dataset_dimensions;
         bool              m_dataset_negate_fn_val;
         bool              m_dataset_endianness;

         size_t            m_num_dimensions;
         chunk_rect_t      m_dataset_rect;
         char              m_dataset_dtype;
         uint8_t           m_element_bytes;
         std::string       m_workdir;

         void          set_instance_id ();
         void          init (const std::string& filename, const dimensions_t& dims, bool is_little_endian, const std::string& dtype);

       public:

         func_val_t*                                          m_chunk_fn_vals;
         func_val_t                                           m_chunk_fn_range_size;
         chunk_rect_t                                         m_chunk_rect;

         typedef struct {

           bool invert_fn_val = false;
           bool use_gpu;

//           plan_t :: plan_item_t chunk_plan_item;

           chunk_rect_t   *bounding_rect, *bounding_cumulative_rect;
           global_index_t bound_start_index, bound_end_index;
           global_index_t compute_start_index, compute_end_index;
           uint64_t func_val_neg_offset;

           func_val_t *device_func_vals = nullptr;
           func_val_t *func_vals        = nullptr;

           uint8_t *device_point_index_array = nullptr;
           uint8_t *point_index_array        = nullptr;

           neighbour_bitmap_t *device_edge_connectivity_array = nullptr;
           neighbour_bitmap_t *edge_connectivity_array        = nullptr;

           const chunk_rect_t  *dataset_chunk_rect             = nullptr;
           chunk_rect_t        *dataset_chunk_rect_device_copy = nullptr;
           point_neighbourhood *p_neighbours                   = nullptr;

           int  *point_contribution = nullptr;
           void *profiling_arr      = nullptr;

           uint64_t ext_graph_time             = 0;
           uint64_t point_classification_time  = 0;
           uint64_t kernel_time                = 0;
           uint64_t path_collection_time       = 0;
           uint64_t maxima_saddle_mapping_time = 0;
           uint64_t cuda_mem_transfer_time     = 0;
           uint64_t point_collection_time      = 0;

           uint64_t regular_pt_count      = 0;
           uint64_t unknown_crit_pt_count = 0;

           uint64_t chunk_maxima_count       = 0;
           uint64_t chunk_saddles_count      = 0;
           uint64_t chunk_1saddles_count     = 0;
           uint64_t chunk_minima_count       = 0;
           uint64_t chunk_multisaddles_count = 0;
           uint64_t chunk_count              = 0;

           int64_t  alternating_sum_computed = 0;

           int num_cpus;
//           std::vector <std::vector<std::pair<point_index_t, point_index_t>>> pending_saddle_maximum_path_tracing;
           std::vector <std::vector<std::tuple<point_index_t, point_index_t, uint64_t>>> pending_saddle_maximum_path_tracing;
           std::vector <uint64_t> aggregate_upper_link_counts;
           std::vector <uint64_t> path_len_totals;

           std::vector <point_index_t> unvisited_vertices;
           std::vector <uint64_t> min_path_len, max_path_len;
         } extremum_graph_computation_data_t;

        __CUDA_HOST_ONLY__ static int create_saddle_maxima_mappings (extremum_graph_computation_data_t& data, dataset_t * const dataset_instance, extremum_graph_t * const ext_graph, const int thread_idx, const int num_cpus);
        __CUDA_HOST_ONLY__ static int create_maxima_saddle_mappings (extremum_graph_computation_data_t& data, dataset_t * const dataset_instance, extremum_graph_t * const ext_graph, const int thread_idx, const int num_cpus);

        __CUDA_HOST_ONLY__ int  perform_point_classification_cpu (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph);
        __CUDA_HOST_ONLY__ int  perform_point_classification_gpu (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph);
        __CUDA_HOST_ONLY__ int  perform_point_collection         (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph);
        __CUDA_HOST_ONLY__ int  perform_path_tracing             (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph);
        __CUDA_HOST_ONLY__ void print_compute_debug              (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph);
//        __CUDA_HOST_ONLY__ void perform_init                     (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph);
        __CUDA_HOST_ONLY__ void perform_cleanup                  (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph, bool on_success);
        __CUDA_HOST_ONLY__ void post_compute_info                (extremum_graph_computation_data_t& data, extremum_graph_t * const ext_graph, bool post_full_compute = false);

       public:

         dataset_t (const std::string& filename, const dimensions_t& dims, const std::string& dtype="f32"); 

         ~dataset_t();

        void          set_workdir    (const std::string& workdir);
        void          set_endianness (bool is_little_endian);

        bool          get_workdir    () const; 
        bool          get_endianness () const; 

        void          negate_fn_value (bool negate);
        bool          is_fn_value_negated () const;

        int           load_chunk (const chunk_rect_t& rect);    // TODO: partially done
        int           unload_chunk ();                          // TODO: partially done
        bool          is_chunk_loaded () const;

        chunk_rect_t  get_chunk_rect () const;
        chunk_rect_t  get_dataset_rect () const;

        func_val_t    get_fn_val (grid_point_t pt) const;
        func_val_t    get_fn_val (point_index_t pt) const;

        bool          compute_ext_graph (extremum_graph_t * const ext_graph, bool use_gpu=CUDA_SUPPORT);

//        bool is_maximum (const point_index_t pt_index) const { return std::binary_search(m_chunk_maximas.begin(), m_chunk_maximas.end(), pt_index); }
//        bool is_saddle  (const point_index_t pt_index) const { return std::binary_search(m_chunk_saddles.begin(), m_chunk_saddles.end(), pt_index); }

        int dump_field_as_csv (const extremum_graph_t * const ext_graph, const std::string& filename) const;
        int dump_geometry_as_vtp (const char* filename) const;

        bool compute_chunk_func_range ();
        bool get_chunk_func_range     (func_val_t& range_size) const noexcept;
        bool get_dataset_func_range   (func_val_t& range_size);

__CUDA_HOST_ONLY__ void saddle_immediate_ascending_manifold (const extremum_graph_t * const ext_graph, const uint64_t saddle_ordinal,  std::vector<std::vector<point_index_t>> * const vector_of_paths) const;
        __CUDA_HOST_ONLY__ void saddle_ascending_manifold (const extremum_graph_t * const ext_graph, const uint64_t saddle_ordinal, std::vector<std::pair<uint64_t, uint64_t>> * const vector_of_edges);
        __CUDA_HOST_ONLY__ void maximum_decending_manifold (const extremum_graph_t * const ext_graph, const uint64_t maximum_ordinal, std::vector<std::pair<uint64_t, uint64_t>> * const vector_of_edges);

        static bool is_machine_little_endian ();

    };

  template <bool collect_path_points>
  point_index_t single_path_tracing (const extremum_graph_t * const ext_graph, const uint64_t exclude_region_start, const uint64_t exclude_region_end, const point_index_t start_pt_index, bool& partial_termination, uint64_t& path_len, std::vector <point_index_t> * const path_vector);

 };

#endif // GRID_DATASET_HPP
