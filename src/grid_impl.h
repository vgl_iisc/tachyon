/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#include <grid.hpp>
#include <utils.hpp>

#include <iostream>
#include <sstream>

#include <cstring>
#include <initializer_list>

#ifndef GRID_IMPL_H
#define GRID_IMPL_H

namespace extgraph {

  template <typename T>
  class uarray
   {

    protected:
  
      T      * m_arr;
      uint64_t m_size;

      const bool wrapping;

      template <typename> friend class uarray;

      #define UARRAY_MALLOC_DEBUG 0

      __CUDA_PREFIX__ T* uarray_malloc (const uint64_t num_elements)
       {
        if (0 == num_elements)
          return nullptr;
        else {
          #if UARRAY_MALLOC_DEBUG==1
            size_t s = num_elements * sizeof(T);
            printf("malloc(%ld)\n", s);
            T* p = (T*)malloc(s);
            printf("malloc returns: %p\n", p);
            return p;
          #else
            return (T*)malloc(num_elements * sizeof(T));
          #endif
        }
       }

      __CUDA_PREFIX__ inline void uarray_free ()
       {
        #if UARRAY_MALLOC_DEBUG==1
        printf("Freeing: %p\n", m_arr);
        #endif
        if (nullptr != m_arr && !wrapping) {
            free((void*)m_arr);
            m_arr = nullptr;
        }
       }

      __CUDA_PREFIX__ inline void init (uint64_t num_elements)
       {
        if (!wrapping)
         {
          m_size = num_elements;
          m_arr = uarray_malloc(m_size);
          for (uint64_t i=0; i<m_size; ++i) {
            new (m_arr + i) T(0);
          }
         }

       }  

    public:

      uarray () = delete;

      __CUDA_PREFIX__ uarray (uint64_t num_elements) : wrapping(false)
       {
        this->init(num_elements);
       }

      __CUDA_PREFIX__ uarray (const uarray<T>& other) : wrapping(false)
       {
        init(other.m_size);
        for (uint64_t i=0; i<m_size; ++i)
          this->m_arr[i] = other.m_arr[i];
       }

      __CUDA_PREFIX__ uarray (T* arr, uint64_t num_elements) : wrapping(true)
       {
        m_arr = arr;
        m_size = num_elements;
       }

      __CUDA_HOST_ONLY__ inline uarray (const std::initializer_list<T>& list) : wrapping(false)
       {
        init(list.size());
        uint64_t i = 0;

        for (T t : list)
            m_arr[i++] = t;
       }

      __CUDA_PREFIX__ ~uarray ()
       {
        for (uint64_t i=0; i<m_size; ++i)
          m_arr[i].~T();

        uarray_free();
       }

      __CUDA_PREFIX__ inline void reset (uint64_t new_size)
       {
        uarray_free();
        init(new_size);
       }

      __CUDA_PREFIX__ inline const T get (uint64_t index) const noexcept
       {
        return m_arr[index];
       }

      __CUDA_PREFIX__ inline T& at (uint64_t index) noexcept
       {
        return m_arr[index];
       }

      __CUDA_PREFIX__ inline T& operator[] (uint64_t index) noexcept
       {
        return m_arr[index];
       }
  
      __CUDA_PREFIX__ inline const T operator[] (uint64_t index) const noexcept
       {
        return m_arr[index];
       }

      __CUDA_PREFIX__ inline uint64_t size() const { return m_size; }
      __CUDA_PREFIX__ inline T*       arr () const { return m_arr;  }

      __CUDA_PREFIX__ static inline int element_comparator (const void* a, const void* b)
       {
        if (*((T*)a) > *((T*)b))  return 1;
        else if (*((T*)a) < *((T*)b)) return -1;
        else return 0;
       }

      __CUDA_PREFIX__ void print () const {
        printf("(");
        for (uint64_t i=0; i<m_size; ++i)
            printf(" %lld, ", ((long long int)m_arr[i]));
        printf(" )");
      }

      template <typename U, typename V, typename LambdaUVBool>
      __CUDA_PREFIX__ static bool uarray_cmp (const uarray <U>& u, const uarray <V>& v, const LambdaUVBool& comparator);

      template <typename U, typename V, typename W, typename LambdaUVW>
      __CUDA_PREFIX__ static uarray <W> uarray_op (const uarray <U>& u, const uarray <V>& v, const LambdaUVW& operation);

      __CUDA_PREFIX__ static uarray <T> zero (uint64_t size);

      //move assignment
      __CUDA_PREFIX__ inline uarray <T>& operator = (uarray <T>&& other)
       {
        if (this != &other) {

          uarray_free();
  
          this->m_arr = other.m_arr;
          this->m_size = other.m_size;
  
          other.m_arr = nullptr;
          other.m_size = 0;
        }

        return *this;
       } 

      __CUDA_PREFIX__ inline uarray <T>& operator = (const uarray <T>& other)
       {
        assert(!wrapping || ( wrapping && (m_size == other.m_size) ));

        uarray_free();
        init(other.m_size);

        for (uint64_t i=0; i<m_size; ++i)
          this->m_arr[i] = other.m_arr[i];

        return *this;
       } 

      template <typename U>
     __CUDA_PREFIX__ inline uarray <T>& operator = (const uarray <U>& other)
       {
        assert(!wrapping || ( wrapping && (m_size == other.m_size) ));

        uarray_free();
        init(other.m_size);

        for (uint64_t i=0; i<m_size; ++i)
          this->m_arr[i] = other.m_arr[i];

        return *this;
       }

      __CUDA_HOST_ONLY__ inline uarray <T>& operator = (const std::initializer_list<T>& list)
       {
        assert(!wrapping || ( wrapping && (m_size == list.size()) ));

        uarray_free();

        init(list.size());
        uint64_t i = 0;

        for (T t : list)
            m_arr[i++] = t;

        return *this;
       }

    template <typename U, typename V, bool last_return>
    __CUDA_PREFIX__ static inline bool uarray_cmp2 (const uarray <U>& u, const uarray <V>& v)
     {
      assert(u.size() == v.size());
 
      #if USE_Z_ORDER
      for (uint64_t i=0; i<u.size(); ++i) {
      #else
      for (uint64_t i=u.size()-1; i<u.size(); --i) {
      #endif
        if      (u[i] > v[i]) return false;
        else if (u[i] < v[i]) return true;
      }

      return last_return;
     }

    __CUDA_PREFIX__ inline bool operator < (const uarray <T>& pt) const
     {
      return uarray<T> :: uarray_cmp2<T, T, false>(*this, pt);
     }

    __CUDA_PREFIX__ inline bool operator <= (const uarray <T>& pt) const
     {
      return uarray<T> :: uarray_cmp2<T, T, true>(*this, pt);
     }

    __CUDA_PREFIX__ inline bool operator > (const uarray <T>& pt) const
     {
      return uarray<T> :: uarray_cmp2<T, T, false>(pt, *this);
     }

    __CUDA_PREFIX__ inline bool operator >= (const uarray <T>& pt) const
     {
      return uarray<T> :: uarray_cmp2<T, T, true>(pt, *this);
     }

    __CUDA_PREFIX__ inline bool operator == (const uarray <T>& pt) const
     {
      return uarray_cmp<T,T>(*this, pt, [](T x,T y) -> bool { return x == y; });
     }

    __CUDA_PREFIX__ inline bool operator != (const uarray <T>& pt) const
     {
      return uarray_cmp<T,T>(*this, pt, [](T x,T y) -> bool { return x != y; });
     }

    template <typename U>
    __CUDA_PREFIX__ inline bool operator < (const uarray <U>& pt) const
     {
      return uarray<T> :: uarray_cmp2<T, U, false>(*this, pt);
     }

    template <typename U>
    __CUDA_PREFIX__ inline bool operator <= (const uarray <U>& pt) const
     {
      return uarray<T> :: uarray_cmp2<T, U, true>(*this, pt);
     }

    template <typename U>
    __CUDA_PREFIX__ inline bool operator > (const uarray <U>& pt) const
     {
      return uarray<T> :: uarray_cmp2<U, T, false>(pt, *this);
     }

    template <typename U>
    __CUDA_PREFIX__ inline bool operator >= (const uarray <U>& pt) const
     {
      return uarray<T> :: uarray_cmp2<U, T, true>(pt, *this);
     }

    template <typename U>
    __CUDA_PREFIX__ inline bool operator == (const uarray <U>& pt) const
     {
      return uarray_cmp<T,U>(*this, pt, [](T x,U y) -> bool { return x == y; });
     }

    template <typename U>
    __CUDA_PREFIX__ inline bool operator != (const uarray <U>& pt) const
     {
      return uarray_cmp<T,U>(*this, pt, [](T x,U y) -> bool { return x != y; });
     }

    template <typename W>
    __CUDA_PREFIX__ inline uarray<W> operator + (const uarray <T>& pt) const
     {
      return uarray_op<T,T,W>(*this, pt, [](T x,T y) -> W { return x + y; });
     }

    template <typename W>
    __CUDA_PREFIX__ inline void minus (const uarray <T>& pt, const uarray<W>& dest) const
     {
      assert(m_size == pt.size());
      for (uint64_t i=0; i<m_size; ++i)
        dest.m_arr[i] = this->m_arr[i] - pt.m_arr[i];
     }

    template <typename W>
    __CUDA_PREFIX__ inline void minus (const uarray <T>* pt, const uarray<W>* dest) const
     {
      assert(m_size == pt->size());
      for (uint64_t i=0; i<m_size; ++i)
        dest->m_arr[i] = this->m_arr[i] - pt->m_arr[i];
     }

    template <typename W>
    __CUDA_PREFIX__ inline void plus (const uarray <T>* pt, const uarray<W>* dest) const
     {
      assert(m_size == pt->size());
      for (uint64_t i=0; i<m_size; ++i)
        dest->m_arr[i] = this->m_arr[i] + pt->m_arr[i];
     }

    template <typename W>
    __CUDA_PREFIX__ inline void plus (const uarray <T>& pt, const uarray<W>& dest) const
     {
      assert(m_size == pt.size());
      for (uint64_t i=0; i<m_size; ++i)
        dest.m_arr[i] = this->m_arr[i] + pt.m_arr[i];
     }

    template <typename W>
    __CUDA_PREFIX__ inline uarray<W> operator - (const uarray <T>& pt) const
     {
      assert(m_size == pt.size());
      return uarray_op<T,T,W>(*this, pt, [](T x,T y) -> W { return x - y; });
     }

    template <typename W, typename U>
    __CUDA_PREFIX__ inline uarray<W> operator + (const uarray <U>& pt) const
     {
      return uarray_op<T,U,W>(*this, pt, [](T x,U y) -> W { return x + y; });
     }

    template <typename W, typename U>
    __CUDA_PREFIX__ inline uarray<W> operator - (const uarray <U>& pt) const
     {
      assert(m_size == pt.size());
      return uarray_op<T,U,W>(*this, pt, [](T x,U y) -> W { return x - y; });
     }

    __CUDA_HOST_ONLY__ std::string to_string () const
     {
      std::stringstream ss;

      ss << "(";

      for (uint64_t i=0; i<this->size(); ++i)
        ss << this->operator[](i) << ",";

      std::string tmp = ss.str();
      return tmp.substr(0,tmp.length()-1) + ")";
     }

   };

  template <typename T>
  template <typename U, typename V, typename LambdaUVBool>
  __CUDA_PREFIX__ inline bool uarray <T> :: uarray_cmp (const uarray <U>& u, const uarray <V>& v, const LambdaUVBool& comparator)
   {
    assert(u.size() == v.size());

    bool retval = true;

    #if USE_Z_ORDER
    for (uint64_t i=0; retval && i<u.size(); ++i)
    #else
    for (uint64_t i=u.size()-1; retval && i<u.size(); --i)
    #endif
      retval &= comparator(u[i],v[i]);

    return retval;
   }

  template <typename T>
  template <typename U, typename V, typename W, typename LambdaUVW>
  __CUDA_PREFIX__ inline uarray <W> uarray <T> :: uarray_op (const uarray <U>& u, const uarray <V>& v, const LambdaUVW& operation)
   {
    assert(u.size() == v.size());

    uarray <W> w(u.size());

    for (uint64_t i=0; i<u.size(); ++i)
      w[i] = operation(u[i],v[i]);

    return w;
   }

  template <typename T>
  __CUDA_PREFIX__ inline uarray <T> uarray <T> :: zero (uint64_t size)
   {
    uarray<T> u(size);
    memset(u.m_arr, 0, size * sizeof(T));
    return u;    
   }

}; // namespace extgraph

#endif // GRID_IMPL_H
