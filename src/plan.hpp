/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#ifndef PLAN_H
#define PLAN_H

#pragma once

#include <grid.hpp>
#include <memutils.hpp>

#include <cmath>

#define PLAN_DEBUG 1

namespace extgraph {

    class plan_t {

        public:

            struct plan_item_t {
                global_index_t global_offset;

                chunk_rect_t bounding_rect;
                chunk_rect_t compute_rect;
                chunk_rect_t bounding_cumulative_rect;

                global_index_t compute_start_index, compute_end_index;
                global_index_t bound_start_index, bound_end_index;

                plan_item_t () : bounding_rect(1), compute_rect(1), bounding_cumulative_rect(1) {}

            };

        protected:

            const chunk_rect_t * const m_rect;

            uint32_t chunk_count;
            uint32_t slicing_dimension;
            uint64_t slice_size, num_slices, num_points;
            dimension_span_t last_slice_end;
            global_index_t last_end_index;

            __CUDA_HOST_ONLY__ void test_item (const plan_item_t item) const noexcept;

        public:

            // Bytes per vertex
            static constexpr long double compute_bytes_lr_slope 
                = sizeof(func_val_t) + sizeof(neighbour_bitmap_t) + sizeof(uint8_t) + (sizeof(int) * ENABLE_DEBUGGING);

            // Static allocations
            static constexpr long double compute_bytes_lr_intercept = 100 * 1024 * 1024;

            __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ plan_t (const chunk_rect_t * const rect);

            __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ bool next_item (const int deviceID, plan_item_t& item);

    };

    __CUDA_HOST_ONLY__ plan_t :: plan_t (const chunk_rect_t * const rect) : m_rect(rect)
     {
      last_slice_end = 0;
      last_end_index = 0;
      num_points     = 0;
      chunk_count    = 0;

      #if USE_Z_ORDER
          slicing_dimension = 0;
      #else
          slicing_dimension = m_rect->num_dimensions()-1;
      #endif

      num_slices = m_rect->dimension_span(slicing_dimension);
      slice_size = m_rect->num_elements() / num_slices;

      printf("Rect      : %s\n", rect->to_string().c_str());
      printf("Elements  : %lu\n", rect->num_elements());
      printf("Num slices: %lu\n", num_slices);
      printf("Block size: %lu\n", slice_size);
      puts("");
     }

    __CUDA_HOST_ONLY__ bool plan_t :: next_item (const int deviceID, plan_item_t& item)
     {
      if (last_slice_end >= num_slices) {
          assert(num_points == m_rect->num_elements());
          return false;
      }

      #if PLAN_DEBUG
//      printf("last_slice_end: %u\n", last_slice_end);
      #endif


      size_t available_memory, total_memory;
      size_t compute_available_memory, compute_total_memory;

      compute_available_memory = available_memory = get_free_memory();
      compute_total_memory    =  total_memory     = get_total_memory();

      #if CUDA_SUPPORT

      if (deviceID>=0) {
        int gpu_count, id;
        cudaGetDeviceCount(&gpu_count);
        assert(deviceID < gpu_count);

        cudaSetDevice(deviceID);
        cudaGetDevice(&id);
        cudaMemGetInfo(&compute_available_memory, &compute_total_memory);
      }
      else

      #endif
            {

             (void)deviceID;         // Hide unused variable warning when not using CUDA
             ++compute_total_memory; // Hide assigned but unused variable warning

             item.bounding_rect       = *m_rect;
             item.bound_start_index   = 0;
             item.bound_end_index     = m_rect->num_elements();

             item.compute_rect        = item.bounding_rect;
             item.compute_start_index = item.bound_start_index;
             item.compute_end_index   = item.bound_end_index;

             item.bounding_cumulative_rect = item.bounding_rect;

             last_slice_end = num_slices;
             num_points     = m_rect->num_elements();

             return true;
      }

      uint64_t max_expected_points = floorl( (((long double)compute_available_memory) - compute_bytes_lr_intercept) / compute_bytes_lr_slope); // New
      max_expected_points -= (max_expected_points % slice_size);

      if (max_expected_points < slice_size) {
        grid_point_t t = m_rect->end_pt();
        t[slicing_dimension] = 1;

        fprintf(stderr, "compute_ext_graph(): Cannot allocate block of size %s on ", t.to_string().c_str());

        #if CUDA_SUPPORT
        if (deviceID == -1)
            fprintf(stderr, "memory!");
        else
            fprintf(stderr, "GPU %d!", deviceID);
        #else
        fprintf(stderr, "memory!");
        #endif

        fprintf(stderr, "\n");

        return false;
      }

      const uint64_t max_slices = max_expected_points / slice_size;
      const uint64_t remaining_slices = num_slices - last_slice_end;

      max_expected_points = min(max_expected_points, remaining_slices * slice_size);

      #if PLAN_DEBUG
      std::cout << "Max slices   : " << max_slices << "\n";
      std::cout << "Rem slices   : " << remaining_slices << "\n";
      #endif

      const bool is_first_chunk = (last_end_index == 0);
      const bool is_last_chunk  = (remaining_slices < max_slices);

      const dimension_span_t num_chunk_slices = min(remaining_slices, max_slices - (!is_first_chunk) - (!is_last_chunk));

      #if PLAN_DEBUG
      std::cout << "Chunk Slices : " << num_chunk_slices               << "\n";
      #endif

      // item.global_offset
      item.global_offset = last_end_index;

      // item.compute_start_index
      item.compute_start_index = last_end_index;
      grid_point_t a(m_rect->num_dimensions());
      m_rect->index_to_point(item.compute_start_index, &a);

      #if PLAN_DEBUG
      printf("compute.start.p_to_i: %lu\n", item.compute_start_index);
      #endif

      // item.compute_end_index
      grid_point_t b = m_rect->last_pt();
      b[slicing_dimension] = last_slice_end + num_chunk_slices - 1;// !is_last_chunk;
      item.compute_end_index = m_rect->point_to_index(b);
      ++item.compute_end_index;
      #if PLAN_DEBUG
      printf("compute.end  .p_to_i: %lu [%lu]\n", item.compute_end_index, (item.compute_end_index/slice_size)  );
      #endif

      // item.compute_rect
      b = m_rect->end_pt();
      b[slicing_dimension] = last_slice_end + num_chunk_slices;
      item.compute_rect = chunk_rect_t(a, b);
      #if PLAN_DEBUG
      std::cout << "Compute rect : " << item.compute_rect.to_string()  << "\n";
      #endif
      assert(item.compute_rect.num_elements() == (num_chunk_slices * slice_size));

      // item.bound_start_index
      item.bound_start_index = (item.compute_start_index > slice_size) ? (item.compute_start_index - slice_size) : 0;
      m_rect->index_to_point(item.bound_start_index, &a);
      #if PLAN_DEBUG
      printf("bound.start.p_to_i  : %lu\n", item.bound_start_index);
      #endif

      // item.bound_end_index
      b = m_rect->last_pt();
      b[slicing_dimension] = min(m_rect->dimension_span(slicing_dimension), last_slice_end + num_chunk_slices - is_last_chunk);
      item.bound_end_index = m_rect->point_to_index(b);
      ++item.bound_end_index;
      #if PLAN_DEBUG
      printf("bound.end  .p_to_i  : %lu [%lu]\n", item.bound_end_index, (item.bound_end_index/slice_size)  );
      #endif

      // item.bounding_rect
      b = m_rect->end_pt();
      b[slicing_dimension] = min(m_rect->dimension_span(slicing_dimension), last_slice_end + num_chunk_slices + 1);
      item.bounding_rect = chunk_rect_t(a, b);
      #if PLAN_DEBUG
      std::cout << "Bounding rect: " << item.bounding_rect.to_string() << "\n";
      #endif

      // item.bounding_cumulative_rect
      item.bounding_cumulative_rect = chunk_rect_t(grid_point_t::zero(m_rect->num_dimensions()), b);

      uint64_t bounding_rect_elements = (num_chunk_slices + !is_first_chunk + (!is_last_chunk)) * slice_size;
      #if PLAN_DEBUG
      printf("bounding_rect size  : %lu [%lu, %lu, %lu] x [%lu]\n", (unsigned long)bounding_rect_elements, (unsigned long)num_chunk_slices, (unsigned long)(is_first_chunk), (unsigned long)(is_last_chunk) , (unsigned long)item.bounding_rect.num_elements());
      #endif
      assert(item.bounding_rect.num_elements() == bounding_rect_elements);

      last_end_index += num_chunk_slices * slice_size;
      last_slice_end += num_chunk_slices;
      num_points     += (item.compute_end_index - item.compute_start_index);

//      test_item(item);

      #if PLAN_DEBUG
      std::cout << std::endl;
      #else
      printf("Chunk %lu\n", (unsigned long)++chunk_count);
      #endif

      return true;
     }


    __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ void plan_t :: test_item (const plan_item_t item) const noexcept
     {
      grid_point_t pt(m_rect->num_dimensions());

      #if PLAN_DEBUG
      puts("plan_t :: test_item: Testing");
      #endif

      for (global_index_t pt_index=item.compute_start_index; pt_index<item.compute_end_index; ++pt_index) {
        m_rect->index_to_point(pt_index, &pt);
        assert(item.bounding_rect.contains_point(&pt));
      }

      #if PLAN_DEBUG
      puts("plan_t :: test_item: Passed");
      #endif
     }

};

#endif // PLAN_H
