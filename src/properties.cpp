/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#ifndef PROPERTIES_CPP
#define PROPERTIES_CPP

#include <cstdio>
#include <string>
#include <cstring>
#include <map>
#include <cctype>

#include <sys/sysinfo.h>
#include <unistd.h>

#include <properties.hpp>

namespace extgraph {

    int properties :: m_num_threads = 0;
    int properties :: m_gpu_id = 0;

    int properties :: num_threads() noexcept { return m_num_threads; }
    int properties :: gpu_id() noexcept { return m_gpu_id; }

    void properties :: init_properties () {
        m_num_threads = get_nprocs();
        m_gpu_id = 0;
    }

    void __attribute__((constructor)) print_properties() {
        printf("m_num_threads: %d\n", properties::m_num_threads);
        printf("m_gpu_id: %d\n", properties::m_gpu_id);
    }

};

#endif // PROPERTIES_CPP
