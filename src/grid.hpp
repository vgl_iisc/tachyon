/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#ifndef GRID_HPP
#define GRID_HPP

#pragma once

#ifndef _LARGEFILE64_SOURCE
  #define _LARGEFILE64_SOURCE
#endif

#define _FILE_OFFSET_BITS 64

#include <properties.hpp>
#include <string>

#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <bitset>

#include <climits>
//#include <limits.h>
#include <cfloat>
#include <cstdint>

#include <cassert>
//#define NDEBUG

#include <cuda_runtime.h>
#include <cuda_profiler_api.h>

#if (defined(__CUDA_ARCH__) && (__CUDA_ARCH__ > 0)) // Set by nvcc for GPU code compilation
    #define CODE_TYPE_DEVICE     1
    #define CODE_TYPE_HOST       0
    #define CUDA_EXEC_FULL_LOOPS 1

    #define __CUDA_PREFIX__   __host__ __device__
    #define __CUDA_HOST_ONLY__ __host__
    #define __CUDA_FORCEINLINE__ __forceinline__
    #define __CUDA_VARIABLE_PREFIX__ __shared__
#else
    #define CODE_TYPE_DEVICE     0
    #define CODE_TYPE_HOST       1
    #define CUDA_EXEC_FULL_LOOPS 0

    #define __CUDA_PREFIX__ 
    #define __CUDA_HOST_ONLY__
    #define __CUDA_FORCEINLINE__ inline
    #define __CUDA_VARIABLE_PREFIX__
#endif

#include <utils.hpp>

#ifndef DIMENSION_MAX
    #define DIMENSION_MAX      7
#endif
#define DIMENSION_SPAN_MAX UINT32_MAX

#define FUNC_CMP_THRESHOLD 1e-7
#define MAX_SADDLE_ADJACENT_MAXIMA_COUNT 64

// DEBUG SYMBOLS
#define USE_OLD_PARSER 1

/*
 * Z_ORDER = 0 =>  (0,0,0) -> (1,0,0)
 * Z_ORDER = 1 =>  (0,0,0) -> (0,0,1)
 */
#define USE_Z_ORDER 0

#include "grid_impl.h"

namespace extgraph
 {

    #if VAL_TYPE_DOUBLE == 1
       typedef double                                               func_val_t;
    #elif VAL_TYPE_DOUBLE == 0
       typedef float                                                func_val_t;
    #else
          #error "Invalid VAL_TYPE_DOUBLE value"
    #endif

   typedef uint32_t                                            dimension_span_t;
   typedef  int32_t                                            dimension_diff_t;

   typedef uint64_t                                            index_t;

   typedef uint64_t                                            global_index_t;
   typedef global_index_t                                      point_index_t;

   #if MAX_DIMENSIONS > 31
       #error "Cannot support more than 31 dimensions"
   #elif MAX_DIMENSIONS > 15
       typedef uint32_t                                             neighbour_bitmap_t;
   #elif MAX_DIMENSIONS > 7
       typedef uint16_t                                             neighbour_bitmap_t;
   #elif MAX_DIMENSIONS > 1
       typedef uint8_t                                              neighbour_bitmap_t;
   #elif MAX_DIMENSIONS < 2
       typedef uint8_t                                              neighbour_bitmap_t;
       #error "Num dimensions must be atleast 2"
   #else
       typedef uint8_t                                              neighbour_bitmap_t;
       #error "Invalid MAX_DIMENSIONS value"
   #endif

   typedef std::vector <index_t>                               index_collection_t;
   typedef std::vector <func_val_t>                            persistence_map_t;

   typedef arc_redirection_info_template_t <point_index_t>     arc_redirection_info_t;
   typedef std::vector <std::pair<uint64_t, uint64_t>>         reachable_extrema_list_t;

   #define SADDLE_MAXIMA_MAPPING_OPTIMIZE 0

   #if SADDLE_MAXIMA_MAPPING_OPTIMIZE
     typedef hybrid_vector <uint64_t, uint8_t, 2>                critical_point_map_element_t;
   #else
     typedef std::vector <uint64_t>                              critical_point_map_element_t;
   #endif

   typedef std::vector <critical_point_map_element_t>          critical_point_map_t;

   using point_t      = uarray <func_val_t>;
   using grid_point_t = uarray <dimension_span_t>;
   using dimensions_t = uarray <dimension_span_t>;
   using point_diff_t = uarray <dimension_diff_t>;

   using saddle_maxima_path_status_t = std::bitset <MAX_SADDLE_ADJACENT_MAXIMA_COUNT>;

   class point_neighbourhood;
   class plan_t;

   class chunk_rect_t
     {

       public:

          friend class point_neighbourhood;
          friend class plan_t;

          grid_point_t       m_start, m_end;
          size_t             m_numdims;
          global_index_t     m_end_index;
          grid_point_t       m_chunk_dimensions;
          uarray <uint64_t>  m_prefix_products;
          bool               m_managed_memory;

          void init ();

       public:

          __CUDA_HOST_ONLY__ chunk_rect_t  (const size_t num_dims);
          __CUDA_HOST_ONLY__ chunk_rect_t  (const grid_point_t& start, const grid_point_t& end);
          __CUDA_HOST_ONLY__ ~chunk_rect_t ();
 
          __CUDA_HOST_ONLY__ chunk_rect_t  (const chunk_rect_t& other_rect);
          __CUDA_HOST_ONLY__ chunk_rect_t& operator = (const chunk_rect_t& other_rect);

          __CUDA_PREFIX__ inline grid_point_t     start_pt       () const noexcept { return m_start; }
          __CUDA_PREFIX__ inline grid_point_t     end_pt         () const noexcept { return m_end; }
          __CUDA_PREFIX__ inline grid_point_t     last_pt        () const noexcept { return grid_point_t :: uarray_op<dimension_span_t,dimension_span_t,dimension_span_t>(m_end, m_end, [](dimension_span_t x,dimension_span_t y) -> dimension_span_t { (void)y; return x - 1; });; }

          __CUDA_PREFIX__ inline uint64_t         num_elements   () const noexcept { return 1 + m_end_index; }
          __CUDA_PREFIX__ inline size_t           num_dimensions () const noexcept { return m_numdims; }
          __CUDA_PREFIX__ inline dimension_span_t dimension_span (const uint32_t dim_index) const noexcept { return m_end[dim_index] - m_start[dim_index]; }

          __CUDA_PREFIX__ bool    contains_point (const grid_point_t& pt)      const;
          __CUDA_PREFIX__ bool    contains_point (const grid_point_t * const pt)      const;
          __CUDA_PREFIX__ bool    contains_point (const point_t& pt)           const;
          __CUDA_PREFIX__ bool    contains_index (const point_index_t& pt)           const;

          template <uint32_t num_dimensions>
          __CUDA_PREFIX__ point_index_t      point_to_index_templatized (const grid_point_t * const pt)                          const noexcept;

          __CUDA_PREFIX__ point_index_t      point_to_index (const grid_point_t * const pt)                          const noexcept;
          __CUDA_PREFIX__ point_index_t      point_to_index (const grid_point_t& pt)                                 const;
          __CUDA_PREFIX__ point_index_t      point_to_index (const grid_point_t& pt, point_diff_t& tmp) const;
          __CUDA_PREFIX__ point_index_t      point_to_index (const grid_point_t * const pt, point_diff_t * const tmp) const;

          __CUDA_PREFIX__ grid_point_t index_to_point (const point_index_t& pt_index)                   const;
          __CUDA_PREFIX__ bool         index_to_point (const point_index_t pt_index, grid_point_t& pt)  const;
          __CUDA_PREFIX__ bool         index_to_point (const point_index_t pt_index, grid_point_t * const pt)  const;

          template <uint32_t num_dimensions>
          __CUDA_PREFIX__ neighbour_bitmap_t index_to_neighbour_bitmap_templatized (const point_index_t ref_pt, const point_index_t neighbour_pt_index) const noexcept;
          __CUDA_PREFIX__ neighbour_bitmap_t index_to_neighbour_bitmap (const point_index_t ref_pt, const point_index_t neighbour_pt_index) const noexcept;

          template <uint32_t num_dimensions>
          __CUDA_PREFIX__ point_index_t      neighbour_bitmap_to_index_templatized (const point_index_t ref_pt, const neighbour_bitmap_t neighbour_pt_bitmap) const noexcept;
          __CUDA_PREFIX__ point_index_t      neighbour_bitmap_to_index (const point_index_t ref_pt, const neighbour_bitmap_t neighbour_ptbitmap) const noexcept;

          __CUDA_PREFIX__ bool         is_boundary_point (const grid_point_t * const pt)  const noexcept;
          __CUDA_PREFIX__ bool         is_boundary_point (const grid_point_t * const pt, uint32_t& boundary_adjacency_mask) const noexcept;

          __CUDA_HOST_ONLY__ std::string   to_string () const;

          #if CUDA_SUPPORT
            __CUDA_HOST_ONLY__ chunk_rect_t* create_device_copy (const int device) const;
            __CUDA_HOST_ONLY__ void move_to_device (const int device);
          #endif

     };

  class point_neighbourhood {

    public:

        const chunk_rect_t *              m_rect;
        const uint64_t                    m_max_neighs;
        uint8_t*                          m_adjacency_arr;

        __CUDA_HOST_ONLY__ void build_adjacency_array     ();

    public:

        __CUDA_HOST_ONLY__ point_neighbourhood (const chunk_rect_t * const rect);
        __CUDA_HOST_ONLY__ ~point_neighbourhood ();

        __CUDA_PREFIX__ static constexpr uint64_t max_neighbours_for_dim (const uint32_t num_dimensions) noexcept;

        __CUDA_PREFIX__ uint64_t max_neighbours () const noexcept { return m_max_neighs; }

        template <uint32_t num_dimensions>
        __CUDA_PREFIX__ bool neighbour_perturbation_templatized (const grid_point_t* const pt, grid_point_t* const dest, point_index_t& dest_index, uint32_t neighbour_index) const noexcept;

        __CUDA_PREFIX__ bool neighbour_perturbation (const grid_point_t* const pt, grid_point_t* const dest, point_index_t& dest_index, uint32_t neighbour_index) const noexcept;

        __CUDA_PREFIX__ bool is_pair_adjacent (const uint64_t neighbour_index_i, const uint64_t neighbour_index_j) const noexcept;
        __CUDA_PREFIX__ bool is_pair_adjacent (const grid_point_t * const pt1, const grid_point_t * const pt2) const;
        __CUDA_PREFIX__ const uint8_t * get_adjacency_arr () const noexcept { return m_adjacency_arr; }

        #if CUDA_SUPPORT
            __CUDA_HOST_ONLY__ void move_to_device (const int device) const;
        #endif

  };

 };

#define FUNC_VAL_MAX  (std :: numeric_limits <extgraph :: func_val_t> :: has_infinity     \
                        ? std::numeric_limits <extgraph :: func_val_t> :: infinity()      \
                        : std::numeric_limits <extgraph :: func_val_t> :: max())

#define FUNC_VAL_MIN  (std :: numeric_limits <extgraph :: func_val_t> :: has_infinity     \
                        ? (- std::numeric_limits <extgraph :: func_val_t> :: infinity())  \
                        :    std::numeric_limits <extgraph :: func_val_t> :: min())

__CUDA_PREFIX__ __CUDA_FORCEINLINE__ constexpr extgraph :: func_val_t func_val_max_toggled (const bool negate_fn_val) noexcept
 {
  return negate_fn_val ? FUNC_VAL_MIN : FUNC_VAL_MAX;
 }

__CUDA_PREFIX__ __CUDA_FORCEINLINE__ constexpr extgraph :: func_val_t func_val_min_toggled (const bool negate_fn_val) noexcept
 {
  return negate_fn_val ? FUNC_VAL_MAX : FUNC_VAL_MIN;
 }

#include <grid_base_inl.hpp>

#endif // GRID_HPP
