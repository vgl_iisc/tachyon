/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#include <unistd.h>
#include <poll.h>

namespace extgraph {

  struct pollfd has_input_pfd[1] = {0, 0, 0};

  void __attribute__((constructor)) init_has_input ()
   {
    has_input_pfd[0].fd     = STDIN_FILENO;
    has_input_pfd[0].events = POLLIN;
   }

}
