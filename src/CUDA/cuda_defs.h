/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#ifndef CUDA_DEFINITIONS
#define CUDA_DEFINITIONS

#include <grid.hpp>
#include <cmath>

#define CUDA_SEPARATE_RECT_ITEMS     1

#define COLLECT_MULTISAD_INTO_SAD    1

#define MULTISADDLE_LABEL            0xfc
#define REGULAR_PT_LABEL             0xfd
#define UNVISITED_VERTEX_LABEL       0xfe
#define UNKNOWN_CRTI_PT_LABEL        0xff

#define CUDA_ENABLE_SYNCTHREADS        0

#define USE_UNION_FIND_BFS             1
#define USE_DIRECT_UF_MAX_PT_TEST      0

#define USE_STATIC_ARRAYS              1
#define USE_BIT_ARRAY_FOR_VISITORS     1
#define USE_FASTER_ADJACENCY_TEST      1
#define CUDA_FASTER_MAX_NEIGHBOUR_ENUM 1

#define CRIT_POINT_IDENTIFIER_DEBUG  0

#define CUDA_USE_ASM_CLOCK           0

#define ENABLE_NVPROF_PROFILING      0

#ifndef ENABLE_DEBUGGING
  #define ENABLE_DEBUGGING 0
#endif

//#define ENABLE_DEBUGGING                    1

#if ENABLE_DEBUGGING
  #define CUDA_SUBKERNEL_PROFILING               0
  #define CUDA_BFS_DEBUG                         1
  #define CUDA_SANITY_CHECKS                     1
  #define ENABLE_INDEX_PLACEHOLDER               0
#else
  // Must be all unset!
  #define CUDA_BFS_DEBUG                         0
  #define CUDA_SUBKERNEL_PROFILING               0
  #define CUDA_SANITY_CHECKS                     0
  #define ENABLE_INDEX_PLACEHOLDER               0
#endif

#if CUDA_SANITY_CHECKS
  #define CUDA_CHECK_ALTERNATING_SUM              1
  #define CUDA_CHECK_UNVISITED_VERTICES           1
  #define CHECK_REGULAR_POINT_CLASSIFICATION      1
  #define CUDA_ENABLE_GENERAL_ALTERNATE_SUM_CHECK 0
#else
  // Must be all unset!
  #define CUDA_CHECK_ALTERNATING_SUM              0
  #define CUDA_CHECK_UNVISITED_VERTICES           0
  #define CHECK_REGULAR_POINT_CLASSIFICATION      0
  #define CUDA_ENABLE_GENERAL_ALTERNATE_SUM_CHECK 0
#endif

#if !CUDA_SUPPORT
  #undef CUDA_SUBKERNEL_PROFILING
  #define CUDA_SUBKERNEL_PROFILING               0
#endif

#if ENABLE_INDEX_PLACEHOLDER
    #define PLACEHOLDER_INDEX 0
#else
    #define PLACEHOLDER_INDEX pt_index
#endif

#define CUDA_SAFE_CALL(call) \
 do { \
  cudaError_t err = call;\
  if (cudaSuccess != err) \
     { \
       fprintf (stderr, "Cuda error(%d) in file '%s' in line %i : %s.", \
             err, __FILE__, __LINE__, cudaGetErrorString(err) ); \
       exit(-1); \
     } \
 } \
 while (0); 


#define CUDA_SAMPLE_STEP1_FULL_BFS                  0
#define CUDA_SAMPLE_STEP1_BFS_COMPONENT_REP         1
#define CUDA_SAMPLE_STEP1_BFS_COMPONENT_TRAVERSAL   2
#define CUDA_SAMPLE_STEP1_BFS_COMPONENT_NEIGHBOURS  3
#define CUDA_SAMPLE_STEP1_POINT_CLASSIFICATION      4
#define CUDA_SAMPLE_STEP1_MAX_NEIGHBOUR             5
#define CUDA_SAMPLE_UNIFIED_STEP                    6

#define PROFILING_SAMPLES 7

#if CUDA_SANITY_CHECKS
    #warning "CUDA_SANITY_CHECKS is set"
#endif
#if CUDA_SUBKERNEL_PROFILING
    #warning "CUDA_SUBKERNEL_PROFILING is set"
#endif
#if CUDA_ENABLE_GENERAL_ALTERNATE_SUM_CHECK
    #warning "CUDA_ENABLE_GENERAL_ALTERNATE_SUM_CHECK is set"
#endif

namespace extgraph {

  typedef struct {
    int64_t values[PROFILING_SAMPLES];
    int64_t totals[PROFILING_SAMPLES] = {0, 0, 0, 0, 0};
  } cuda_profiling_unit_t;

  typedef struct {
      uint32_t   parent;
      uint32_t   size;

      func_val_t neighbour_func_val;

      point_index_t    subset_max_pt;
      func_val_t subset_max_pt_val;
  } uf_bfs_data_t;

//  template <bool find_component_max, bool include_padding, uint32_t num_dimensions, uint32_t max_neighbour_count> // = 2 * ( (1UL << num_dimensions) - 1)>
  template <bool find_component_max, bool include_padding, uint32_t num_dimensions, uint32_t max_neighbour_count = point_neighbourhood :: max_neighbours_for_dim(num_dimensions)>
  __CUDA_PREFIX__ __attribute__((hot))
  void neighbour_bfs_union_find (
      const point_index_t pt_index, const chunk_rect_t* const rect, const point_neighbourhood* const p_neighbours,
      const func_val_t* const fn_vals, const uint64_t global_index_offset, const bool invert_fn,

      #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
      cuda_profiling_unit_t* const profiling_arr,
      #endif

      uint32_t& true_upper_link_count, uint32_t& upper_link_count, uint32_t& lower_link_count, point_index_t* const component_highest,

      grid_point_t * pt, grid_point_t * const pt_i, grid_point_t * const pt_j, uf_bfs_data_t* const uf_data_arr, uint32_t * const tmp_arr

      );

  __CUDA_HOST_ONLY__ __attribute__((hot))
  void neighbour_bfs_union_find_non_template (
      const point_index_t pt_index, const chunk_rect_t* const rect, const point_neighbourhood* const p_neighbours,
      const func_val_t* const fn_vals, const uint64_t global_index_offset, const bool invert_fn,

      #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
      cuda_profiling_unit_t* const profiling_arr,
      #endif

      uint32_t& true_upper_link_count, uint32_t& upper_link_count, uint32_t& lower_link_count, point_index_t* const component_highest,

      grid_point_t * pt, grid_point_t * const pt_i, grid_point_t * const pt_j, uf_bfs_data_t* const uf_data_arr, uint32_t * const tmp_arr

      );

  template <uint32_t num_dimensions>
  __CUDA_PREFIX__
  void extremum_graph_point_single_point
    (
      const point_index_t pt_index, const chunk_rect_t* __restrict__ const full_rect, const chunk_rect_t* __restrict__ const rect, const point_neighbourhood* __restrict__ const p_neighbours,
      const func_val_t* __restrict__ const fn_vals, const uint64_t func_val_neg_offset, const uint64_t global_index_offset, const bool invert_fn,
      uint8_t* __restrict__ const point_index_array, neighbour_bitmap_t* __restrict__ const edge_connectivity_array

      #if CUDA_CHECK_ALTERNATING_SUM
      , int * __restrict__ const point_contribution
      #endif

      #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
      , cuda_profiling_unit_t * __restrict__ const profiling_arr
      #endif
    );

  #if CUDA_SUPPORT

  __CUDA_HOST_ONLY__
  void cuda_extremum_graph_point_classification_non_template
    (
      const dim3 grid_layout, const dim3 block_layout,
      const point_index_t pt_index_start, const point_index_t pt_index_end,
      const chunk_rect_t* __restrict__ const full_rect, const chunk_rect_t* __restrict__ const rect, const point_neighbourhood* __restrict__ const p_neighbours,
      const func_val_t* __restrict__ fn_vals, const uint64_t func_val_neg_offset, const uint64_t global_index_offset, const bool invert_fn,
      uint8_t* __restrict__ const point_index_array, neighbour_bitmap_t* __restrict__ const edge_connectivity_array

      #if CUDA_CHECK_ALTERNATING_SUM
      , int* __restrict__ const point_contribution
      #endif

      #if CUDA_SUBKERNEL_PROFILING
      , cuda_profiling_unit_t* __restrict__ const profiling_arr
      #endif
    );

  __CUDA_PREFIX__ inline int popcount (const uint32_t v) {

    #if CODE_TYPE_DEVICE
      return __popc(v);
    #else
      return __builtin_popcount(v);
    #endif
  }

  __CUDA_PREFIX__ inline int _ConvertSMVer2Cores(int major, int minor) {
    // Defines for GPU Architecture types (using the SM version to determine
    // the # of cores per SM
    typedef struct {
      int SM;  // 0xMm (hexidecimal notation), M = SM Major version,
      // and m = SM minor version
      int Cores;
    } sSMtoCores;
  
    static sSMtoCores nGpuArchCoresPerSM[] = {
        {0x30, 192},
        {0x32, 192},
        {0x35, 192},
        {0x37, 192},
        {0x50, 128},
        {0x52, 128},
        {0x53, 128},
        {0x60,  64},
        {0x61, 128},
        {0x62, 128},
        {0x70,  64},
        {0x72,  64},
        {0x75,  64},
        {0x80,  64},
        {0x86, 128},
        {-1, -1}};
  
    int index = 0;
  
    while (nGpuArchCoresPerSM[index].SM != -1) {
      if (nGpuArchCoresPerSM[index].SM == ((major << 4) + minor)) {
        return nGpuArchCoresPerSM[index].Cores;
      }
  
      index++;
    }

    // If we don't find the values, we default use the previous one
    // to run properly
    printf(
        "MapSMtoCores for SM %d.%d is undefined."
        "  Default to use %d Cores/SM\n",
        major, minor, nGpuArchCoresPerSM[index - 1].Cores);
    return nGpuArchCoresPerSM[index - 1].Cores;
  }

  __CUDA_HOST_ONLY__ inline int CUDA_core_count ()
   {
    return 0;
    int deviceID;
    cudaDeviceProp props;
    cudaGetDevice(&deviceID);
    cudaGetDeviceProperties(&props, deviceID);
    return _ConvertSMVer2Cores(props.major, props.minor) * props.multiProcessorCount;
   }

  __CUDA_HOST_ONLY__ inline void CUDA_core_stats (int* multi_processor_count, int* cores_per_multi_processor)
   {
    int deviceID;
    cudaDeviceProp props;
    cudaGetDevice(&deviceID);
    cudaGetDeviceProperties(&props, deviceID);

    *cores_per_multi_processor = _ConvertSMVer2Cores(props.major, props.minor);
    *multi_processor_count     =  props.multiProcessorCount;
   }

  __device__ __CUDA_FORCEINLINE__ void cuda_clock (uint64_t& t)
   {
    asm volatile("mov.u64 %0, %%globaltimer;" : "=l"(t));
   }

  __device__ __CUDA_FORCEINLINE__ double clock_to_ms (uint64_t value)
   {
    return ((double)value) * 3.2e-8;
   }

  __device__ __CUDA_FORCEINLINE__  uint64_t cuda_asm_clock ()
   {
    uint64_t t;
    asm volatile ("mov.u64 %0, %%clock64;" : "=l" (t) );
//    asm volatile ("mov.u64 %0, %%globalclock;" : "=l" (t) );
    return t;
   }

  __device__ __CUDA_FORCEINLINE__  void cuda_sample_start (cuda_profiling_unit_t* const arr, const point_index_t idx, const int sample_index)
   {
    #if CODE_TYPE_DEVICE
      arr[idx].values[sample_index] = 
        #if CUDA_USE_ASM_CLOCK
          cuda_asm_clock();
        #else
          clock64();
        #endif
    #else
      (void)arr;
      (void)idx;
      (void)sample_index;
    #endif
   }

  __device__ __CUDA_FORCEINLINE__  void cuda_sample_end (cuda_profiling_unit_t* const arr, const point_index_t idx, const int sample_index)
   {
    #if CODE_TYPE_DEVICE
      uint64_t t =
        #if CUDA_USE_ASM_CLOCK
          cuda_asm_clock();
        #else
          clock64();
        #endif
  
      if (t < arr[idx].values[sample_index])
        arr[idx].values[sample_index] = (0xffffffffffffffff - arr[idx].values[sample_index]) + t;
      else
        arr[idx].values[sample_index] = t - arr[idx].values[sample_index];
  
      arr[idx].totals[sample_index] += arr[idx].values[sample_index];
    #else
      (void)arr;
      (void)idx;
      (void)sample_index;
    #endif
   }

  __CUDA_HOST_ONLY__ inline void cuda_sample_aggregate (cuda_profiling_unit_t* const arr, const uint64_t count, double* const totals)
   {
    for (uint64_t j=0; j<PROFILING_SAMPLES; ++j)
      totals[j] = 0;

    for (uint64_t i=0; i<count; ++i) {
      for (uint64_t j=0; j<PROFILING_SAMPLES; ++j)
        totals[j] += arr[i].totals[j];
    }

    int deviceID, peak_clk;
    cudaGetDevice(&deviceID);
    cudaDeviceGetAttribute(&peak_clk, cudaDevAttrClockRate, deviceID);

    for (uint64_t j=0; j<PROFILING_SAMPLES; ++j)
      #if CUDA_USE_ASM_CLOCK
        totals[j] /= (31350000.0f * peak_clk);
      #else
        totals[j] /= (10000000.0f * peak_clk);
      #endif

    // 31350000 = 1/32ns

   }

  #endif

  __CUDA_PREFIX__ __CUDA_FORCEINLINE__ func_val_t get_fn_val (const func_val_t* const fn_vals, const point_index_t pt_index, const bool invert_fn) {
    return invert_fn ? (-fn_vals[pt_index]) : (fn_vals[pt_index]);
  }

  // Compare f(ref) < f(other_pt)
  __CUDA_PREFIX__ __CUDA_FORCEINLINE__  bool point_cmp_lt (const point_index_t ref_index, const func_val_t ref_val, const point_index_t other_pt_index, const func_val_t other_val) {
    return (ref_val == other_val) ? (ref_index < other_pt_index) : (ref_val < other_val);
  }

  __CUDA_PREFIX__ __CUDA_FORCEINLINE__  bool point_cmp_impl (const point_index_t ref, const func_val_t ref_val, const point_index_t other_pt, const func_val_t other_val) {
    return point_cmp_lt(ref, ref_val, other_pt, other_val);
  }

  __CUDA_PREFIX__ __CUDA_FORCEINLINE__ __attribute__((hot))  int8_t point_component_sign (const bool cmp_impl_result) {
    return cmp_impl_result ? -1 :  1;
  }

   extern std::set <int> debug_pt_set;

  __CUDA_PREFIX__ __CUDA_FORCEINLINE__  bool neighbour_bfs_debug (const point_index_t pt_index) {
    (void)pt_index;
    return false;
  }

  __CUDA_PREFIX__ __CUDA_FORCEINLINE__  bool neighbour_bfs_debug (const chunk_rect_t * const rect, const grid_point_t * const pt, const point_index_t pt_index) {
    (void)pt_index;
    (void)rect;
    (void)pt;
    return false;
  }

  __CUDA_PREFIX__ __CUDA_FORCEINLINE__  bool interesting_points (const point_index_t pt_index) {
    (void)pt_index;
        return false;
  }

  __CUDA_PREFIX__ __CUDA_FORCEINLINE__  void point_debug_info (const point_index_t pt_index, const grid_point_t* const other_pt, const point_index_t other_pt_index, const func_val_t* fn_vals, const bool invert_fn, const char* suffix="\n")
   {
    if (neighbour_bfs_debug(pt_index)) {
      printf("[pt=%6lu]: ", (unsigned long)other_pt_index);
      other_pt->print();
      func_val_t other_pt_val = get_fn_val(fn_vals, other_pt_index, invert_fn);
      printf(" pt_val=%1.8f in_lower_link: %d%s", other_pt_val, point_cmp_lt(other_pt_index, other_pt_val, pt_index, get_fn_val(fn_vals, pt_index, invert_fn)), suffix);
    }
   }

 };

#endif // CUDA_DEFINITIONS
