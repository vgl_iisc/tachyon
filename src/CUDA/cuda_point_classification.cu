/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#ifndef CUDA_POINT_CLASSIFICATION
#define CUDA_POINT_CLASSIFICATION

#include <grid.hpp>
#include <grid_dataset.hpp>
#include <CUDA/cuda_defs.h>
#include <cmath>

namespace extgraph
 {

  std::set <int> debug_pt_set = { 31, 43, 78, 106, 134, 162, 190, 218, 246, 274, 302, 330, 358, 384, 1293, 1321, 1347, 2438, 5600, 8026, 8839, 8847, 9229, 9236, 9646, 9684, 9754, 9782, 9796, 9810, 9824, 9852, 9880, 9908, 9935, 9963, 9990, 10030, 10070, 10099, 10113, 10127, 10141, 10169, 10238, 10266, 10294, 10322, 10349, 10377, 10437, 10457, 12026, 12430, 14800, 18478, 18506, 18534, 19278, 19631, 19662, 19693, 19721, 19747, 19775, 19803, 19831, 19859, 19887, 19916, 19944, 19971 };

  #if 0

  __CUDA_PREFIX__
  // Recursive version
  // TODO: Make this tail recursive/faster
  uint32_t UF_find (const uint32_t key, uf_bfs_data_t* uf_data_arr, uint32_t* tmp_arr)
   {
    (void)tmp_arr;

    if (key != uf_data_arr[key].parent)
      uf_data_arr[key].parent = UF_find(uf_data_arr[key].parent, uf_data_arr, tmp_arr);

    return uf_data_arr[key].parent;
   }

  #else

  __CUDA_PREFIX__ __attribute__((hot))
  __forceinline__
  // Iterative version
  uint32_t UF_find (uint32_t key, uf_bfs_data_t* const uf_data_arr, uint32_t* tmp_arr)
   {
    uint32_t count  = 0;
    uint32_t parent = 0;

    while (key != uf_data_arr[key].parent) {
      tmp_arr[count++] = key;
      key = uf_data_arr[key].parent;
    }

    parent = key;

    while (true) {
      if (0 == count) break;
      uf_data_arr[tmp_arr[--count]].parent = parent;      
    }

//    for (uint32_t i=0; i<count; ++i)
//      uf_data_arr[tmp_arr[i]].parent = parent;

    return parent;
   }

  #endif

  __CUDA_PREFIX__ __attribute__((hot))
  __forceinline__
  void UF_union (const uint32_t keyX, const uint32_t keyY, uf_bfs_data_t* const uf_data_arr, uint32_t* const tmp_arr, uint32_t surviving_components_count)
   {
    uint32_t parentX = UF_find(keyX, uf_data_arr, tmp_arr);
    uint32_t parentY = UF_find(keyY, uf_data_arr, tmp_arr);

    if (parentX != parentY)
      --surviving_components_count;

    if (
        ( (uf_data_arr[parentX].size < uf_data_arr[parentY].size) && !isinf(uf_data_arr[parentY].neighbour_func_val) ) ||
        (isinf(uf_data_arr[parentX].neighbour_func_val) && !isinf(uf_data_arr[parentY].neighbour_func_val))
       )
      swap(parentX, parentY);

    // |Component X| > |Component Y| && 
    #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
    __syncthreads();
    #endif

    uf_data_arr[parentX].size   += uf_data_arr[parentY].size;
    uf_data_arr[parentY].parent =  parentX;

    #if USE_DIRECT_UF_MAX_PT_TEST
    if (uf_data_arr[parentY].subset_max_pt_val > uf_data_arr[parentX].subset_max_pt_val) {
    #else

    if (point_cmp_lt(uf_data_arr[parentX].subset_max_pt, uf_data_arr[parentX].subset_max_pt_val, 
                     uf_data_arr[parentY].subset_max_pt, uf_data_arr[parentY].subset_max_pt_val)
       && !isinf(uf_data_arr[parentY].subset_max_pt_val)
      ) {

    #endif
      uf_data_arr[parentX].subset_max_pt     = uf_data_arr[parentY].subset_max_pt;
      uf_data_arr[parentX].subset_max_pt_val = uf_data_arr[parentY].subset_max_pt_val;      
    }

    /*

        if (point_cmp_lt(uf_data_arr[parentY].subset_max_pt, uf_data_arr[parentY].subset_max_pt_val, 
                     uf_data_arr[parentX].subset_max_pt, uf_data_arr[parentX].subset_max_pt_val)
       && !isinf(uf_data_arr[parentX].subset_max_pt_val)
      ) {

    #endif
      uf_data_arr[parentY].subset_max_pt     = uf_data_arr[parentX].subset_max_pt;
      uf_data_arr[parentY].subset_max_pt_val = uf_data_arr[parentX].subset_max_pt_val;      
    }
    */

    #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
    __syncthreads();
    #endif

   }

  template <uint32_t num_dimensions, uint32_t max_neighbour_count>
  __CUDA_PREFIX__ __attribute__((hot)) void print_component_data
      (
       const point_neighbourhood * const p_neighbours,
       const grid_point_t * const pt, const point_index_t pt_index, const func_val_t pt_val,
       grid_point_t * const pt_i, grid_point_t * const pt_j, uf_bfs_data_t* const uf_data_arr, uint32_t* tmp_arr
      )
   {
      point_index_t pti_index, ptj_index;

      for (uint32_t i=0, component_count = 0; i<max_neighbour_count; ++i) {

        if (uf_data_arr[i].size == 0 || uf_data_arr[i].parent != i)
          continue;

        ++component_count;
        p_neighbours->neighbour_perturbation_templatized<num_dimensions>(pt, pt_i, pti_index, i);

        int8_t component_sign = point_component_sign(point_cmp_impl(pti_index, uf_data_arr[i].neighbour_func_val, pt_index, pt_val));

        printf("Component [%2u: %+d]: [%lu, %2u] ", component_count, (int)component_sign, (unsigned long)pti_index, i); pt_i->print(); printf(", ");

        for (uint32_t j=0; j<max_neighbour_count; ++j)
          if (i != j && UF_find(j, uf_data_arr, tmp_arr) == i) {
            p_neighbours->neighbour_perturbation_templatized<num_dimensions>(pt, pt_j, ptj_index, j);
            printf("[%lu, %2u] ", (unsigned long)ptj_index, j); pt_j->print(); printf(", ");
          }

        printf("\n");
      }

      printf("\n");
   }

  template <uint32_t num_dimensions, bool include_padding>
  __CUDA_PREFIX__ __attribute__((hot)) __forceinline__ void point_info_update (

      const grid_point_t * const pt, const point_index_t pt_index, const func_val_t pt_val,
      const point_neighbourhood * const p_neighbours, const uf_bfs_data_t * const uf_data_arr,
      grid_point_t * const update_pt, point_index_t& update_pt_index, uint32_t i, bool& pt_in_rect, bool& pt_in_lower_link

      #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_SEPARATE_RECT_ITEMS
      , const dimension_span_t * const m_start, const dimension_span_t * const m_end, const uint64_t * const m_prefix_products
      #endif
      )
   {
    if (include_padding) {
      #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_SEPARATE_RECT_ITEMS
        neighbour_perturbation_templatized_generic <num_dimensions> (pt, update_pt, update_pt_index, i, m_start, m_end ,m_prefix_products);
      #else
        p_neighbours->neighbour_perturbation_templatized <num_dimensions> (pt, update_pt, update_pt_index, i);
      #endif

//      pt_in_rect = true;
      pt_in_lower_link = point_cmp_lt(update_pt_index, uf_data_arr[i].neighbour_func_val, pt_index, pt_val);
    }

    else {

      #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_SEPARATE_RECT_ITEMS
        pt_in_rect = neighbour_perturbation_templatized_generic <num_dimensions> (pt, update_pt, update_pt_index, i, m_start, m_end ,m_prefix_products);
      #else
        pt_in_rect = (uf_data_arr[i].size > 0) && p_neighbours->neighbour_perturbation_templatized <num_dimensions> (pt, update_pt, update_pt_index, i);
      #endif

      pt_in_lower_link = pt_in_rect ? point_cmp_lt(update_pt_index, uf_data_arr[i].neighbour_func_val, pt_index, pt_val) : false;
    }

   }

  template <bool find_component_max, bool include_padding, uint32_t num_dimensions, uint32_t max_neighbour_count>
  __CUDA_PREFIX__ __attribute__((hot))
  void neighbour_bfs_union_find (
      const point_index_t pt_index, const chunk_rect_t* const rect, const point_neighbourhood* const p_neighbours,
      const func_val_t* const fn_vals, const uint64_t global_index_offset, const bool invert_fn,

      #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
      cuda_profiling_unit_t* const profiling_arr,
      #endif

      uint32_t& true_upper_link_count, uint32_t& upper_link_count, uint32_t& lower_link_count, point_index_t* const component_highest,

      grid_point_t * pt, grid_point_t * const pt_i, grid_point_t * const pt_j, uf_bfs_data_t* const uf_data_arr, uint32_t * const tmp_arr

      )
   {
    (void)global_index_offset;
    point_index_t pti_index = pt_index, ptj_index = pt_index;
    bool pti_in_lower_link = false, ptj_in_lower_link = false;
    bool pti_in_rect = include_padding, ptj_in_rect = include_padding;

    #if 0
    if (func_val_neg_offset != 0) {
      pt->print(); printf("\n");
    }
    #endif

    uint32_t i = 0, j = 0;
    bool increment_i = false;
    bool neighbour_in_rect = false;
    uint32_t surviving_components_count = max_neighbour_count;

    const uint8_t * adjacency_arr = p_neighbours->get_adjacency_arr();
    const func_val_t pt_val = get_fn_val(fn_vals, pt_index, invert_fn);

    #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_SEPARATE_RECT_ITEMS

    #define USE_SHARED_MEM 1

    #if USE_SHARED_MEM
      const uint64_t threadNumInBlock =   ((uint64_t)threadIdx.x)
                                        + ((uint64_t)threadIdx.y) * blockDim.x
                                        + ((uint64_t)threadIdx.z) * blockDim.x * blockDim.y;

      #define CUDA_SHARED_ATTR __shared__
    #else
      #define CUDA_SHARED_ATTR //__shared__
    #endif

    CUDA_SHARED_ATTR dimension_span_t m_start[num_dimensions], m_end[num_dimensions];
    CUDA_SHARED_ATTR uint64_t m_prefix_products[num_dimensions];

    #if USE_SHARED_MEM

    switch (threadNumInBlock) {

      case 0 :
                #pragma unroll
                for (uint32_t i=0; i<num_dimensions; ++i)
                  m_start[i] = rect->m_start[i];
                break;

      case 1 :
                #pragma unroll
                for (uint32_t i=0; i<num_dimensions; ++i)
                  m_end[i] = rect->m_end[i];
                break;

      case 2 :
                #pragma unroll
                for (uint32_t i=0; i<num_dimensions; ++i)
                  m_prefix_products[i] = rect->m_prefix_products[i];
                break;

    }

    #endif

    #pragma unroll
    for (uint32_t i=0; i<num_dimensions; ++i) {
      m_start[i]           = rect->m_start[i];
      m_end[i]             = rect->m_end[i];
      m_prefix_products[i] = rect->m_prefix_products[i];
    }

    #if USE_SHARED_MEM
    __syncthreads();
    #endif

    #endif

    rect->index_to_point(pt_index, pt);

    #if CUDA_BFS_DEBUG
    uint32_t kk = 0;
    #endif

    for (uint32_t i=0; i<max_neighbour_count; ++i) {

      #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_SEPARATE_RECT_ITEMS
        neighbour_in_rect =  neighbour_perturbation_templatized_generic <num_dimensions> (pt, pt_i, pti_index, i, m_start, m_end, m_prefix_products);
      #else
        neighbour_in_rect =  p_neighbours->neighbour_perturbation_templatized <num_dimensions> (pt, pt_i, pti_index, i);
      #endif

      uf_data_arr[i].size = (neighbour_in_rect | include_padding) ? 1 : 0;

      if (!include_padding && !neighbour_in_rect) {
        uf_data_arr[i].subset_max_pt_val  = func_val_max_toggled(invert_fn);
        uf_data_arr[i].subset_max_pt      = pt_index;
        continue;
      }

      #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
      __syncthreads();
      #endif

      #if CUDA_BFS_DEBUG
      kk += neighbour_in_rect;
      #endif

      uf_data_arr[i].parent             = i;
      uf_data_arr[i].subset_max_pt      = neighbour_in_rect ? pti_index : pt_index;

      #if CODE_TYPE_DEVICE && 0
      if (m_start[num_dimensions-1]==122)
        printf("CUDA[%lu]: func_val_neg_offset: %lu\n", pt_index, func_val_neg_offset);
      #endif

        uf_data_arr[i].neighbour_func_val
      = uf_data_arr[i].subset_max_pt_val
      = neighbour_in_rect ? get_fn_val(fn_vals, pti_index, invert_fn) : func_val_max_toggled(invert_fn);

    }

    #if CUDA_BFS_DEBUG
    if (neighbour_bfs_debug(rect, pt, pt_index)) {
      printf("\n");
      for (int i=0; i<10; ++i)
        printf("*********");
      printf("\n");
      printf("\npt: [%lu] ", (unsigned long)pt_index);
      pt->print();
      printf(" pt_val: %1.8f Num Neighbours: %lu\n", pt_val, (unsigned long)kk);
    }
    #endif

    true_upper_link_count = 0;
    upper_link_count = 0;
    lower_link_count = 0;

    #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
    cuda_sample_start(profiling_arr, pt_index, CUDA_SAMPLE_STEP1_FULL_BFS);
    #endif

    #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
    __syncthreads();
    #endif

    point_info_update<num_dimensions, include_padding>(pt, pt_index, pt_val, p_neighbours, uf_data_arr, pt_i, pti_index, i, pti_in_rect, pti_in_lower_link

      #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_SEPARATE_RECT_ITEMS
      , m_start, m_end, m_prefix_products
      #endif
      );

    while (i < max_neighbour_count) {

      ++adjacency_arr;

      if (max_neighbour_count == ++j) {
        increment_i = true;
        ++i;
        j = i + 1;
        adjacency_arr += j;

        if (i == (max_neighbour_count-1) && j == max_neighbour_count)
          break;
      }

      if (!*adjacency_arr)
        continue;
//      if (!p_neighbours->is_pair_adjacent(i, j))
//        continue;

      #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
      __syncthreads();
      #endif

      if (increment_i) {
        point_info_update<num_dimensions, include_padding>(pt, pt_index, pt_val, p_neighbours, uf_data_arr, pt_i, pti_index, i, pti_in_rect, pti_in_lower_link
            #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_SEPARATE_RECT_ITEMS
            , m_start, m_end, m_prefix_products
            #endif
          );
        increment_i = false;

        if (!include_padding && !pti_in_rect)
          continue;
     }

      #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
      __syncthreads();
      #endif

      point_info_update<num_dimensions, include_padding>(pt, pt_index, pt_val, p_neighbours, uf_data_arr, pt_j, ptj_index, j, ptj_in_rect, ptj_in_lower_link
            #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_SEPARATE_RECT_ITEMS
            , m_start, m_end, m_prefix_products
            #endif
        );

      if (!include_padding && !ptj_in_rect)
        continue;

      if (include_padding) {
        if (ptj_in_lower_link == pti_in_lower_link)
          UF_union(i, j, uf_data_arr, tmp_arr, surviving_components_count);
      }
      else {
        if (pti_in_rect && ptj_in_rect && (ptj_in_lower_link == pti_in_lower_link) )
          UF_union(i, j, uf_data_arr, tmp_arr, surviving_components_count);
      }


      #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
      __syncthreads();
      #endif

    }

    #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
    __syncthreads();
    #endif

   if (find_component_max) {

     bool is_upper_link_pt;
     int8_t component_sign;
     bool is_component_representative = false;
     uint32_t components_visited = 0;

     for (uint32_t i=0; i<max_neighbour_count; ++i) {  

          #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
          __syncthreads();
          #endif

          if (components_visited == surviving_components_count)
            break;

          component_sign = point_component_sign(point_cmp_impl(uf_data_arr[i].subset_max_pt, uf_data_arr[i].subset_max_pt_val, pt_index, pt_val));

          #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
          __syncthreads();
          #endif

          component_highest[upper_link_count] = uf_data_arr[i].subset_max_pt;// + global_index_offset;

          is_component_representative = ( i == uf_data_arr[i].parent );
          is_upper_link_pt            = ( 1 == component_sign );

          components_visited    += is_component_representative;
          true_upper_link_count += is_component_representative * is_upper_link_pt;
          upper_link_count      += is_component_representative * (!isinf(uf_data_arr[i].subset_max_pt_val)) * is_upper_link_pt;
          lower_link_count      += is_component_representative * (!is_upper_link_pt);

          #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
          __syncthreads();
          #endif
      }
  
   }

    #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
    __syncthreads();
    #endif

    #define CHECK_NON_REG_POINTS 1

    #if CUDA_BFS_DEBUG && CHECK_NON_REG_POINTS
//    if (true) {
    if (upper_link_count != 1 && lower_link_count!=1) {      
    uint32_t error_pts = 0;
    for (uint32_t i=0; i<max_neighbour_count; ++i)
      if (uf_data_arr[i].parent == i && isinf(uf_data_arr[i].neighbour_func_val)) {
        printf("ERROR in [pt_index: %8lu]: inf val point is representative!\n", (unsigned long)pt_index);
        ++error_pts;
//        break;
      }
    }
    #endif

    #if CUDA_BFS_DEBUG && !CHECK_NON_REG_POINTS
    if (true_upper_link_count >=2) { //} neighbour_bfs_debug(rect, pt, pt_index)) { // && (upper_link_count != 1 && lower_link_count != 1)) {
    #if 0
    if (
        459 == pt_index 
//        upper_link_count != 1 &&
//        rect->is_boundary_point(pt)
       ) {
    #endif

      printf("pt[%lu] pt_val=%1.8f: ", (unsigned long)pt_index, pt_val); pt->print();
      printf("[upper, lower]: [%u, %u]; total=%u\n", upper_link_count, lower_link_count, upper_link_count + lower_link_count);

      printf("upper_highest: [");
      for (uint32_t i=0; i<upper_link_count; ++i)
        printf("%lu, ", (unsigned long)component_highest[i]);
      printf("]\n\n");

      printf("Components:\n");
      print_component_data<num_dimensions, max_neighbour_count>(p_neighbours, pt, pt_index, pt_val, pt_i, pt_j, uf_data_arr, tmp_arr);

      printf("Neighbours: \n");
      for (uint32_t i=0; i<max_neighbour_count; ++i) {

        p_neighbours->neighbour_perturbation_templatized<num_dimensions>(pt, pt_i, pti_index, i);

        printf("[pt=%6lu, %2u, %f]: ", (unsigned long)pti_index, i, get_fn_val(fn_vals, pti_index, invert_fn));
        pt_i->print();
        func_val_t other_pt_val = uf_data_arr[i].neighbour_func_val;
        printf(" pt_val=%1.8f in_lower_link: %d", other_pt_val, point_cmp_lt(pti_index, other_pt_val, pt_index, pt_val));

        p_neighbours->neighbour_perturbation_templatized<num_dimensions>(pt, pt_i, pti_index, UF_find(uf_data_arr[i].parent, uf_data_arr, tmp_arr));
        printf("; [parent=%lu] ", (unsigned long)pti_index); pt_i->print(); printf("\n");

      }

      for (int i=0; i<10; ++i)
        printf("=========");
      printf("\n\n");
    }
    #endif
  }

  template <uint32_t num_dimensions>
  __CUDA_PREFIX__ __CUDA_FORCEINLINE__  uint8_t cuda_point_classification
    (
      const uint32_t true_upper_link_count, 
      const uint32_t upper_link_count,
      const uint32_t lower_link_count
    )
  {
    (void)lower_link_count;

    #if CHECK_REGULAR_POINT_CLASSIFICATION
      if (true_upper_link_count == 1 && upper_link_count == 1 && lower_link_count == 1)
        return REGULAR_PT_LABEL;

      else
    #endif

    #if ENABLE_DEBUGGING
      if (true_upper_link_count >=2 && lower_link_count >= 2)
        return MULTISADDLE_LABEL;

      else
    #endif

      if (true_upper_link_count == 0) // Maximum
        return num_dimensions;

      else if (true_upper_link_count == 1 && upper_link_count == 0) // Boundary maxima
        return num_dimensions; // Regular point classified as maxima when it's neighbourhood is restricted to its boundary

      else if (true_upper_link_count >= 2) // (n-1)-saddle
        return (num_dimensions - 1);

    #if ENABLE_DEBUGGING

      else if (lower_link_count == 0) // Minimum
        return 0;

      else if (lower_link_count >= 2) // 1-saddle
        return 1;

    #endif

      else
        #if CHECK_REGULAR_POINT_CLASSIFICATION
          return UNKNOWN_CRTI_PT_LABEL;
        #else
          return REGULAR_PT_LABEL;
        #endif
  }

  template <uint32_t num_dimensions>
  __CUDA_PREFIX__  __CUDA_FORCEINLINE__ int cuda_point_contribution
    (
      const uint32_t upper_link_count, const uint32_t lower_link_count
    )
  {
    #if CUDA_ENABLE_GENERAL_ALTERNATE_SUM_CHECK
      return (upper_link_count - lower_link_count) * ((num_dimensions & 0x1) ?  1 : -1);
    #else

    if (upper_link_count == 0)
        return (num_dimensions & 0x1) ? -1 : 1;

    else if (lower_link_count == 0)
      return 1;

    else if (upper_link_count >= 2 && lower_link_count >= 2)
        return ((int)upper_link_count) - ((int)lower_link_count);

    else if (upper_link_count >= 2)
      return ((num_dimensions-1) & 0x1) ? (1 - ((int)upper_link_count)) : (((int)upper_link_count) - 1);

    else if (lower_link_count >= 2)
      return (1 - ((int)lower_link_count));

    else
      return 0;

    #endif
  }

//  template <uint32_t num_dimensions, uint32_t max_neighbour_count = 2 * ( (1UL << num_dimensions) - 1)>
  template <uint32_t num_dimensions, uint32_t max_neighbour_count = point_neighbourhood :: max_neighbours_for_dim(num_dimensions)>
  __CUDA_PREFIX__ __attribute__((hot)) __CUDA_FORCEINLINE__ point_index_t cuda_find_max_neighbour
      (
        #if !CUDA_FASTER_MAX_NEIGHBOUR_ENUM
        const grid_point_t* __restrict__ const pt, grid_point_t* __restrict__  neighbour_pt, const point_neighbourhood* __restrict__ const p_neighbours,
        #endif

        const point_index_t pt_index, const func_val_t* __restrict__ const fn_vals, const uint64_t func_val_neg_offset, const bool invert_fn

        #if CUDA_FASTER_MAX_NEIGHBOUR_ENUM
        ,const point_index_t* __restrict__ const component_highest, const uint32_t upper_link_count
        #endif

      #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
      , const point_index_t saddle_pt_index
      , cuda_profiling_unit_t* __restrict__ const profiling_arr
      #endif
      )
   {

    #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
    cuda_sample_start(profiling_arr, saddle_pt_index, CUDA_SAMPLE_STEP1_MAX_NEIGHBOUR);
    #endif

    point_index_t neighbour_index;
    const func_val_t pt_val = get_fn_val(fn_vals, pt_index, invert_fn);
    func_val_t neighbour_val;

    func_val_t max_neighbour_val = pt_val;
    point_index_t max_neighbours_index = pt_index;

    #if CUDA_FASTER_MAX_NEIGHBOUR_ENUM

      #if CODE_TYPE_DEVICE && CUDA_EXEC_FULL_LOOPS && 0

          for (uint32_t i=0; i<max_neighbour_count; ++i) {
            neighbour_index = (i < upper_link_count) ? component_highest[i] : pt_index;

      #else

          for (uint32_t i=0; i<upper_link_count; ++i) {
            neighbour_index = component_highest[i];

      #endif

    #else
  
    for (uint32_t i=0; i<max_neighbour_count; ++i) {

      if (!p_neighbours->neighbour_perturbation_templatized<num_dimensions>(pt, neighbour_pt, neighbour_index, i ))
          continue;
  
    #endif

      neighbour_val = get_fn_val(fn_vals, neighbour_index, invert_fn);

      if (point_cmp_lt(max_neighbours_index, max_neighbour_val, neighbour_index, neighbour_val)) {
        max_neighbours_index = neighbour_index;
        max_neighbour_val    = neighbour_val;
      }

    }

    #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
    cuda_sample_end(profiling_arr, saddle_pt_index, CUDA_SAMPLE_STEP1_MAX_NEIGHBOUR);
    #endif

    return max_neighbours_index;
   }

//    template <uint32_t num_dimensions, uint32_t max_neighbour_count = 2 * ( (1ULL << num_dimensions) - 1)>
  template <uint32_t num_dimensions, uint32_t max_neighbour_count = point_neighbourhood :: max_neighbours_for_dim(num_dimensions)>
  __CUDA_PREFIX__ __CUDA_FORCEINLINE__ void extremum_graph_point_single_point_impl
    (
      const point_index_t pt_index, const chunk_rect_t* __restrict__ const full_rect, const chunk_rect_t* __restrict__ const rect, const point_neighbourhood* __restrict__ const p_neighbours,
      const func_val_t* __restrict__ const fn_vals, const uint64_t func_val_neg_offset, const uint64_t global_index_offset, const bool invert_fn,
      uint8_t* __restrict__ const point_index_array, neighbour_bitmap_t* __restrict__ const edge_connectivity_array

      #if CUDA_CHECK_ALTERNATING_SUM
      , int * __restrict__ const point_contribution
      #endif

      #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
      , cuda_profiling_unit_t * __restrict__ const profiling_arr
      #endif

      ,grid_point_t * __restrict__ const pt, grid_point_t * __restrict__ const pt_i, grid_point_t * __restrict__ const pt_j, uf_bfs_data_t* __restrict__ const uf_data_arr, uint32_t * __restrict__ const tmp_arr
      , point_index_t* upper_link_highest_points

      )
   {
    uint32_t true_upper_link_count, upper_link_count, lower_link_count;

    neighbour_bfs_union_find <true, true, num_dimensions, max_neighbour_count>
      (
        PLACEHOLDER_INDEX, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, upper_link_highest_points
        ,pt ,pt_i, pt_j, uf_data_arr, tmp_arr
     );

    #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
    __syncthreads();
    #endif

    const point_index_t offset_pt_index = pt_index - func_val_neg_offset;

    point_index_array[offset_pt_index]  = cuda_point_classification<num_dimensions>(true_upper_link_count, upper_link_count, lower_link_count);

    #if 0
    edge_connectivity_array[offset_pt_index] =
        global_index_offset +
          ( (point_index_array[offset_pt_index] == num_dimensions) ? (offset_pt_index) : (upper_link_highest_points[0] - func_val_neg_offset));
    #endif

    edge_connectivity_array[offset_pt_index] =
        full_rect->index_to_neighbour_bitmap_templatized <num_dimensions> (global_index_offset + offset_pt_index,
            global_index_offset + ((point_index_array[offset_pt_index] == num_dimensions) ? offset_pt_index : (upper_link_highest_points[0] - func_val_neg_offset)) );

    #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
    cuda_sample_start(profiling_arr, pt_index, CUDA_SAMPLE_STEP1_POINT_CLASSIFICATION);
    #endif

    #if CUDA_CHECK_ALTERNATING_SUM
    point_contribution[offset_pt_index] = cuda_point_contribution  <num_dimensions>(true_upper_link_count, lower_link_count);
    #endif

    #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
    cuda_sample_end(profiling_arr, pt_index, CUDA_SAMPLE_STEP1_POINT_CLASSIFICATION);
    #endif

    #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
    __syncthreads();
    #endif

    #if 0
    edge_connectivity_array[pt_index] = cuda_find_max_neighbour <num_dimensions, max_neighbour_count> (

        #if !CUDA_FASTER_MAX_NEIGHBOUR_ENUM
        pt, pt_i, p_neighbours,
        #endif

        PLACEHOLDER_INDEX, fn_vals, invert_fn

        #if CUDA_FASTER_MAX_NEIGHBOUR_ENUM
        ,upper_link_highest_points, upper_link_count
        #endif

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
         ,pt_index , profiling_arr
        #endif
      );
    #endif

    #if CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_ENABLE_SYNCTHREADS
    __syncthreads();
    #endif

    #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
    cuda_sample_end(profiling_arr, pt_index, CUDA_SAMPLE_UNIFIED_STEP);
    #endif

   }

  template <uint32_t num_dimensions>
  __CUDA_PREFIX__
  //__CUDA_FORCEINLINE__
  void extremum_graph_point_single_point
    (
      const point_index_t pt_index, const chunk_rect_t* __restrict__ const full_rect, const chunk_rect_t* __restrict__ const rect, const point_neighbourhood* __restrict__ const p_neighbours,
      const func_val_t* __restrict__ const fn_vals, const uint64_t func_val_neg_offset, const uint64_t global_index_offset, const bool invert_fn,
      uint8_t* __restrict__ const point_index_array, neighbour_bitmap_t* __restrict__ const edge_connectivity_array

      #if CUDA_CHECK_ALTERNATING_SUM
      , int * __restrict__ const point_contribution
      #endif

      #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
      , cuda_profiling_unit_t * __restrict__ const profiling_arr
      #endif
      )
   {
//    constexpr uint32_t max_neighbour_count = 2 * ( ( 1UL << num_dimensions) - 1);
    constexpr uint32_t max_neighbour_count = point_neighbourhood :: max_neighbours_for_dim(num_dimensions);

    #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
    cuda_sample_start(profiling_arr, pt_index, CUDA_SAMPLE_UNIFIED_STEP);
    #endif

    dimension_span_t buf_pt[num_dimensions], buf_pti[num_dimensions], buf_ptj[num_dimensions];
    uf_bfs_data_t uf_data_arr[max_neighbour_count];
    uint32_t tmp_arr[max_neighbour_count];
    point_index_t upper_link_highest_points[max_neighbour_count];

    grid_point_t pt  (buf_pt , num_dimensions);
    grid_point_t pt_i(buf_pti, num_dimensions);
    grid_point_t pt_j(buf_ptj, num_dimensions);

    extremum_graph_point_single_point_impl <num_dimensions, max_neighbour_count>
      (
        pt_index, full_rect, rect, p_neighbours,
        fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
        point_index_array, edge_connectivity_array

        #if CUDA_CHECK_ALTERNATING_SUM
        , point_contribution

        #endif

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        , profiling_arr
        #endif

         ,&pt, &pt_i, &pt_j, uf_data_arr, tmp_arr, upper_link_highest_points
      );

    #if ENABLE_DEBUGGING
      if (interesting_points(pt_index) ) { // (BOUNDARY_2SADDLE_LABEL == point_index_array[pt_index]) ) {
        printf("debug pt: [%lu] ", (unsigned long)pt_index); pt.print(); printf(" classification: %d [%x]; contrib: %d\n", point_index_array[pt_index], point_index_array[pt_index], point_contribution[pt_index]);
      }
    #endif
   }

  template <uint32_t num_dimensions>
  __global__ void
//  __launch_bounds__(64, 1)
   cuda_extremum_graph_point_classification_filtered
    (
      const point_index_t pt_index_start, const point_index_t pt_index_end,
      const chunk_rect_t* __restrict__ const full_rect, const chunk_rect_t* __restrict__ const rect, const point_neighbourhood* __restrict__ const p_neighbours,
      const func_val_t* const __restrict__ fn_vals, const uint64_t func_val_neg_offset, const uint64_t global_index_offset, const bool invert_fn,
      uint8_t* __restrict__ const point_index_array, neighbour_bitmap_t* __restrict__ const edge_connectivity_array

      #if CUDA_CHECK_ALTERNATING_SUM
      , int* __restrict__ const point_contribution
      #endif

      #if CUDA_SUBKERNEL_PROFILING
      , cuda_profiling_unit_t* __restrict__ const profiling_arr
      #endif
      )
   {
    const uint64_t threadsPerBlock  =   ((uint64_t)blockDim.x) * blockDim.y * blockDim.z;

    const uint64_t threadNumInBlock =   ((uint64_t)threadIdx.x)
                                      + ((uint64_t)threadIdx.y) * blockDim.x
                                      + ((uint64_t)threadIdx.z) * blockDim.x * blockDim.y;

    const uint64_t blockNumInGrid   =   ((uint64_t)blockIdx.x)
                                      + ((uint64_t)blockIdx.y)  * gridDim.x
                                      + ((uint64_t)blockIdx.z)  * gridDim.x  * gridDim.y;

    const point_index_t pt_index = blockNumInGrid * threadsPerBlock + threadNumInBlock;

//    if ( !(pt_index_start <= pt_index && pt_index < pt_index_end) )
    if ( (pt_index < pt_index_start) || (pt_index >= pt_index_end) )
      return;

    extremum_graph_point_single_point <num_dimensions> (
        pt_index, full_rect, rect, p_neighbours,
        fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
        point_index_array, edge_connectivity_array

        #if CUDA_CHECK_ALTERNATING_SUM
        , point_contribution
        #endif

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        , profiling_arr
        #endif
        );

  }

  __CUDA_HOST_ONLY__
  void cuda_extremum_graph_point_classification_non_template
    (
      const dim3 grid_layout, const dim3 block_layout,
      const point_index_t pt_index_start, const point_index_t pt_index_end,
      const chunk_rect_t* __restrict__ const full_rect, const chunk_rect_t* __restrict__ const rect, const point_neighbourhood* __restrict__ const p_neighbours,
      const func_val_t* __restrict__ fn_vals, const uint64_t func_val_neg_offset, const uint64_t global_index_offset, const bool invert_fn,
      uint8_t* __restrict__ const point_index_array, neighbour_bitmap_t* __restrict__ const edge_connectivity_array

      #if CUDA_CHECK_ALTERNATING_SUM
      , int* __restrict__ const point_contribution
      #endif

      #if CUDA_SUBKERNEL_PROFILING
      , cuda_profiling_unit_t* __restrict__ const profiling_arr
      #endif
      )
   {

    if (rect->num_dimensions() == 2) {

        cuda_extremum_graph_point_classification_filtered <2> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );

      }
    else if (rect->num_dimensions() == 3) {

        cuda_extremum_graph_point_classification_filtered <3> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }

    else if (rect->num_dimensions() == 4) {

        cuda_extremum_graph_point_classification_filtered <4> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 5) {

        cuda_extremum_graph_point_classification_filtered <5> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }

    else if (rect->num_dimensions() == 6) {

        cuda_extremum_graph_point_classification_filtered <6> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    #if MAX_DIMENSIONS > 7
    else if (rect->num_dimensions() == 7) {

        cuda_extremum_graph_point_classification_filtered <7> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 8) {

        cuda_extremum_graph_point_classification_filtered <8> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 9) {

        cuda_extremum_graph_point_classification_filtered <9> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 10) {

        cuda_extremum_graph_point_classification_filtered <10> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 11) {

        cuda_extremum_graph_point_classification_filtered <11> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 12) {

        cuda_extremum_graph_point_classification_filtered <12> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 13) {

        cuda_extremum_graph_point_classification_filtered <13> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 14) {

        cuda_extremum_graph_point_classification_filtered <14> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 15) {

        cuda_extremum_graph_point_classification_filtered <15> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    #if MAX_DIMENSIONS > 15
    else if (rect->num_dimensions() == 16) {

        cuda_extremum_graph_point_classification_filtered <16> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 17) {

        cuda_extremum_graph_point_classification_filtered <17> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 18) {

        cuda_extremum_graph_point_classification_filtered <18> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 19) {

        cuda_extremum_graph_point_classification_filtered <19> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 20) {

        cuda_extremum_graph_point_classification_filtered <20> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 21) {

        cuda_extremum_graph_point_classification_filtered <21> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 22) {

        cuda_extremum_graph_point_classification_filtered <22> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 23) {

        cuda_extremum_graph_point_classification_filtered <23> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }


    else if (rect->num_dimensions() == 24) {

        cuda_extremum_graph_point_classification_filtered <24> <<<grid_layout, block_layout>>>
          (
            pt_index_start, pt_index_end,
            full_rect, rect, p_neighbours,
            fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
            point_index_array, edge_connectivity_array

            #if CUDA_CHECK_ALTERNATING_SUM
            , point_contribution
            #endif

            #if CUDA_SUBKERNEL_PROFILING
            , profiling_arr
            #endif
            );
    
      }
    #endif
    #endif

    else {
        fprintf(stderr, "CUDA kernel call not configured for %lu dimensions!\n", rect->num_dimensions());
        exit(-1);
    }

    #if 0
    cudaError_t kernel_error = cudaDeviceSynchronize();

    if (kernel_error != cudaSuccess)
      printf("Kernel exited with [%d: %s]\n", kernel_error, cudaGetErrorString(kernel_error));

    assert(kernel_error == cudaSuccess);
    #endif
   }

  __CUDA_HOST_ONLY__ __attribute__((hot))
  void neighbour_bfs_union_find_non_template (
      const point_index_t pt_index, const chunk_rect_t* const rect, const point_neighbourhood* const p_neighbours,
      const func_val_t* const fn_vals, const uint64_t global_index_offset, const bool invert_fn,

      #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
      cuda_profiling_unit_t* const profiling_arr,
      #endif

      uint32_t& true_upper_link_count, uint32_t& upper_link_count, uint32_t& lower_link_count, point_index_t* const component_highest,

      grid_point_t * pt, grid_point_t * const pt_i, grid_point_t * const pt_j, uf_bfs_data_t* const uf_data_arr, uint32_t * const tmp_arr

      )
  {
    constexpr bool find_component_max = true;
    constexpr bool include_padding    = false;

    if (rect->num_dimensions() == 2) {
        neighbour_bfs_union_find <find_component_max, include_padding, 2, 6> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 3) {
        neighbour_bfs_union_find <find_component_max, include_padding, 3, 14> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 4) {
        neighbour_bfs_union_find <find_component_max, include_padding, 4, 30> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 5) {
        neighbour_bfs_union_find <find_component_max, include_padding, 5, 62> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 6) {
        neighbour_bfs_union_find <find_component_max, include_padding, 6, 126> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

   #if MAX_DIMENSIONS > 7
    else if (rect->num_dimensions() == 7) {
        neighbour_bfs_union_find <find_component_max, include_padding, 7, 254> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 8) {
        neighbour_bfs_union_find <find_component_max, include_padding, 8, 510> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 9) {
        neighbour_bfs_union_find <find_component_max, include_padding, 9, 1022> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 10) {
        neighbour_bfs_union_find <find_component_max, include_padding, 10, 2046> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 11) {
        neighbour_bfs_union_find <find_component_max, include_padding, 11, 4094> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 12) {
        neighbour_bfs_union_find <find_component_max, include_padding, 12, 8190> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 13) {
        neighbour_bfs_union_find <find_component_max, include_padding, 13, 16382> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 14) {
        neighbour_bfs_union_find <find_component_max, include_padding, 14, 32766> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 15) {
        neighbour_bfs_union_find <find_component_max, include_padding, 15, 65534> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    #if MAX_DIMENSIONS > 15
    else if (rect->num_dimensions() == 16) {
        neighbour_bfs_union_find <find_component_max, include_padding, 16, 131070> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 17) {
        neighbour_bfs_union_find <find_component_max, include_padding, 17, 262142> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 18) {
        neighbour_bfs_union_find <find_component_max, include_padding, 18, 524286> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 19) {
        neighbour_bfs_union_find <find_component_max, include_padding, 19, 1048574> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 20) {
        neighbour_bfs_union_find <find_component_max, include_padding, 20, 2097150> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 21) {
        neighbour_bfs_union_find <find_component_max, include_padding, 21, 4194302> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 22) {
        neighbour_bfs_union_find <find_component_max, include_padding, 22, 8388606> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 23) {
        neighbour_bfs_union_find <find_component_max, include_padding, 23, 16777214> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 24) {
        neighbour_bfs_union_find <find_component_max, include_padding, 24, 33554430> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 25) {
        neighbour_bfs_union_find <find_component_max, include_padding, 25, 67108862> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 26) {
        neighbour_bfs_union_find <find_component_max, include_padding, 26, 134217726> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 27) {
        neighbour_bfs_union_find <find_component_max, include_padding, 27, 268435454> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 28) {
        neighbour_bfs_union_find <find_component_max, include_padding, 28, 536870910> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 29) {
        neighbour_bfs_union_find <find_component_max, include_padding, 29, 1073741822> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 30) {
        neighbour_bfs_union_find <find_component_max, include_padding, 30, 2147483646> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }

    else if (rect->num_dimensions() == 31) {
        neighbour_bfs_union_find <find_component_max, include_padding, 31, 4294967294> (
        pt_index, rect, p_neighbours,
        fn_vals, global_index_offset, invert_fn,

        #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
        profiling_arr,
        #endif

        true_upper_link_count, upper_link_count, lower_link_count, component_highest,
        pt, pt_i, pt_j, uf_data_arr, tmp_arr
       );
    }
    #endif
    #endif

    else {
      fprintf(stderr, "Union Find BFS call not configured for %lu dimensions!\n", rect->num_dimensions());
      exit(-1);
    }

  }

  __CUDA_HOST_ONLY__ void extremum_graph_point_single_point_non_template (
      const point_index_t pt_index, const chunk_rect_t* __restrict__ const full_rect, const chunk_rect_t* __restrict__ const rect, const point_neighbourhood* __restrict__ const p_neighbours,
      const func_val_t* __restrict__ const fn_vals, const uint64_t func_val_neg_offset, const uint64_t global_index_offset, const bool invert_fn,
      uint8_t* __restrict__ const point_index_array, neighbour_bitmap_t* __restrict__ const edge_connectivity_array

      #if CUDA_CHECK_ALTERNATING_SUM
      , int * __restrict__ const point_contribution
      #endif

      #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
      , cuda_profiling_unit_t * __restrict__ const profiling_arr
      #endif
    )
  {

    if (rect->num_dimensions() == 2) {
      extremum_graph_point_single_point <2> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 3) {
      extremum_graph_point_single_point <3> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 4) {
      extremum_graph_point_single_point <4> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 5) {
      extremum_graph_point_single_point <5> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 6) {
      extremum_graph_point_single_point <6> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 7) {
      extremum_graph_point_single_point <7> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    #if MAX_DIMENSIONS > 7
    else if (rect->num_dimensions() == 8) {
      extremum_graph_point_single_point <8> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 9) {
      extremum_graph_point_single_point <9> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 10) {
      extremum_graph_point_single_point <10> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 11) {
      extremum_graph_point_single_point <11> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 12) {
      extremum_graph_point_single_point <12> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 13) {
      extremum_graph_point_single_point <13> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 14) {
      extremum_graph_point_single_point <14> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 15) {
      extremum_graph_point_single_point <15> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    #if MAX_DIMENSIONS > 15
    else if (rect->num_dimensions() == 16) {
      extremum_graph_point_single_point <16> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 17) {
      extremum_graph_point_single_point <17> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 18) {
      extremum_graph_point_single_point <18> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 19) {
      extremum_graph_point_single_point <19> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 20) {
      extremum_graph_point_single_point <20> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 21) {
      extremum_graph_point_single_point <21> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 22) {
      extremum_graph_point_single_point <22> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 23) {
      extremum_graph_point_single_point <23> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 24) {
      extremum_graph_point_single_point <24> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 25) {
      extremum_graph_point_single_point <25> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 26) {
      extremum_graph_point_single_point <26> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 27) {
      extremum_graph_point_single_point <27> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 28) {
      extremum_graph_point_single_point <28> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 29) {
      extremum_graph_point_single_point <29> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 30) {
      extremum_graph_point_single_point <30> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }

    else if (rect->num_dimensions() == 31) {
      extremum_graph_point_single_point <31> (
          pt_index, full_rect, rect, p_neighbours,
          fn_vals, func_val_neg_offset, global_index_offset, invert_fn,
          point_index_array, edge_connectivity_array

          #if CUDA_CHECK_ALTERNATING_SUM
          , point_contribution
          #endif

          #if CUDA_SUBKERNEL_PROFILING && CODE_TYPE_DEVICE
          profiling_arr
          #endif
      );
    }
    #endif
    #endif

    // Dont delete this comment. Anchor for DIMENSION_ADDITION

    else {
        fprintf(stderr, "Extremum Graph call for data dimensions=%lu not configured\n", rect->num_dimensions());
        exit(-1);
    }

  }

};

#endif // CUDA_POINT_CLASSIFICATION
