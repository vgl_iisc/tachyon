/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#include <grid.hpp>
#include <cstdio>
#include <cerrno>
#include <cassert>
#include <cstring>
#include <string>
#include <map>
#include <unistd.h>
#include <fcntl.h>

#define DEBUG_XML_GENERATION 0

namespace extgraph {

  // Ref: https://kitware.github.io/vtk-examples/site/VTKFileFormats/#xml-file-example

  class XMLTag
   {

     protected:

      /* state=0 (ready) -> 1 (writing) -> 2 (closed) */

      int state;
      FILE * file;
      std::string filename;

      std::string tag_name;
      std::map <std::string, std::string> tag_attributes;

      std::string create_tmpfilename (int& filefd, bool keep_open=true) {
        char * tmp_filename = new char[31];
        strncpy(tmp_filename, "/tmp/xmltagappender-XXXXXX.xml", 31);
        filefd = mkostemps(tmp_filename, 1 + 3, O_SYNC);
        if (!keep_open) close(filefd);
        std::string newfilename = std::string(tmp_filename);
        delete [] tmp_filename;
        return newfilename;
      }

      void create_tmpfile (const std::string& preferred_filename) {
        int fd;

        if (preferred_filename.length() == 0) {
          filename = create_tmpfilename(fd);
          /*
          char * tmp_filename = new char[31];
          strncpy(tmp_filename, "/tmp/xmltagappender-XXXXXX.xml", 31);
          fd = mkstemps(tmp_filename, 1 + 3);
          filename = std::string(tmp_filename);
          delete [] tmp_filename;
          */
        }

        else {
          filename = preferred_filename;
          fd = open(filename.c_str(), O_CREAT|O_RDWR|O_SYNC|O_DIRECT, ((S_IRWXU) | (S_IRGRP|S_IXGRP) | (S_IROTH|S_IXOTH)) /* 755 */);
//          fd = open(filename, O_CREAT|O_RDONLY, (S_IRUSR|S_IWUSR) /* 600 */);
        }

        assert(fd > 0);
        file = fdopen(fd, "a+");
      }

     public:

      XMLTag (const std::string& tag_name, const std::string& preferred_filename) {
        this->tag_name = tag_name;
        this->state = 0;
        create_tmpfile(preferred_filename);
      }

      XMLTag (const std::string& tag_name="DataArray")
        : XMLTag(tag_name, "") {
      }

      ~XMLTag() {
        if (file != nullptr) {
          if (this->state==0) {
            fflush(file);
            fclose(file);
            file = nullptr;
          }
          else if (this->state==1)
            this->stop();
        }
//        delete_file();
      }

      std::string get_filename() const {
        return this->filename;
      }

      void add_attr (const std::string& key, const std::string& val) {
        tag_attributes[key] = val;
      }

      void add_attr (const std::string& key, const uint64_t val) {
        tag_attributes[key] = std::to_string(val);
      }

      bool start() {
        if (this->state!=0)
          return false;

        assert(fprintf(file, "  <%s", this->tag_name.c_str()) > 0);

        for (auto entry_iter: this->tag_attributes) {
          assert(fprintf(file, " %s=\"%s\"", entry_iter.first.c_str(), entry_iter.second.c_str()) > 0);
        }

        assert(fprintf(file, ">\n") > 0);
        this->state = 1;
        return true;
      }

      bool stop() {
        if (this->state != 1)
          return false;

        assert(fprintf(file, "\n</%s>\n\n", this->tag_name.c_str()) > 0);
        fflush(file);
        fclose(file);
        file = nullptr;
        this->state = 2;
        return true;
      }

      void delete_file() {
        if (this->state != 2)
          return;
        if (file != nullptr) {
          fflush(file);
          fclose(file);
          file = nullptr;
        }
        unlink(this->filename.c_str());
      }

    };

  class XMLTagAppender : public XMLTag {

    public:

      bool addI (const int64_t element) {
        if (this->state != 1)
          return false;

        assert(fprintf(file, "%ld ", element) > 0);
        return true;
      }

      bool add (const uint64_t element) {
        if (this->state != 1)
          return false;

        assert(fprintf(file, "%lu ", element) > 0);
        return true;
      }

      bool addF (const func_val_t element) {
        if (this->state != 1)
          return false;

        assert(fprintf(file, "%f ", element) > 0);
        return true;
      }

      bool add (const grid_point_t * const pt) {
        if (this->state != 1)
          return false;

        for (uint32_t i=0; i<pt->size(); ++i)
          assert(fprintf(file, "%lu ", (uint64_t)pt->operator[](i)) > 0);
        return true;
      }

      bool add (const std::string& element) {
        if (this->state != 1)
          return false;

        assert(fprintf(file, "%s ", element.c_str()) > 0);
        return true;
      }

   };

  class NestedXMLWriter : public XMLTag {

    protected:

      std::vector <std::string> content_filenames;

    public:

      NestedXMLWriter (const std::string tagname): XMLTag(tagname) { }

      void add (const std::string& filename) {
        content_filenames.push_back(filename);
      }

      void add (const XMLTag& appender) {
        #if DEBUG_XML_GENERATION
          printf("NestedXMLWriter[%s].add(%s)\n", tag_name.c_str(), appender.get_filename().c_str());
        #endif

        content_filenames.push_back(appender.get_filename());
      }

      void create() {
        if (this->state == 2)
          return;

        start();

        #if 1
        if (file != nullptr) {
          fflush(file);
          fclose(file);
        }
        #endif

        std::string cmd_string = "cat \"" + get_filename() + "\"";

        for (std::string fname: content_filenames)
          cmd_string = cmd_string + " \"" + fname + "\" ";

        int tmpfd;
        std::string tmp_file = create_tmpfilename(tmpfd, false);
        cmd_string = cmd_string + " > \"" + tmp_file + "\"";

        #if DEBUG_XML_GENERATION
          printf("cat-cmd: %s\n", cmd_string.c_str());
        #endif

        assert(system(cmd_string.c_str())==0);
        filename = tmp_file;
        FILE * f = fopen(filename.c_str(), "a+");
        assert(f != nullptr);
        assert(fprintf(f, "\n</%s>\n\n", tag_name.c_str()));
        fflush(f);
        fclose(f);
        this->state = 2;
      }

  };

};
