/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#ifndef EXT_GRAPH_BASE
#define EXT_GRAPH_BASE

#include <extremum_graph.hpp>
#include <grid_dataset.hpp>
#include <xmlappender.hpp>

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#define FILE_PERMISSIONS ((S_IRWXU) | (S_IRGRP|S_IXGRP) | (S_IROTH|S_IXOTH)) // 755
#define DEBUG_STD_PERSISTENCE_ALGO 0

namespace extgraph {

  extremum_graph_t :: extremum_graph_t (dataset_t * const dataset_instance) :
    m_dataset_rect(dataset_instance->get_dataset_rect()),
    m_ref_dataset_instance(dataset_instance)
   {
    m_max_neighbour_array      = nullptr;
    debug_simplification       = false;
    m_edge_bundling_performed  = false;
    m_std_persistence_simplification_performed = false;
    m_saturated_persistence_simplification_performed = false;
    m_invert_fn_val            = dataset_instance->is_fn_value_negated();
    dataset_instance->get_dataset_func_range(m_dataset_func_range);

    m_maxima_surviving.clear();
    m_saddle_surviving.clear();

//    assert(dataset_instance->get_dataset_func_range(m_dataset_func_range));
//    printf("m_dataset_func_range: %f\n", m_dataset_func_range);
   }

  extremum_graph_t :: ~extremum_graph_t ()
   {
    if (nullptr != m_max_neighbour_array)
      delete [] m_max_neighbour_array;
   }

  void extremum_graph_t :: init_after_setup ()
   {
    m_maxima_surviving.reserve(m_chunk_maxima.size());
    m_saddle_surviving.reserve(m_chunk_saddles.size());
    m_saddle_removed.reserve(m_chunk_saddles.size());

    for (uint64_t i=0; i<m_chunk_maxima.size(); ++i)
        m_maxima_surviving.push_back(true);

    for (uint64_t i=0; i<m_chunk_saddles.size(); ++i) {
        m_saddle_surviving.push_back(true);
        m_saddle_removed.push_back(false);
        m_saddle_zero_persistence.push_back(false);
        m_saddle_maxima_mapping_status.push_back(saddle_maxima_path_status_t());
        m_saddle_persistence_removed.push_back(false);
    }

    m_maxima_surviving.shrink_to_fit();
    m_saddle_surviving.shrink_to_fit();
    m_saddle_removed.shrink_to_fit();
    m_saddle_zero_persistence.shrink_to_fit();

    auto sort_saddle_maxima_mappings = [=] (const int thread_idx, const int num_threads) -> void {

      const uint64_t start_index = segment_range(m_saddle_maxima_mapping.size(), num_threads, thread_idx);
      const uint64_t   end_index = segment_range(m_saddle_maxima_mapping.size(), num_threads, thread_idx + 1);

      for (uint64_t i=start_index; i<end_index; ++i)
        sort_vector(&m_saddle_maxima_mapping[i]);
    };

    begin_thread_pool(sort_saddle_maxima_mappings);
   }

  void extremum_graph_t :: add_maximum  (const point_index_t maximum_index)
   {
    m_chunk_maxima.push_back(maximum_index);
   }

  void extremum_graph_t :: add_saddle  (const point_index_t saddle_index)
   {
    m_chunk_saddles.push_back(saddle_index);
   }

  bool extremum_graph_t :: add_maximum_saddle_map  (const point_index_t maximum_index, const point_index_t saddle_index )
   {
    const uint64_t maximum_ordinal = get_maximum_ordinal(maximum_index);
    if (maximum_ordinal == m_chunk_maxima.size())
        return false;

    m_maxima_saddle_mapping[maximum_ordinal].push_back(saddle_index);
    return true;
   }

  bool extremum_graph_t :: add_saddle_maximum_map  (const point_index_t saddle_index , const point_index_t maximum_index)
   {
    const uint64_t saddle_ordinal = get_saddle_ordinal(saddle_index);
    if (saddle_ordinal == m_chunk_saddles.size())
        return false;

    m_maxima_saddle_mapping[saddle_ordinal].push_back(maximum_index);
    return true;
   }

  bool extremum_graph_t :: add_maximum_ordinal_saddle_map  (const uint64_t maximum_ordinal, const point_index_t saddle_index )
   {
    if (maximum_ordinal >= maxima_count())
        return false;

    m_maxima_saddle_mapping[maximum_ordinal].push_back(saddle_index);
    return true;
   }

  bool extremum_graph_t :: add_saddle_ordinal_maximum_map  (const uint64_t saddle_ordinal , const point_index_t maximum_index)
   {
    if (saddle_ordinal >= saddle_count())
        return false;

    m_maxima_saddle_mapping[saddle_ordinal].push_back(maximum_index);
    return true;
   }

  uint64_t extremum_graph_t :: get_maximum_ordinal (const point_index_t maximum_index) const
   {
    uint64_t maximum_ordinal;
    if (binary_search_find_index<point_index_t>(&m_chunk_maxima, maximum_index, maximum_ordinal))
      return maximum_ordinal;
    else
      return m_chunk_maxima.size();
   }

  uint64_t extremum_graph_t :: get_saddle_ordinal (const point_index_t saddle_index) const
   {
    uint64_t saddle_ordinal;
    if (binary_search_find_index<point_index_t>(&m_chunk_saddles, saddle_index, saddle_ordinal))
      return saddle_ordinal;
    else
      return m_chunk_saddles.size();
   }

  bool extremum_graph_t :: erase_saddle (const uint64_t saddle_ordinal)
   {
    if (saddle_ordinal >= m_chunk_saddles.size())
      return false;

    for (uint64_t i=0; i<m_saddle_maxima_mapping[saddle_ordinal].size(); ++i)
      if (m_saddle_maxima_mapping[saddle_ordinal][i] >= m_chunk_maxima.size())
        return false;

    for (uint64_t i=0; i<m_saddle_maxima_mapping[saddle_ordinal].size(); ++i) {
      const uint64_t adjacent_maximum_ordinal = m_saddle_maxima_mapping[saddle_ordinal][i];
      vector_remove_item(&m_maxima_saddle_mapping[adjacent_maximum_ordinal], saddle_ordinal);
    }

    m_saddle_maxima_mapping[saddle_ordinal].clear();
    m_saddle_removed[saddle_ordinal]   = true;
    m_saddle_surviving[saddle_ordinal] = false;
    return true;
   }

  bool extremum_graph_t :: saddle_exists (const uint64_t saddle_ordinal) const noexcept
   {
    return (saddle_ordinal < m_chunk_saddles.size()) && !m_saddle_removed[saddle_ordinal] && m_saddle_surviving[saddle_ordinal];
   }

  bool extremum_graph_t :: remove_sadle_maximum_mapping (const uint64_t saddle_ordinal, const uint64_t maximum_ordinal)
   {
    const uint64_t maximum_ordinal_index = vector_get_index(&m_saddle_maxima_mapping[saddle_ordinal], maximum_ordinal);

    if (maximum_ordinal_index >= m_saddle_maxima_mapping[saddle_ordinal].size())
      return false;

    else {

      for (uint64_t i=maximum_ordinal_index; i<m_saddle_maxima_mapping[saddle_ordinal].size()-1; ++i)
        m_saddle_maxima_mapping_status[saddle_ordinal].set(i, m_saddle_maxima_mapping_status[saddle_ordinal][i+1]);

      m_saddle_maxima_mapping_status[saddle_ordinal][m_saddle_maxima_mapping[saddle_ordinal].size()-1] = 0;
      vector_remove_item(&m_saddle_maxima_mapping[saddle_ordinal], maximum_ordinal);
      vector_remove_item(&m_maxima_saddle_mapping[maximum_ordinal], saddle_ordinal);
      return true;
    }
   }

  __CUDA_HOST_ONLY__ void extremum_graph_t :: clear () noexcept
   {
    m_chunk_maxima.clear();
    m_chunk_saddles.clear();
    m_chunk_1saddles.clear();
    m_chunk_minima.clear();
    m_chunk_multi_saddles.clear();

    m_maxima_surviving.clear();
    m_saddle_surviving.clear();

    m_maxima_saddle_mapping.clear();
    m_saddle_maxima_mapping.clear();
   }

  __CUDA_HOST_ONLY__ uint64_t get_edge_count (const std::vector <std::vector<point_index_t>> * const adjacency_map) noexcept
   {
    uint64_t tmp_start, tmp_end;

    host_clock(tmp_start);

    const int num_cpus = properties::num_threads();
    const uint64_t num_elements = adjacency_map->size();
    const uint64_t num_elements_per_cpu = (num_elements + num_cpus - 1) / num_cpus;

    std::vector <uint64_t> worker_thread_edge_count;
    worker_thread_edge_count.reserve(num_cpus);

    for (int i=0; i<num_cpus; ++i)
      worker_thread_edge_count.push_back(0);

    auto edge_count_lambda =
    [&]
    (const int thread_idx) -> void {

      const uint64_t start = thread_idx * num_elements_per_cpu;
      const uint64_t end   = min(start + num_elements_per_cpu, num_elements);

      for (uint64_t i=start; i<end; ++i)
        worker_thread_edge_count[thread_idx] += adjacency_map->at(i).size();

      printf("edge_count_lambda[%d] exiting\n", thread_idx);
    };

    std::vector <std::thread> worker_thread_list;

    for (int i=0; i<num_cpus; ++i)
      worker_thread_list.push_back(std::thread(edge_count_lambda, i));

    uint64_t edge_count_final = 0;

    for (int i=0; i<num_cpus; ++i) {
      worker_thread_list[i].join();
      edge_count_final += worker_thread_edge_count[i];
    }

    host_clock(tmp_end);
    print_time_diff(tmp_end - tmp_start, "Edge Count Time");

    return edge_count_final;
   }

  __CUDA_HOST_ONLY__ bool file_exists (const std::string * const fname)
   {
    struct stat ss;
    return 0 == stat(fname->c_str(), &ss);
   }

  __CUDA_HOST_ONLY__ std::string generate_header (const uint64_t num_elements, const uint64_t edge_count, const uint64_t num_dimensions)
   {
    char buf[] = "/tmp/parallel-ext-XXXXXX.vtp.header";
    const int header_fd = mkstemps(buf, 1 + 3 + 1 + 6);

    if (header_fd < 0)
      return std::string();

    #if VAL_TYPE_DOUBLE

    const char VTP_HEADER[] = 

        "<VTKFile type=\"PolyData\" version=\"1.0\" byte_order=\"%sEndian\" header_type=\"UInt64\">\n" \
        "  <PolyData>\n" \
        "    <Piece NumberOfPoints=\"%lu\" NumberOfLines=\"%lu\">\n" \
        "      <Points>\n" \
        "        <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"%lu\" format=\"ascii\">\n";

    #else

    const char VTP_HEADER[] = 

        "<VTKFile type=\"PolyData\" version=\"1.0\" byte_order=\"%sEndian\" header_type=\"UInt64\">\n" \
        "  <PolyData>\n" \
        "    <Piece NumberOfPoints=\"%lu\" NumberOfLines=\"%lu\">\n" \
        "      <Points>\n" \
        "        <DataArray type=\"Float32\" Name=\"Points\" NumberOfComponents=\"%lu\" format=\"ascii\">\n";

    #endif

    assert(dprintf(header_fd, VTP_HEADER, ( dataset_t :: is_machine_little_endian()  ? "Little" : "Big" ), num_elements, edge_count, num_dimensions) > 0);
    close(header_fd);

    return std::string(buf);
   }

  __CUDA_HOST_ONLY__ void generate_grid_points_for_rect_partition (const int thread_idx, const int num_threads, const chunk_rect_t * const rect, std::vector <std::string> * const parts_filenames)
   {
    std::string tmp_grid_points_part_filename = "/tmp/parallel-ext-part-rect=" + rect->to_string () + "XXXXXX.vtp.grid";
    char * buf = new char [tmp_grid_points_part_filename.length() + 1];
    strncpy(buf, tmp_grid_points_part_filename.c_str(), tmp_grid_points_part_filename.length() + 1);
    const int part_file_fd = mkstemps(buf, 1 + 3 + 1 + 4);
    tmp_grid_points_part_filename = buf;
    delete [] buf;
    printf("Grid points tmp [%d]: Writing to %s\n", thread_idx, tmp_grid_points_part_filename.c_str());

    const point_index_t start_index = segment_range(rect->num_elements(), num_threads, thread_idx);
    const point_index_t end_index   = segment_range(rect->num_elements(), num_threads, thread_idx + 1);

    if (-1 == part_file_fd) {
      return;
    }

    grid_point_t p(rect->num_dimensions());

    for (point_index_t i=start_index; i<end_index; ++i) {

      if (!thread_idx && has_input()) {
        printf("generate_grid_points_for_rect: %lu/%lu\n", i-start_index, end_index-start_index);
      }

      rect->index_to_point(i,&p);

      for (uint32_t i=0; i+1<p.size(); ++i)
        assert(dprintf(part_file_fd, "%u ", p[i]) > 0);

      assert(dprintf(part_file_fd, "%u\n", p[p.size()-1]) > 0);
    }

    close(part_file_fd);
    parts_filenames->at(thread_idx) = tmp_grid_points_part_filename;
   }

  __CUDA_HOST_ONLY__ std::string generate_grid_points_for_rect (const chunk_rect_t * const rect)
   {
    uint64_t t1, t2;
    host_clock(t1);

    std::string tmp_grid_points_filename = "/tmp/parallel-ext-rect=" + rect->to_string () + ".vtp.grid";
    std::vector <std::string> parts_filenames;

    if (file_exists(&tmp_grid_points_filename)) {
      printf("Grid points file '%s' exists. Skipping\n", tmp_grid_points_filename.c_str());
      return tmp_grid_points_filename;
    }

    grid_point_t p(rect->num_dimensions());
    const int num_threads = properties::num_threads();

    for (int i=0; i<num_threads; ++i)
      parts_filenames.push_back(std::string());

    begin_thread_pool(num_threads, generate_grid_points_for_rect_partition, rect, &parts_filenames);

    for (int i=0; i<num_threads; ++i)
      assert(parts_filenames[i].length() > 0);

    unlink(tmp_grid_points_filename.c_str());
    if (system(("touch \"" + tmp_grid_points_filename + "\"").c_str()))
      return std::string();

    for (int i=0; i<num_threads; ++i)
      if (system(("cat \"" + parts_filenames[i] + "\" >> \"" + tmp_grid_points_filename.c_str() + "\"").c_str()))
        return std::string();

    int header_fd = open(tmp_grid_points_filename.c_str(), O_CREAT|O_RDWR, FILE_PERMISSIONS);

    if (-1 == header_fd)
      return std::string();

    lseek(header_fd, 0, SEEK_END);

    const char VTP_MIDDLE_1[] = 
        "        </DataArray>\n" \
        "      </Points>\n" \
        "      <Lines>\n" \
        "        <DataArray type=\"Int64\" Name=\"connectivity\" format=\"ascii\">\n";

    assert(dprintf(header_fd, VTP_MIDDLE_1) > 0);
    close(header_fd);

    printf("Grid Points file created\n");
    host_clock(t2);
    print_time_diff(t2-t1, "Grid points file creation");
    return tmp_grid_points_filename;
   }

  __CUDA_HOST_ONLY__ void adjacency_map_sanity_check (const critical_point_map_t * const adjacency_map, const uint64_t num_maxima)
   {
    for (uint64_t ordinal=0; ordinal<adjacency_map->size(); ++ordinal) {

      const uint64_t from_ordinal = (ordinal < num_maxima) ? ordinal : (ordinal - num_maxima);

      for (uint64_t k=0; k<adjacency_map->at(ordinal).size(); ++k) {

        const uint64_t to_ordinal =   (ordinal < num_maxima)
                                    ? (num_maxima + adjacency_map->at(ordinal).at(k))
                                    : adjacency_map->at(ordinal).at(k);

	#if SADDLE_MAXIMA_MAPPING_OPTIMIZE
	assert(!adjacency_map->at(to_ordinal).contains(from_ordinal));
	#else
        assert(
                std::find(
                          adjacency_map->at(to_ordinal).begin(),
                          adjacency_map->at(to_ordinal).end(),
                          from_ordinal
                         )
                       == adjacency_map->at(to_ordinal).end()
              );
	#endif
      }

    }

   }

 uint64_t get_saddle_adjacent_extremum_for_path
  (
     const extremum_graph_t       * const ext_graph,
     const uint64_t                       saddle_ordinal,
     const uint64_t                       target_extremum,
           std::vector <uint64_t> * const collect_vec = nullptr
  );

  void trace_transitive_arcs
   (
    const extremum_graph_t       * const ext_graph,
    const uint64_t                       saddle_ordinal,
    const uint64_t                       extrema_ordinal,
          std::vector <uint64_t> * const arc_vector,
          std::vector <uint64_t> * const debug_arc_vector
   )
   {
    (void)debug_arc_vector;
    get_saddle_adjacent_extremum_for_path(ext_graph, saddle_ordinal, extrema_ordinal, arc_vector);
   }

  uint64_t get_terminal_maximum
   (
    const extremum_graph_t       * const ext_graph,
    const uint64_t                       saddle_ordinal,
    const uint64_t                       target_extremum
   )
   {
    for (auto new_old_pair : ext_graph->m_saddle_adjacent_cancelled_extrema_paths[saddle_ordinal]) {
      if (new_old_pair.surviving_connected_extremum == target_extremum)
        return new_old_pair.adjacent_extremum;
    }

    return 0xffffffffffffffff;
   }

void trace_transitive_paths
   (
    const extremum_graph_t       * const ext_graph,
    const uint64_t                       saddle_ordinal,
    const uint64_t                       extrema_index,
          std::vector <uint64_t> * const path_vector
   )
   {
    path_vector->clear();

    #if 0
      path_vector->push_back(ext_graph->m_chunk_saddles[saddle_ordinal]);
      path_vector->push_back(extrema_index);
      return;
    #endif

    std::vector <uint64_t> debug_arc_vector;
    std::vector <uint64_t> arc_vector;
    std::vector <std::vector<uint64_t>> saddle_ascending_manifold;

    const uint64_t extrema_ordinal = ext_graph->get_maximum_ordinal(extrema_index);
    trace_transitive_arcs(ext_graph, saddle_ordinal, extrema_ordinal, &arc_vector, &debug_arc_vector);

    for (uint64_t i=0; i<arc_vector.size(); i+=2) {
      const uint64_t curr_saddle_ordinal = arc_vector[i]; // ext_graph->get_saddle_ordinal(arc_vector[i]);

      saddle_ascending_manifold.clear();

      ext_graph->m_ref_dataset_instance->saddle_immediate_ascending_manifold(ext_graph, curr_saddle_ordinal, &saddle_ascending_manifold);

      bool flag = false;
      uint64_t terminal, terminal_ordinal, k;

      for (k=0; k<saddle_ascending_manifold.size(); ++k) {

        std::vector <uint64_t>& path = saddle_ascending_manifold[k];
        terminal = path[path.size()-1];
        terminal_ordinal = ext_graph->get_maximum_ordinal(terminal);

//        val = get_terminal_maximum(ext_graph, curr_saddle_ordinal, terminal_ordinal);

        if (terminal_ordinal == arc_vector[i+1])
//        if (val == extrema_ordinal) // arc_vector[i+1])
        {

          if (i > 0)
            path_vector->push_back(ext_graph->m_chunk_saddles[arc_vector[i]]);

          path_vector->insert(path_vector->end(), path.begin(), path.end());

          flag = true;
        }

      }

      assert(flag);
    }

   }

  void fill_traced_paths
   (
    const extremum_graph_t       * const ext_graph,
    const uint64_t                       saddle_ordinal,
    const std::vector <uint64_t> * const arc_vector,
          std::vector <uint64_t> * const path_vector,
          std::set <std::pair<uint64_t, uint64_t>> * const visited_arc_set
   )
   {
    (void)saddle_ordinal;
    (void)visited_arc_set;
    path_vector->clear();

    std::vector <std::vector<uint64_t>> saddle_ascending_manifold;

    for (uint64_t i=0; i<arc_vector->size(); i+=2) {
      #if 0
      const std::pair <uint64_t, uint64_t> arc = std::make_pair(arc_vector->operator[](i), arc_vector->operator[](i+1));

      if (visited_arc_set->find(arc) != visited_arc_set->end())
        continue;

      visited_arc_set->insert(arc);
      #endif

      const uint64_t curr_saddle_ordinal = arc_vector->operator[](i); // ext_graph->get_saddle_ordinal(arc_vector->operator[](i));

      saddle_ascending_manifold.clear();

      ext_graph->m_ref_dataset_instance->saddle_immediate_ascending_manifold(ext_graph, curr_saddle_ordinal, &saddle_ascending_manifold);

      bool flag = false;
      uint64_t terminal, terminal_ordinal, k;

      for (k=0; k<saddle_ascending_manifold.size(); ++k) {

        std::vector <uint64_t>& path = saddle_ascending_manifold[k];
        terminal = path[path.size()-1];
        terminal_ordinal = ext_graph->get_maximum_ordinal(terminal);

        if (terminal_ordinal == arc_vector->at(i+1))
        {

//          if (i > 0)
            path_vector->push_back(ext_graph->m_chunk_saddles[curr_saddle_ordinal]); // ascending manifold paths dont contain origin saddle, insert them manually


          path_vector->insert(path_vector->end(), path.begin(), path.end());

          flag = true;
        }

      }

      assert(flag);
    }

   }

  void path_traversal_v3
   (
    const uint64_t                                    saddle_ordinal,
    const extremum_graph_t                    * const ext_graph,
          std::vector <std::vector<uint64_t>> * const traversed_paths,
          reachable_extrema_list_t            * const reachable_extrema
   );

  void trace_and_fill_paths
   (
    const extremum_graph_t                         * const ext_graph,
    const uint64_t                                         saddle_ordinal,
    const std::vector <uint64_t>                   * const connected_maxima_collection,
          std::vector <std::vector<uint64_t>>      * const paths_list,
          std::set <std::pair<uint64_t, uint64_t>> * const visited_arc_set
   )
   {
    reachable_extrema_list_t reachable_extrema;
    std::vector <std::vector<uint64_t>> traversed_paths;
    std::vector <uint64_t> path_vector;

    path_traversal_v3(saddle_ordinal, ext_graph, &traversed_paths, &reachable_extrema);

    for (uint32_t i=0; i<traversed_paths.size(); ++i) {
      const uint64_t adjacent_maximum_ordinal = traversed_paths[i][1];
      const uint64_t adjacent_maximum = ext_graph->m_chunk_maxima[adjacent_maximum_ordinal];

      if (vector_contains(connected_maxima_collection, adjacent_maximum)) {
        fill_traced_paths(ext_graph, saddle_ordinal, &traversed_paths[i], &path_vector, visited_arc_set);
        paths_list->push_back(path_vector);
      }

    }

   }

  class vtp_point_mapper {

    protected:

      bool first_map_call;

      const dataset_t * m_ref_dataset_instance;
      int dim_count;
      grid_point_t buffer = grid_point_t :: zero(3);

    public:

      vtp_point_mapper (const dataset_t * const ref) : m_ref_dataset_instance(ref) {
        dim_count = ref->m_num_dimensions;
        first_map_call = true;
      }

      const grid_point_t * map (const grid_point_t * const pt, const uint64_t pt_index=0xffffffffffffffff) {
        (void)pt_index;
        switch (dim_count) {

          case 3: return pt;

          case 2: {
            buffer[0] = pt->operator[](0);
            buffer[1] = pt->operator[](1);
            #if 0
            buffer[2] = 1000 * ((pt_index == 0xffffffffffffffff) ? 0 : m_ref_dataset_instance->get_fn_val(pt_index));
            #else
            buffer[2] = 0;
            #endif
            return &buffer;
          }

          default:
            if (first_map_call)
              fprintf(stderr, "No %d-to-3D map defined for %d dimensional points\n", dim_count, dim_count);

            first_map_call = false;
            return nullptr;
        }

      }

  };

  __CUDA_HOST_ONLY__ int extremum_graph_t :: dump_geometry_as_vtp
    (
      const arc_redirection_info_t * const arc_redirection_list,
      const std::string&                   filename,
      const uint8_t                        direction_flag)
   {
    return dump_geometry_as_vtp_v2(arc_redirection_list, filename, direction_flag);
   }

  __CUDA_HOST_ONLY__ int extremum_graph_t :: dump_geometry_as_vtp_v2
    (
      const arc_redirection_info_t * const arc_redirection_list,
      const std::string&                   filename,
      const uint8_t                        direction_flag)
    {

      if (!maxima_saddle_mapping_cleared) {
        for (uint64_t i=0; i<m_chunk_maxima.size(); ++i)
          m_maxima_saddle_mapping[i].clear();
      }

      const bool direction_up   = direction_flag & 0b01; // Saddle-Extrema
      const bool direction_down = direction_flag & 0b10; // Extrema-Saddle

      uint64_t num_maxima = 0, num_saddles = 0, num_arcs = 0;
      vtp_point_mapper point_mapper(m_ref_dataset_instance);

      XMLTagAppender points, functionVals, survivingCP, morseIndex, connectivity, offsets, types, SourceSaddle, DestinationExtremum;
      NestedXMLWriter pointsTag("Points"), pointDataTag("PointData"), cellsTag("Cells"), cellDataTag("CellData"), pieceTag("Piece"), unstructuredGridTag("UnstructuredGrid");

      uint64_t num_points=0, num_edges = 0;
      std::vector <uint64_t> connected_maxima_collection;
      std::vector <std::vector<uint64_t>> paths_list;
      std::set <std::pair<uint64_t, uint64_t>> visited_arc_set;

      NestedXMLWriter vtkFileTag("VTKFile");


    #if !DEBUG_STD_PERSISTENCE_ALGO
//    adjacency_map_sanity_check(adjacency_map, m_chunk_maxima.size());path_vector
    #endif

    uint64_t down_arcs = 0, up_arcs = 0;

    grid_point_t pt1(m_dataset_rect.num_dimensions()), pt2(m_dataset_rect.num_dimensions());

    // type="Int64" Name="Points" NumberOfComponents="3" format="ascii">
    points.add_attr("type", "Int64");
    points.add_attr("Name", "Points");
    points.add_attr("NumberOfComponents", 3);
    points.add_attr("format", "ascii");
    points.start();

    #if VAL_TYPE_DOUBLE
    functionVals.add_attr("type", "Float64");
    #else
    functionVals.add_attr("type", "Float32");
    #endif
    functionVals.add_attr("Name", "FunctionVals");
    functionVals.add_attr("format", "ascii");
    functionVals.start();

    morseIndex.add_attr("type", "Int64");
    morseIndex.add_attr("Name", "MorseIndex");
    morseIndex.add_attr("format", "ascii");
    morseIndex.start();

    survivingCP.add_attr("type", "Int64");
    survivingCP.add_attr("Name", "survivingCP");
    survivingCP.add_attr("format", "ascii");
    survivingCP.start();

    connectivity.add_attr("type", "Int64");
    connectivity.add_attr("Name", "connectivity");
    connectivity.add_attr("format", "ascii");
    connectivity.start();

    offsets.add_attr("type", "Int64");
    offsets.add_attr("Name", "offsets");
    offsets.add_attr("format", "ascii");
    offsets.start();

    types.add_attr("type", "Int64");
    types.add_attr("Name", "types");
    types.add_attr("format", "ascii");
    types.start();

    SourceSaddle.add_attr("type", "Int64");
    SourceSaddle.add_attr("Name", "SourceSaddle");
    SourceSaddle.add_attr("format", "ascii");
    SourceSaddle.start();

    DestinationExtremum.add_attr("type", "Int64");
    DestinationExtremum.add_attr("Name", "DestinationExtremum");
    DestinationExtremum.add_attr("format", "ascii");
    DestinationExtremum.start();

    std::map <uint64_t, uint64_t> critical_point_reuse_map;

    auto morse_index_calc = [=] (const uint64_t pt_index) -> int {
      if (is_maximum(pt_index))
        return m_dataset_rect.num_dimensions();
      else if (is_saddle(pt_index))
        return (m_dataset_rect.num_dimensions() - 1);
      else
        return -1;
    };

    auto add_point_and_get_index_in_file_lambda = [&] (const uint64_t s, bool is_surviving_cp) -> uint64_t {
      const int pt_morse_index = morse_index_calc(s);

      uint64_t final_index_in_file = 0xffffffffffffffff;

      if (critical_point_reuse_map.find(s) == critical_point_reuse_map.end()) {
        critical_point_reuse_map[s] = num_points;
        final_index_in_file = num_points++;
      }
      else {
        return critical_point_reuse_map[s];
      }

      #if 0
      if (pt_morse_index != -1) {
        if (critical_point_reuse_map.find(s) == critical_point_reuse_map.end()) {
          critical_point_reuse_map[s] = num_points;
          final_index_in_file = num_points++;
        }
        else {
          return critical_point_reuse_map[s];
        }
      }
      else
        final_index_in_file = num_points++;
      #endif

      #if 0
      num_maxima  += ((pt_morse_index ==  m_ref_dataset_instance->m_num_dimensions     ) ? 1 : 0);
      num_saddles += ((pt_morse_index == (m_ref_dataset_instance->m_num_dimensions - 1)) ? 1 : 0);
      #endif

      m_dataset_rect.index_to_point(s, &pt1);

      // Point "s" data
//      points.add(&pt1);
      points.add(point_mapper.map(&pt1, s));
      functionVals.addF(m_ref_dataset_instance->get_fn_val(s));
      morseIndex.addI(pt_morse_index);
      survivingCP.addI(is_surviving_cp ? 1 : 0);

      return final_index_in_file;
    };

    auto add_edge_lambda = [&] (const uint64_t s, const uint64_t t) -> void {
      // Edge "(s, t)" data
      connectivity.add(s);
      connectivity.add(t);

      offsets.add(2 * (++num_edges));
      types.add(3);
    };

    auto add_edge_with_metadata_lambda = [&] (const uint64_t s, const uint64_t t, const uint64_t source_saddle_index, const bool is_first_surviving_cp, const uint64_t dest_maximum_index, const bool is_second_surviving_cp) -> void {
      const uint64_t first  = add_point_and_get_index_in_file_lambda(s, is_first_surviving_cp);
      const uint64_t second = add_point_and_get_index_in_file_lambda(t, is_second_surviving_cp);
      add_edge_lambda(first, second);

      SourceSaddle.add(add_point_and_get_index_in_file_lambda(source_saddle_index, is_first_surviving_cp));
      DestinationExtremum.add(add_point_and_get_index_in_file_lambda(dest_maximum_index, is_second_surviving_cp));
    };

    for (uint64_t saddle_ordinal=0; saddle_ordinal<m_chunk_saddles.size(); ++saddle_ordinal) {

      if (has_input()) {
        printf("Creating edges file: %lu/%lu\n", saddle_ordinal, m_chunk_saddles.size());
      }

      if (!saddle_exists(saddle_ordinal) || m_saddle_persistence_removed[saddle_ordinal])
        continue;

      connected_maxima_collection.clear();

      if (direction_down) {
        for (uint64_t maximum_ordinal: m_saddle_maxima_mapping[saddle_ordinal])

          if (arc_redirection_list->has_redirection(maximum_ordinal))
            connected_maxima_collection.push_back(m_chunk_maxima[maximum_ordinal]);

        down_arcs += connected_maxima_collection.size();
      }

      if (direction_up) {
        for (uint64_t k=0; k<m_saddle_maxima_mapping[saddle_ordinal].size(); ++k) {
          const uint64_t maximum_index = m_chunk_maxima[m_saddle_maxima_mapping[saddle_ordinal][k]];
          if (!vector_contains(&connected_maxima_collection, maximum_index)) {
            connected_maxima_collection.push_back(maximum_index);
            ++up_arcs;
          }
        }

      }

      #if 0
      if (workers_saddle_list.size() == num_cpus) {
        begin_thread_pool(worker_thread_main);
      }

      else {
        const int idx = workers_saddle_list.size();
        workers_saddle_list.push_back(saddle_ordinal);
        worker_connected_maxima_collection[idx] = connected_maxima_collection;
        continue;
      }
      #endif

      num_saddles += 1;

      for (int idx=0; idx<1; ++idx) {

//        std::vector <uint64_t>& connected_maxima_collection = worker_connected_maxima_collection[idx];
//        std::vector <std::vector<uint64_t>>& paths_list = worker_paths_list[idx];
//        const point_index_t saddle_index = m_chunk_saddles[workers_saddle_list[idx]];

        const point_index_t saddle_index = m_chunk_saddles[saddle_ordinal];
        const uint64_t source_saddle_index = saddle_index;

        trace_and_fill_paths(this, saddle_ordinal, &connected_maxima_collection, &paths_list, &visited_arc_set);

        for (uint64_t a=0; a<paths_list.size(); ++a) {

          const std::vector <uint64_t> & path_vector = paths_list[a];

          int num_edges_per_arc = 0;

          if (!path_vector.empty()) {
            const uint64_t dest_maximum_index = path_vector[path_vector.size()-1];

            #if 1
//            if (critical_point_reuse_map.find(m_chunk_saddles[saddle_ordinal]) == critical_point_reuse_map.end())
//              num_saddles += 1;
            if (critical_point_reuse_map.find(dest_maximum_index) == critical_point_reuse_map.end())
              num_maxima += 1;
            #endif

            add_point_and_get_index_in_file_lambda(path_vector[path_vector.size()-1], true);


            for (uint64_t i=0; i<path_vector.size()-1; ++i) {
              std::pair <uint64_t, uint64_t> edge = std::make_pair(path_vector[i], path_vector[i+1]);
              if (visited_arc_set.find(edge) != visited_arc_set.end())
                continue;

              num_edges_per_arc += 1;

              visited_arc_set.insert(edge);
              const bool next_iter_cond_check = false; // (i+1) == (path_vector.size() - 1);
              add_edge_with_metadata_lambda(path_vector[i], path_vector[i+1], source_saddle_index, i==0, dest_maximum_index, next_iter_cond_check);
            }

          }

          num_arcs += (num_edges_per_arc > 0) ?  1 : 0;


        }

//        connected_maxima_collection.clear();
//        paths_list.clear();
      }

//      workers_saddle_list.clear();
    }

    critical_point_reuse_map.clear();
    visited_arc_set.clear();

    points.stop();
    functionVals.stop();
    morseIndex.stop();
    survivingCP.stop();
    connectivity.stop();
    offsets.stop();
    types.stop();
    SourceSaddle.stop();
    DestinationExtremum.stop();

    pointsTag.add(points);
    pointsTag.create();

    pointDataTag.add_attr("Scalars", "FunctionVals, MorseIndex, SurvivingCP");
    pointDataTag.add(functionVals);
    pointDataTag.add(morseIndex);
    pointDataTag.add(survivingCP);
    pointDataTag.create();

    cellsTag.add(connectivity);
    cellsTag.add(offsets);
    cellsTag.add(types);
    cellsTag.create();

    cellDataTag.add_attr("Scalars", "SourceSaddle, DestinationExtremum");
    cellDataTag.add(SourceSaddle);
    cellDataTag.add(DestinationExtremum);
    cellDataTag.create();

    pieceTag.add_attr("NumberOfPoints", num_points);
    pieceTag.add_attr("NumberOfCells", num_edges);
    pieceTag.add(pointsTag);
    pieceTag.add(pointDataTag);
    pieceTag.add(cellsTag);
    pieceTag.add(cellDataTag);
    pieceTag.create();

    unstructuredGridTag.add(pieceTag);
    unstructuredGridTag.create();

    vtkFileTag.add_attr("version", "1.0");
    vtkFileTag.add_attr("type", "UnstructuredGrid");
    vtkFileTag.add_attr("byte_order", (dataset_t :: is_machine_little_endian()  ? "LittleEndian" : "BigEndian" ));
    vtkFileTag.add_attr("header_type", "UInt64");
    vtkFileTag.add(unstructuredGridTag);
    vtkFileTag.create();

    unlink(filename.c_str());
    std::string cmd = "mv \"" + vtkFileTag.get_filename() + "\" \"" + filename + "\"";
    assert(system(cmd.c_str()) == 0);
    return 0;
    }

  __CUDA_HOST_ONLY__ int extremum_graph_t :: dump_geometry_as_vtp_v1
    (
      const arc_redirection_info_t * const arc_redirection_list,
      const std::string&                   filename,
      const uint8_t                        direction_flag)
    const
   {
    errno = 0;

    const bool direction_up   = direction_flag & 0b01; // Saddle-Extrema
    const bool direction_down = direction_flag & 0b10; // Extrema-Saddle

    printf("dump_geometry_as_vtp: [up: %d, down:%d]\n", (int)direction_up, (int)direction_down);

    const char VTP_MIDDLE_2[] = 
        "        </DataArray>\n" \
        "        <DataArray type=\"Int64\" Name=\"offsets\" format=\"ascii\">\n";

    const char VTP_SUFFIX[] = 
        "        </DataArray>\n" \
        "      </Lines>\n" \
        "    </Piece>\n" \
        "  </PolyData>\n" \
        "</VTKFile>\n";

    uint64_t edge_count = 0;
    const uint64_t num_elements   = m_dataset_rect.num_elements();
    const uint64_t num_dimensions = m_dataset_rect.num_dimensions();

    char tmp_edge_connectivity_filename[] = "/tmp/parallel-ext-XXXXXX.vtp.edges";
    int paths_fd = mkstemps(tmp_edge_connectivity_filename, 1 + 3 + 1 + 5);
    if (paths_fd == -1) {
      fprintf(stderr, "Failed to create temp file '%s'\n", tmp_edge_connectivity_filename);
      return -1;
    }

    std::vector <point_index_t> path_vector;
    std::set <point_index_t> visited_saddle_set;
    std::vector<std::vector<point_index_t>> saddle_ascending_manifold;
    std::vector <uint64_t> connected_maxima_collection;
    std::vector <uint64_t> adjacent_maxima_collection;

    #if !DEBUG_STD_PERSISTENCE_ALGO
//    adjacency_map_sanity_check(adjacency_map, m_chunk_maxima.size());path_vector
    #endif

    uint64_t down_arcs = 0, up_arcs = 0;

    for (uint64_t saddle_ordinal=0; saddle_ordinal<m_chunk_saddles.size(); ++saddle_ordinal) {

      if (has_input()) {
        printf("Creating edges file: %lu/%lu\n", saddle_ordinal, m_chunk_saddles.size());
      }

      if (m_saddle_removed[saddle_ordinal])
        continue;

      connected_maxima_collection.clear();
      adjacent_maxima_collection.clear();

      if (direction_down) {
        for (uint64_t maximum_ordinal: m_saddle_maxima_mapping[saddle_ordinal])

          if (arc_redirection_list->has_redirection(maximum_ordinal))
            connected_maxima_collection.push_back(m_chunk_maxima[maximum_ordinal]);

        down_arcs += connected_maxima_collection.size();
      }

      if (direction_up) {
        for (uint64_t k=0; k<m_saddle_maxima_mapping[saddle_ordinal].size(); ++k) {
          const uint64_t maximum_index = m_chunk_maxima[m_saddle_maxima_mapping[saddle_ordinal][k]];
          if (!vector_contains(&connected_maxima_collection, maximum_index)) {
            connected_maxima_collection.push_back(maximum_index);
            ++up_arcs;
          }
        }

        /*
        connected_maxima_collection.insert(
          connected_maxima_collection.end(),
          m_saddle_maxima_mapping[saddle_ordinal].begin(),
          m_saddle_maxima_mapping[saddle_ordinal].end()
        );
        */
      }

      const point_index_t saddle_index = m_chunk_saddles[saddle_ordinal];

//      m_ref_dataset_instance->saddle_immediate_ascending_manifold(this, saddle_ordinal, &saddle_ascending_manifold);

//      for (uint64_t i=0; i<saddle_ascending_manifold.size(); ++i) {
//        adjacent_maxima_collection.push_back(saddle_ascending_manifold[i][saddle_ascending_manifold[i].size()-1]);
//      }

      for (uint64_t a=0; a<connected_maxima_collection.size(); ++a) {

        const uint64_t connected_maximum = connected_maxima_collection[a];

        trace_transitive_paths(this, saddle_ordinal, connected_maximum, &path_vector);

        if (!path_vector.empty()) {

          assert(dprintf(paths_fd, "%lu %lu\n", (uint64_t)saddle_index, (uint64_t)path_vector[0]) > 0);

          for (uint64_t i=0; i<path_vector.size()-1; ++i)
            assert(dprintf(paths_fd, "%lu %lu\n", (uint64_t)path_vector[i], (uint64_t)path_vector[i+1]) > 0);
        }

        edge_count += path_vector.size();

      }

    }

    printf("down_arcs: %lu, up_arcs: %lu\n", down_arcs, up_arcs);

    printf("Edge connectivity file created. edge_count: %lu\n", edge_count);
//    puts("dump_geometry_as_vtp: 2");
    close(paths_fd);

    std::string tmp_header               = generate_header(num_elements, edge_count, num_dimensions);
    if (tmp_header.empty()) {
      fprintf(stderr, "Failed to create temp file '%s'\n", tmp_header.c_str());
      return -2;
    }
    printf("Header file created\n");

    std::string tmp_grid_points_filename = generate_grid_points_for_rect(&m_dataset_rect);

    if (tmp_grid_points_filename.empty()) {
      fprintf(stderr, "Failed to create temp file '%s'\n", tmp_grid_points_filename.c_str());
      return -3;
    }

    char tmp_footer_filename[] = "/tmp/parallel-ext-XXXXXX.vtp.footer";

    {
      int footer_hd = mkstemps(tmp_footer_filename, 1 + 3 + 1 + 6);
//      printf("tmp_footer_filename: %s\n", tmp_footer_filename);

      if (-1 == footer_hd) {
        fprintf(stderr, "Failed to create temp file '%s'\n", tmp_footer_filename);
        return -4;
      }

      assert(dprintf(footer_hd, VTP_MIDDLE_2) > 0);

      for (uint64_t i=1; i<=edge_count; ++i)
        assert(dprintf(footer_hd, "%lu\n", (2*i)) > 0);

      printf("Footer file created\n");
//      puts("dump_geometry_as_vtp: 3");

      assert(dprintf(footer_hd, VTP_SUFFIX));
      close(footer_hd);
    }

    std::string tmp_cmd_str = "cat \"";
    unlink(filename.c_str());
    tmp_cmd_str = tmp_cmd_str + tmp_header + "\" \"" + tmp_grid_points_filename + "\" \"" + tmp_edge_connectivity_filename + "\" \"" + tmp_footer_filename + "\" > \"" + filename + "\"";
    printf("CMD: %s\n", tmp_cmd_str.c_str());
//    int retval = -1;
    int retval = system(tmp_cmd_str.c_str());

    unlink(tmp_header.c_str());
    unlink(tmp_edge_connectivity_filename);
//    unlink(tmp_grid_points_filename.c_str());
    unlink(tmp_footer_filename);

    return retval;
   }

};

#endif // EXT_GRAPH_BASE
