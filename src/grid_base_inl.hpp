/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#include <grid.hpp>

#include <iostream>
#include <sstream>
#include <string>

namespace extgraph
 {

      #if USE_Z_ORDER
        #define DIMENSION_INDEX_MAP(k, dims)         k
        #define DIMENSION_INDEX_INVERSE_MAP(k, dims) (dims - 1 - k)
      #else
        #define DIMENSION_INDEX_MAP(k, dims)         (dims - 1 - k)
        #define DIMENSION_INDEX_INVERSE_MAP(k, dims) k
      #endif

    __CUDA_FORCEINLINE__ void chunk_rect_t :: init ()
     {
      assert(m_start.size()==m_end.size());

      m_managed_memory = false;
      m_numdims = m_start.size();
      m_chunk_dimensions = grid_point_t(m_start.size());

      for (size_t i=0; i<m_numdims; ++i)
        m_chunk_dimensions[i] = m_end[i]-m_start[i];

      m_prefix_products = uarray <uint64_t> (m_numdims);

      index_t product = 1;
      for (uint32_t i=0; i<m_numdims; ++i) {
        m_prefix_products[DIMENSION_INDEX_INVERSE_MAP(i, m_numdims)] = product;
        product *= m_chunk_dimensions[DIMENSION_INDEX_INVERSE_MAP(i, m_numdims)];
      }

      m_end_index = m_prefix_products[DIMENSION_INDEX_INVERSE_MAP(m_numdims-1, m_numdims)] * m_chunk_dimensions[DIMENSION_INDEX_INVERSE_MAP(m_numdims-1, m_numdims)] - 1;

      #if 0
      print_container<dimension_span_t>(m_start,"m_start: ");
      print_container<dimension_span_t>(m_end  ,"m_end  : ");
      print_container<dimension_span_t>(m_chunk_dimensions  ,"m_dim  : ");
      print_container<index_t>(m_prefix_products  ,"m_pre  : ");
      printf("end_idx: %lu\n",m_end_index);
      #endif

     }

    __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ chunk_rect_t :: chunk_rect_t (const size_t num_dims) :
        m_start(num_dims),
        m_end(num_dims),
        m_chunk_dimensions(num_dims),
        m_prefix_products(num_dims)
     {
      init();
     }

    __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ chunk_rect_t :: chunk_rect_t (const grid_point_t& start, const grid_point_t& end) :
        m_start(start),
        m_end(end),
        m_chunk_dimensions(start.size()),
        m_prefix_products(start.size())
     {
      init();
     }

    __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ chunk_rect_t :: chunk_rect_t (const chunk_rect_t& other_rect) :
        m_start(other_rect.m_start),
        m_end(other_rect.m_end),
        m_chunk_dimensions(other_rect.m_chunk_dimensions),
        m_prefix_products(other_rect.m_prefix_products)
     {
      init();
     }

    __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ chunk_rect_t :: ~chunk_rect_t ()
     {
      #if CUDA_SUPPORT
      if (m_managed_memory) {
        dimension_span_t* t;
        uint64_t* tt;

        t = m_start.arr();
        ufree<dimension_span_t>(t);

        t = m_end.arr();
        ufree<dimension_span_t>(t);

        t = m_chunk_dimensions.arr();
        ufree<dimension_span_t>(t);

        tt = m_prefix_products.arr();
        ufree<uint64_t>(tt);
      }
      #endif
     }

    __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ chunk_rect_t& chunk_rect_t :: operator = (const chunk_rect_t& other_rect)
     {
      if (this == &other_rect)
        return *this;

      this->m_start = other_rect.m_start;
      this->m_end   = other_rect.m_end;
      init();
      return *this;
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ bool chunk_rect_t :: contains_point  (const grid_point_t * const pt) const
     {
      for (size_t i=0; i<m_start.size(); ++i)
        if ( (m_start[i] > pt->get(i)) || (pt->get(i) >= m_end[i]) ) {
          return false;
        }

      return true;
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ bool chunk_rect_t :: contains_point  (const grid_point_t& pt) const
     {
//      print_container<dimension_span_t>(pt  ,"grid_point_t query  : ");
      #if 0
      printf("m_start <=    pt: %d\n", (m_start.operator<=(pt)));
      printf("pt      <  m_end: %d\n\n", (pt.operator<(m_end)));
      #endif

      for (size_t i=0; i<m_start.size(); ++i)
        if ( (m_start[i] > pt[i]) || (pt[i] >= m_end[i]) ) {
          #if 0
          if (debug) {
            printf("m_start: %s; m_end: %s; pt: %s; fail at %ld\n", m_start.to_string().c_str(), m_end.to_string().c_str(), pt.to_string().c_str(), i);
          }
          #endif
          return false;
        }

      return true;
//      return ( m_start.operator<=(pt) ) && ( pt.operator<(m_end) );
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ bool chunk_rect_t :: contains_point  (const point_t& pt) const
     {
      print_container<func_val_t>(pt  ,"point_t query  : ");
      return m_start <= pt && pt < m_end;
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ bool chunk_rect_t :: contains_index  (const point_index_t& pt_index) const
     {
      return pt_index <= m_end_index;
//      point_t pt;
//      pt = index_to_point(pt_index);
//      return contains_point(pt);
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ point_index_t chunk_rect_t :: point_to_index (const grid_point_t * const pt, point_diff_t * const diff) const
     {
//      assert(pt.size() == m_numdims);
      pt->minus(&m_start, diff);

      point_index_t final_idx = 0;

      for (uint64_t i=0; i<m_numdims; ++i)
        final_idx += diff->get(i) * m_prefix_products[i];

      return final_idx;
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ point_index_t chunk_rect_t :: point_to_index (const grid_point_t& pt, point_diff_t& diff) const
     {
//      assert(pt.size() == m_numdims);

//      auto subtraction_lambda    = [](dimension_span_t x, dimension_span_t y) -> index_t { return ((index_t)x)-((index_t)y); };
//      uarray <index_t> diff = vector_op<dimension_span_t,dimension_span_t,index_t>(pt,m_start, subtraction_lambda);

      pt.minus(m_start, diff);
//      print_container<dimension_diff_t>(diff,"diff   : ");

      point_index_t final_idx = 0;

      for (uint64_t i=0; i<m_numdims; ++i)
        final_idx += diff[i] * m_prefix_products[i];

//      printf("p-to-i : %lu\n",final_idx);
      return final_idx;
     }

    template <uint32_t num_dimensions>
    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ __attribute__((hot))
    point_index_t  point_to_index_templatized2 (const grid_point_t * const pt, const dimension_span_t* const m_start, const uint64_t * const m_prefix_products) noexcept
     {
      point_index_t final_idx = 0;

      #if CUDA_SUPPORT && CODE_TYPE_DEVICE
      #pragma unroll
      #endif
      for (uint32_t i=0; i<num_dimensions; ++i)
        final_idx += (pt->get(i) - m_start[i]) * m_prefix_products[i];

      return final_idx;
     }

    template <uint32_t num_dimensions>
    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ __attribute__((hot)) point_index_t chunk_rect_t :: point_to_index_templatized (const grid_point_t * const pt) const noexcept
     {
      return point_to_index_templatized2<num_dimensions>(pt, m_start.arr(), m_prefix_products.arr());
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ __attribute__((hot)) point_index_t chunk_rect_t :: point_to_index (const grid_point_t * const pt) const noexcept
     {
//      assert(pt->size() == m_numdims);

//      point_diff_t diff = pt->operator-<dimension_diff_t>(m_start);

      point_index_t final_idx = 0;

      for (uint64_t i=0; i<m_numdims; ++i)
        final_idx += (pt->get(i) - m_start[i]) * m_prefix_products[i];

//      printf("p-to-i : %s -> %lu\n", pt->to_string().c_str(), final_idx);
//      assert(final_idx <= m_end_index);
      return final_idx;
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ point_index_t chunk_rect_t :: point_to_index (const grid_point_t& pt) const
     {
//      assert(pt.size() == m_numdims);

//      auto subtraction_lambda    = [](dimension_span_t x, dimension_span_t y) -> index_t { return ((index_t)x)-((index_t)y); };
//      uarray <index_t> diff = vector_op<dimension_span_t,dimension_span_t,index_t>(pt,m_start, subtraction_lambda);

      point_diff_t diff = pt.operator-<dimension_diff_t>(m_start);
//      print_container<dimension_diff_t>(diff,"diff   : ");

      point_index_t final_idx = 0;

      for (uint64_t i=0; i<m_numdims; ++i)
        final_idx += diff[i] * m_prefix_products[i];

//      printf("p-to-i : %lu\n",final_idx);
      return final_idx;
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ bool chunk_rect_t :: index_to_point (const point_index_t pt_index, grid_point_t * const gpt) const
     {
      assert(pt_index <= m_end_index);
      point_index_t tmp_index = pt_index, tmp2;

        for (uint32_t i=0; i<m_numdims; ++i) {

          tmp2       = tmp_index / m_prefix_products[DIMENSION_INDEX_MAP(i, m_numdims)];
          tmp_index -= tmp2 * m_prefix_products[DIMENSION_INDEX_MAP(i, m_numdims)];
  
          gpt->at(DIMENSION_INDEX_MAP(i, m_numdims)) = tmp2 + m_start[DIMENSION_INDEX_MAP(i, m_numdims)];
        }

      return true;
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ bool chunk_rect_t :: index_to_point (const point_index_t pt_index, grid_point_t& gpt) const
     {
      return index_to_point(pt_index, &gpt);
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ grid_point_t chunk_rect_t :: index_to_point (const point_index_t& pt_index) const
     {

      if ( pt_index > m_end_index) {
      print_container<dimension_span_t>(m_start,"m_start: ");
      print_container<dimension_span_t>(m_end  ,"m_end  : ");
      print_container<dimension_span_t>(m_chunk_dimensions  ,"m_dim  : ");
      print_container<point_index_t>(m_prefix_products  ,"m_pre  : ");
      printf("end_idx: %lu\n", (unsigned long)m_end_index);
        printf("pt_index: %lu, m_end_index: %lu\n", (unsigned long)pt_index, (unsigned long)m_end_index);
      }

      grid_point_t gpt(m_numdims);
      index_to_point(pt_index, gpt);
      return gpt;
     }

    __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ std::string chunk_rect_t :: to_string () const
     {
      std::stringstream ss;

      ss << "[ " << m_start.to_string() << " - " << m_end.to_string() << " ]";

      return ss.str();
     }

    #if CUDA_SUPPORT

    __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ chunk_rect_t* chunk_rect_t :: create_device_copy (const int device) const
     {
      (void)device; // Unused variable warning

      chunk_rect_t* rect                = umalloc <chunk_rect_t>    (1, device);
      dimension_span_t* start_buf       = umalloc <dimension_span_t>(m_numdims, device);
      dimension_span_t* end_buf         = umalloc <dimension_span_t>(m_numdims, device);
      dimension_span_t* chunk_dim_buf   = umalloc <dimension_span_t>(m_numdims, device);
      uint64_t* prefix_products_buf     = umalloc <uint64_t>        (m_numdims, device);

      new (&rect->m_start) grid_point_t (start_buf, m_numdims);
      rect->m_start = m_start;

      new (&rect->m_end)   grid_point_t (end_buf  , m_numdims);
      rect->m_end = m_end;

      rect->m_numdims = m_numdims;
      rect->m_end_index = m_end_index;

      new (&rect->m_chunk_dimensions) dimensions_t (chunk_dim_buf  , m_numdims);
      rect->m_chunk_dimensions = m_chunk_dimensions;

      new (&rect->m_prefix_products) uarray <uint64_t> (prefix_products_buf  , m_numdims);
      rect->m_prefix_products = m_prefix_products;

      rect->m_managed_memory = true;

      return rect;
     }

    __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ void chunk_rect_t :: move_to_device (const int device)
     {
      if (!m_managed_memory) return;

      cudaMemPrefetchAsync(m_start.arr(), sizeof(dimension_span_t) * num_dimensions(), device, nullptr);
      cudaMemPrefetchAsync(m_end.arr(), sizeof(dimension_span_t) * num_dimensions(), device, nullptr);
      cudaMemPrefetchAsync(m_chunk_dimensions.arr(), sizeof(dimension_span_t) * num_dimensions(), device, nullptr);

      cudaMemPrefetchAsync(m_prefix_products.arr(), sizeof(uint64_t) * num_dimensions(), device, nullptr);
      cudaMemPrefetchAsync(this, sizeof(chunk_rect_t), device, nullptr);
     }

    #endif

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ __attribute__((hot)) bool chunk_rect_t :: is_boundary_point (const grid_point_t * const pt)  const noexcept
     {
      #if CUDA_EXEC_FULL_LOOPS
        bool is_on_boundary = false;
  
        for (uint32_t i=0; i<m_numdims; ++i)
          if ( (pt->get(i) == m_start[i]) || (pt->get(i) == (m_end[i] - 1)) )
            is_on_boundary = true;
  
        return is_on_boundary;

      #else

        for (uint32_t i=0; i<m_numdims; ++i)
          if ( (pt->get(i) == m_start[i]) || (pt->get(i) == (m_end[i] - 1)) )
            return true;

        return false;
      #endif
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ __attribute__((hot)) bool chunk_rect_t :: is_boundary_point (const grid_point_t * const pt, uint32_t& boundary_adjacency_mask) const noexcept
     {
      boundary_adjacency_mask = 0;
  
      for (uint32_t i=0; i<m_numdims; ++i)
        if ( (pt->get(i) == m_start[i]) || (pt->get(i) == (m_end[i] - 1)) )
          boundary_adjacency_mask |= (1UL << i);
  
      return (boundary_adjacency_mask > 0);
     }

    __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ point_neighbourhood :: point_neighbourhood (const chunk_rect_t * const rect) :
        m_rect(rect),
//        m_max_neighs(2 * ( (1ULL << rect->num_dimensions()) - 1))
        m_max_neighs(point_neighbourhood :: max_neighbours_for_dim(rect->num_dimensions()))
     {
      build_adjacency_array();
     }

    __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ point_neighbourhood :: ~point_neighbourhood ()
     {
      ufree<uint8_t>(m_adjacency_arr);
     }

    __CUDA_PREFIX__ constexpr uint64_t point_neighbourhood :: max_neighbours_for_dim (const uint32_t num_dimensions) noexcept
     {
      return (2 * (((uint64_t)1) << num_dimensions)) - 2;
     }

    __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ void point_neighbourhood :: build_adjacency_array ()
     {
      m_adjacency_arr = umalloc <uint8_t> (m_max_neighs * m_max_neighs);

      grid_point_t start(m_rect->num_dimensions()), end(m_rect->num_dimensions()), ref(m_rect->num_dimensions());
      grid_point_t pt_i(m_rect->num_dimensions()), pt_j(m_rect->num_dimensions());

      for (uint32_t i=0; i<m_rect->num_dimensions(); ++i) {
        start[i] = 0;
        end[i]   = 5;
        ref[i]   = 2;
      }

      point_index_t pti_index, ptj_index;
      chunk_rect_t tmp_rect(start, end);
      const chunk_rect_t * const original_rect = m_rect;
      m_rect = &tmp_rect;

      for (uint64_t i=0; i<m_max_neighs; ++i) {
        neighbour_perturbation(&ref, &pt_i, pti_index, i);

        m_adjacency_arr[i * m_max_neighs + i] = 0;

        for (uint64_t j=i+1; j<m_max_neighs; ++j) {
          neighbour_perturbation(&ref, &pt_j, ptj_index, j);

          if (is_pair_adjacent(&pt_i, &pt_j))
            m_adjacency_arr[i * m_max_neighs + j] = m_adjacency_arr[j * m_max_neighs + i] = 1;
          else
            m_adjacency_arr[i * m_max_neighs + j] = m_adjacency_arr[j * m_max_neighs + i] = 0;

        }

      }

      m_rect = original_rect;
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ __attribute__((hot)) bool point_neighbourhood :: neighbour_perturbation (const grid_point_t * const pt, grid_point_t * const dest, point_index_t& dest_index, uint32_t neighbour_index) const noexcept
     {
      dimension_diff_t offset0 = 0, offset1 = 0;

      if ( (1 + neighbour_index) >= ( 1ULL << m_rect->num_dimensions() ) ) {
        offset1 =  1;
        neighbour_index = (neighbour_index + 2) & ( ( 1ULL << m_rect->num_dimensions() ) - 1 );
      }
      else {
        offset0 = -1;
      }

      int32_t tmp_val;

      #if (CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_EXEC_FULL_LOOPS) || MERGE_GPU_CPU
      bool retval = true;
      #endif

      for (size_t i=0; i<m_rect->num_dimensions(); ++i) {

          tmp_val = pt->get(i) + ((neighbour_index & (1ULL<<i)) ? offset1 : offset0);

          if ( ( tmp_val < ((int64_t) m_rect->m_start[i]) ) || ( ((int64_t)m_rect->m_end[i]) <= tmp_val) ) {

            #if (CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_EXEC_FULL_LOOPS) || MERGE_GPU_CPU
                retval = false;
            #else
                return false;
            #endif

          }

          dest->at(i) = tmp_val;
      }

      #if (CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_EXEC_FULL_LOOPS) || MERGE_GPU_CPU

          const grid_point_t * const t = (retval) ? dest : pt;
          dest_index = m_rect->point_to_index(t);
          return retval;

      #else

          dest_index = m_rect->point_to_index(dest);
          return true;

      #endif
     }

    template <uint32_t num_dimensions>
    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ __attribute__((hot)) bool neighbour_perturbation_templatized_generic
      (
        const grid_point_t * const pt, grid_point_t * const dest, point_index_t& dest_index, uint32_t neighbour_index,
        const dimension_span_t * const m_start, const dimension_span_t * const m_end, const uint64_t * const m_prefix_products
      ) noexcept
     {
      constexpr uint32_t pow_2_num_dims_minus_1 = (1ULL << num_dimensions) - 1;

      dimension_diff_t offset0 = 0, offset1 = 0;

      if ( neighbour_index >= pow_2_num_dims_minus_1 ) {
        offset1 =  1;
        neighbour_index = (neighbour_index + 2) & pow_2_num_dims_minus_1;
      }
      else {
        offset0 = -1;
      }

      #if (CUDA_SUPPORT && CODE_TYPE_DEVICE && CUDA_EXEC_FULL_LOOPS) // GPU Code

          volatile int64_t tmp_val;
          bool retval = true;

          #pragma unroll
          for (uint32_t i=0; i<num_dimensions; ++i)
              dest->at(i) = pt->get(i) + ( (neighbour_index & (1UL<<i)) ? offset1 : offset0);

          #pragma unroll
          for (uint32_t i=0; i<num_dimensions; ++i) {
              tmp_val = dest->get(i);
              retval &= ( ( tmp_val >= ((int64_t) m_start[i]) ) && ( ((int64_t)m_end[i]) > tmp_val) );
          }

          const grid_point_t * const t = (retval) ? dest : pt;
          dest_index = point_to_index_templatized2<num_dimensions>(t, m_start, m_prefix_products);
          return retval;

      #else // CPU Code

          int64_t tmp_val;

          for (uint32_t i=0; i<num_dimensions; ++i) {
              dest->at(i) = tmp_val = pt->get(i) + ( (neighbour_index & (1ULL<<i) ) ? offset1 : offset0);
              if ( ( tmp_val < ((int64_t) m_start[i]) ) || ( ((int64_t)m_end[i]) <= tmp_val) )
                    return false;
          }

          dest_index = point_to_index_templatized2<num_dimensions>(dest, m_start, m_prefix_products);
          return true;

      #endif

     }

    template <uint32_t num_dimensions>
    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ __attribute__((hot)) bool point_neighbourhood :: neighbour_perturbation_templatized
      (const grid_point_t * const pt, grid_point_t * const dest, point_index_t& dest_index, uint32_t neighbour_index) const noexcept
     {
      return neighbour_perturbation_templatized_generic <num_dimensions> (pt, dest, dest_index, neighbour_index, m_rect->m_start.arr(), m_rect->m_end.arr(), m_rect->m_prefix_products.arr());
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ __attribute__((hot)) bool point_neighbourhood ::is_pair_adjacent (const uint64_t neighbour_index_i, const uint64_t neighbour_index_j) const noexcept
     {
      return m_adjacency_arr[neighbour_index_i * m_max_neighs + neighbour_index_j];
     }

    template <uint32_t num_dimensions>
    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ __attribute__((hot)) neighbour_bitmap_t index_to_neighbour_bitmap_templatized_generic
     (const point_index_t ref_pt, const point_index_t neighbour_pt_index, const uint64_t * const m_prefix_products)
     {
      constexpr neighbour_bitmap_t pow_2_num_dims_plus_1 = 1ULL << (num_dimensions);

      neighbour_bitmap_t neighbour_pt_bitmap = (neighbour_pt_index < ref_pt) ? pow_2_num_dims_plus_1 : 0;

      point_index_t index_diff               = (neighbour_pt_index < ref_pt)
                                               ? (ref_pt - neighbour_pt_index)
                                               : (neighbour_pt_index - ref_pt);

      for (uint32_t i=0; i<num_dimensions; ++i) {

        const bool ith_bit_set = (index_diff >= (m_prefix_products[DIMENSION_INDEX_MAP(i, num_dimensions)]));

        neighbour_pt_bitmap |= (ith_bit_set << DIMENSION_INDEX_MAP(i, num_dimensions));

        index_diff -= ((ith_bit_set) ? (m_prefix_products[DIMENSION_INDEX_MAP(i, num_dimensions)]) : 0);
      }

      return neighbour_pt_bitmap;
     }

    template <uint32_t num_dimensions>
    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ neighbour_bitmap_t chunk_rect_t :: index_to_neighbour_bitmap_templatized
      (const point_index_t ref_pt, const point_index_t neighbour_pt_index) const noexcept
     {
      return index_to_neighbour_bitmap_templatized_generic <num_dimensions> (ref_pt, neighbour_pt_index, m_prefix_products.arr());
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ neighbour_bitmap_t chunk_rect_t :: index_to_neighbour_bitmap
      (const point_index_t ref_pt, const point_index_t neighbour_pt_index) const noexcept
     {
      switch (num_dimensions())
       {
        case 3: return index_to_neighbour_bitmap_templatized_generic <3> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 2: return index_to_neighbour_bitmap_templatized_generic <2> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 4: return index_to_neighbour_bitmap_templatized_generic <4> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 5: return index_to_neighbour_bitmap_templatized_generic <5> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 6: return index_to_neighbour_bitmap_templatized_generic <6> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 7: return index_to_neighbour_bitmap_templatized_generic <7> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        #if MAX_DIMENSIONS > 7
        case 8: return index_to_neighbour_bitmap_templatized_generic <8> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 9: return index_to_neighbour_bitmap_templatized_generic <9> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 10: return index_to_neighbour_bitmap_templatized_generic <10> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 11: return index_to_neighbour_bitmap_templatized_generic <11> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 12: return index_to_neighbour_bitmap_templatized_generic <12> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 13: return index_to_neighbour_bitmap_templatized_generic <13> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 14: return index_to_neighbour_bitmap_templatized_generic <14> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        #if MAX_DIMENSIONS > 15
        case 15: return index_to_neighbour_bitmap_templatized_generic <15> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 16: return index_to_neighbour_bitmap_templatized_generic <16> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 17: return index_to_neighbour_bitmap_templatized_generic <17> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 18: return index_to_neighbour_bitmap_templatized_generic <18> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 19: return index_to_neighbour_bitmap_templatized_generic <19> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 20: return index_to_neighbour_bitmap_templatized_generic <20> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 21: return index_to_neighbour_bitmap_templatized_generic <21> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 22: return index_to_neighbour_bitmap_templatized_generic <22> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 23: return index_to_neighbour_bitmap_templatized_generic <23> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 24: return index_to_neighbour_bitmap_templatized_generic <24> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 25: return index_to_neighbour_bitmap_templatized_generic <25> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 26: return index_to_neighbour_bitmap_templatized_generic <26> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 27: return index_to_neighbour_bitmap_templatized_generic <27> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 28: return index_to_neighbour_bitmap_templatized_generic <28> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 29: return index_to_neighbour_bitmap_templatized_generic <29> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 30: return index_to_neighbour_bitmap_templatized_generic <30> (ref_pt, neighbour_pt_index, m_prefix_products.arr());

        case 31: return index_to_neighbour_bitmap_templatized_generic <31> (ref_pt, neighbour_pt_index, m_prefix_products.arr());
        #endif
        #endif

        default:
          assert(false);
          return 0;
       }
     }

    template <uint32_t num_dimensions>
    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ __attribute__((hot)) point_index_t neighbour_bitmap_to_index_templatized_generic
      (const point_index_t ref_pt, const neighbour_bitmap_t neighbour_pt_bitmap, const uint64_t * const m_prefix_products)
     {
      constexpr neighbour_bitmap_t pow_2_num_dims_plus_1 = 1ULL << (num_dimensions);

      const bool bitmap_sign = neighbour_pt_bitmap & (pow_2_num_dims_plus_1);

      point_index_t neighbour_pt_index = ref_pt;

      for (uint32_t i=0; i<num_dimensions; ++i) {

        const bool ith_bit_set = neighbour_pt_bitmap & (1ULL << DIMENSION_INDEX_MAP(i, num_dimensions));

        neighbour_pt_index -= (ith_bit_set &&  bitmap_sign) ? (m_prefix_products[DIMENSION_INDEX_MAP(i, num_dimensions)]) : 0;
        neighbour_pt_index += (ith_bit_set && !bitmap_sign) ? (m_prefix_products[DIMENSION_INDEX_MAP(i, num_dimensions)]) : 0;
      }

      return neighbour_pt_index;
     }

    template <uint32_t num_dimensions>
    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ point_index_t chunk_rect_t :: neighbour_bitmap_to_index_templatized
      (const point_index_t ref_pt, const neighbour_bitmap_t neighbour_pt_bitmap) const noexcept
     {
      return neighbour_bitmap_to_index_templatized_generic <num_dimensions> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__ point_index_t chunk_rect_t :: neighbour_bitmap_to_index
      (const point_index_t ref_pt, const neighbour_bitmap_t neighbour_pt_bitmap) const noexcept
     {
      switch (num_dimensions())
       {
        case 3:
          return neighbour_bitmap_to_index_templatized_generic <3> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 2:
          return neighbour_bitmap_to_index_templatized_generic <2> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 4:
          return neighbour_bitmap_to_index_templatized_generic <4> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 5:
          return neighbour_bitmap_to_index_templatized_generic <5> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 6:
          return neighbour_bitmap_to_index_templatized_generic <6> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 7:
          return neighbour_bitmap_to_index_templatized_generic <7> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        #if MAX_DIMENSIONS > 7
        case 8:
          return neighbour_bitmap_to_index_templatized_generic <8> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 9:
          return neighbour_bitmap_to_index_templatized_generic <9> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 10:
          return neighbour_bitmap_to_index_templatized_generic <10> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 11:
          return neighbour_bitmap_to_index_templatized_generic <11> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 12:
          return neighbour_bitmap_to_index_templatized_generic <12> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 13:
          return neighbour_bitmap_to_index_templatized_generic <13> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 14:
          return neighbour_bitmap_to_index_templatized_generic <14> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 15:
          return neighbour_bitmap_to_index_templatized_generic <15> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        #if MAX_DIMENSIONS > 15
        case 16:
          return neighbour_bitmap_to_index_templatized_generic <16> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 17:
          return neighbour_bitmap_to_index_templatized_generic <17> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 18:
          return neighbour_bitmap_to_index_templatized_generic <18> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 19:
          return neighbour_bitmap_to_index_templatized_generic <19> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 20:
          return neighbour_bitmap_to_index_templatized_generic <20> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 21:
          return neighbour_bitmap_to_index_templatized_generic <21> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 22:
          return neighbour_bitmap_to_index_templatized_generic <22> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 23:
          return neighbour_bitmap_to_index_templatized_generic <23> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 24:
          return neighbour_bitmap_to_index_templatized_generic <24> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 25:
          return neighbour_bitmap_to_index_templatized_generic <25> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 26:
          return neighbour_bitmap_to_index_templatized_generic <26> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 27:
          return neighbour_bitmap_to_index_templatized_generic <27> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 28:
          return neighbour_bitmap_to_index_templatized_generic <28> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 29:
          return neighbour_bitmap_to_index_templatized_generic <29> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 30:
          return neighbour_bitmap_to_index_templatized_generic <30> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());

        case 31:
          return neighbour_bitmap_to_index_templatized_generic <31> (ref_pt, neighbour_pt_bitmap, m_prefix_products.arr());
        #endif
        #endif

        default:
          #if CODE_TYPE_DEVICE
               printf("neighbour_bitmap_to_index_templatized_generic<T> not declared for dimensions=%lu\n", num_dimensions());
          #else
               fprintf(stderr, "neighbour_bitmap_to_index_templatized_generic<T> not declared for dimensions=%lu\n", num_dimensions());
          #endif
          assert(false);
          return 0;
       }
     }

    __CUDA_PREFIX__ __CUDA_FORCEINLINE__
    bool point_neighbourhood :: is_pair_adjacent (const grid_point_t * const pt1, const grid_point_t * const pt2) const
     {
      bool pos_exists = false, neg_exists = false;
      dimension_diff_t diff;

      #define is_pair_adjacent_AAA 0

      #if is_pair_adjacent_AAA
      bool out_of_range = false;
      #endif

      for (size_t i=0; i<pt1->size(); ++i) {

        diff = pt1->operator[](i) - pt2->operator[](i);

        if ( (diff > 1) || (diff < -1) )
          #if is_pair_adjacent_AAA
            out_of_range = true;
          #else
            return false;
          #endif
  
          else if (diff == 1)
            pos_exists = true;
          else if (diff == -1)
            neg_exists = true;
      }

      return
          #if is_pair_adjacent_AAA
           !out_of_range &&
          #endif
           !(pos_exists && neg_exists);
     }

    #if CUDA_SUPPORT
    __CUDA_HOST_ONLY__ __CUDA_FORCEINLINE__ void point_neighbourhood :: move_to_device (const int device) const
     {
      (void)device;
      cudaMemPrefetchAsync(m_adjacency_arr, m_max_neighs * m_max_neighs * sizeof(uint8_t), device, nullptr);        
     }
    #endif

 };
