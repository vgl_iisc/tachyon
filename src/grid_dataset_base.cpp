/*=========================================================================

  Program:   tachyon

  Copyright (c) 2022 Abhijath Ande, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#include <grid.hpp>
#include <grid_dataset.hpp>

#include <ioutils.hpp>

#include <cmath>
#include <fcntl.h>
#include <errno.h>

namespace extgraph {

  void dataset_t :: init
      (const std::string& filename, const dimensions_t& dims, bool is_little_endian, const std::string& dtype)
   {
    printf("dataset: %s\n", filename.c_str());
//    std::cout << "dataset: " << filename << "\n";

    m_dataset_filename     = filename;
//    m_dataset_dimensions = dims;
    m_dataset_negate_fn_val = false;
    m_dataset_endianness    = is_little_endian;

    m_num_dimensions = dims.size();

    m_dataset_dtype = dtype[0];
    m_element_bytes = strtol(&dtype.c_str()[1],nullptr,10);
    printf("[dtype, dsize] = [%c, %u]\n",m_dataset_dtype,m_element_bytes);
    m_element_bytes /= 8;

////    grid_point_t point_zero = grid_point_t::zero(m_num_dimensions);
////    grid_point_t point_last = grid_point_t::zero(m_num_dimensions);
////    for (size_t i=0; i<m_num_dimensions; ++i)
////       point_last[i] = dims[i];
////
////    m_dataset_rect = chunk_rect_t(point_zero, point_last);

//    printf("m_dataset_rect: %s\n", m_dataset_rect.to_string().c_str());
//    std::cout<<"m_dataset_rect: "<<m_dataset_rect.to_string()<<"\n";

    m_chunk_fn_vals       = nullptr;
////    m_chunk_rect    = chunk_rect_t(m_num_dimensions);
//    std::cout<<"m_chunk_rect: "<<m_chunk_rect.to_string()<<"\n";

    set_instance_id();
   }

  dataset_t :: dataset_t
      (const std::string& filename, const dimensions_t& dims, const std::string& dtype) :
          m_dataset_dimensions(dims),
          m_dataset_rect(dimensions_t::zero(dims.size()),dims),
          m_chunk_rect(dimensions_t::zero(dims.size()),dims) //dims.size())
   {
    init(filename, dims, false, dtype);
   }

  void dataset_t :: set_instance_id ()
   {
    union {
      char b[8];
      uint64_t r;
    } u;

    int rfd = open("/dev/urandom",O_RDONLY);
    assert(read(rfd,u.b,8) == 8);
    close(rfd);

    m_instance_id = u.r;
//    printf("instance_id: %0lx\n",m_instance_id );
   }

  dataset_t :: ~dataset_t()
   {
    if (is_chunk_loaded()) {
      unload_chunk();
    }

   }

  void dataset_t :: set_workdir (const std::string& workdir)
   {
    m_workdir = workdir;
   }

  void dataset_t :: negate_fn_value (bool negate)
   {
    m_dataset_negate_fn_val = negate;
   }

  bool dataset_t :: is_fn_value_negated () const
   {
    return m_dataset_negate_fn_val;
   }

  void dataset_t :: set_endianness (bool is_little_endian)
   {
    m_dataset_endianness = is_little_endian;
   }

  bool dataset_t :: get_endianness () const
   {
    return m_dataset_endianness;
   }

  bool dataset_t :: is_chunk_loaded () const
   {
    return nullptr != m_chunk_fn_vals;
   }

  chunk_rect_t dataset_t :: get_chunk_rect () const
   {
    return m_chunk_rect;
   }

  chunk_rect_t dataset_t :: get_dataset_rect () const
   {
    return m_dataset_rect;
   }

  func_val_t dataset_t :: get_fn_val (grid_point_t pt) const
   {
    assert(is_chunk_loaded());
    return m_chunk_fn_vals[m_chunk_rect.point_to_index(pt)];
   }

  func_val_t dataset_t :: get_fn_val (point_index_t pt_index) const
   {
    assert(is_chunk_loaded());
    assert(pt_index < m_chunk_rect.num_elements());
    return m_chunk_fn_vals[pt_index];
   }

  int dataset_t :: load_chunk (const chunk_rect_t& rect)
   {
    if (is_chunk_loaded()) {
      return -1;
    }

    global_index_t first_element_index = m_dataset_rect.point_to_index(rect.start_pt());
    global_index_t last_element_index  = first_element_index + rect.num_elements();

//    printf("load_chunk: [%lu, %lu)\n", first_element_index, last_element_index);
    off64_t seek_pos = first_element_index * m_element_bytes;

//    printf("num_elements: %lu\n",rect.num_elements());

    #if USE_OLD_PARSER==1
    int fd = open(m_dataset_filename.c_str(),O_RDONLY | O_LARGEFILE);

    if (fd < 0) {
        fprintf(stderr, "Failed to open file: \"%s\". %s\n", m_dataset_filename.c_str(), strerror(errno));
        exit(-1);
    }

    lseek(fd, seek_pos, SEEK_SET);

    #endif

    try {
      m_chunk_fn_vals = nullptr;
      m_chunk_fn_vals = new func_val_t [rect.num_elements()]; // umalloc<func_val_t>(rect.num_elements());
    }
    catch (const std::bad_alloc& e) {
        fprintf(stderr, "Memory allocation failed for rect %s!\n", rect.to_string().c_str());
//        std::cout<<"Memory allocation failed for rect " <<rect.to_string()<< "!"<<std::endl;
        exit(-1);
    }
    assert(nullptr != m_chunk_fn_vals);

    const bool m_machine_endianness = dataset_t :: is_machine_little_endian();

//    printf("m_dataset_endianness: %d\n",m_dataset_endianness);
//    printf("m_machine_endianness: %d\n",m_machine_endianness);

    bool requires_endianness_correction = m_dataset_endianness ^ m_machine_endianness;
    ssize_t read_bytes;

    const ssize_t block_size = 1UL << 12;
    const ssize_t num_file_vals_per_block = block_size / m_element_bytes;

    char* buf = new char[block_size];

   #if USE_OLD_PARSER==1

//    for (global_index_t i=first_element_index; i<=last_element_index; ++i) {
//      if ((read_bytes = read(fd, buf, m_element_bytes)) != m_element_bytes) {
//        delete [] m_chunk_fn_vals;
//        m_chunk_fn_vals = NULL;
//        printf("attempted to read %d bytes, got %d. [index=%lu, errno=%d]\n", (int)m_element_bytes, (int)read_bytes, i, errno );
//        return -1;
//      }
//
//      m_chunk_fn_vals[i] = data_conversion(i, buf, m_dataset_dtype, m_element_bytes, requires_endianness_correction);
//    }

    uint64_t total_bytes_read = 0;

    uint64_t num_elements_read = 0;
    const uint64_t num_elements_to_read = last_element_index - first_element_index;

    for (global_index_t i=first_element_index; i<min(last_element_index, i + num_file_vals_per_block); i += num_file_vals_per_block) {
      read_bytes = read(fd, buf, block_size);

      if ( read_bytes < 0 ) {
        delete [] m_chunk_fn_vals;
        delete [] buf;
        printf("attempted to read %lu bytes, got %ld. [index=%lu, errno=%d]\n", block_size, read_bytes, i, errno);
        return -1;
      }

      assert(read_bytes >= 0);

      total_bytes_read += min(((uint64_t)read_bytes), (num_elements_to_read - num_elements_read) * m_element_bytes);
      uint64_t block_elements = read_bytes / m_element_bytes;

      for (global_index_t j=0; j<block_elements && num_elements_read < num_elements_to_read; ++j, ++num_elements_read)
        m_chunk_fn_vals[i - first_element_index + j] = data_conversion(i+j, &buf[j*m_element_bytes], m_dataset_dtype, m_element_bytes, requires_endianness_correction);
    }

    const uint64_t total_bytes_expected = m_element_bytes * rect.num_elements();

   if (total_bytes_read != total_bytes_expected) {
       printf("total_bytes_read   : %ld\ntotal_elements_read: %ld\n", total_bytes_read, total_bytes_read/m_element_bytes );
       printf("num_indices        : %lu\n\n", rect.num_elements());

       if (total_bytes_read < total_bytes_expected)
         fprintf(stderr, "Expected %lu elements, got %lu. Corrupt file or wrong data type?\n", rect.num_elements(), num_elements_read);

       else if (total_bytes_read > total_bytes_expected)
         fprintf(stderr, "Found more elements than expected. Wrong data type?\n");

       assert(total_bytes_read == total_bytes_expected);
//       assert(total_bytes_read == (m_element_bytes * rect.num_elements()));
//       assert((total_bytes_read / m_element_bytes) == rect.num_elements());
   }

   close(fd);
   delete [] buf;

   #else
   int64_t error = read_from_file (m_chunk_fn_vals, rect.num_elements(), m_dataset_filename, seek_pos, m_dataset_dtype, m_element_bytes, requires_endianness_correction);
   if (error) {
      printf("Error reading file at block %ld\n",-error);
      exit(-1);
   }

   #endif

    m_chunk_rect = rect;
    compute_chunk_func_range();

    return 0;
   }

  int dataset_t :: unload_chunk ()
   {
    if (!is_chunk_loaded())
      return 0;

//    ufree<func_val_t>(m_chunk_fn_vals);

    delete [] m_chunk_fn_vals;
    m_chunk_fn_vals = nullptr;

    return 0;
   }

  bool dataset_t :: compute_chunk_func_range ()
   {
    if (!is_chunk_loaded())
      return false;

    func_val_t max_val = FUNC_VAL_MIN, min_val = FUNC_VAL_MAX;

    for (uint64_t i=0; i<m_chunk_rect.num_elements(); ++i) {
      min_val = min(min_val, m_chunk_fn_vals[i]);
      max_val = max(max_val, m_chunk_fn_vals[i]);
    }

    m_chunk_fn_range_size = (max_val - min_val);
    return true;
   }

  bool dataset_t :: get_chunk_func_range (func_val_t& range_size) const noexcept
   {
    if (!is_chunk_loaded())
      return false;

    range_size = m_chunk_fn_range_size;
    return true;
   }

  bool dataset_t :: get_dataset_func_range (func_val_t& range_size)
   {
    if (!is_chunk_loaded())
      return false;

    if (m_chunk_rect.num_elements() < m_dataset_rect.num_elements()) {
      unload_chunk();
      load_chunk(m_dataset_rect);
    }

    return get_chunk_func_range(range_size);
   }

  bool dataset_t :: is_machine_little_endian ()
   {
    union { char b[4]; uint32_t v; } u_32;
    u_32.v = 1;

    return u_32.b[3] != 1;
   }

};
