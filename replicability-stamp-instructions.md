
For the replicability stamp, we attempt to reproduce figure 12 from the publication **`TACHYON: Efficient shared memory parallel computation of extremum graphs`**, more specifically the tachyon portion of the figure. This subset of the figure simply represents the computation time trend against increasing sizes of popularly used datasets found on [Open Scientific Visualization Datasets](https://klacansky.com/open-scivis-datasets/ "Open Scivis home page").

The project is tested on various ubuntu versions above 18.04 and should work on most linux distros as it has minimal dependencies.

* Prerequisites
    * Setup build tools by running
        * `sudo apt-get install git cmake g++ nvidia-cuda-toolkit`
    * Install python3 (if not already installed) and necessary libraries by running the following commands
        * `sudo apt-get install python3-matplotlib python3-pip`
        * `sudo pip3 install requests`
    * The replicability stamp python script will download upto 2GB worth of datasets for producing its results. Please ensure sufficient storage on your machine.
    * The script produces the resulting graph in a UI window and not as an image file on disk. Thus this script will not run on headless machines and needs an X server for displaying this graphical result.

1. Build the project with the following instructions:
    * Create `build` directory under repository root and `chdir` into it
    * Configure cmake by running `cmake .. -DCUDA_SUPPORT=[0|1]`
    * For the flag `CUDA_SUPPORT`, pass `1` if your machine has an NVidia GPU installed, and `0` otherwise.
    * Run `make -jN`, where `N` is the number of parallel jobs you can allocate
    * A binary `tachyon` will be generated on a successful build

2. Move to repository root directory and run the following command:
    ```
    python3 generate-graph.py
    ```
    The python script will download necessary datasets, verify their integrity and perform extremum graph computation 7 times on each dataset. Average computation time is computed as average of 5 runs after discarding the best and worst times for each dataset.

3. A plot should appear matching figure 12 from the publication.
