
#include "test_functions.h"

int main ()
 {
  test_point_assignment();
  test_point_to_index();
  test_neighbour_perturbation();
  test_plan_seq();
  test_neighbour_bitmap();
 }
