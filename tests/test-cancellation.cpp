
#include <grid.hpp>
#include <extremum_graph.hpp>
#include <memutils.hpp>
#include <dataset_config.h>

#include <iostream>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

using namespace extgraph;

#ifndef DATASET_CHOICE
  #define DATASET_CHOICE -1
#endif

#ifndef DATASET_DIR
  #define DATASET_DIR "./"
#endif

void create_hexagonal_graph (extremum_graph_t * const ext_graph);

int main ()
 {
  dimensions_t dim(3);

  std::string dsname, dtype, dsprefix;
  func_val_t range_max;

  std::string outfiles_dir = "./output";

  get_dataset_config(DATASET_CHOICE, dim, dsprefix, dtype, range_max);

  std::string dsdir  = DATASET_DIR;
  dsname = dsdir + dsprefix + ".raw";
  grid_point_t end_pt = dim;

  dataset_t dataset_chunk(dsname, dim, dtype);
  dataset_chunk.set_endianness(false);
  dataset_chunk.negate_fn_value(false);

  grid_point_t start_pt = grid_point_t::zero(dim.size());

  chunk_rect_t dataset_rect = chunk_rect_t(start_pt, end_pt);
  extremum_graph_t ext_graph(&dataset_chunk);

  puts("\nLoading chunk");
  if (dataset_chunk.load_chunk(dataset_rect)) {
    puts("load_chunk error!");
    exit(-1);
  }

  const float threshold = 0.05;
  puts("Computing Extremum Graph");
  dataset_chunk.negate_fn_value(0);
  dataset_chunk.compute_ext_graph(&ext_graph);
  uint64_t num_arcs_init = 0;
  for (uint64_t s=0; s<ext_graph.m_saddle_maxima_mapping.size(); ++s)
    num_arcs_init += ext_graph.m_saddle_maxima_mapping[s].size();

  printf("Num arcs init: %9lu\n", num_arcs_init);

  const float p_lo = threshold;
  const float p_hi = 0.95;//1.0;

  persistence_map_t persistence_vals;                // Size == num_maxima + num_saddles
  persistence_map_t saturated_persistence_vals;      // Size == num_saddles
  critical_point_map_t adjacency_map, tmp_adjacency_map; // Size == num_maxima + num_saddles
  arc_redirection_info_t arc_redirection_list;

  if (1==1) ext_graph.perform_edge_bundling();

  if (1==1) ext_graph.std_persistence_based_cancellation(threshold);

  if (1==0) ext_graph.perform_edge_bundling();

  if (1==1) {
    ext_graph.persistence_simplification_compute(&persistence_vals, &saturated_persistence_vals, &arc_redirection_list, &adjacency_map);
    ext_graph.persistence_simplification_cancellation(&persistence_vals, &saturated_persistence_vals, &arc_redirection_list, &tmp_adjacency_map, &adjacency_map, p_lo, p_hi);
  }

  if (1==0) ext_graph.perform_edge_bundling();

  if (1==0) ext_graph.std_persistence_based_cancellation(threshold);

  if (1==0) ext_graph.perform_edge_bundling();

  arc_redirection_list.init(ext_graph.m_chunk_maxima.size(), ext_graph.m_chunk_saddles.size());
  ext_graph.dump_geometry_as_vtp(&arc_redirection_list, (outfiles_dir + "/parallel_ext_" + dsprefix + "_paths_simplified.vtp").c_str() );
  pretty_print_max_mem_usage("Max Mem Usage: ");
  return 0;
}
