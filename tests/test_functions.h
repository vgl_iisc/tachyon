
#ifndef TEST_FUNCTIONS_H
#define TEST_FUNCTIONS_H

#include <grid.hpp>
#include <plan.hpp>

#include <cstdlib>

using namespace extgraph;

void test_plan_seq ()
 {
    puts("Creating plan rect");
    grid_point_t start(3), end(3);
    start = { 0,
        0,
        0 };
    end   = { 800,
       768,
       4096 };

//    end = { 512, 1024, 1024 };
//    end = { 832, 832, 494 };
//    end = { 958, 646, 1088 };
//    end = { 1024, 1024, 750 };
//    end = { 2048, 2048, 2048 };
    end = { 1024, 1024, 1080 };
    end = { 2048, 2048, 2612 };
//    end = { 10240, 7680, 1536 };
    chunk_rect_t prect(start, end);

    int dev = 0;
    uint64_t last_index = 0;

    plan_t planner(&prect);
    plan_t::plan_item_t item;

    while (planner.next_item(dev, item)) {

      if (last_index > 0)
        assert(item.compute_start_index == last_index);

      last_index = item.compute_end_index;
    }

    assert(last_index == prect.num_elements());
    puts("\n");
 }

void test_point_assignment ()
 {
    grid_point_t start(3),end(3);
    point_t      query(3);

    query[0] = 3.999;
    query[1] = 5.999;
    query[2] = 5.999;
  
    grid_point_t gpoint(query.size());
    gpoint.operator=<float>(query);
 }

void test_point_to_index ()
 {
    grid_point_t start(3), end(3);
    start = { 2, 3, 4 };
    end   = { 8, 9, 10 };
    chunk_rect_t pppp_rect(start, end);
    printf("Creating rect %s\n", pppp_rect.to_string().c_str());
    puts("\n");

    puts("Point to index for sample points");
    for (point_index_t i=0; i<150; ++i) {
      grid_point_t pppp = pppp_rect.index_to_point(i);
      printf("point[%lu]: %s\n", (unsigned long)i, pppp.to_string().c_str());
      point_index_t pt_index = pppp_rect.point_to_index(pppp);
      if (pt_index != i) {
        printf("pt; %s; pt_index: %lu; pt_to_pt_index: %lu\n", pppp.to_string().c_str(), (unsigned long)i, (unsigned long)pt_index);

       assert(pppp_rect.point_to_index(pppp) == i);
      }
    }
    puts("");
  }

void test_neighbour_perturbation () {
    grid_point_t start(3), end(3);
    start = { 0, 0, 0 };
    end   = { 10, 10, 10 };
    chunk_rect_t pppp_rect(start, end);
    printf("Creating rect %s\n", pppp_rect.to_string().c_str());
    point_neighbourhood p_neighbours(&pppp_rect);
    puts("\n");

    grid_point_t pt(3);
    grid_point_t neighbour_pt(3);
    point_index_t neighbour_index;

    std::vector <grid_point_t> test_pts;

    for (unsigned int i=0; i<8; ++i) {
      test_pts.push_back(grid_point_t(3));

      for (int j=0; j<3; ++j)
        test_pts[i][j] = (i & (1<<j)) ? (end[j]-1) : 0;
    }

    test_pts.push_back({ end[0]/2, end[1]/2, end[2]/2 });
    test_pts.push_back({ end[0]/2, end[1]/2, 0        });
    test_pts.push_back({ end[0]/2, 0       , 0        });

    puts("Neighbours for sample points");

    for (unsigned int ii=0; ii<test_pts.size(); ++ii) {

    uint64_t count = 0;
    pt = test_pts[ii];
    point_index_t i = pppp_rect.point_to_index(&pt);

      printf("Neighbours of [%3lu] %s:\n", (unsigned long)i, pt.to_string().c_str());
      for (uint64_t j=0; j<p_neighbours.max_neighbours(); ++j) {
        if (!p_neighbours.neighbour_perturbation(&pt, &neighbour_pt, neighbour_index, j)) {
//          printf("BAD  [%3lu, %2lu, %2lu] %s\n", neighbour_index, j, ++count, neighbour_pt.to_string().c_str());
          continue;
        }

        printf("MAIN [%3lu, %2lu, %2lu] %s\n", (unsigned long)neighbour_index, (unsigned long)j, (unsigned long)++count, neighbour_pt.to_string().c_str());

      }

     puts("");
    }

    puts("");
  }

void test_neighbour_bitmap () {

    constexpr uint8_t num_dimensions = 3;

    grid_point_t start(num_dimensions), end(num_dimensions);
    for (uint8_t i=0; i<num_dimensions; ++i) start[i] = 0;
    for (uint8_t i=0; i<num_dimensions; ++i) end[i]   = 10;
    end = { 1024, 1024, 750 };
    chunk_rect_t pppp_rect(start, end);
    printf("Creating rect %s\n", pppp_rect.to_string().c_str());
    point_neighbourhood p_neighbours(&pppp_rect);
    puts("\n");

    grid_point_t pt(num_dimensions), neighbour_pt(num_dimensions);
    point_index_t neighbour_index;

    for (uint8_t i=0; i<num_dimensions; ++i) pt[i] = 3;

    const point_index_t pt_index = pppp_rect.point_to_index(&pt);

    printf("max_neighbours: %lu\n", p_neighbours.max_neighbours());

    for (uint32_t j=0; j<p_neighbours.max_neighbours(); ++j) {

      if (!p_neighbours.neighbour_perturbation_templatized<num_dimensions>(&pt, &neighbour_pt, neighbour_index, j)) {
        continue;
      }

      neighbour_bitmap_t bitmap = pppp_rect.index_to_neighbour_bitmap(pt_index, neighbour_index);

      printf("Bitmap for neighbour[%2u]: %2x, %2lx\n", j, bitmap, bitmap & ((1UL << num_dimensions) - 1) );

      point_index_t reverse_neighbour_index = pppp_rect.neighbour_bitmap_to_index(pt_index, bitmap);

      assert(reverse_neighbour_index == neighbour_index);
    }

    {
      neighbour_bitmap_t bitmap = pppp_rect.index_to_neighbour_bitmap(pt_index, pt_index);

      printf("Bitmap for self         : %2x, %2lx\n", bitmap, bitmap & ((1UL << num_dimensions) - 1) );

      point_index_t reverse_neighbour_index = pppp_rect.neighbour_bitmap_to_index(pt_index, bitmap);

      assert(reverse_neighbour_index == pt_index);
    }

    puts("");
}

#endif // TEST_FUNCTIONS_H
